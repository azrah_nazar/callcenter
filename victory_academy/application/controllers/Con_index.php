<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Con_index extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
        $this->load->model('Index_model');
	}

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('index');
	}

    /******************** INSERT ********************/
	public function add()
	{
		$data = array(
			'con_no' => $this->input->post('con_no'),
			'name' => $this->input->post('con_name'),
			'address' => $this->input->post('con_adrz'),
			'whatsapp' => $this->input->post('con_whatsapp'),
			'job' => $this->input->post('con_job'),
			'dob' => $this->input->post('con_dob'),
			'date' => date("Y-m-d H:i:s"),
			'user_id' => '',
			'site_flag' => '1',
		);

		echo $result = $this->Index_model->add($data);
		$this->Index_model->call_allocation();
	}

}
