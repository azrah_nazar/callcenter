<?php
class Index_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /******************** INSERT ********************/
    public function add($data)
    {

        $result = $this->db->get_where("con_list", array('con_no' => $data['con_no']));
        $rowcount = $result->num_rows();

        if ($rowcount == 0) {
            if ($this->db->insert("con_list", $data)) {
                return "success";
            } else {
                return "error";
            }
        } else {
            return "exist";
        }
    }

    /******************** CALL ALLOCATION ********************/
    public function call_allocation()
    {
        $st = 'success';
        $sql = "SELECT * FROM con_list where emp_accNo = ''";
        $query = $this->db->query($sql);
        $rowcount = $query->num_rows();
        $res_arr = $query->result_array();

        $sqle = "SELECT * FROM Usert where call_ID > 0 ORDER BY call_ID ASC";
        $querye = $this->db->query($sqle);
        $ucount = $querye->num_rows();
        $res_arry = $querye->result_array();

        $this->db->select('iterate_no');
        $this->db->from('invoice_no');
        $query = $this->db->get();
        $row = $query->row();
        $new_it_no = $row->iterate_no;

        $this->db->select('con_upload');
        $this->db->from('invoice_no');
        $qry = $this->db->get();
        $rw = $qry->row();
        $con_upload = $rw->con_upload;

        if ($ucount > 0) {
            while ($rowcount > 0) {
                $new_it_no += 1;

                $this->db->select('acc_no');
                $this->db->from('Usert');
                $this->db->where('call_ID', $new_it_no);
                $query2 = $this->db->get();
                $row2 = $query2->row();
                $empid = $row2->acc_no;

                $sql1 = "SELECT * FROM con_list where emp_accNo = '' ORDER BY id ASC";
                $query1 = $this->db->query($sql1);
                $row_res = $query1->row_array();
                $id = $row_res['id'];

                $sqlu = $this->db->query("UPDATE con_list SET emp_accNo='$empid', upload_id = '$con_upload' WHERE id='$id'");
                if (!$sqlu) {
                    $st = 'error';
                } else {
                    if ($ucount == $new_it_no) {
                        $new_it_no = 0;
                    }
                    $this->db->query("UPDATE invoice_no SET iterate_no='$new_it_no'");
                }

                $rowcount--;
            }
            $nw_con_upload = 1;
            $this->db->query("UPDATE invoice_no SET con_upload='$nw_con_upload'");
        } else {
            $st = 'empty';
        }
        return $st;
    }
}
