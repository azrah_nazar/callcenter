<!DOCTYPE html>
<html lang="zxx">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
  <meta charset="utf-8">
  <title>Victory Academy</title>

  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/plugins/bootstrap/bootstrap.min.css">
  <!-- slick slider -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/plugins/slick/slick.css">
  <!-- themefy-icon -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/plugins/themify-icons/themify-icons.css">
  <!-- animation css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/plugins/animate/animate.css">
  <!-- aos -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/plugins/aos/aos.css">
  <!-- venobox popup -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/plugins/venobox/venobox.css">

  <!-- Main Stylesheet -->
  <link href="<?php echo base_url(); ?>/assets/css/style.css" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap-sweetalert/lib/sweet-alert.css" />

  <!--Favicon-->
  <link href="<?php echo base_url(); ?>assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon">

  <style>
    /* Extra small devices (phones, 600px and down) */
    @media only screen and (max-width: 500px) {
      .hero-section {
        padding: 50px 0 30px;
      }

      .sm-center {
        text-align: center!important;
      }

      .youtubeiframe{
        width: 100%;
        height: 100%;
      }

      .youtubeiframe2{
        width: 100%;
        height: 200px;
      }
    }

    /* Portrait */
    @media (min-device-width: 600px) and (orientation: portrait) {
      .hero-section {
        padding: 50px 0 50px;
      }

      .youtubeiframe{
        width: 100%;
        height: 150px;
      }
      .con2{
          padding:unset;
      }
      
      .youtubeiframe2{
        width: 100%;
        height: 300px;
      }

    }

    /* Landscape */
    @media (min-device-width: 1280px) and (orientation: landscape) {
      .hero-section {
        padding: 150px 0 150px;
      }

      .youtubeiframe{
        width: 100%;
        height: 250px;
      }
      
      .youtubeiframe2{
        width: 100%;
        height: 250px;
        margin-top: 10%;
      }
      
      .sm-center {
        text-align: center!important;
        margin-top: 5%;
      }

    }
  </style>

</head>

<body>


  <!-- header -->
  <header class="fixed-top header">
    <!-- top header -->
    <div class="top-header py-2 bg-white">
      <div class="container">
        <div class="row no-gutters">
          <div class="col-lg-4 text-center text-lg-left">
            <a class="text-color mr-3" href="callto:+443003030266"><strong>CALL</strong> +94 71 060 0777</a>
            <ul class="list-inline d-inline">
              <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="https://www.facebook.com/VNTAcademy"><i class="ti-facebook"></i></a></li>
              <li class="list-inline-item mx-0"><a class="d-inline-block p-2 text-color" href="https://www.youtube.com/channel/UCUxMKdSm4954jk29tizxCeg"><i class="ti-youtube"></i></a></li>
            </ul>
          </div>
          <div class="col-lg-8 text-center text-lg-right">
            <ul class="list-inline">
              <li class="list-inline-item"><a class="text-uppercase text-color p-sm-2 py-2 px-0 d-inline-block" href="#" data-toggle="modal" data-target="#signupModal">register now</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- navbar -->
    <div class="navigation w-100">
      <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light p-0">
          <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo"></a>
          <button class="navbar-toggler rounded-0" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navigation">
            <ul class="navbar-nav ml-auto text-center">
              <li class="nav-item active pull-right">
                <a class="nav-link" href="<?php echo base_url(); ?>">Home</a>
              </li>
              <li class="nav-item pull-right">
                <a class="nav-link" href="#faq">FAQ</a>
              </li>
              <li class="nav-item pull-right">
                <a class="nav-link" href="#testimonials">Testimonials</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </header>
  <!-- /header -->