<!-- Modal -->
<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content rounded-0 border-0 p-4">
            <div class="modal-header border-0">
                <h3>100% ක් නොමිලේ නැරඹිය හැකි හඳුන්වාදීමේ වැඩසටහනට </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="login">
                    <p>පහත විස්තර පිරවීමෙන් පසු ඔබට වැඩසටහන හා සම්බන්දවීමට අවකාශ හිමිවන අතර අපගේ නියෝජිතවරියක් අද දින තුල වැඩි දුර විස්තර සඳහා ඔබ හා සම්බන්ධවනු ඇත </p>
                    <form action="javascript:void(0);" id="form_update">
                        <input type="hidden" name="id_up" id="id_up">
                        <div class="form-horizontal">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Name:<font color="#FF0000"><strong>*</strong></font></label>
                                            <input type="text" class="form-control txt_nav" name="con_name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Contact Number:<font color="#FF0000"><strong>*</strong></font></label>
                                            <input type="text" autofocus class="form-control txt_nav" name="con_no" id="con_no" style="width: 100%;" required maxlength="10">
                                        </div>
                                    </div>

                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Whatsapp Number: <font color="#FF0000"><strong>*</strong></font></label>
                                            <input type="text" class="form-control txt_nav" name="con_whatsapp" maxlength="10">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address:</label>
                                            <input type="text" class="form-control txt_nav" name="con_adrz">
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Job/Business:<font color="#FF0000"><strong>*</strong></font> </label>
                                            <input type="text" class="form-control txt_nav" name="con_job" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date of Birth:</label>
                                            <input type="text" class="form-control txt_nav date" name="con_dob">
                                        </div>
                                    </div>
                                </div>
                                <br>

                            </div>
                        </div>

                        <div class="box-footer">
                            <button class="btn btn-primary">REGISTER</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- hero slider -->
<section class="hero-section overlay bg-cover" data-background="<?php echo base_url() ?>assets/images/banner/banner-1.jpg">
    <div class="container">
        <div class="hero-slider">
            <!-- slider item -->
            <div class="hero-slider-item">
                <br>
                <div class="row">
                    <div class="col-sm-12 col-lg-7 sm-center" style="text-align: center!important;">
                        <h1 class="text-white" data-animation-out="fadeOutRight" data-delay-out="5" data-duration-in=".3" data-animation-in="fadeInLeft" data-delay-in=".1">ඔබගේ ජීවිතය වෙනස් කරන නැරඹිය යුතුම වැඩසටහන</h1>
                    </div>
                    <div class="col-sm-12 col-lg-5 position-relative success-video sm-center" style="text-align: center!important;">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/Z9V5pyjRMn0" frameborder="0" allowfullscreen class="youtubeiframe youtubeiframe2"></iframe>
                    </div>
                </div>
            </div>

            <!-- slider item -->
            <div class="hero-slider-item">
                <br>
                <div class="row">
                    <div class="col-sm-12 col-lg-7 sm-center" style="text-align: center!important;">
                        <h1 class="text-white" data-animation-out="fadeOutDown" data-delay-out="5" data-duration-in=".3" data-animation-in="fadeInUp" data-delay-in=".1">ඔබගේ ජීවිතය වෙනස් කරන නැරඹිය යුතුම වැඩසටහන</h1>
                    </div>
                    <div class="col-sm-12 col-lg-5 position-relative success-video sm-center" style="text-align: center!important;">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/Z9V5pyjRMn0" frameborder="0" allowfullscreen class="youtubeiframe youtubeiframe2"></iframe>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- /hero slider -->

<!-- banner-feature -->
<section class="bg-gray" id="faq">
    <div class="container-fluid p-0">
        <div class="row no-gutters">

            <div class="col-xl-12 col-lg-12">
                <h2 class="text-center" style="margin-top: 10px;">නිතර ඇසෙන ප්‍රශ්ණ සහ පිළිතුරු </h2>

                <div class="row feature-blocks bg-gray justify-content-between" style="padding: 20px; margin-top:20px;">
                    <div class="col-sm-6 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/hWDhtjIQlL8" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-6 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/itwiQ2teh3Q" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-6 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/_Lr7MfZ6Be8" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-6 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/8bpus9ZSn9k" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-6 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/1D5RBfa8Bb0" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /banner-feature -->
<br>

<section class="section-sm" style="padding: 15px; padding-top:unset;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex align-items-center section-title justify-content-between">
                    <h2 class="mb-0 mr-3">සුදුසුකම් ලැබීම ඉතා පහසුය</h2>
                    <div class="border-top w-100 border-primary d-none"></div>
                </div>
            </div>
        </div>
        <!-- course list -->
        <div class="row justify-content-center">
            <!-- event speakers -->
            <div class="row">
                <div class="col-lg-6 col-sm-12 mb-4 mb-lg-0">
                    <div class="media mb-4">
                        <div class="d-md-table-cell text-center p-2 bg-primary text-white mb-2 mb-md-0"><span class="h2 d-block"><i class="fa fa-desktop"></i></span></div>
                        <div class="text" style="padding-left: 10px;">
                            <h5>No Computer Literacy required </h5>
                            <p class="mb-0">පරිගණක ආශ්‍රිත දැනුමක් අවශ්‍ය නොවේ</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 mb-4 mb-lg-0">
                    <div class="media mb-4">
                        <div class="d-md-table-cell text-center p-2 bg-primary text-white mb-2 mb-md-0"><span class="h2 d-block"><i class="fa fa-gear"></i></span></div>
                        <div class="text" style="padding-left: 10px;">
                            <h5>No Technical knowledge / skill required </h5>
                            <p class="mb-0">මුදල් ඉපයීමට තාක්ෂණික දැනුමක් හෝ කුසලතා අවශ්‍ය නොවේ</p>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class="col-lg-6 col-sm-12 mb-4 mb-lg-0">
                    <div class="media mb-4">
                        <div class="d-md-table-cell text-center p-2 bg-primary text-white mb-2 mb-md-0"><span class="h2 d-block"><i class="fa fa-laptop"></i></span></div>
                        <div class="text" style="padding-left: 10px;">
                            <h5>No desktop / laptop required </h5>
                            <p class="mb-0">ඩෙස්ක්ටොප් හෝ ලැප්ටොප් පරිගණකයක් අවශ්‍ය නොවේ</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 mb-4 mb-lg-0">
                    <div class="media mb-4">
                        <div class="d-md-table-cell text-center p-2 bg-primary text-white mb-2 mb-md-0"><span class="h2 d-block"><i class="fa fa-book"></i></span></div>
                        <div class="text" style="padding-left: 10px;">
                            <h5>Basic Knowledge in English is sufficient </h5>
                            <p class="mb-0">ඉංග්‍රීසි සරල දැනුමක් ප්‍රමාණවත්</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
</section>

<!-- banner-feature -->
<section class="bg-gray" id="testimonials">
    <div class="container-fluid p-0">
        <div class="row no-gutters">

            <div class="col-xl-12 col-lg-12">
                <h2 class="text-center" style="margin-top: 10px;">Testimonials </h2>

                <div class="row feature-blocks bg-gray justify-content-between" style="padding: 20px; margin-top:20px;">
                    <div class="col-sm-12 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/bbjP-rDBJc0" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-12 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/Z6EGK04hnls" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-12 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/vBxZtZ8U5CI" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-12 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/H-hu5rqRMYs" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-12 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/u4VaLE05utM" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section class="section-sm" id="testimonials">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex align-items-center section-title justify-content-between">
                    <h2 class="mb-0 text-nowrap mr-3">Testimonials</h2>
                    <div class="border-top w-100 border-primary d-none d-sm-block"></div>
                </div>
            </div>
        </div>
        
        <div class="row justify-content-center">
        <div class="row feature-blocks bg-gray justify-content-between" style="padding: 20px; margin-top:20px;">
                    <div class="col-sm-6 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/bbjP-rDBJc0" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-6 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/Z6EGK04hnls" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-6 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/vBxZtZ8U5CI" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-6 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed//H-hu5rqRMYs" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                    <div class="col-sm-6 col-xl-4 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/u4VaLE05utM" frameborder="0" allowfullscreen class="youtubeiframe"></iframe>
                    </div>
                </div>
        </div>

    </div>
</section> -->
<!-- /courses -->

<!-- about us -->
<section class="section bg-gray" style="padding-top: 10px; padding-bottom:55px;">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12 order-2 order-md-1">
                <h3 style="text-align: justify;">අපගේ ආයතනය විසින් පවතින covid 19 තත්වය හා සමාජ වාතාවරණය සලකා බලා පාඨමාලා ගාස්තු සීමිත කාලසීමාවකට අඩු කර ඇති බව සතුටින් දැනුම්දෙමු</h3>
            </div>
        </div>
    </div>
</section>
<!-- /about us -->

<div id="gallery-videos-demo">
    <!-- YouTube Video --->
    <a
        data-lg-size="1280-720"
        data-src="https://www.youtube.com/embed/u4VaLE05utM"
        data-poster="https://img.youtube.com/vi/egyIeygdS_E/maxresdefault.jpg"
        data-sub-html="<h4>Visual Soundscapes - Mountains | Planet Earth II | BBC America</h4><p>On the heels of Planet Earth II’s record-breaking Emmy nominations, BBC America presents stunning visual soundscapes from the series' amazing habitats.</p>"
    >
        <img
            width="300"
            height="100"
            class="img-responsive"
            src="https://img.youtube.com/vi/egyIeygdS_E/maxresdefault.jpg"
        />
    </a>

</div>




<!-- footer -->
<footer>
    <!-- footer content -->
    <div class="footer bg-footer section border-bottom" style="padding-top: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-8">
                    <!-- logo -->
                    <a class="logo-footer" href="<?php echo base_url(); ?>"><img class="img-fluid mb-4" src="<?php echo base_url() ?>assets/images/logo.png" alt="logo"></a>
                    <ul class="list-unstyled">
                        <li class="mb-2"> No.152, Second Floor, Super Commercial Complex, Nawalapitiya</li>
                        <li class="mb-2"> +94 71 060 0777</li>
                        <li class="mb-2"> info@victoryacademylk.com</li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <!-- copyright -->
    <div class="copyright py-4 bg-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 text-sm-left text-center">
                    <p class="mb-0">Copyright 2021 © . All Rights Reserved.</p>
                </div>
                <div class="col-sm-5 text-sm-right text-center">
                    <ul class="list-inline">
                        <li class="list-inline-item"><a class="d-inline-block p-2" href="https://www.facebook.com/VNTAcademy"><i class="ti-facebook"></i></a></li>
                        <li class="list-inline-item"><a class="d-inline-block p-2" href="https://www.youtube.com/channel/UCUxMKdSm4954jk29tizxCeg"><i class="ti-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /footer -->


<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/bootstrap.min.js"></script>
<!-- slick slider -->
<script src="<?php echo base_url() ?>assets/plugins/slick/slick.min.js"></script>
<!-- aos -->
<script src="<?php echo base_url() ?>assets/plugins/aos/aos.js"></script>
<!-- venobox popup -->
<script src="<?php echo base_url() ?>assets/plugins/venobox/venobox.min.js"></script>
<!-- mixitup filter -->
<script src="<?php echo base_url() ?>assets/plugins/mixitup/mixitup.min.js"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
<script src="<?php echo base_url() ?>assets/plugins/google-map/gmap.js"></script>
<script src="<?php echo base_url() ?>assets/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.2.0-beta.4/lightgallery.umd.min.js"></script>

<!-- Main Script -->
<script src="<?php echo base_url() ?>assets/js/script.js"></script>


<script>

lightGallery(document.getElementById('gallery-videos-demo'));

    $(document).ready(function() {
        $("#signupModal").modal("show");
    });

    function validatePhoneNumber(input_str) {
        var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

        return re.test(input_str);
    }

    //////////////////////////////con_no ADD /////////////////////////////////
    $("#form_update").on('submit', (function(e) {
        var con_no = document.getElementById('con_no').value;

        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>Con_index/add",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                console.log(data);
                if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                } else {
                    if ($.trim(data) === 'error') {
                        swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'exist') {
                        swal("", "Contact No already Exists!", "warning");
                    } else if ($.trim(data) === 'updated') {
                        $('#pid').focus();
                        swal({
                            title: "",
                            text: "Successfully Updated!",
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false,
                        });
                        setTimeout(function() {
                            window.location.href = "<?php echo base_url() ?>Con_index/"
                        }, 2000);

                    } else if ($.trim(data) === 'success') {
                        swal({
                            title: "",
                            text: "Successfully Registered!",
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false,
                        });
                        setTimeout(function() {
                            // window.location.href = "<?php //echo base_url() 
                                                        ?>Con_index/index/";
                            $("#signupModal").modal("hide");
                        }, 2000);

                    }
                }
            },
            error: function(e) {
                swal({
                    title: "Error!",
                    text: "Try Again",
                    type: "warning",
                    timer: 2000,
                    showConfirmButton: false,
                });
                setTimeout(function() {}, 2000);
            }
        });

    }));
</script>

</body>

</html>