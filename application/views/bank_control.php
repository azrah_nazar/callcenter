<?php
  $menu = $this->uri->segment(3);
  ?>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1> <i class="fa fa-sitemap"></i> Bank Control
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
        if (isset($load_data)) { ?>
          <div class="box box-info">
            <div class="box-body">
              <form action="javascript:void(0);" method="post">
                <div class="table-responsive ptt10">
                  <table class="table table-hover table-striped example">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Actions</th>
                        <th>Reg No</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Bank</th>
                        <th>Description</th>
                        <!-- <th>Balance</th> -->
                      </tr>
                    </thead>
                    <tbody>

                      <?php
                      $count = 1;

                      foreach ($load_data as $value) {

                      ?>
                        <tr>
                          <td class="mailbox-name"> <?php echo $count; ?>.</td>
                          <td>
                            <button type="button" class="btn btn-sm btn-success btn_accept_reject" data-res-flag="1" title="Accept" value="<?php echo $value->bid; ?>" data-reg="<?php echo $value->reg_no; ?>"><i class="fa fa-check"></i></button>
                            <button type="button" class="btn btn-sm btn-danger btn_accept_reject" data-res-flag="2" title="Reject" value="<?php echo $value->bid; ?>" data-reg="<?php echo $value->reg_no; ?>"><i class="fa fa-close"></i></button>
                          </td>
                          <td class="mailbox-name"> <?php echo $value->reg_no; ?></td>
                          <td class="mailbox-name"> <?php echo $value->name; ?></td>
                          <td class="mailbox-name"> <?php echo $value->deposit_date; ?></td>
                          <td class="mailbox-name"> <?php echo $value->cr; ?></td>
                          <td class="mailbox-name"> <?php echo $value->bank; ?></td>
                          <td class="mailbox-name"> <?php echo $value->p_type; ?></td>
                          <!-- <td class="mailbox-name"> <?php //echo $course_fee - $value->cr; ?></td> -->
                        </tr>
                      <?php
                        $count++;
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
</div>



<script>
  /**************************** ACCEPT / REJECT  ****************************/
  $(".btn_accept_reject").click(function() {
    var menu = $("#menu").val();
    bid = $(this).val();
    var res_flag = $(this).attr('data-res-flag');
    var reg = $(this).attr('data-reg');

    if(res_flag == 1){
      txt = 'Accepted';
    }else{
      txt = "Rejected";
    }


    $.post("<?php echo base_url() ?>Con_post_registry/response_accept", {
        response_accept: "data",
        res_flag: res_flag, bid:bid, reg:reg
      },
      function(data) {
        if ($.trim(data.status) === 'success') {
          swal({
            title: 'Successfully '+txt,
            text: '',
            type: "success",
            timer: 2000,
            showConfirmButton: false,
          });
          setTimeout(function() {
            location.reload();
          }, 2000);
          
        } else if ($.trim(data.status) === 'error') {
          swal({
            title: "",
            text: "Error!",
            type: "error",
            timer: 2000,
            showConfirmButton: false,
          });
          setTimeout(function() {
            location.reload();
          }, 2000);

        }
      }, "json");

  });

  
</script>