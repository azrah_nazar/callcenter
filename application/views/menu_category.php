<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i> Menu Category</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Menu Category</h3>
                        </div> 
                        <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                        <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
                            <div class="box-body">

                              <div class="form-group">
                                <label>Select Main Project:<font color="#FF0000"><strong>*</strong></font></label>
                                <select class="form-control txt_nav" name="pid" style="width: 100%;" required>
                                  <?php
                                  echo "<option ></option>"; 
                                  foreach($project as $proj) {
                                    echo "<option value=".$proj->ID.">".$proj->prjct."</option>"; 
                                  }
                                  ?>
                                </select>
                              </div>
                              <div class="form-group">
                                <div class="form-group">                 
                                  <label>Category Name:<font color="#FF0000"><strong>*</strong></font></label>
                                  <input type="text" class="form-control" name="cat_name" id="cat_name" placeholder="Enter ..." autocomplete="off" required>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="form-group">                 
                                  <label for="icon">Icon<font color="#FF0000"><strong>*</strong></font></label>
                                  <input type="text" class="form-control" name="icon" id="icon" placeholder="Enter ..." autocomplete="off" required>
                                </div>
                              </div>

                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>  
                </div>   
            <div class="col-md-8">             
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Menu Category List</h3>
                    </div>
                    <div class="box-body ">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label">Menu Category</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th> Project </th>
                                        <th> Icon </th>
                                        <th> Menu Category </th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                   

                                    <?php
                                    $count = 0;
                                     
                                    foreach($load_data as $value) { $count++; 
                                        ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $value->prjct;?></td>
                                            <td><i class="<?php echo $value->icon;?>"></i></td>
                                            <td><?php echo $value->cat;?></td>
                                            <td class="mailbox-date pull-right">

                                                <button class="btn btn-default btn-xs btn_up" data-toggle="modal" data-target="#edit_data" value="<?php echo $value->ID; ?>"  title="Edit">
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
           
        </div> 

    </section>
</div>

<div class="modal fade" id="edit_data" role="dialog">
    <div class="modal-dialog">       
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title text-center modal_title"> Edit Menu Category</h4>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" id="form_update">
                  <input type="hidden" name="id_up" id="id_up">

                  <div class="form-horizontal">
                      <div class="box-body">           
                          <div class="form-group">
                            <label>Select Main Project:<font color="#FF0000"><strong>*</strong></font></label>
                            <select class="form-control" name="pid_up" id="pid_up" style="width: 100%;" required>
                              <?php
                              echo "<option ></option>"; 
                              foreach($project as $proj) {
                                echo "<option value=".$proj->ID.">".$proj->prjct."</option>"; 
                              }
                              ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Category Name:<font color="#FF0000"><strong>*</strong></font></label>
                            <input type="text" class="form-control" name="cat_name_up" id="cat_name_up" placeholder="Enter ..." autocomplete="off" required>
                          </div>

                          <div class="form-group">
                            <label for="icon_up">Icon<font color="#FF0000"><strong>*</strong></font></label>
                            <input type="text" class="form-control" name="icon_up" id="icon_up" placeholder="Enter ..." autocomplete="off" required>
                          </div>



                      </div>                   
                  </div>

                  <div class="box-footer"> 
                    <button class="btn btn-danger">UPDATE</button>
                  </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<script>
  $(function() {
    $(':input[required=""],:input[required]').bind('focusout', function() {
      if ($(this).val() == "") {
        $(this).css("border-color", "red");
      } else {
        $(this).css("border-color", "#ccc");
      }
    });

    $('.txt_nav:first').focus();
    var $inp = $('.txt_nav');
    $inp.bind('keydown', function(e) {
      var n = $inp.length;
      var key = e.which;
      if (key == 13) {
        e.preventDefault();
        var nxtIdx = $inp.index(this) + 1;
        if(nxtIdx < n) {
          $('.txt_nav')[nxtIdx].focus();
        }
        else
        {
         $('.txt_nav')[nxtIdx-1].blur();
         $('#submit_btn').focus();
       }
     }
   });

    $( "#submit_btn" ).keypress(function( event ) {
      if ( event.which == 13 ) {
        req_input_check();     
      }
    });         

  });

  function req_input_check() {
    $(':input[required=""],:input[required]').bind('keypress', function(event){
      if ( event.which == 13 ) {
        if ($(this).val() == ""){
          $(this).css("border-color", "red");
        } else {
          $(this).css("border-color", "#ccc");
          $('#form_submit').submit();
        }
      }
    });
  }

  /**************************** INSERT  ****************************/

   $("#form_submit").on('submit',(function(e) {
      var menu = $("#menu").val();

      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url()?>Con_menu_category/add",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
        },
        success: function(data){
          if($.trim(data) === '') {
            swal("Oops...", "Something went wrong!", "warning");

          } else {
            if($.trim(data) ==='error')
            {
              swal("SQL Error!", "Please Try Again!", "warning");

            } else if($.trim(data) ==='data exists') {
              $('#cat_name').focus();
              swal({
                title: "",
                text: "Menu Category Already Exists!",
                type: "warning",
                timer: 2000,
                showConfirmButton: false,
              });           

            } else if($.trim(data) ==='success') {
              swal({
                title: "",
                text: "Successfully Added!",
                type: "success",
                timer: 2000,
                showConfirmButton: false,
              });
              setTimeout(function() 
              {
                window.location.href = "<?php echo base_url()?>Con_menu_category/index/"+menu;
              }, 2000);
              
            }
          }
        },
        error: function(e) 
        {
          alert("err2");
        }
      });
    }));

    /********************************* Edit ******************************/

    $("#form_update").on('submit',(function(e) {
      var menu = $("#menu").val();

      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url()?>Con_menu_category/update",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
        },
        success: function(data){
          $('#edit_data').modal('hide')
          if($.trim(data) === '') {
            swal("Oops...", "Something went wrong!", "warning");

          } else {
            if($.trim(data) ==='error')
            {
              swal("SQL Error!", "Please Try Again!", "warning");

            } else if($.trim(data) ==='data exists') {
              swal("", "Menu Category Already Exist!", "warning");

            } else if($.trim(data) ==='success') {
              swal({
                title: "Updated!",
                text: "Successfully Updated!",
                type: "success",
                confirmButtonText: "OK"
              },
              function (isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url()?>Con_menu_category/index/"+menu;
                }
              });
            }
          }      
        },
        error: function(e) 
        {
          alert("err2");
        }
      });
    }));  

  /**************************** Load Form Data ***********************/

  $(".btn_up").click(function(){
    var id = $(this).val();
    $("#id_up").val(id);

    $.post( "<?php echo base_url()?>Con_menu_category/get_dataset", { get_dataset: "data", id : id}, function( data ) {
      $.each(data.result, function (index, data) {
        $("#pid_up").val(data.p_id);
        $("#cat_name_up").val(data.cat);
        $("#icon_up").val(data.icon);
      });

    }, "json");   
  });

  </script> 