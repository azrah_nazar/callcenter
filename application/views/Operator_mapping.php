<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i>Map Operators
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Map Operator</h3>
                    </div>
                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                    <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
                        <div class="box-body">

                            <div class="form-group">
                                <label for="male">Males Operator<font color="#FF0000"><strong>*</strong></font></label>
                                <select autofocus class="form-control" name="male" id="male" style="width: 100%;" required>
                                    <option></option>
                                    <?php
                                    foreach ($load_males as $male) {
                                        echo "<option value=" . $male->Acc_No . ">" . $male->Name . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="female">Female Operator<font color="#FF0000"><strong>*</strong></font></label>
                                <select autofocus class="form-control" name="female" id="female" style="width: 100%;" required>
                                    <option></option>
                                    <?php
                                    foreach ($load_females as $female) {
                                        echo "<option value=" . $female->Acc_No . ">" . $female->Name . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Save</button>
                        </div>
                    </form>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Back Date Duration</h3>
                    </div>
                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                    <form action="javascript:void(0);" id="form_backdate" accept-charset="utf-8">
                        <div class="box-body">

                            <div class="form-group">
                                <label for="no_of_days">No of Days<font color="#FF0000"><strong>*</strong></font></label>
                                <input type="text" autofocus class="form-control" value="<?php echo $backdate_days; ?>" name="no_of_days" id="no_of_days" style="width: 100%;" required onkeypress="return isNumberKey(event, this)">
                            </div>

                        </div>
                        <div class="box-footer">
                            <button class="btn btn-success pull-right btn_backdate">Update</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Operators Mapped</h3>
                    </div>
                    <div class="box-body ">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label">Operators Mapped</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th> Male Operator </th>
                                        <th> Female Operator </th>


                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $count = 0;

                                    foreach ($load_data as $value) {
                                        $count++;
                                    ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $value->male; ?></td>
                                            <td><?php echo $value->female; ?></td>

                                            <td class="mailbox-date pull-right">
                                                <button class="btn btn-danger btn-xs btn_reset" data-toggle="tooltip" title="Reset" data-acc='<?php echo $value->male_acc; ?>'>
                                                    <i class="fa fa-undo"> Reset</i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php
                                    }

                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
</div>



<script>
    function isNumberKey(evt, obj) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        var value = obj.value;
        var dotcontains = value.indexOf(".") != -1;
        if (dotcontains)
            if (charCode == 46) return false;
        if (charCode == 46) return true;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    /**************************** INSERT  ****************************/
    $("#form_submit").on('submit', (function(e) {
        var menu = $("#menu").val();

        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>Con_mapping/add",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                } else {
                    if ($.trim(data) === 'error') {
                        swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'order no exists') {
                        swal("Exists!", "Already Exists!", "warning");
                    } else if ($.trim(data) === 'data exists') {
                        $('#pid').focus();
                        swal({
                            title: "",
                            text: "Already Exists!",
                            type: "warning",
                            timer: 2000,
                            showConfirmButton: false,
                        });

                    } else if ($.trim(data) === 'success') {
                        swal({
                            title: "",
                            text: "Successfully Added!",
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false,
                        });

                        $(".clr").val('');
                        setTimeout(function() {
                            window.location.href = "<?php echo base_url() ?>Con_mapping/index/" + menu;
                        }, 2000);

                    }
                }
            },
            error: function(e) {
                swal({
                    title: "Error!",
                    text: "Try Again",
                    type: "warning",
                    timer: 2000,
                    showConfirmButton: false,
                });
                setTimeout(function() {
                    window.location.href = "<?php echo base_url() ?>Con_Contact_CSV/index/" + menu;
                }, 2000);
            }
        });
    }));

    /******************************** RESET ***************************/
    $(".btn_reset").click(function() {
        var acc_no = $(this).attr('data-acc');
        var menu = $("#menu").val();

        swal({
                title: "Are you sure you want to Reset?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Reset it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_mapping/reset", {
                        remove_data: "data",
                        acc_no: acc_no
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal({
                                    title: "Reset!",
                                    text: "Successfully Reset Details!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_mapping/index/" + menu;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    /******************************** BACK DATE ***************************/
    $(".btn_backdate").click(function() {
        var no_of_days = $('#no_of_days').val();
        var menu = $("#menu").val();

        if (no_of_days == '') {
            swal("Please enter the no of days to be back dated!!")
        } else {
            swal({
                    title: "Are you sure you want to update?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Update it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.post("<?php echo base_url() ?>Con_mapping/backdate", {
                            remove_data: "data",
                            no_of_days: no_of_days
                        }, function(data) {
                            if ($.trim(data.status) === 'success') {
                                swal({
                                        title: "Updated!",
                                        text: "Successfully Updated Details!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },
                                    function(isConfirm) {
                                        if (isConfirm) {
                                            window.location.href = "<?php echo base_url() ?>Con_mapping/index/" + menu;
                                        }
                                    });

                            } else if ($.trim(data.status) === ' error') {
                                swal("", "Error!", "warning");

                            } else {
                                swal("Oops...", "Something went wrong!", "warning");
                            }
                        }, "json");
                    } else {
                        swal("Cancelled", "", "error");
                    }
                }
            );
        }
    });
</script>