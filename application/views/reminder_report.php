<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
    $date1 = date("Y-m-d");
    $date2 = date("Y-m-d");
} else {
    $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
    $date1 = substr($daterange, 0, 10);
    $date2 = substr($daterange, 17, 20);
}
?>
?>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1> <i class="fa fa-sitemap"></i> Agent-wise Reminder Report
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12 col-sm-6">
                <div class="row">
                  <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                  <form role="form" action="<?php echo site_url('Con_Call_Report/reminder_report/') ?><?php echo $menu ."/".$date1.' - '.$date2; ?>" method="post" class="">

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                        </div>
                      </div>
                    </div>
                    <?php if($val == 12 || $val == 15){?>
                    <div class="form-group col-md-6">
                      <label for="agent">Agent</label>
                      <select id="agent" name="agent" class="form-control">
                        <option value="">Select</option>
                        <?php foreach ($load_agent as $agent) { ?>
                          <option value="<?php echo $agent->Acc_No ?>"><?php echo $agent->Name ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <?php } ?>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <button name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle btn-search"><i class="fa fa-search"></i> Search</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php if (isset($resultlist)) { ?>
          <div class="box box-info">
            <div class="box-body">
              <form action="javascript:void(0);" method="post">
                <div class="table-responsive ptt10">
                  <table class="table table-hover table-striped example">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Agent</th>
                        <th>Name</th>
                        <th>Contact No</th>
                        <th>Reminder date</th>
                        <th>Note</th>
                        <th class="pull-right">Action</th>
                    </thead>
                    <tbody>
                      <?php
                      $row_count = 1;

                      foreach ($resultlist as $key => $value) {
                        $dt = $value['reminder_date'];
                        if ($dt == '0000-00-00') {
                          $rem = '';
                        } else {
                          $rem = $dt;
                        }
                      ?>
                        <tr>
                          <td><?php echo $row_count . '.'; ?></td>
                          <td><?php echo $value['emp_name']; ?></td>
                          <td><?php echo $value['name']; ?></td>
                          <td><?php echo $value['con_no']; ?></td>
                          <td><?php echo $rem; ?></td>
                          <td><?php echo $value['reminder_note']; ?></td>
                          <td class="mailbox-date pull-right">
                          <button class="btn btn-default btn-xs btn_reset_reminder" title="Reset Reminder" value="<?php echo $value['con_no']; ?>">
                            <i class="fa fa-trash"></i>
                          </button>
                        </td>

                        </tr>
                      <?php
                        $row_count++;
                      }

                      ?>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
  /**************************** DATE ***********************/
  $('#date_range').daterangepicker({
    autoclose: false,
    todayBtn: true,
    pickerPosition: "bottom-left"
  });

  function formHandler(date){
    var menu = $("#menu").val();
    window.location.href = "<?php echo base_url()?>Con_Call_Report/reminder_report/"+menu+"/"+date;
  }

  /**************************** SEND SMS ***********************/
  $(document).on('click', '.btn_reset_reminder', function(e) {
        var menu = $("#menu").val();
        var con_no = $(this).val();

        swal({
                title: "Are you sure you want to Reset the Reminder?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Reset!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_Call_Report/reset_reminder", {
                        delete: "data",
                        con_no: con_no
                    }, function(data) {
                        if ($.trim(data) === 'success') {
                            swal({
                                    title: "Success!",
                                    text: "Successfully Reset!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_Call_Report/reminder_report/" + menu;
                                    }
                                });

                        } else if ($.trim(data) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });
</script>