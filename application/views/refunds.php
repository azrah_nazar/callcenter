<?php
$menu = $this->uri->segment(3);
?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-id-card"></i> Refund Payment
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-6">
                                <div class="row">
                                    <form role="form" action="<?php echo site_url('Con_refund/index/') ?><?php echo $menu; ?>" method="post" class="">
                                        <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Search By Keyword</label>
                                                <input type="text" name="search_text" class="form-control" placeholder="Search By Contact Number, WR No, CR No..">
                                                <span class="text-danger"><?php echo form_error('search_text'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <button type="submit" name="search" value="search_full" class="btn btn-primary btn-sm pull-right checkbox-toggle"><i class="fa fa-search"></i> Search</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(isset($load_data)){?>
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Accepted Payment List</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label"> Accepted Payment List</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Refund</th>
                                        <th>Reg No</th>
                                        <th>CR No</th>
                                        <th>Name</th>
                                        <th>Deposited Amount</th>
                                        <th>Contact No</th>
                                        <th>Whatsapp No</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    $count = 1;

                                    foreach ($load_data as $value) {
                                    ?>
                                        <tr>
                                            <td> <?php echo $count; ?>.</td>
                                            <td>
                                                <button class="btn btn-success btn-xs btn_refund" data-toggle="modal" data-target="#refund_model" title="Edit" data-deposit='<?php echo $value->deposits; ?>' value="<?php echo $value->reg_no; ?>">
                                                    <i class="fa fa-money"></i>
                                                </button>
                                            </td>
                                            <td> <?php echo $value->reg_no; ?></td>
                                            <td> <?php echo $value->inv_no; ?></td>
                                            <td> <?php echo $value->name; ?></td>
                                            <td> <?php echo $value->deposits; ?></td>
                                            <td> <?php echo $value->con_no; ?></td>
                                            <td> <?php echo $value->whatsapp; ?></td>
                                        </tr>
                                    <?php
                                        $count++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>

        </div>

        <div class="modal fade" id="refund_model" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title title text-center modal_title"> Refund Payment</h4>
                    </div>
                    <div class="modal-body">
                        <form action="javascript:void(0);" id="form_refund">
                            <input type="hidden" name="reg_up" id="reg_up">
                            <input type="hidden" name="ac_deposits" id="ac_deposits">
                            <input type="hidden" name="tot_refunded" id="tot_refunded">

                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Refund Amount:</label>
                                        <input type="text" class="form-control" name="refund_amt" id="refund_amt" placeholder="Enter ..." autocomplete="off">
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button class="btn btn-danger">Refund</button>
                            </div>

                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>


    </section>
</div>



<script>
    /**************************** LOAD FORM DATA ***********************/
    $(".btn_refund").click(function() {
        var reg_no = $(this).val();
        var deposit = $(this).attr('data-deposit');
        $('#reg_up').val(reg_no);
        $('#ac_deposits').val(deposit);

        $.post("<?php echo base_url() ?>Con_refund/get_dataset", {
            get_dataset: "data",
            reg_no: reg_no
        }, function(data) {
            $.each(data.result, function(index, data) {

                $("#tot_refunded").val(data.tot_refunded);

            });
        }, "json");
    });

    /********************************* refund ******************************/
    $("#form_refund").on('submit', (function(e) {
        var menu = $("#menu").val();

        ac_deposits = $('#ac_deposits').val();
        refund = $('#refund_amt').val();
        tot_refunded = $('#tot_refunded').val();
        if (tot_refunded == '') {
            remaining_ref = ac_deposits;
            refund_amt = refund;
        } else {
            remaining_ref = parseInt(ac_deposits) - parseInt(tot_refunded);
            refund_amt = refund;
        }

        if (parseInt(refund_amt) <= parseInt(remaining_ref)) {

            e.preventDefault();
            $.ajax({
                url: "<?php echo base_url() ?>Con_refund/insert",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {},
                success: function(data) {
                    $('#edit_data').modal('hide')
                    if ($.trim(data) === '') {
                        swal("Oops...", "Something went wrong!", "warning");

                    } else {
                        if ($.trim(data) === 'error') {
                            swal("SQL Error!", "Please Try Again!", "warning");

                        } else if ($.trim(data) === 'success') {
                            swal({
                                    title: "Success!",
                                    text: "Successfully made Refund!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_refund/index/" + menu;
                                    }
                                });
                        }
                    }
                },
                error: function(e) {
                    swal({
                        title: "Error!",
                        text: "Try Again",
                        type: "warning",
                        timer: 2000,
                        showConfirmButton: false,
                    });
                    setTimeout(function() {
                        window.location.href = "<?php echo base_url() ?>Con_refund/index/" + menu;
                    }, 2000);
                }
            });
        } else {
            swal('Please enter a valid amount for  refund! You can take a refund upto LKR.' + remaining_ref + "!");
        }
    }));
</script>