<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1><i class="fa fa-sitemap"></i> Staff Directory
          <small class="pull-right">
            <?php
              $menu = $this->uri->segment(3);
            ?>
              <!--<a href="<?php //echo base_url(); ?>Con_agent_registration/index/<?php //echo $menu; ?>" class="btn btn-primary btn-sm"   >-->
              <!--    <i class="fa fa-plus"></i> Add Agent-->
              <!--</a>-->
              <!--<a href="<?php //echo base_url(); ?>Con_affiliate_registration/index/<?php //echo $menu; ?>" class="btn btn-primary btn-sm"   >-->
              <!--    <i class="fa fa-plus"></i> Add Affiliate-->
              <!--</a>-->

              <a target="_blank" href="<?php echo base_url(); ?>Con_staff_registration/index/<?php echo $menu; ?>" class="btn btn-success btn-sm">
                  <i class="fa fa-plus"></i> Add New Staff
              </a>
          </small>
        </h1>

    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="row">
                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                    <form role="form" action="<?php echo site_url('Con_staff_list/index/'.$menu) ?>" method="post" class="">
                      <div class="col-sm-12">
                        <div class="form-group"> 
                            <label>Role</label><small class="req"> *</small>
                              <select autofocus name="role" class="form-control">
                                <option value="">Select</option>
                                <?php
                                foreach($designations as $desig) { ?>
                                  <option value="<?php echo $desig->ID ?>" <?php if (set_value('role') == $desig->ID) echo "selected=selected" ?>><?php echo $desig->desig ?></option>
                                <?php }
                                    ?>
                              </select>
                          </div>  
                      </div>

                      <div class="col-sm-12">
                        <div class="form-group">
                          <button type="submit" name="search" value="search_filter" class="btn btn-primary btn-sm pull-right checkbox-toggle"><i class="fa fa-search"></i> Search</button>
                        </div>
                      </div>
                    </form>
                  </div>  
                </div>
                
                <div class="col-md-6">
                  <div class="row">
                      <form role="form" action="<?php echo site_url('Con_staff_list/index/'.$menu) ?>" method="post" class="">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label>Search by Keyword</label>
                            <input type="text" name="search_text" class="form-control"  placeholder="Search by Staff ID, Name, Designation">
                            </div>
                        </div>

                        <div class="col-sm-12">
                          <div class="form-group">
                              <button type="submit" name="search" value="search_full" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> Search</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false"><i class="fa fa-newspaper-o"></i> Card View</a></li>
                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true"><i class="fa fa-list"></i> List View</a></li>
              </ul>
              <div class="tab-content">
                <div class="download_label">Staff Search</div>
                  <div class="tab-pane table-responsive no-padding" id="tab_2">
                    <table class="table table-striped table-bordered table-hover example" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Staff ID</th>
                          <th>Name</th>
                          <th>Designation</th>
                          <th>Division</th>
                          <th>Contact No</th>
                          <th class="text-right">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php

                        if (empty($load_data)) {?>
                          <tr>
                            <td colspan="12" class="text-danger text-center">No Record Found!!</td>
                          </tr>                                           
                        <?php  } else {
                          $count = 0;
                         if (!empty($resultlist)) {
                            $val = $resultlist;
                         }else{
                            $val = $load_data;
                         }
                            foreach ($val as $value) {
                          
                            $count ++; 
                            ?>
                            <tr>
                              <td><?php echo $count; ?></td>
                              <td><?php echo $value->Acc_No; ?></td>
                              <td><?php echo $value->Name; ?></td>
                              <td><?php echo $value->desig; ?></td>
                              <td><?php echo $value->division; ?></td>
                              <td><?php echo $value->Tp; ?></td>
                              <td class="pull-right">
                                <!-- <button value="<?php //echo $value->ID; ?>" class="btn btn-default btn-xs"  data-toggle="tooltip" title="Show" ><i class="fa fa-reorder"></i></button> -->
                                <button data-value="<?php echo $value->ID; ?>" class="btn btn-default btn-xs btn_up"  data-toggle="tooltip" title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </button>
                              </td>
                            </tr>
                          <?php } }?>
                        </tbody>
                      </table>
                    </div> 

                    <div class="tab-pane active" id="tab_1">
                      <div class="mediarow">   
                        <div class="row">   
                          <?php if (empty($load_data)) { ?>
                          <div class="alert alert-info">No Records Found!!</div>
                            <?php } else {
                              $count = 1;
                              if (!empty($resultlist)) {
                                 $val = $resultlist;
                              }else{
                                 $val = $load_data;
                              }
                              foreach ($val as $staff) {  ?>
                                <div class="col-lg-3 col-md-4 col-sm-6 img_div_modal">
                                  <div class="staffinfo-box">
                                    <div class="staffleft-box">
                                      <?php
                                        if (!empty($staff->prof_pic)) {
                                          $image = $staff->prof_pic;
                                        } else {
                                          $image = "uploads/staff_images/no_image.png";
                                        }
                                      ?>
                                      <img  src="<?php echo base_url(). $image ?>" />
                                    </div>
                                    <div class="staffleft-content">
                                      <h5><span data-toggle="tooltip" title="Name" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><?php echo $staff->Name; ?></span></h5>
                                      <p><font data-toggle="tooltip" title="<?php echo "Employee Id"; ?>" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><?php echo $staff->Acc_No; ?></font></p>
                                      <p><font data-toggle="tooltip" title="<?php echo "Contact Number"; ?>" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><?php echo $staff->Tp; ?></font></p>
                                      <p><font data-toggle="tooltip" title="<?php echo 'Location'; ?>" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">
                                      </font><font data-toggle="tooltip" title="<?php echo 'Department'; ?>" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"> <?php echo $staff->division; ?></font></p>
                                      <p class="staffsub" ><span data-toggle="tooltip" title="Designation" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing"><?php echo $staff->desig; ?></span> </p>
                                    </div>
                                    <div class="overlay3">
                                      <div class="stafficons">
                                        <!-- <a title="Show"  href="<?php //echo base_url() . "admin/staff/profile/" . $staff->ID ?>"><i class="fa fa-navicon"></i></a> -->
                                        <a title="Edit"  data-value="<?php  echo $staff->ID;?>" href="#" class="btn_up"><i class=" fa fa-pencil"></i></a>
                              
                                        </div>
                                    </div>
                                  </div>
                                </div><!--./col-md-3-->
                              <?php }
                            }
                            ?>
                          </div><!--./col-md-3-->
                        </div><!--./row-->  
                      </div><!--./mediarow-->                           
                  </div>                                                          
              </div> 
          </div>
        </div>



        

    </section>
</div>



<script>
  /********************************* Edit ******************************/

  $(".btn_up").click(function(){

    var menu = $('#menu').val();
    var id = $(this).attr('data-value');

    window.open("<?php echo base_url()?>Con_staff_registration/index/"+menu+'/'+id, '_blank');

  });
</script>   