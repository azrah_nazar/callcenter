<?php
if ($edit_det == '') {
  $emp_id = '';
  $app_date = date('Y-m-d');

  $div_id = '';
  $division = '';
  $desig_id = '';
  $designation = '';
  $name = '';
  $sinhala_name = '';
  $other_name = '';
  $nic = '';
  $emp_no = '';
  $tp = '';
  $tp2 = '';
  $email = '';
  $gender = '';

  $prof_pic = "no_pic";

  $salary = '';
  $BRA = '';
  $incentive = '';
  $special = '';
  $family = '';
  $transport = '';
  $tot_allowance = '';
  $epf_cal = '';
  $contract = '';
  $epf = '';

  $annual = '';
  $sick = '';
  $casual = '';
  $att_bonus = '';
  $shift_in = '';
  $shift_out = '';
} else {
  foreach ($edit_det as $edit_det) {
    $dt = $edit_det->appoinment;
    $app_date = date("Y-m-d", strtotime($dt));

    $div_id = $edit_det->div_id;;
    $division = $edit_det->division;;
    $desig_id = $edit_det->desig_id;
    $designation = $edit_det->desig;
    $name = $edit_det->Name;
    $sinhala_name = $edit_det->sinhala_name;
    $other_name = $edit_det->othr_nm;
    $nic = $edit_det->Nic;
    $emp_no = $edit_det->epf_no;
    $tp = $edit_det->Tp;
    $tp2 = $edit_det->tpOffice;
    $email = $edit_det->email;
    if ($edit_det->prof_pic == '') {
      $prof_pic = "no_pic";
    } else {
      $prof = $edit_det->prof_pic;
      $prof_pic = base_url() . $prof;
    }
    $gender = $edit_det->gender;

    $salary = $edit_det->Salary;
    $BRA = $edit_det->BRA;
    $incentive = $edit_det->Incentive;
    $special = $edit_det->Spec;
    $family = $edit_det->Family;
    $transport = $edit_det->transport;
    $tot_allowance = $edit_det->Tot_allowance;
    $epf_cal = $edit_det->epf_trans;
    $contract = $edit_det->cntrct;
    $epf = $edit_det->epf_basic;

    $annual = $edit_det->annual;
    $sick = $edit_det->sick;
    $casual = $edit_det->casual;
    $att_bonus = $edit_det->att_bonus;

    if ($edit_det->in_t == '1900-01-01 00:00:00.000') {
      $shift_in = '';
    } else {
      $shift_in = $edit_det->in_t;
    }
    if ($edit_det->Out_t == '1900-01-01 00:00:00.000') {
      $shift_out = '';
    } else {
      $shift_out = $edit_det->Out_t;
    }
  }
}
?>

<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1>
      <i class="fa fa-sitemap"></i> Staff Registration
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
          <form action="javascript:void(0);" id="form_submit">
            <div class="box-body">
              <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id; ?>">

              <div class="tshadow mb25 bozero">

                <h4 class="pagetitleh2">Basic Information </h4>

                <div class="around10">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Division:<font color="#FF0000"><strong>*</strong></font></label>
                        <select autofocus class="form-control" name="division" id="division" style="width: 100%;" required>
                          <option value="<?php echo $div_id; ?>"><?php echo $division; ?></option>
                          <?php
                          foreach ($divisions as $div) {
                            echo "<option value=" . $div->id . ">" . $div->division . "</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Designation:<font color="#FF0000"><strong>*</strong></font></label>
                        <select class="form-control" name="desig" id="desig" style="width: 100%;" required>
                          <option value="<?php echo $desig_id; ?>"><?php echo $designation; ?></option>
                          <?php
                          foreach ($designations as $desig) {
                            echo "<option value=" . $desig->ID . ">" . $desig->desig . "</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Staff Name:<font color="#FF0000"><strong>*</strong></font></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter ..." value="<?php echo $name; ?>" autocomplete="off" required>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Staff Name in Sinhala:<font color="#FF0000"><strong>*</strong></font></label>
                        <input type="text" class="form-control" name="sinhala_name" id="sinhala_name" placeholder="Enter ..." value="<?php echo $sinhala_name; ?>" autocomplete="off" required>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>NIC:<font color="#FF0000"><strong>*</strong></font></label>
                        <input type="text" class="form-control" name="nic" id="nic" placeholder="Enter ..." autocomplete="off" value="<?php echo $nic; ?>" required>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Gender:<font color="#FF0000"><strong>*</strong></font></label>
                        <select autofocus class="form-control" name="gender" id="gender" style="width: 100%;" required>
                          <option value="<?php echo $gender; ?>"><?php echo $gender; ?></option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                        </select>
                      </div>
                    </div>

                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="url">Contact No:</label>
                        <input type="text" class="form-control" name="tp" id="tp" placeholder="Enter ..." autocomplete="off" value="<?php echo $tp; ?>">
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="url">Contact No 2:</label>
                        <input type="text" class="form-control" name="tp2" id="tp2" placeholder="Enter ..." autocomplete="off" value="<?php echo $tp2; ?>">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="url">Email:</label>
                        <input type="emali" class="form-control" name="email" id="email" placeholder="Enter ..." autocomplete="off" value="<?php echo $email; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <label for="app_date">Appointment Date:</label>
                      <div class=" txt-full-width">
                        <input class="form-control mat_date date" name="app_date" type="text" value="<?php echo $app_date; ?>" id="app_date">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="image" class="control-label">Upload Staff Image:</label>
                        <div>
                          <?php if ($prof_pic != 'no_pic') { ?>
                            <img src="<?php echo $prof_pic; ?>" alt="" width="150" height="150" class="img-bordered img-responsive">
                            <br>
                          <?php } ?>
                          <input class="filestyle form-control" type="file" id="image" name="image" size='100' height="100px">
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="box-footer">
              <?php
              if ($edit_det == '') {
              ?>
                <button type="submit" class="btn btn-info" id="submit_btn">Save</button>
                <button type="reset" class="btn btn-default">Cancel</button>
              <?php } else { ?>
                <button type="submit" class="btn btn-info" id="submit_btn">Update</button>
              <?php } ?>
            </div>
          </form>
        </div>
      </div>
    </div>

  </section>
</div>


<script>
  /**************************** DATE ***********************/
  var date_format = 'yyyy-mm-dd';
  $(document).ready(function() {
    $(".date").datepicker({
      format: date_format,
      autoclose: true,
      endDate: '+0d',
      todayHighlight: true
    });

    $(".datechk").datepicker({
      format: date_format,
      autoclose: true,
      todayHighlight: true
    });
  });

  $('.date').mask('YYYY-MM-DD', {
    'translation': {
      Y: {
        pattern: /[0-9]/
      },
      M: {
        pattern: /[0-9]/
      },
      D: {
        pattern: /[0-9]/
      }
    }
  });

  $(function() {
    $(':input[required=""],:input[required]').bind('focusout', function() {
      if ($(this).val() == "") {
        $(this).css("border-color", "red");
      } else {
        $(this).css("border-color", "#ccc");
      }
    });

    $('.txt_nav:first').focus();
    var $inp = $('.txt_nav');
    $inp.bind('keydown', function(e) {
      var n = $inp.length;
      var key = e.which;
      if (key == 13) {
        e.preventDefault();
        var nxtIdx = $inp.index(this) + 1;
        if (nxtIdx < n) {
          $('.txt_nav')[nxtIdx].focus();
        } else {
          $('.txt_nav')[nxtIdx - 1].blur();
          $('#submit_btn').focus();
        }
      }
    });

    $("#submit_btn").keypress(function(event) {
      if (event.which == 13) {
        req_input_check();
      }
    });

  });

  function req_input_check() {
    $(':input[required=""],:input[required]').bind('keypress', function(event) {
      if (event.which == 13) {
        if ($(this).val() == "") {
          $(this).css("border-color", "red");
        } else {
          $(this).css("border-color", "#ccc");
          $('#form_submit').submit();
        }
      }
    });
  }


  /**************************** INSERT  ****************************/

  $("#form_submit").on('submit', (function(e) {
    var menu = $("#menu").val();
    var emp_id = $("#emp_id").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_staff_registration/add",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'order no exists') {
            swal("Exists!", "Order No already Exists!", "warning");
          } else if ($.trim(data) === 'updated') {
            $('#pid').focus();
            swal({
              title: "",
              text: "Successfully Updated!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              window.location.href = "<?php echo base_url() ?>Con_staff_registration/index/" + menu
            }, 2000);

          } else if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Added!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              window.location.href = "<?php echo base_url() ?>Con_staff_registration/index/" + menu;
            }, 2000);

          }
        }
      },
      error: function(e) {
        swal({
          title: "ERROR!",
          text: "Please Try Again!",
          type: "warning",
          timer: 2000,
          showConfirmButton: false,
        });
        //err2
      }
    });
  }));
</script>
<script>
  $("#bank_name").change(function() {

    // var bank_name = $(this).val();
    var bank_name = $("#bank_name option:selected").text();
    var bank_id = $("#bank_name").val();


    $.post('<?php echo base_url() ?>Con_staff_registration/get_bank_code', {
      'get_bank_code': 'data',
      bank_id: bank_id
    }, function(data) {
      $.each(data, function(index, data) {
        $('#bank_code').val(data.code);
      });

    }, "json");

  });


  $("#branch_name").change(function() {

    var branch_name = $("#branch_name option:selected").text();
    var branch_id = $("#branch_name").val();


    $.post('<?php echo base_url() ?>Con_staff_registration/get_branch_code', {
      'get_branch_code': 'data',
      branch_id: branch_id
    }, function(data) {
      $.each(data, function(index, data) {
        console.log(data);
        $('#branch_code').val(data.code);
      });

    }, "json");

  });


  $('#bank_name').change(function() {
    var bank_name = $("#bank_name option:selected").text();
    var bank_id = $("#bank_name").val();
    $.post('<?php echo base_url() ?>Con_staff_registration/get_branch', {
      'get_branch': 'data',
      bank_id: bank_id
    }, function(data) {
      //console.log(data.records);
      // if (data.records === undefined || data.records.length === 0 || data.records === null) {
      //   rowData = '<option>No Category Found for this Project</option>';
      //   $('#cat_id').html('').append(rowData);
      // } else {
      var rowData = '';
      rowData += '<option></option>';
      $.each(data.records, function(index, data) {
        rowData += "<option value='" + data.id + "'>" + data.branch_name + "</option>";
      });
      $('#branch_name').html('').append(rowData);
      // }

    }, "json");
  });
</script>