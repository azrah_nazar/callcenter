<?php
$menu = $this->uri->segment(3);
?>

<style type="text/css">
	.error {
		color: red;
		size: 80%
	}

	.hidden {
		display: none;
	}
</style>
<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			<i class="fa fa-id-card"></i> Zoom Attendance Sync</h1>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">

			<div class="col-md-4">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Add Zoom Details</h3>
					</div>
					<input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
					<form class="form-sample" id="form_submit" action="javascript:void(0);">
						<div class="box-body">
							<div class="form-group">
								<label>Registration No:<font color="#FF0000"><strong>*</strong></font></label>
								<input type="text" autofocus class="form-control txt_nav" name="reg_no" id="reg_no" style="width: 100%;" required>
							</div>

							<div class="form-group">
								<label>User Email:</label>
								<input type="email" class="form-control txt_nav" name="email">
							</div>

							<div class="form-group">
								<label>Join Time:</label>
								<input type="text" class="form-control txt_nav" name="join_time">
							</div>

							<div class="form-group">
								<label>Leave Time:</label>
								<input type="text" class="form-control txt_nav" name="leave_time">
							</div>

							<div class="form-group">
								<label>Duration (Minutes):</label>
								<input type="text" class="form-control txt_nav" name="duration">
							</div>

							<div class="form-group">
								<label>Is Guest:</label>
								<input type="text" class="form-control txt_nav" name="is_guest">
							</div>

							<button type="submit" class="btn btn-info pull-left" id="btn_add">Add Details</button>

						</div>

					</form>
					<hr>
					<form class="form-sample" id="form_csv_submit" action="javascript:void(0);">
						<div class="box-body">


							<div class="form-group">
								<label>Select CSV file<font color="#FF0000"><strong>*</strong></font></label>
								<input class="filestyle form-control" type="file" id="con_csv" name="con_csv" size='100' height="100px">

							</div>


						</div>
						<div class="box-footer">
							<button type="submit" class="btn btn-info pull-left" id="btn_upload" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Uploading">Upload</button>
						</div>
					</form>

				</div>
			</div>
			<div class="col-md-8">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12 col-sm-6">
								<div class="row">
									<form role="form" action="<?php echo site_url('Con_zoom_upload/index/') ?><?php echo $menu . "/" ?>" method="post" class="">

									
										<div class="form-group col-md-12">
											<label for="workshop">Workshop Name</label>
											<select id="workshop" name="workshop" class="form-control select2">
												<option value="">Select</option>
												<?php foreach ($load_ws as $workshop) { ?>
													<option value="<?php echo $workshop->id; ?>"><?php echo $workshop->w_name." (".$workshop->w_date." / ".date("h:i A", strtotime($workshop->w_time)).")"; ?></option>
												<?php } ?>
											</select>
										</div>

										<div class="col-sm-12">
											<div class="form-group">
												<button name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle btn-search"><i class="fa fa-search"></i> Search</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="box box-primary">
					<div class="box-header ptbnull">
						<h3 class="box-title titlefix">Zoom Attendance List</h3>
					</div>
					<div class="box-body ">
						<div class="table-responsive mailbox-messages">
							<div class="download_label"> Zoom Attendance List</div>
							<table class="table table-striped table-bordered table-hover example">
								<!-- example -->
								<thead>
									<tr>
										<th>#</th>
										<th>Reg. No</th>
										<th>Workshop</th>
										<th>Email</th>
										<th>Join Time</th>
										<th>Leave Time</th>
										<th>Duration</th>
										<th>Is Guest</th>
									</tr>
								</thead>
								<tbody>

									<?php
									$count = 1;

									foreach ($load_data as $value) {
									?>
										<tr>
											<td class="mailbox-name"> <?php echo $count; ?>.</td>
											<td class="mailbox-name"> <?php echo $value->reg_no; ?></td>
											<td class="mailbox-name"> <?php echo $value->w_name; ?></td>
											<td class="mailbox-name"> <?php echo $value->user_email; ?></td>
											<td class="mailbox-name"> <?php echo $value->join_time; ?></td>
											<td class="mailbox-name"> <?php echo $value->leave_time; ?></td>
											<td class="mailbox-name"> <?php echo $value->duration; ?></td>
											<td class="mailbox-name"> <?php echo $value->is_guest; ?></td>
										</tr>
									<?php
										$count++;
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>

		<script src="<?php echo base_url(); ?>js/file-upload.js"></script>

		<script>
			var date_format = 'yyyy-mm-dd';
			$(document).ready(function() {
				$(".date").datepicker({
					format: date_format,
					autoclose: true,
					endDate: '+0d',
					todayHighlight: true
				});
			});

			//////////////////////////////con_no ADD /////////////////////////////////
			$("#form_submit").on('submit', (function(e) {
					var menu = $("#menu").val();

					e.preventDefault();
					$.ajax({
						url: "<?php echo base_url() ?>Con_zoom_upload/add",
						type: "POST",
						data: new FormData(this),
						contentType: false,
						cache: false,
						processData: false,
						beforeSend: function() {},
						success: function(data) {
							console.log(data);
							if ($.trim(data) === '') {
								swal("Oops...", "Something went wrong!", "warning");

							} else {
								if ($.trim(data) === 'error') {
									swal("SQL Error!", "Please Try Again!", "warning");

								} else if ($.trim(data) === 'exist') {
									swal("", "Registration No already Exists!", "warning");
								} else if ($.trim(data) === 'updated') {
									$('#pid').focus();
									swal({
										title: "",
										text: "Successfully Updated!",
										type: "success",
										timer: 2000,
										showConfirmButton: false,
									});
									setTimeout(function() {
										window.location.href = "<?php echo base_url() ?>Con_zoom_upload/index/" + menu + "/";
									}, 2000);

								} else if ($.trim(data) === 'success') {
									swal({
										title: "",
										text: "Successfully Added!",
										type: "success",
										timer: 2000,
										showConfirmButton: false,
									});
									setTimeout(function() {
										window.location.href = "<?php echo base_url() ?>Con_zoom_upload/index/" + menu;
									}, 2000);

								}
							}
						},
						error: function(e) {
							swal({
								title: "Error!",
								text: "Try Again",
								type: "warning",
								timer: 2000,
								showConfirmButton: false,
							});
							setTimeout(function() {
								window.location.href = "<?php echo base_url() ?>Con_zoom_upload/index/" + menu;
							}, 2000);
						}
					});

				})

			);

			///////////////////////////////// CSV upload //////////////////////////////////
			$("#form_csv_submit").on('submit', (function(e) {
				var menu = $("#menu").val();
				var btn = $('#btn_upload');
				btn.button('loading');

				e.preventDefault();
				$.ajax({
					url: "<?php echo base_url() ?>Con_zoom_upload/readExcel",
					type: "POST",
					data: new FormData(this),
					contentType: false,
					cache: false,
					dataType: 'json',
					processData: false,
					beforeSend: function()

					{},

					success: function(data) {
						btn.button('reset');
						if (data.status === 'success') {
							swal({
								title: "",
								text: "Successfully Added!",
								type: "success",
								timer: 2000,
								showConfirmButton: false,
							});
							location.reload(true);

						} else if (data.status === 'error2') {
							swal("", "Error While uploading file on the server. Please Try Again!", "warning");

						} else if (data.status === 'invalid') {
							swal("", "Invaild File Type! File must have .csv extension", "warning");

						} else {
							swal("", "Something went wrong! Please Try Again!", "warning");
						}
					},

					error: function(e) {
						btn.button('reset');
						swal({
							title: "Error!",
							text: "Try Again",
							type: "warning",
							timer: 2000,
							showConfirmButton: false,
						});
						setTimeout(function() {
							window.location.href = "<?php echo base_url() ?>Con_zoom_upload/index/" + menu;
						}, 2000);
					}
				});
			}));


			//**************************reg_no**********************************************

			var reg_no = document.getElementById("reg_no").value;

			$("#reg_no").autocomplete({
				autoFocus: true,
				source: '<?php echo site_url("Con_zoom_upload/search_reg_no") ?>',
				minLength: 1,
				select: function(event, ui) {},
				open: function(event, ui) {
					$(".ui-autocomplete").css("z-index", 1000000)
				}
			});
		</script>