<?php
  $menu = $this->uri->segment(3);
?>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1> <i class="fa fa-sitemap"></i> Registered List
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php 
          if($val == 12 || $val == 15){
        ?>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>
            
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12 col-sm-6">
                <div class="row">
                  <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(4); ?>">
                  <form role="form" action="<?php echo site_url('Con_pre_registry/registered_list/') ?><?php echo $menu."/" ?>"  method="post" class="">

                    <div class="form-group col-md-12">
                      <label for="agent">Agent</label>
                      <select id="agent" name="agent" class="form-control">
                        <option value="">Select</option>
                        <?php foreach ($load_agent as $agent) {?>
                        <option value="<?php echo $agent->Acc_No ?>"><?php echo $agent->Name ?></option>
                        <?php } ?>
                      </select>
                    </div>
              
                    <div class="col-sm-12">
                      <div class="form-group">
                        <button name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle btn-search"><i class="fa fa-search"></i> Search</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } if (isset($resultlist)) { ?>
        <div class="box box-info">
          <div class="box-body">
            <form action="javascript:void(0);" method="post">
              <div class="table-responsive ptt10">
                <table class="table table-hover table-striped example"> 
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Operator</th>
                      <th>Name</th>
                      <th>Contact No</th>
                      <th>Address</th>
                      <th>WhatsApp No</th>
                      <th>Occupation</th>
                      <th>DOB</th>
                    </thead>
                    <tbody>
                      <?php
                      $row_count = 1;

                      foreach ($resultlist as $key => $value) {
                        $birthday = $value['dob'];
                        if($birthday == '0000-00-00'){
                          $dob = '';
                        }else{
                          $dob = $birthday;
                        }
                        ?>
                        <tr>
                          <td><?php echo $row_count.'.'; ?></td>
                          <td><?php echo $value['op_name']; ?></td>
                          <td><?php echo $value['name']; ?></td>
                          <td><?php echo $value['con_no']; ?></td>
                          <td><?php echo $value['address']; ?></td>
                          <td><?php echo $value['whatsapp']; ?></td>
                          <td><?php echo $value['job']; ?></td>
                          <td><?php echo $dob; ?></td>

                        </tr>
                        <?php
                        $row_count++;
                      }

                      ?>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>
          </div>
        <?php } ?>
        </div>
      </div>
    </section>
  </div>
