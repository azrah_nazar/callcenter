<?php
$menu = $this->uri->segment(3);
$dte = $this->uri->segment(4);

if (empty($dte)) {
    $dt = date('Y-m-d');
} else {
    $dt = $dte;
}
$datee = date("d", strtotime($dt));
$mon = date("m", strtotime($dt));
if ($datee > '10') {
    $date1 = date('Y-'.$mon.'-11');
    $date2 = $dt;

    $last_month_name = date("F", strtotime($dt));
} else {
    $last_month = date("m", strtotime('-1 month', strtotime($dt)));
    $last_month_name = date("F", strtotime('-1 month', strtotime($dt)));
    $date1 = date('Y-' . $last_month . '-11');
    $date2 = $dt;
}

?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-id-card"></i> Daily / Total Sales
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-md-12">
                <div class="col-md-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

                        </div>
                        <div class="box-body">
                            <form role="form" action="<?php echo site_url('Con_sales_report/daily_sales/') ?><?php echo $menu . "/" . $dt; ?>" method="post" class="">
                                <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                                        <input type="text" class="form-control datechk" name="w_date" id="w_date" placeholder="Enter ..." autocomplete="off" required value="<?php echo $dt; ?>" onChange="formHandler2($(this).val())">
                                    </div>
                                </div>

                                <!-- <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div> -->
                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header ptbnull">
                            <h3 class="box-title titlefix">Daily Sales - <?php echo $dt; ?></h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive mailbox-messages">
                                <div class="download_label"> Daily Sales - <?php echo $dt; ?></div>
                                <table class="table table-striped table-bordered table-hover example">
                                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Sales</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php
                                        $count = 1;
                                        $tot = 0;

                                        foreach ($daily_sales as $value) {
                                        ?>
                                            <tr>
                                                <td> <?php echo $count; ?>.</td>
                                                <td> <?php echo $value->agent_code; ?></td>
                                                <td> <?php echo $value->Name; ?></td>
                                                <td> <?php echo $value->pay_cnt;
                                                        $tot += $value->pay_cnt; ?></td>
                                            </tr>
                                        <?php
                                            $count++;
                                        }
                                        ?>
                                        <tr>
                                            <td> <b>Total Sales</b></td>
                                            <td></td>
                                            <td></td>
                                            <td> <b><?php echo $tot; ?></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header ptbnull">

                            <h3 class="box-title titlefix">Total Sales of <?php echo $last_month_name; ?> from <?php echo $date1 . ' to ' . $date2; ?></h3>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive mailbox-messages">
                                <div class="download_label"> Total Sales of <?php echo $last_month_name; ?> from <?php echo $date1 . ' to ' . $date2; ?></div>
                                <table class="table table-striped table-bordered table-hover example">
                                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Sales</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        <?php
                                        $count = 1;
                                        $tot = 0;

                                        foreach ($total_sales as $value) {
                                        ?>
                                            <tr>
                                                <td> <?php echo $count; ?>.</td>
                                                <td> <?php echo $value->agent_code; ?></td>
                                                <td> <?php echo $value->Name; ?></td>
                                                <td> <?php echo $value->pay_cnt;
                                                        $tot += $value->pay_cnt; ?></td>
                                            </tr>
                                        <?php
                                            $count++;
                                        }
                                        ?>
                                        <tr>
                                            <td> <b>Total Sales</b></td>
                                            <td></td>
                                            <td></td>
                                            <td> <b><?php echo $tot; ?></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </div>


    </section>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
    /**************************** DATE ***********************/
    var date_format = 'yyyy-mm-dd';
    $(document).ready(function() {

        $(".datechk").datepicker({
            format: date_format,
            autoclose: true,
            todayHighlight: true
        });

        $('.timepicker').datetimepicker({
            format: 'LT'
        });
    });

    function formHandler2(date) {
        var menu = $("#menu").val();
        window.location.href = "<?php echo base_url() ?>Con_sales_report/daily_sales/" + menu + "/" + date;
    }
</script>