<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i> Menu List</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

                <div class="col-md-3">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Menu List</h3>
                        </div>
                        <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>"> 
                        <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
                            <div class="box-body">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Select Main Project:</label><small class="req"> *</small>
                                    <select autofocus class="form-control" name="pid" id="pid" style="width: 100%;" required>
                                      <?php
                                      echo "<option ></option>"; 
                                      foreach($project as $proj) {
                                        echo "<option value=".$proj->ID.">".$proj->prjct."</option>"; 
                                      }
                                      ?>
                                    </select>
                                </div>

                                <div class="form-group"> 
                                  <label>Select Category:<font color="#FF0000"><strong>*</strong></font></label>
                                  <select class="form-control" name="cat_id" id="cat_id" style="width: 100%;" autocomplete="off" required>
                                  </select>
                                  <input type="hidden" name="category_name" id="category_name">
                                </div>

                                <div class="form-group" style="display:none" id="rep_cat_list">                 
                                  <label>Report Category:<font color="#FF0000"><strong>*</strong></font></label>

                                  <div id="report_list"></div>
                                </div>      

                                <div class="form-group">                 
                                  <label for="name">Name:<font color="#FF0000"><strong>*</strong></font></label>
                                  <input type="text" class="form-control clr" name="name" id="name" placeholder="Enter ..." autocomplete="off" required>
                                </div>

                                <div class="form-group">                 
                                  <label for="url">URL / Form<font color="#FF0000"><strong>*</strong></font></label>
                                  <input type="text" class="form-control clr" name="url" id="url" placeholder="Enter ..." autocomplete="off" required>
                                </div>

                                <div class="form-group">
                                  <label for="oid">Order ID<font color="#FF0000"><strong>*</strong></font></label>
                                  <input type="text" class="form-control clr" name="oid" id="oid" placeholder="Enter ..." autocomplete="off" required>
                                </div>



                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>  
                </div>   
            <div class="col-md-9">             
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Menu List</h3>
                    </div>
                    <div class="box-body ">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label">Menu List</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th> Project </th>
                                        <th> Menu Category </th>
                                        <th> Name </th>
                                        <th> URL / Form </th>
                                        <th> Order ID </th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                   

                                    <?php
                                    $count = 0;
                                     
                                    foreach($load_data as $value) { $count++; 
                                        ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $value->prjct;?></td>
                                            <td><?php echo $value->Category;?></td>
                                            <td><?php echo $value->Caption;?></td>
                                            <td><?php echo $value->Name;?></td>
                                            <td><?php echo $value->subcat_id;?></td>
                                            <td class="mailbox-date pull-right">

                                                <button class="btn btn-default btn-xs btn_up" data-toggle="modal" data-target="#edit_data" value="<?php echo $value->form_list_id; ?>"  title="Edit">
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                                   
                                                <button class="btn btn-default btn-xs btn_del"  data-toggle="tooltip" title="Delete" value="<?php echo $value->form_list_id; ?>">
                                                    <i class="fa fa-remove"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
           
        </div> 

    </section>
</div>

<div class="modal fade" id="edit_data" role="dialog">
    <div class="modal-dialog">       
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title text-center modal_title"> Edit Section</h4>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" id="form_update">
                  <input type="hidden" name="id_up" id="id_up">

                  <div class="form-horizontal">
                      <div class="box-body">
                          <div class="form-group">                 
                            <label>Main Project:<font color="#FF0000"><strong>*</strong></font></label>
                            <select class="form-control" name="pid_up" id="pid_up" style="width: 100%;" autocomplete="off" required>
                              <?php
                              echo "<option ></option>"; 
                              foreach($project as $proj) {
                                echo "<option value=".$proj->ID.">".$proj->prjct."</option>"; 
                              }
                              ?>
                            </select>
                          </div>            


                          <div class="form-group"> 
                            <label>Select Category:<font color="#FF0000"><strong>*</strong></font></label>
                            <select class="form-control" name="cat_id_up" id="cat_id_up" style="width: 100%;" autocomplete="off" required>
                            </select>
                          </div>

                          <div class="form-group" style="display:none" id="rep_cat_list_up">                 
                            <label>Report Category:<font color="#FF0000"><strong>*</strong></font></label>

                            <div id="report_list_up"></div>
                          </div>

                          <div class="form-group">                 
                            <label>Name:<font color="#FF0000"><strong>*</strong></font></label>
                            <input type="text" class="form-control" name="name_up" id="name_up" placeholder="Enter ..." autocomplete="off" required>
                          </div>

                          <div class="form-group">                 
                            <label>URL/Form:<font color="#FF0000"><strong>*</strong></font></label>
                            <input type="text" class="form-control" name="url_up" id="url_up" placeholder="Enter ..." autocomplete="off" required>
                          </div>

                           <div class="form-group">
                             <label>Order ID:</label>
                             <input type="text" class="form-control" name="oid_up" id="oid_up" placeholder="Enter ..." autocomplete="off">
                           </div>

                      </div>                   
                  </div>

                  <div class="box-footer"> 
                    <button class="btn btn-danger">UPDATE</button>
                  </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<script>
  /**************************** INSERT  ****************************/

   $("#form_submit").on('submit',(function(e) {
      var menu = $("#menu").val();

      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url()?>Con_menu_list/add",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
        },
        success: function(data){
          if($.trim(data) === '') {
            swal("Oops...", "Something went wrong!", "warning");

          } else {
            if($.trim(data) ==='error')
            {
              swal("SQL Error!", "Please Try Again!", "warning");

            } else if($.trim(data) ==='order no exists') {
              swal("Exists!", "Order No already Exists!", "warning");
            } else if($.trim(data) ==='data exists') {
              $('#pid').focus();
              swal({
                title: "",
                text: "Menu List Already Exists!",
                type: "warning",
                timer: 2000,
                showConfirmButton: false,
              });           

            } else if($.trim(data) ==='success') {
              swal({
                title: "",
                text: "Successfully Added!",
                type: "success",
                timer: 2000,
                showConfirmButton: false,
              });

              $(".clr").val('');
              setTimeout(function() 
              {
                //window.location.href = "<?php //cho base_url()?>Con_menu_list";
              }, 2000);
              
            }
          }
        },
        error: function(e) 
        {
          alert("err2");
        }
      });
    }));

    /********************************* EDIT ******************************/

    $("#form_update").on('submit',(function(e) {
      var menu = $("#menu").val();

      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url()?>Con_menu_list/update",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
        },
        success: function(data){
          $('#edit_data').modal('hide')
          if($.trim(data) === '') {
            swal("Oops...", "Something went wrong!", "warning");

          } else {
            if($.trim(data) ==='error')
            {
              swal("SQL Error!", "Please Try Again!", "warning");

            } else if($.trim(data) ==='data not exists') {
              swal("", "Menu Category Already Exist!", "warning");

            } else if($.trim(data) ==='success') {
              swal({
                title: "Updated!",
                text: "Successfully Updated!",
                type: "success",
                confirmButtonText: "OK"
              },
              function (isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url()?>Con_menu_list/index/"+menu;
                }
              });
            }
          }      
        },
        error: function(e) 
        {
          alert("err2");
        }
      });
    }));

  /******************************** Remove ***************************/

  $(".btn_del").click(function(){
    var row_id = $(this).val();
    var menu = $("#menu").val();

    swal({
     title: "Are you sure?",
     text: "",
     type: "warning",
     showCancelButton: true,
     confirmButtonClass: "btn-danger",
     confirmButtonText: "Yes, delete it!",
     cancelButtonText: "No, cancel!",
     closeOnConfirm: false,
     closeOnCancel: false
   },
   function(isConfirm) {
     if (isConfirm) {
      $.post( "<?php echo base_url()?>Con_menu_list/delete", {remove_data: "data", id : row_id}, function( data ) {   
       if($.trim(data.status) === 'success') {
         swal({
          title: "Deleted!",
          text: "Successfully Deleted!",
          type: "success",
          confirmButtonText: "OK"
        },
        function (isConfirm) {
          if (isConfirm) {
           window.location.href = "<?php echo base_url()?>Con_menu_list/index/"+menu;
         }
       });

       } else if($.trim(data.status) ===' error') {
         swal("", "Error!", "warning");

       } else{
         swal("Oops...", "Something went wrong!", "warning");
       }
     }, "json");
    } else {
      swal("Cancelled", "", "error");
    }
  });

  }); 



  /**************************** LOAD FORM DATA ***********************/

  $(".btn_up").click(function(){
    var id = $(this).val();
    $("#id_up").val(id);

    $.post( "<?php echo base_url()?>Con_menu_list/get_dataset", { get_dataset: "data", id : id}, function( data ) {
      $.each(data.result, function (index, data) {
        $("#name_up").val(data.Caption);    
        $("#url_up").val(data.Name); 
        $("#pid_up").val(data.p_id);
        $("#oid_up").val(data.subcat_id);
        get_cat_list_up(data.p_id, data.Cat_id);
      });

    }, "json");   
  });

  /********************************* PRORJECT CHANGE ******************************/
  $('#pid').change(function () {
    var id = $(this).val();
    $.post('<?php echo base_url()?>Con_menu_list/get_category', {'get_category': 'data', id: id}, function (data) {
      //console.log(data.records);
      if (data.records === undefined || data.records.length === 0 || data.records === null) {
        rowData = '<option>No Category Found for this Project</option>';
        $('#cat_id').html('').append(rowData);
      } else {
        var rowData = '';
        rowData += '<option>Select Category</option>';
        $.each(data.records, function (index, data) {
          rowData += "<option value='"+data.ID+"'>"+data.cat+"</option>";
        });
        $('#cat_id').html('').append(rowData);
      }

    }, "json");
  });

  $('#cat_id').change(function () {
    $("#category_name").val($("#cat_id option:selected").text());
    var pid = $('#pid').val();
    var cat = $(this).val();
    var catID = $("#cat_id option[value='"+cat+"']").text();
    
  });

  $('#pid_up').change(function () {
    var id = $(this).val();
    get_cat_list_up(id,'');
  });

  function get_cat_list_up(id, cat) {
    $.post('<?php echo base_url()?>Con_menu_list/get_category', {'get_category': 'data', id: id}, function (data) {
      if (data.records === undefined || data.records.length === 0 || data.records === null) {
        rowData = '<option>No Category Found for this Project</option>';
        $('#cat_id_up').html('').append(rowData);
      } else {
        var rowData = '';
        rowData += '<option>Select Category</option>';
        $.each(data.records, function (index, data) {
          rowData += "<option value='"+data.ID+"'>"+data.cat+"</option>";
        });
        $('#cat_id_up').html('').append(rowData);
        if (cat.records === undefined || cat.length === 0 || cat === null) {
          $("#cat_id_up").val(cat);
        }
      }

    }, "json");
  }

  </script> 