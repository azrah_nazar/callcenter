<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
  $date1 = date("Y-m-d");
  $date2 = date("Y-m-d");
} else {
  $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
  $date1 = substr($daterange, 0, 10);
  $date2 = substr($daterange, 17, 23);
}
?>

<style type="text/css">
  .error {
    color: red;
    size: 80%
  }

  .hidden {
    display: none;
  }

  .toggle.btn-xs {
    min-width: 70px;
    min-height: 22px;
  }
</style>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1>
      <?php
      if ($acc_no == '4-8-9-1010') {
        $txt = "Form";
      } else if ($acc_no == '4-8-9-7978') {
        $txt = "WhatsApp";
      } else if ($acc_no == '4-8-9-7993') {
        $txt = "Facebook";
      } else if ($acc_no == '4-8-9-7979') {
        $txt = "Form";
      } else {
        $txt = "Contact";
      }
      ?>
      <i class="fa fa-id-card"></i> <?php echo $txt; ?> Upload
      <?php if ($desig != '13') { ?>
        <small class="pull-right">
          <a class="btn btn-success pull-right btn-sm btn_allocate"><i class="fa fa-group"></i> Pass to Allocation</a>
        </small>
      <?php } ?>
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header with-border">
            <input type="hidden" class="form-control" name="backdate_days" id="backdate_days" value="<?php echo $backdate_days; ?>">
            <?php if ($gender == 'Male') {
              if ($mapped_operator == '') { ?>
                <div class="form-group">
                  <label for="operator">Select Operator<font color="#FF0000"><strong>*</strong></font></label>
                  <select id="operator" name="operator" class="form-control" required>
                    <option value="">Select</option>
                    <?php foreach ($map_operator as $operator) { ?>
                      <option value="<?php echo $operator->Acc_No ?>"><?php echo $operator->Name ?></option>
                    <?php } ?>
                  </select>
                </div>
                <button class="btn-xs btn-warning pull-right" id="btn_map">Map Operator</button>
                <br>
                <hr>
              <?php } else { ?>
                <label>Mapped Operator: <font color="#FF0000"><strong><?php echo $mapped_operator_name; ?></strong></font></label>
                <br>
                <button class="btn-xs btn-success" id="btn_reject">Retrieve Workshop Reject </button> &nbsp;&nbsp;<br><br>
                <button class="btn-xs btn-danger" id="btn_workshop">Retrieve Workshop SMS </button>&nbsp;&nbsp;<br><br>
                <button class="btn-xs btn-warning" id="btn_reapplied_reject">Retrieve Reapplied Reject </button>&nbsp;&nbsp;<br>
              <?php } ?>
              <br>
              <hr>
            <?php }
            ?>

            <h3 class="box-title">Add Contacts</h3>
          </div>
          <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
          <form class="form-sample" id="form_con_no" action="javascript:void(0);">
            <div class="box-body">

              <div class="form-group">
                <label>Contact Number:<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" autofocus class="form-control txt_nav" name="con_no" id="con_no" style="width: 100%;" required>
                <div id="con_no_error" class="error hidden">Please enter a valid phone number</div>
              </div>

              <div class="form-group">
                <label>Name:</label>
                <input type="text" class="form-control txt_nav" name="con_name">
              </div>

              <div class="form-group">
                <label>Address:</label>
                <input type="text" class="form-control txt_nav" name="con_adrz">
              </div>

              <div class="form-group">
                <label>Whatsapp Number:</label>
                <input type="text" class="form-control txt_nav" name="con_whatsapp">
              </div>

              <div class="form-group">
                <label>Date of Birth:</label>
                <input type="text" class="form-control txt_nav date" name="con_dob">
              </div>

              <div class="form-group">
                <label>Job/Business:</label>
                <input type="text" class="form-control txt_nav" name="con_job">
              </div>

              <?php if ($desig != '13') { ?>
                <div class="form-group">
                  <label for="upload_type">Upload Type<font color="#FF0000"><strong>*</strong></font></label>
                  <select id="upload_type" name="upload_type" class="form-control" required>
                    <option value="">Select</option>
                    <option value="whatsapp">WhatsApp</option>
                    <option value="facebook">Facebook</option>
                    <option value="leads">Leads / Google Contacts</option>
                  </select>
                </div>
              <?php } ?>

              <button type="submit" class="btn btn-info pull-left" id="btn_add">Add Number</button>

            </div>

          </form>
          <hr>
          <?php if ($desig != '13') { ?>
            <form class="form-sample" id="form_csv_submit" action="javascript:void(0);">
              <div class="box-body">

                <div class="form-group">
                  <label for="upload_type">Upload Type<font color="#FF0000"><strong>*</strong></font></label>
                  <select id="upload_type" name="upload_type" class="form-control" required>
                    <option value="">Select</option>
                    <option value="whatsapp">WhatsApp</option>
                    <option value="facebook">Facebook</option>
                    <option value="lead">Leads/ Google Contacts</option>
                  </select>
                </div>

                <div class="form-group">
                  <label>Select CSV file<font color="#FF0000"><strong>*</strong></font></label>
                  <input class="filestyle form-control" type="file" id="con_csv" name="con_csv" size='100' height="100px">
                </div>


              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-left" id="btn_upload" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Uploading">Upload</button>
              </div>
            </form>
          <?php } ?>

        </div>
      </div>
      <div class="col-md-8">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12 col-sm-6">
                <div class="row">
                  <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                  <form role="form" action="javascript:void(0);">

                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onchange="formHandler($(this).val())">
                        </div>
                      </div>
                    </div>

                    <!-- <div class="col-sm-12">
                      <div class="form-group">
                        <button name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle btn-search"><i class="fa fa-search"></i> Search</button>
                      </div>
                    </div> -->
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="box box-primary">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12 col-sm-6">
                <canvas id="myChart" style="width:100%;max-width:800px"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="<?php echo base_url(); ?>js/file-upload.js"></script>
    <!-- date-range-picker -->
    <script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

    <script>
      $(function() {
        $(':input[required=""],:input[required]').bind('focusout', function() {
          if ($(this).val() == "") {
            $(this).css("border-color", "red");
          } else {
            $(this).css("border-color", "#ccc");
          }
        });

        $('.txt_nav:first').focus();
        var $inp = $('.txt_nav');
        $inp.bind('keydown', function(e) {
          var n = $inp.length;
          var key = e.which;
          if (key == 13) {
            e.preventDefault();
            var nxtIdx = $inp.index(this) + 1;
            if (nxtIdx < n) {
              $('.txt_nav')[nxtIdx].focus();
            } else {
              $('.txt_nav')[nxtIdx - 1].blur();
              $('#btn_add').focus();
            }
          }
        });

        $("#btn_add").keypress(function(event) {
          if (event.which == 13) {
            req_input_check();
          }
        });

      });

      function req_input_check() {
        $(':input[required=""],:input[required]').bind('keypress', function(event) {
          if (event.which == 13) {
            if ($(this).val() == "") {
              $(this).css("border-color", "red");
            } else {
              $(this).css("border-color", "#ccc");
              $('#form_submit').submit();
            }
          }
        });
      }

      /**************************** DATE ***********************/
      $('#date_range').daterangepicker({
        autoclose: false,
        todayBtn: true,
        pickerPosition: "bottom-left"
      });

      function formHandler(date) {
        var menu = $('#menu').val();
        window.location.href = "<?php echo base_url() ?>Con_Contact_CSV/index/" + menu + "/" + date;
      }

      /**************************** BAR CHART ***********************/
      var cData = JSON.parse('<?php echo $chart_data; ?>');

      var barColors = ["#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12", "#f39c12"];

      new Chart("myChart", {
        type: "horizontalBar",
        data: {
          labels: cData.label,
          datasets: [{
            backgroundColor: barColors,
            data: cData.data
          }]
        },
        options: {
          legend: {
            display: false
          },
          title: {
            display: true,
            text: "Call Allocation Summary"
          },
          scales: {
            xAxes: [{
              ticks: {
                min: 0
              }
            }]
          }
        }
      });
    </script>

    <script>
      function validatePhoneNumber(input_str) {
        var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

        return re.test(input_str);
      }

      var date_format = 'yyyy-mm-dd';
      $(document).ready(function() {
        $(".date").datepicker({
          format: date_format,
          autoclose: true,
          endDate: '+0d',
          todayHighlight: true
        });
      });

      //////////////////////////////con_no ADD /////////////////////////////////
      $("#form_con_no").on('submit', (function(e) {
          var menu = $("#menu").val();
          var con_no = document.getElementById('con_no').value;

          if (!validatePhoneNumber(con_no)) {
            document.getElementById('con_no_error').classList.remove('hidden');

          } else {
            document.getElementById('con_no_error').classList.add('hidden');


            e.preventDefault();
            $.ajax({
              url: "<?php echo base_url() ?>Con_Contact_CSV/add",
              type: "POST",
              data: new FormData(this),
              contentType: false,
              cache: false,
              processData: false,
              beforeSend: function() {
                // alert("before");
              },
              success: function(data) {
                console.log(data);
                if ($.trim(data) === '') {
                  swal("Oops...", "Something went wrong!", "warning");

                } else {
                  if ($.trim(data) === 'error') {
                    swal("SQL Error!", "Please Try Again!", "warning");

                  } else if ($.trim(data) === 'updated') {
                    $('#pid').focus();
                    swal({
                      title: "",
                      text: "Successfully Updated!",
                      type: "success",
                      timer: 2000,
                      showConfirmButton: false,
                    });
                    setTimeout(function() {
                      window.location.href = "<?php echo base_url() ?>Con_Contact_CSV/index/" + menu + "/";
                    }, 2000);

                  } else if ($.trim(data) === 'success') {
                    swal({
                      title: "",
                      text: "Successfully Added!",
                      type: "success",
                      timer: 2000,
                      showConfirmButton: false,
                    });
                    setTimeout(function() {
                      window.location.href = "<?php echo base_url() ?>Con_Contact_CSV/index/" + menu;
                    }, 2000);

                  } else if ($.trim(data) === 'rejected')  {
                    swal("Registration Blocked!", "Contact No Rejected! ", "warning");
                  } else {
                    swal("", "Contact No / WhatsApp No registered under " + $.trim(data), "warning");
                  }
                }
              },
              error: function(e) {
                swal({
                  title: "Error!",
                  text: "Try Again",
                  type: "warning",
                  timer: 2000,
                  showConfirmButton: false,
                });
                setTimeout(function() {
                  //window.location.href = "<?php echo base_url() ?>Con_Contact_CSV/index/"+menu;
                }, 2000);
              }
            });
          }
        })

      );

      ///////////////////////////////// CSV upload //////////////////////////////////
      $("#form_csv_submit").on('submit', (function(e) {
        var menu = $("#menu").val();
        var btn = $('#btn_upload');
        btn.button('loading');

        e.preventDefault();
        $.ajax({
          url: "<?php echo base_url() ?>Con_Contact_CSV/readExcel",
          type: "POST",
          data: new FormData(this),
          contentType: false,
          cache: false,
          dataType: 'json',
          processData: false,
          beforeSend: function()

          {},

          success: function(data) {
            btn.button('reset');
            if (data.status === 'success') {
              swal({
                title: "",
                text: "Successfully Added!",
                type: "success",
                timer: 2000,
                showConfirmButton: false,
              });
              window.location.href = "<?php echo base_url() ?>Con_Contact_CSV/inserted_contacts/" + menu;

            } else if (data.status === 'error') {
              swal("Error!", "Something went wrong! Please Try Again!", "warning");
            } else if (data.status === 'file exists') {
              swal("CSV Exists!", "CSV File name already exists!", "warning");
            } else if (data.status === 'error2') {
              swal("", "Error While uploading file on the server. Please Try Again!", "warning");

            } else if (data.status === 'invalid') {
              swal("", "Invaild File Type! File must have .csv extension", "warning");

            } else {
              swal("", "Something went wrong! Please Try Again!", "warning");
            }
          },

          error: function(e) {
            btn.button('reset');
            swal({
              title: "Error!",
              text: "Try Again",
              type: "warning",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              //window.location.href = "<?php //echo base_url() 
                                        ?>Con_Contact_CSV/index/" + menu;
            }, 2000);
          }
        });
      }));


      //**************************con_no**********************************************
      var con_no = document.getElementById("con_no").value;

      $("#con_no").autocomplete({
        autoFocus: true,
        source: '<?php echo site_url("Con_Contact_CSV/search_con_no") ?>',
        minLength: 8,
        select: function(event, ui) {},
        open: function(event, ui) {
          $(".ui-autocomplete").css("z-index", 1000000)
        }
      });

      $('#con_no').keydown(function(e) {
        var key = e.which;
        if (key == 13) {
          var con_no = $(this).val();
          e.preventDefault();
          load_item_tbl(con_no);
        }
      });

      function load_item_tbl(con_no) {
        $.post("<?php echo base_url() ?>Con_Contact_CSV/get_dataset", {
          get_dataset: "data",
          con_no: con_no
        }, function(data) {
          var Data = "";
          if (data === undefined || data.length === 0 || data === null) {
            Data = '<div> <h4>No details available!!</h4> </div>';
            $('#user_tbl').html('').append(Data);
          } else {

            $.each(data.result, function(index, data) {

              Data += '<tr>';
              Data += '<td>' + data.id + '</td>';
              Data += '<td>' + data.con_no + '</td>';
              Data += '</tr>';

            });

            $('#user_tbl').html('').append(Data);
          }

        }, "json");
      }

      /**************************** LOAD FORM DATA ***********************/
      $(".btn_allocate").click(function() {

        $.post("<?php echo base_url() ?>Con_Contact_CSV/allocate", {
          allocate: "data"
        }, function(data) {
          if (data.status === 'success') {
            swal({
              title: "",
              text: "Successfully Added!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            location.reload(true);

          } else if (data.status === 'error') {
            swal("Error!", "Something went wrong! Please Try Again!", "warning");
          } else if (data.status === 'error2') {
            swal("", "Error While uploading file on the server. Please Try Again!", "warning");

          } else if (data.status === 'invalid') {
            swal("", "Invaild File Type! File must have .csv extension", "warning");

          } else {
            swal("", "Something went wrong! Please Try Again!", "warning");
          }

        }, "json");
      });

      /**************************** MAP OPERTAOR ***********************/
      $("#btn_map").click(function() {
        var operator = $("#operator option:selected").val();

        $.post("<?php echo base_url() ?>Con_Contact_CSV/map_operator", {
          map_operator: "data",
          operator: operator
        }, function(data) {
          if (data.status === 'success') {
            swal({
              title: "",
              text: "Successfully Mapped Operator!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            location.reload(true);

          } else if (data.status === 'error') {
            swal("Error!", "Something went wrong! Please Try Again!", "warning");

          } else {
            swal("", "Something went wrong! Please Try Again!", "warning");
          }

        }, "json");
      });

      /**************************** WORKSHOP REJECT LIST ***********************/
      $("#btn_reject").click(function() {
        var mapped_operator = '<?php echo $mapped_operator; ?>';
        var backdate_days = '<?php echo $backdate_days; ?>';
        var menu = $('#menu').val();

        $.post("<?php echo base_url() ?>Con_Contact_CSV/transfer_operator", {
          transfer_operator: "data", mapped_operator:mapped_operator, backdate_days:backdate_days
        }, function(data) {
          if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Retrieved Customers!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            window.location.href = "<?php echo base_url() ?>Con_workshop/male_ws_reject/" + menu;

          } else if ($.trim(data) === 'error') {
            swal("Error!", "Something went wrong! Please Try Again!", "warning");

          } else {
            swal("", "Something went wrong! Please Try Again!", "warning");
          }

        });
      });

      /**************************** WORKSHOP SMS LIST ***********************/
      $("#btn_workshop").click(function() {
        var mapped_operator = '<?php echo $mapped_operator; ?>';
        var backdate_days = '<?php echo $backdate_days; ?>';
        var menu = $('#menu').val();

        $.post("<?php echo base_url() ?>Con_Contact_CSV/transfer_operator_ws", {
          transfer_operator: "data", mapped_operator:mapped_operator, backdate_days:backdate_days
        }, function(data) {
          if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Retrieved Customers!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            window.location.href = "<?php echo base_url() ?>Con_workshop/male_ws_sms/" + menu;

          } else if ($.trim(data) === 'error') {
            swal("Error!", "Something went wrong! Please Try Again!", "warning");

          } else {
            swal('', "Something went wrong! Please Try Again!", "warning");
          }

        });
      });
      
      /**************************** REAPPLIED REJECT LIST ***********************/
      $("#btn_reapplied_reject").click(function() {
        var mapped_operator = '<?php echo $mapped_operator; ?>';
        var backdate_days = '<?php echo $backdate_days; ?>';
        var menu = $('#menu').val();

        $.post("<?php echo base_url() ?>Con_Contact_CSV/reapplied_reject_retrieve", {
          reapplied_reject_retrieve: "data", mapped_operator:mapped_operator, backdate_days:backdate_days
        }, function(data) {
          if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Retrieved Customers!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            window.location.href = "<?php echo base_url() ?>Con_workshop/male_reapplied_reject/" + menu;

          } else if ($.trim(data) === 'error') {
            swal("Error!", "Something went wrong! Please Try Again!", "warning");

          } else {
            swal("", "Something went wrong! Please Try Again!", "warning");
          }

        });
      });
    </script>