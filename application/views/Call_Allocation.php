<?php
$menu = $this->uri->segment(3);
$num = $this->uri->segment(4);
?>

<style>
  .error {
    color: red;
    size: 80%
  }

  .hidden {
    display: none;
  }

  .toggle.btn-xs {
    min-width: 70px;
    min-height: 22px;
  }
</style>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1>
      <i class="fa fa-id-card"></i> Contact Allocation

      <?php if ($desig == '12') { ?>
        <small class="pull-right">
          <a class="btn btn-warning pull-right btn-sm" data-toggle="modal" data-target="#pause_agent" title="Pause Agent"><i class="fa fa-pause"></i> Pause Agent</a>
        </small>
      <?php } ?>
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">

      <div class="col-md-12">
        <div class="box box-primary text-center">
          <br><br>
          <button class="btn btn-info" id="btn_process">Process</button>
          <br><br>
          <br>
        </div>
      </div>

      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header ptbnull">
            <h3 class="box-title titlefix">Unallocated List</h3>
          </div>
          <div class="box-body ">
            <div class="table-responsive mailbox-messages">
              <div class="download_label">Unallocated List</div>
              <table class="table table-striped table-bordered table-hover example">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Contact Number</th>
                    <th>Address</th>
                    <th>Whatsapp Number</th>
                    <th>Job/Business</th>
                    <th>Date of Birth</th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                  $count = 0;

                  foreach ($load_data as $value) {
                    $count++;
                    $birthday = $value['dob'];
                    if ($birthday == '0000-00-00') {
                      $dob = '';
                    } else {
                      $dob = $birthday;
                    }
                  ?>
                    <tr>
                      <td> <?php echo $count; ?>.</td>
                      <td> <?php echo $value['name']; ?></td>
                      <td> <?php echo $value['con_no']; ?></td>
                      <td> <?php echo $value['address']; ?></td>
                      <td> <?php echo $value['whatsapp']; ?></td>
                      <td> <?php echo $value['job']; ?></td>
                      <td> <?php echo $dob; ?></td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<div class="modal fade" id="pause_agent" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title"> Pause Call Operators</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_update">
          <input type="hidden" name="id_up" id="id_up">

          <div class="form-horizontal">
            <div class="box-body">
              <div class="table-responsive mailbox-messages">
                <div class="download_label">Operator List</div>
                <table class="table table-striped table-bordered table-hover example">
                  <thead>
                    <tr>
                      <th></th>
                      <th> Call Operator</th>
                      <th class="text-right">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                    $count = 0;

                    foreach ($load_operator as $value) {
                      $count++;
                    ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $value->Name; ?></td>
                        <td class="mailbox-date pull-right">
                          <input type="checkbox" data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-size="mini" data-on="Active" data-off="Paused" id="is_present" name="is_present" class="is_present" data-id="<?php echo $value->ID; ?>" <?php if ($value->is_present == '1') { ?> checked <?php } ?>>
                        </td>
                      </tr>
                    <?php
                    }

                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<script>
  $("#btn_process").click(function() {
    var btn = $('#btn_process');
    btn.button('loading');

    $.post("<?php echo base_url() ?>Con_Contact_Allocation/process", {
        next: "data"
      },
      function(data) {
        if ($.trim(data.status) === 'success') {
          swal({
            title: "",
            text: "Successfully Added!",
            type: "success",
            timer: 2000,
            showConfirmButton: false,
          });
          location.reload(true);
        } else if ($.trim(data.status) === 'empty') {
          swal({
            title: "",
            text: "No contacts to process!",
            type: "success",
            timer: 2000,
            showConfirmButton: false,
          });
          location.reload(true);

        } else {
          swal({
            title: "",
            text: "Error",
            type: "error",
            timer: 2000,
            showConfirmButton: false,
          });
          location.reload(true);

        }
      }, "json");
  });

  $('.is_present').change(function() {
    id = $(this).attr("data-id");
    chk = $(this).prop('checked');

    if ($(this).prop('checked')) {
      is_present = '1';
    } else {
      is_present = '0';
    }

    $.post("<?php echo base_url() ?>Con_Contact_CSV/pause_operator", {
      pause_operator: "data",
      id: id,
      is_present: is_present
    }, function(data) {

    });

  })
</script>