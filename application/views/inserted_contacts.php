<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
  $date1 = date("Y-m-d");
  $date2 = date("Y-m-d");
} else {
  $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
  $date1 = substr($daterange, 0, 10);
  $date2 = substr($daterange, 17, 23);
}
?>

<style type="text/css">
  .error {
    color: red;
    size: 80%
  }

  .hidden {
    display: none;
  }
</style>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1>
      <i class="fa fa-id-card"></i> CSV Existing Contacts
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

          </div>
          <div class="box-body">
            <div class="col-md-6">
              <form role="form" action="<?php echo site_url('Con_Contact_CSV/inserted_contacts/') ?><?php echo $menu . "/" . $date1 . ' - ' . $date2; ?>" method="post" class="">
                <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">

                <div class="col-md-12">
                  <div class="form-group">
                    <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>CSV Name</label>
                <select id="csv" name="csv" class="form-control">
                  <option></option>
                  <?php foreach ($get_csv as $csv) {
                  ?>
                    <option><?php echo $csv->csv_name; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <button class="btn btn-danger pull-right btn-sm btn_delete"><i class="fa fa-trash"></i> Delete</button>
              </div>
            </div>
          </div>
        </div>
        <?php if (isset($load_data)) { ?>
          <div class="box box-primary">
            <div class="box-header ptbnull">
              <h3 class="box-title titlefix">CSV Existing Contacts Report</h3>
            </div>
            <div class="box-body ">
              <div class="table-responsive mailbox-messages">
                <div class="download_label">CSV Existing Contacts List</div>
                <table class="table table-striped table-bordered table-hover example">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Contact Number</th>
                      <th>Whatsapp Number</th>
                      <th>Uploaded Date</th>
                      <th>CSV Name</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                    $count = 1;

                    foreach ($load_data as $value) {
                      $birthday = $value->dob;
                      if ($birthday == '0000-00-00') {
                        $dob = '';
                      } else {
                        $dob = $birthday;
                      }
                    ?>
                      <tr>
                        <td class="mailbox-name"> <?php echo $count; ?>.</td>
                        <td class="mailbox-name"> <?php echo $value->name; ?></td>
                        <td class="mailbox-name"> <?php echo $value->con_no; ?></td>
                        <td class="mailbox-name"> <?php echo $value->whatsapp; ?></td>
                        <td class="mailbox-name"> <?php echo $value->date; ?></td>
                        <td class="mailbox-name"> <?php echo $value->csv_name; ?></td>
                      </tr>
                    <?php
                      $count++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>

    </div>


    <!-- date-range-picker -->
    <script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

    <script>
      /**************************** DATE ***********************/
      $('#date_range').daterangepicker({
        autoclose: false,
        todayBtn: true,
        pickerPosition: "bottom-left"
      });

      function formHandler(date) {
        var menu = $("#menu").val();
        window.location.href = "<?php echo base_url() ?>Con_Contact_CSV/inserted_contacts/" + menu + "/" + date;
      }


      /******************************** Remove ***************************/

      $(".btn_delete").click(function() {
        var csv_name = $("#csv option:selected").text();
        var menu = $("#menu").val();
        if (csv_name == '') {
          swal("", "Please select CSV to delete!", "warning");
        } else {
          swal({
              title: "Are you sure?",
              text: "",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes, delete it!",
              cancelButtonText: "No, cancel!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm) {
              if (isConfirm) {
                $.post("<?php echo base_url() ?>Con_Contact_CSV/delete_csv", {
                  remove_data: "data",
                  csv_name: csv_name
                }, function(data) {
                  if ($.trim(data.status) === 'success') {
                    swal({
                        title: "Deleted!",
                        text: "Successfully Deleted!",
                        type: "success",
                        confirmButtonText: "OK"
                      },
                      function(isConfirm) {
                        if (isConfirm) {
                          location.reload();
                        }
                      });

                  } else if ($.trim(data.status) === ' error') {
                    swal("", "Error!", "warning");

                  } else {
                    swal("Oops...", "Something went wrong!", "warning");
                  }
                }, "json");
              } else {
                swal("Cancelled", "", "error");
              }
            });
        }


      });
    </script>