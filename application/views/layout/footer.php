<script src="<?php echo base_url(); ?>backend/dist/js/moment.min.js"></script>
<footer class="main-footer">
</footer>
<div class="control-sidebar-bg"></div>
</div>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<link href="<?php echo base_url(); ?>backend/toast-alert/toastr.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>backend/toast-alert/toastr.js"></script>
<script src="<?php echo base_url(); ?>backend/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/select2/select2.min.css">
<script src="<?php echo base_url(); ?>backend/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url(); ?>backend/dist/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="<?php echo base_url(); ?>backend/plugins/iCheck/icheck.min.js"></script>

<script src="<?php echo base_url(); ?>backend/toggle/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">
  //iCheck for checkbox and radio inputs
  $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
  });
  //Red color scheme for iCheck
  $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
    checkboxClass: 'icheckbox_minimal-red',
    radioClass: 'iradio_minimal-red'
  });
  //Flat red color scheme for iCheck
  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });

  $(document).ready(function() {
    $(".studentsidebar").mCustomScrollbar({
      theme: "minimal"
    });

    $('.studentsideclose, .overlay').on('click', function() {
      $('.studentsidebar').removeClass('active');
      $('.overlay').fadeOut();
    });

    $('#sidebarCollapse').on('click', function() {
      $('.studentsidebar').addClass('active');
      $('.overlay').fadeIn();
      $('.collapse.in').toggleClass('in');
      $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });


  });
</script>


<script src="<?php echo base_url(); ?>backend/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/chartjs/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/fastclick/fastclick.min.js"></script>
<!-- <script type="text/javascript" src="<?php //echo base_url();   
                                          ?>backend/dist/js/bootstrap-filestyle.min.js"></script> -->
<script src="<?php echo base_url(); ?>backend/dist/js/app.min.js"></script>

<!--nprogress-->
<script src="<?php echo base_url(); ?>backend/dist/js/nprogress.js"></script>
<!--file dropify-->
<script src="<?php echo base_url(); ?>backend/dist/js/dropify.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/datatables/js/ss.custom.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>backend/calender/zabuto_calendar.min.js"></script>
<script src="<?php echo base_url() ?>backend/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>backend/sweet-alert/sweetalert2.min.js"></script>

<script src="<?php echo base_url(); ?>backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>



</body>

</html>
<!-- jQuery 3 -->


<script type="text/javascript">
  $(document).ready(function() {

    <?php
    if ($this->session->flashdata('success_msg')) {
    ?>
      successMsg("<?php echo $this->session->flashdata('success_msg'); ?>");
    <?php
    } else if ($this->session->flashdata('error_msg')) {
    ?>
      errorMsg("<?php echo $this->session->flashdata('error_msg'); ?>");
    <?php
    } else if ($this->session->flashdata('warning_msg')) {
    ?>
      infoMsg("<?php echo $this->session->flashdata('warning_msg'); ?>");
    <?php
    } else if ($this->session->flashdata('info_msg')) {
    ?>
      warningMsg("<?php echo $this->session->flashdata('info_msg'); ?>");
    <?php
    }
    ?>
  });
</script>



<!-- Button trigger modal -->
<!-- Modal -->
<div class="row">
  <div class="modal fade" id="sessionModal" tabindex="-1" role="dialog" aria-labelledby="sessionModalLabel">
    <form action="<?php echo site_url('admin/admin/activeSession') ?>" id="form_modal_session" class="form-horizontal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="sessionModalLabel"><?php echo $this->lang->line('session'); ?></h4>
          </div>
          <div class="modal-body sessionmodal_body pb0">

          </div>
          <div class="modal-footer">

            <button type="button" class="btn btn-primary submit_session" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Please wait.."><?php echo $this->lang->line('save'); ?></button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>



<script type="text/javascript">
  $calendar = $('#calendar');
  var base_url = '<?php echo base_url() ?>';
  today = new Date();
  y = today.getFullYear();
  m = today.getMonth();
  d = today.getDate();
  var viewtitle = 'month';
  var pagetitle = "<?php
                    if (isset($title)) {
                      echo $title;
                    }
                    ?>";

  if (pagetitle == "Dashboard") {

    viewtitle = 'agendaWeek';
  }

  $calendar.fullCalendar({
    viewRender: function(view, element) {
      // We make sure that we activate the perfect scrollbar when the view isn't on Month
      //if (view.name != 'month'){
      //  $(element).find('.fc-scroller').perfectScrollbar();
      //}
    },

    header: {
      center: 'title',
      right: 'month,agendaWeek,agendaDay',
      left: 'prev,next,today'
    },
    defaultDate: today,
    defaultView: viewtitle,
    selectable: true,
    selectHelper: true,
    views: {
      month: { // name of view
        titleFormat: 'MMMM YYYY'
        // other view-specific options here
      },
      week: {
        titleFormat: " MMMM D YYYY"
      },
      day: {
        titleFormat: 'D MMM, YYYY'
      }
    },
    timezone: "Asia/Colombo",
    draggable: false,

    editable: false,
    eventLimit: false, // allow "more" link when too many events


    // color classes: [ event-blue | event-azure | event-green | event-orange | event-red ]
    events: {
      url: base_url + 'Con_calendar/getevents'

    },

    eventRender: function(event, element) {
      element.attr('title', event.title);
      element.attr('onclick', event.onclick);
      element.attr('data-toggle', 'tooltip');
      if ((!event.url) && (event.event_type != 'task')) {
        element.attr('title', event.title + '-' + event.description);
        element.click(function() {
          view_event(event.id);
        });
      }
    },
    dayClick: function(date, jsEvent, view) {
      var d = date.format();
      if (!$.fullCalendar.moment(d).hasTime()) {
        d += ' 05:30';
      }
      //var vformat = (app_time_format == 24 ? app_date_format + ' H:i' : app_date_format + ' g:i A');
      <?php //if ($this->rbac->hasPrivilege('calendar_to_do_list', 'can_add')) { 
      ?>


      $("#input-field").val('');
      $("#desc-field").text('');
      $("#date-field").daterangepicker({
        startDate: date,
        endDate: date,
        timePicker: true,
        timePickerIncrement: 5,
        locale: {
          format: 'MM/DD/YYYY hh:mm a'
        }
      });
      $('#newEventModal').modal('show');

      <?php //} 
      ?>
      return false;
    }

  });

  $(document).ready(function() {
    $("#date-field").daterangepicker({
      timePicker: true,
      timePickerIncrement: 5,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    });


  });

  function datepic() {
    $("#date-field").daterangepicker();
  }

  function view_event(id) {
    //$("#28B8DA").removeClass('cpicker-small').addClass('cpicker-big');
    $('.selectevent').find('.cpicker-big').removeClass('cpicker-big').addClass('cpicker-small');
    var base_url = '<?php echo base_url() ?>';
    if (typeof(id) == 'undefined') {
      return;
    }
    $.ajax({
      url: base_url + 'Con_calendar/view_event/' + id,
      type: 'POST',
      //data: '',
      dataType: "json",
      success: function(msg) {


        $("#event_title").val(msg.event_title);
        $("#event_desc").text(msg.event_description);
        $('#eventdates').val(msg.start_date + '-' + msg.end_date);
        $('#eventid').val(id);
        if (msg.event_type == 'public') {

          $('input:radio[name=eventtype]')[0].checked = true;

        } else if (msg.event_type == 'private') {
          $('input:radio[name=eventtype]')[1].checked = true;

        } else if (msg.event_type == 'sameforall') {
          $('input:radio[name=eventtype]')[2].checked = true;

        } else if (msg.event_type == 'protected') {
          $('input:radio[name=eventtype]')[3].checked = true;

        }
        // $("#red#28B8DA").removeClass('cpicker-big').addClass('cpicker-small');

        //$(this).removeClass('cpicker-small', 'fast').addClass('cpicker-big', 'fast');
        $("#eventdates").daterangepicker({
          startDate: msg.startdate,
          endDate: msg.enddate,
          timePicker: true,
          timePickerIncrement: 5,
          locale: {
            format: 'MM/DD/YYYY hh:mm A'
          }
        });
        $("#event_color").val(msg.event_color);
        $("#delete_event").attr("onclick", "deleteevent(" + id + ",'Event')")

        // $("#28B8DA").removeClass('cpicker-big').addClass('cpicker-small');
        $("#" + msg.colorid).removeClass('cpicker-small').addClass('cpicker-big');
        $('#viewEventModal').modal('show');
      }
    });


  }




  function chk_validate() {

  }
</script>
<script>
  /*********************************  TICKET ******************************/
  $("#form_sendzz").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/send_reply",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_datazz').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal("Sent!", "Successfully Sent Message!", "success");
            location.reload();
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  $('.btn_historyzz').click(function() {
    var con_id = $(this).attr('data-value');
    $("#id_upzz").val(con_id);
    var tic_id = $(this).attr('data-tic');
    $("#tic_idzz").val(tic_id);

    $.post('<?php echo base_url() ?>Con_Call_Report/get_history', {
      'get_history': 'data',
      con_id: con_id
    }, function(data) {

      var Data = "";
      var reply_id = data.reply_id;


      if (data === undefined || data.length === 0 || data === null) {

        Data = '<div> <h4>This chat has no records!!</h4> </div>';
        $('#chat_listzz').html('').append(Data);

      } else {

        $.each(data.result, function(index, data) {
          var msg = "";
          var val = data.Val;
          var time = data.dt_time;
          var contact_no = data.con_no;
          var std_name = data.st_name;
          if (std_name == '') {
            var st_name = '';
          } else {
            var st_name = "Name: " + data.st_name + " | ";
          }
          var whatsapp = data.whatsapp;
          if (whatsapp == '') {
            var whatsapp = '';
          } else {
            var whatsapp = "Whatsapp: " + data.whatsapp;
          }
          $('.chat_contact_no').text(st_name + "\nContact No : " + contact_no + "\n | " +whatsapp);

          var user_type = '';
          var class_type = '';
          var user_name = '';

          if ((val == '12') || (val == '15')) {
            user_type = 'left';
            class_type = 'left';
            user_name = data.Name;
          } else {
            user_type = 'right';
            class_type = 'right';
            user_name = data.Name;
          }

          Data += '<div class="direct-chat-msg ' + user_type + '">';
          Data += '<div class="direct-chat-info clearfix">';
          Data += '<span class="direct-chat-name pull-' + class_type + '">' + user_name + ' &nbsp;</span>';
          Data += '<span class="direct-chat-timestamp pull-' + user_type + '">' + time + '&nbsp;</span>';
          Data += '</div>';
          Data += '<img class="direct-chat-img" src="<?php echo base_url(); ?>uploads/student_images/no_image.png" alt="message user image">';
          Data += '<div class="direct-chat-text">';
          Data += data.message;
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
        });

        $('#chat_listzz').html('').append(Data);
      }
    }, "json");
  });

  /******************************** CLOSE CHAT ***************************/
  $(".btn_msg_close").click(function() {
    var tic_id = $("#tic_idzz").val();
    var id_up = $("#id_upzz").val();
    var menu = $("#menu").val();

    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, close it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_Call_Report/close_chat", {
            close_chat: "data",
            tic_id: tic_id,
            id_up: id_up
          }, function(data) {
            if ($.trim(data.status) === 'success') {
              swal("Closed", "Successfully Closed Chat!", "success");
              window.location.reload();

            } else if ($.trim(data.status) === ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          }, "json");
        } else {
          swal("Cancelled", "", "error");
        }
      });

  });

  var intervalId = window.setInterval(function() {
    $.post("<?php echo base_url() ?>Con_workshop/regular_noti", {
      regular_noti: "data"
    }, function(data) {
      if (data.result === undefined || data.length === 0 || data.result === null || data.result == '') {
      } else {
        successMsg('Reminder Alert!!');
      }
    }, "json");
  }, 300000); 
</script>