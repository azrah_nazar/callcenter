<?php
date_default_timezone_set('Asia/Colombo');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CALL CENTER</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta name="theme-color" content="#424242" />
    <link href="<?php echo base_url(); ?>backend/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/style-main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/jquery.mCustomScrollbar.min.css">
    <?php
    $this->load->view('layout/theme');
    ?>

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/ionicons.min.css">

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/iCheck/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/morris/morris.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/colorpicker/bootstrap-colorpicker.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/custom_style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/datepicker/css/bootstrap-datetimepicker.css">
    <!--file dropify-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/dropify.min.css">
    <!--file nprogress-->
    <link href="<?php echo base_url(); ?>backend/dist/css/nprogress.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>backend/plugins/jQueryUI/jquery-ui.min.css" rel="stylesheet">

    <!--print table-->
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/buttons.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <!--print table mobile support-->
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/rowReorder.dataTables.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>backend/custom/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/dist/js/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/datepicker/js/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo base_url(); ?>backend/plugins/colorpicker/bootstrap-colorpicker.js"></script>
    <script src="<?php echo base_url(); ?>backend/datepicker/date.js"></script>
    <script src="<?php echo base_url(); ?>backend/dist/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/js/school-custom.js"></script>
    <script src="<?php echo base_url(); ?>backend/js/sstoast.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/js/jquery.mask.min.js"></script>


    <!-- fullCalendar -->
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/calender/zabuto_calendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/fullcalendar/dist/fullcalendar.print.min.css" media="print">
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/sweet-alert/sweetalert2.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/toggle/css/bootstrap-toggle.min.css">


    <script src="<?php echo base_url(); ?>backend/plugins/iCheck/icheck.min.js"></script>



</head>

<body class="hold-transition skin-blue fixed sidebar-mini">

    <div class="wrapper">

        <header class="main-header" id="alert">
            <a href="<?php echo base_url(); ?>Con_my_profile/index" class="logo">
                <span class="logo-mini">V A</span>
                <span class="logo-lg"><img src="<?php echo base_url(); ?>backend/images/logo.png"/></span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="col-md-5 col-sm-3 col-xs-5">
                    <span href="#" class="sidebar-session">
                    </span>
                </div>
                <div class="col-md-7 col-sm-9 col-xs-7">
                    <div class="pull-right">

                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav headertopmenu">
                                <li class="dropdown user-menu">
                                    <a class="dropdown-toggle" style="padding: 15px 13px;" data-toggle="dropdown" href="#" aria-expanded="false" title="Messages Unread"><i class="fa fa fa-envelope"><span class="badge" style="position: absolute; top: 15px; right: 5px; padding: 0px 5px; border-radius: 50%; background: red; color: white;"><?php echo strip_tags($this->session->userdata('msg_count')); ?></span></i></a>

                                    <ul class="dropdown-menu dropdown-user menuboxshadow">
                                        <li>
                                            <div class="sstopuser">
                                            <?php 
                                            if(isset($unread_msg)){
                                                foreach ($unread_msg as $msg_key => $msg_value) { ?>
                                                    <div class="sstopuser-test" id="noti-area">
                                                        <a style="text-transform: capitalize; cursor: pointer;" class="btn_historyzz" data-value="<?php echo $msg_value->con_id; ?>" data-tic="<?php echo $msg_value->reply_id; ?>" data-toggle="modal" data-target="#chat_historyzz" title="Reply"><i class="fa fa-comment"> <?php echo $msg_value->con_no; ?></i>
                                                        
                                                        </a>
                                                    </div>
                                                    <div class="divider"></div>    
                                                <?php 
                                                }
                                                }
                                            ?>
                                                
                                            </div>
                                            <!--./sstopuser-->
                                        </li>
                                    </ul>
                                </li>


                                <li class="dropdown user-menu">
                                    <a class="dropdown-toggle" style="padding: 15px 13px;" data-toggle="dropdown" href="#" aria-expanded="false">
                                        <img src="<?php if ($user_details->prof_pic == '') {echo base_url(); ?>uploads/staff_images/user.png<?php } else {echo base_url() . $user_details->prof_pic;} ?>" class="topuser-image" alt="User Image">
                                    </a>
                                    <ul class="dropdown-menu dropdown-user menuboxshadow">
                                        <li>
                                            <div class="sstopuser">
                                                <div class="ssuserleft">
                                                    <a href="<?php if ($user_details->prof_pic == '') {echo base_url(); ?>uploads/staff_images/user.png<?php } else {echo base_url() . $user_details->prof_pic; } ?>"><img src="<?php if ($user_details->prof_pic == '') {echo base_url(); ?>uploads/staff_images/user.png<?php } else {echo base_url() . $user_details->prof_pic;} ?>" alt="User Image"></a>
                                                </div>

                                                <div class="sstopuser-test">
                                                    <h4 style="text-transform: capitalize;"><?php echo $user_details->Name; ?></h4>
                                                </div>

                                                <div class="divider"></div>
                                                <div class="sspass">
                                                    <a href="<?php echo base_url() . "Con_my_profile/index" ?>" data-toggle="tooltip" title="" data-original-title="My Profile"><i class="fa fa-user"></i>My Profile</a>
                                                    <a class="pl25" href="<?php echo base_url(); ?>Con_my_profile/changepass" data-toggle="tooltip" title="" data-original-title="Change Password"><i class="fa fa-key"></i>Password</a> <a class="pull-right" href="<?php echo base_url(); ?>Con_login/logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                                                </div>
                                            </div>
                                            <!--./sstopuser-->
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

<div class="modal fade" id="chat_historyzz" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="col-md-12" id="edit_datazz">
          <!-- DIRECT CHAT SUCCESS -->
          <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Notifications / Reminder</h3>

            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="direct-chat-messages" style="height:500px;">
              <strong><p class="chat_contact_no"></p></strong>

                <div id="chat_listzz">


                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <form action="javascript:void(0);" id="form_sendzz">
                <input type="hidden" name="id_up" id="id_upzz">
                <input type="hidden" name="tic_id" id="tic_idzz">
                <div class="input-group">
                  <input type="text" name="reply_up" id="reply_upzz" placeholder="Type Message ..." class="form-control" required>
                  <span class="input-group-btn">
                    <button class="btn btn-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Send</button>
                  </span>
                </div>
              </form>
            </div><!-- /.box-footer-->
          </div>
          <!--/.direct-chat -->
        </div><!-- /.col -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn_msg_close">Close Chat</button>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

        <?php $this->load->view('layout/sidebar'); ?>
