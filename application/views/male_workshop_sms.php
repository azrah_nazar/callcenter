<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);
$today = date('Y-m-d');

if (empty($date)) {
    $monthsBack = date('Y-m-d', strtotime($today . '-' . $backdate_days . ' days'));
    $date1 = $monthsBack;
    $date2 = $monthsBack;
} else {
    $monthsBack = date('Y-m-d', strtotime($today . '-' . $backdate_days . ' days'));
    $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
    $date1 = substr($daterange, 0, 10);
    $date2 = substr($daterange, 17, 20);
}
?>

<style>
    .ajax-loader {
        visibility: hidden;
        background-color: rgba(255, 255, 255, 0.7);
        position: absolute;
        z-index: 10000 !important;
        width: 100%;
        height: 100%;
    }

    .ajax-loader img {
        position: relative;
        top: 50%;
        left: 50%;
    }
</style>

<div class="ajax-loader">
    <img src="<?php echo base_url() ?>backend/images/loading.gif" class="img-responsive" />
</div>

<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i> Workshop SMS Retrieved from <?php echo $mapped_operator_name; ?>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-6">
                                <div class="row">
                                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                                    <form role="form" action="<?php echo site_url('Con_workshop/male_ws_sms/') ?><?php echo $menu . "/" . $date1 . ' - ' . $date2; ?>" method="post" class="">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Actions</h3>
                    </div>
                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                    <input type="hidden" name="num" id="num" value="<?php echo  $this->uri->segment(4); ?>">
                    <form class="form-sample" id="form_con_no" action="javascript:void(0);">
                        <div class="box-body">
                            <div class="form-group" align="center">
                                <label id="number" style="font-size: 30px;"></label><br>
                                <label id="st_name" style="font-size: 30px;"></label>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group response_set" align="center" style="font-size: 15px; display: none;">

                                <button class="btn btn-success btn-sm btn_new_ws" data-value="" data-con="" title="New Workshop" style="display: none;">
                                    <i class="fa fa-user-plus"></i> Transfer to Pending Contacts
                                </button> &nbsp;&nbsp;

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <div class="row">
            <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
            <input type="hidden" name="wid" id="wid" value="<?php echo  $this->uri->segment(4); ?>">

            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Workshop SMS Students from <?php echo $mapped_operator_name; ?> </h3>
                    </div>
                    <div class="box-body ">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label">Workshop SMS Students from <?php echo $mapped_operator_name; ?> </div>
                            <table class="table table-striped table-bordered table-hover example22">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Reg No </th>
                                        <th>Student Name </th>
                                        <th>Contact No</th>
                                        <th>Try</th>
                                        <th>Workshop SMS</th>
                                        <th>Reminder SMS</th>
                                        <th>Yes/No SMS</th>
                                        <th>Yes/No Status</th>
                                        <th>Bank SMS</th>
                                        <th>Chat</th>
                                        <th>Workshop Date</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $count = 0;

                                    foreach ($load_data as $value) {
                                        $count++;
                                        if ($value->sms_flag == 1) {
                                            $feedback = '<i class="fa fa-check"></i>';
                                        } else {
                                            $feedback = '<i class="fa fa-close"></i>';
                                        }

                                        if ($value->ws_reminder == 1) {
                                            $ws_reminder = '<i class="fa fa-check"></i>';
                                        } else {
                                            $ws_reminder = '<i class="fa fa-close"></i>';
                                        }

                                        if ($value->confirmation >= 1) {
                                            $confirmation = '<i class="fa fa-check"></i>';
                                        } else {
                                            $confirmation = '<i class="fa fa-close"></i>';
                                        }

                                        if ($value->confirmation == 3) {
                                            $confirmation_st = 'Yes';
                                        } else if ($value->confirmation == 2) {
                                            $confirmation_st = 'No';
                                        } else {
                                            $confirmation_st = '';
                                        }

                                        if ($value->bank_sms == 1) {
                                            $bank = '<i class="fa fa-check"></i>';
                                        } else {
                                            $bank = '<i class="fa fa-close"></i>';
                                        }

                                        if ($value->pending_payment == 1) {
                                            $pay = 'background-color: #fff8a2;';
                                        } else {
                                            $pay = '';
                                        }

                                        if ($value->pending_reallocation == 1) {
                                            $pay = 'background-color: #ffb9bfbf;';
                                        }

                                        $date = $value->w_date . ' ' . date("h:i A", strtotime($value->w_time));
                                        $currentDate = strtotime($date);
                                        $futureDate = $currentDate + (60 * 5);
                                        $formatDate = date("Y-m-d H:i:s", $futureDate);
                                        $nowDate = date("Y-m-d H:i:s");

                                        if ($formatDate <= $nowDate) {
                                            $txt = "display:none";
                                        } else {
                                            $txt = '';
                                        }

                                        $spl_cus = $value->spl_cus;
                                        if ($spl_cus == 1) {
                                            $splCus = "background-color: red; color:#fff;";
                                        } else {
                                            $splCus = "";
                                        }
                                    ?>

                                        <tr style="<?php echo $pay; ?> cursor: pointer;" onclick="getButtons('<?php echo $value->cid; ?>', '<?php echo $value->con_no; ?>', '<?php echo $value->name; ?>')">
                                            <td><?php echo $count; ?>.</td>
                                            <td><?php echo $value->reg_no; ?></td>
                                            <td><?php echo $value->name; ?></td>
                                            <td><?php echo $value->con_no; ?></td>
                                            <td><?php if ($value->feedback_no == 0) {
                                                    echo "";
                                                } else {
                                                    echo $value->feedback_no;
                                                }; ?></td>
                                            <td><?php echo $feedback; ?></td>
                                            <td><?php echo $ws_reminder; ?></td>
                                            <td><?php echo $confirmation; ?></td>
                                            <td><?php echo $confirmation_st; ?></td>
                                            <td><?php echo $bank; ?></td>
                                            <td>
                                                <?php
                                                if ($value->tic_id == '' || $value->tic_id == '0') {

                                                ?>
                                                    <!-- <a class="btn btn-default btn-xs btn_ticket" data-value="<?php //echo $value->cid; ?>" data-toggle="modal" data-target="#chat_open" title="Give Reminder" style="<?php //echo $splCus; ?>">
                                                        <i class="fa fa-bell"></i>
                                                    </a> -->
                                                <?php
                                                } else { ?>
                                                    <a class="btn btn-default btn-xs btn_history" data-value="<?php echo $value->cid; ?>" data-tic="<?php echo $value->tic_id; ?>" data-toggle="modal" data-target="#chat_history" title="Reply" style="<?php echo $splCus; ?>">
                                                        <i class="fa fa-comment"></i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                            <td><?php if ($value->prev_wdate == '0000-00-00' || $value->prev_wdate == '') {
                                                    echo "";
                                                } else {
                                                    echo $value->prev_wdate;
                                                }; ?></td>
                                        </tr>
                                    <?php
                                    }

                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
</div>

<div class="modal fade" id="chat_open" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title text-center modal_title"> Chat Messages</h4>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
                    <div class="box-body">
                        <input type="hidden" name="con_id" id="con_id" value="">

                        <div class="form-group">
                            <label for="email">Message</label><small class="req"> *</small>
                            <textarea name="message" id="compose-textarea" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Send</button>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="chat_history" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-md-12" id="edit_data2">
                    <!-- DIRECT CHAT SUCCESS -->
                    <div class="box box-primary direct-chat direct-chat-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Chat Messages</h3>

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="direct-chat-messages" style="height:500px;">
                                <strong>
                                    <p class="chat_contact_no"></p>
                                </strong>

                                <div id="chat_list">


                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <form action="javascript:void(0);" id="form_send">
                                <input type="hidden" name="id_up" id="cid_up">
                                <input type="hidden" name="tic_id" id="tic_id">
                                <!-- <div class="input-group">
                                    <input type="text" name="reply_up" id="reply_up" placeholder="Type Message ..." class="form-control" required>
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Send</button>
                                    </span>
                                </div> -->
                            </form>
                        </div><!-- /.box-footer-->
                    </div>
                    <!--/.direct-chat -->
                </div><!-- /.col -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn_close">Close Chat</button>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
    /**************************** DATE ***********************/
    $('#date_range').daterangepicker({
        autoclose: false,
        pickerPosition: "bottom-left",
        maxDate: '<?php echo $monthsBack; ?>',
    });

    function formHandler(date) {
        var menu = $("#menu").val();
        window.location.href = "<?php echo base_url() ?>Con_workshop/male_ws_sms/" + menu + "/" + date;
    }

    /**************************** BUTTONS  ****************************/
    function getButtons(cid, telNo, name) {
        $('#number').text(telNo);
        $('#st_name').text(name);

        $('.response_set').show();
        $('.btn_new_ws').show();
        $('.btn_new_ws').attr('data-value', cid);
        $('.btn_new_ws').attr('data-con', telNo);

        $(document).scrollTop(0);
    }

    /**************************** INSERT  ****************************/
    $('.btn_ticket').click(function() {
        var con_id = $(this).attr('data-value');
        $("#con_id").val(con_id);
    });

    $("#form_submit").on('submit', (function(e) {
        var menu = $("#menu").val();
        var wid = $("#wid").val();

        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>Con_Call_Report/add_ticket",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                } else {
                    if ($.trim(data) === 'error') {
                        swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'data exists') {
                        swal({
                            title: "",
                            text: "Bank Already Exists!",
                            type: "warning",
                            timer: 2000,
                            showConfirmButton: false,
                        });

                    } else if ($.trim(data) === 'success') {
                        swal({
                            title: "",
                            text: "Successfully Added!",
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false,
                        });
                        setTimeout(function() {
                            window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + '/' + wid;
                        }, 2000);

                    }
                }
            },
            error: function(e) {
                alert("err2");
            }
        });
    }));

    /*********************************  TICKET ******************************/
    $("#form_send").on('submit', (function(e) {
        var menu = $("#menu").val();
        var wid = $("#wid").val();

        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>Con_Call_Report/send_reply",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                $('#edit_data2').modal('hide')
                if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                } else {
                    if ($.trim(data) === 'error') {
                        swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                        swal("Sent!", "Successfully Sent Message!", "success");
                        location.reload();
                    }
                }
            },
            error: function(e) {
                alert("err2");
            }
        });
    }));

    $('.btn_history').click(function() {
        var con_id = $(this).attr('data-value');
        $("#cid_up").val(con_id);
        var tic_id = $(this).attr('data-tic');
        $("#tic_id").val(tic_id);

        $.post('<?php echo base_url() ?>Con_Call_Report/get_history', {
            'get_history': 'data',
            con_id: con_id
        }, function(data) {
            $('.chat_contact_no').text('');
            var Data = "";
            var reply_id = data.reply_id;


            if (data === undefined || data.length === 0 || data === null) {

                Data = '<div> <h4>This chat has no records!!</h4> </div>';
                $('#chat_list').html('').append(Data);

            } else {

                $.each(data.result, function(index, data) {
                    var msg = "";
                    var val = data.Val;
                    var time = data.dt_time;
                    var contact_no = data.con_no;
                    var std_name = data.st_name;
                    if (std_name == '') {
                        var st_name = '';
                    } else {
                        var st_name = "Name: " + data.st_name + " | ";
                    }
                    var whatsapp = data.whatsapp;
                    if (whatsapp == '') {
                        var whatsapp = '';
                    } else {
                        var whatsapp = "Whatsapp: " + data.whatsapp;
                    }
                    $('.chat_contact_no').text(st_name + "\nContact No : " + contact_no + "\n | " + whatsapp);

                    var user_type = '';
                    var class_type = '';
                    var user_name = '';

                    if ((val == '12') || (val == '15')) {
                        user_type = 'left';
                        class_type = 'left';
                        user_name = data.Name;
                    } else {
                        user_type = 'right';
                        class_type = 'right';
                        user_name = data.Name;
                    }

                    Data += '<div class="direct-chat-msg ' + user_type + '">';
                    Data += '<div class="direct-chat-info clearfix">';
                    Data += '<span class="direct-chat-name pull-' + class_type + '">' + user_name + ' &nbsp;</span>';
                    Data += '<span class="direct-chat-timestamp pull-' + user_type + '">' + time + '&nbsp;</span>';
                    Data += '</div>';
                    Data += '<img class="direct-chat-img" src="<?php echo base_url(); ?>uploads/student_images/no_image.png" alt="message user image">';
                    Data += '<div class="direct-chat-text">';
                    Data += data.message;
                    Data += '</div>';
                    Data += '</div>';
                    Data += '</div>';
                    Data += '</div>';
                });

                $('#chat_list').html('').append(Data);
            }
        }, "json");
    });

    /******************************** CLOSE CHAT ***************************/
    $(".btn_close").click(function() {
        var tic_id = $("#tic_id").val();
        var id_up = $("#cid_up").val();
        var menu = $("#menu").val();
        var wid = $("#wid").val();

        swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, close it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_Call_Report/close_chat", {
                        close_chat: "data",
                        tic_id: tic_id,
                        id_up: id_up
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal("Closed", "Successfully Closed Chat!", "success");
                            window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + '/' + wid;

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });

    });

    /******************************** NEW WORKSHOP***************************/
    $(".btn_new_ws").click(function() {
        var con_id = $(this).attr('data-value');
        var telNo = $(this).attr('data-con');
        var menu = $("#menu").val();

        swal({
                title: "Are you sure you want to move to pending contacts?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, move it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_workshop/pending_contacts", {
                        pending_contacts: "data",
                        telNo: telNo
                    }, function(data) {
                        if ($.trim(data) === 'success') {
                            swal({
                                    title: "Moved!",
                                    text: "Successfully moved!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/male_ws_sms/" + menu;
                                    }
                                });

                        } else if ($.trim(data) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });



    $(document).ready(function() {
        var col_len = '';
        $('.example22').DataTable({
            "aaSorting": [],
            "sScrollY": "350px",
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "bScrollCollapse": true,

            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            dom: "Bfrtip",
            buttons: [

                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'excelHtml5',
                    header: true,


                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',

                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5],

                    }
                },

                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'

                    }
                },

                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    title: $('.download_label').html(),
                    customize: function(win) {
                        $(win.document.body)
                            .css('font-size', '10pt');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'colvis',
                    text: '<i class="fa fa-columns"></i>',
                    titleAttr: 'Columns',
                    title: $('.download_label').html(),
                    postfixButtons: ['colvisRestore']
                },
            ]
        });
    });
</script>