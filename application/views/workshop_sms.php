<?php 
date_default_timezone_set('Asia/Colombo');

$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
    $date = date('Y-m-d');
	$date1 = date('Y-m-d', strtotime('-7 day', strtotime($date)));
    $date2 = $wdate;
} else {
    $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
    $date1 = substr($daterange, 0, 10);
    $date2 = substr($daterange, 17, 24);
}
?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i> Workshop SMS
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

                    </div>
                    <div class="box-body">
                        <form role="form" action="<?php echo site_url('Con_workshop/workshop_sms/') ?><?php echo $menu; ?>" method="post" class="">
                            <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <!-- <button type="submit" name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> Search</button> -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Workshop SMS</h3>
                    </div>
                    <div class="box-body ">
                        <div class="table-responsive mailbox-messages" style="overflow-y: scroll; overflow-x:scroll;">
                            <div class="download_label">Workshop SMS</div>
                            <table class="table table-striped table-bordered table-hover example22">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th> Workshop </th>
                                        <th> Date / Time </th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $count = 0;

                                    foreach ($load_data as $value) {
                                        $count++;
                                        $date = $value->w_date . ' ' . date("h:i A", strtotime($value->w_time));
                                        $currentDate = strtotime($date);
                                        $futureDate = $currentDate + (60 * 5);
                                        $formatDate = date("Y-m-d H:i:s", $futureDate);
                                        $nowDate = date("Y-m-d H:i:s"); 

                                        if($formatDate <= $nowDate){
                                            $txt = "display:none";
                                        }else{
                                            $txt = '';
                                        }
                                    ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $value->w_name; ?></td>
                                            <td><?php echo $value->w_date . ' / ' . date("h:i A", strtotime($value->w_time)); ?></td>
                                            <td class="mailbox-date">
                                                <button class="btn btn-info btn-sm btn_sms" value="<?php echo $value->id; ?>" title="Send SMS" style="<?php echo $txt; ?>"><i class="fa fa-mobile"> Workshop</i></button>
                                                <button class="btn btn-info btn-sm btn_reminder" value="<?php echo $value->id; ?>" title="Reminder" style="background:#63c2f9; border-color:#63c2f9; <?php echo $txt; ?>"><i class="fa fa-bell"> Reminder</i></button>
                                                <button class="btn btn-warning btn-sm btn_feedback" value="<?php echo $value->id; ?>" title="Feedback SMS"><i class="fa fa-reply"> Yes / No</i></button>
                                                <!-- <button class="btn btn-danger btn-sm btn_bank" value="<?php //echo $value->id; 
                                                                                                            ?>" title="Bank SMS" style="background-color: #d4b4fb; border-color: #d4b4fb;"><i class="fa fa-bank" > Bank</i></button> -->
                                                <a target="_blank" href="<?php echo base_url() ?>Con_workshop/view_details/<?php echo $this->uri->segment(3) . "/" . $value->id; ?>" class="btn btn-success btn-sm btn_details" title="View Students"><i class="fa fa-eye"> View Details</i></a>

                                                <a target="_blank" href="<?php echo base_url() ?>Con_workshop/spl_csv/<?php echo $this->uri->segment(3) . "/" . $value->id; ?>" class="btn btn-danger btn-sm btn_details" title="View Students"><i class="fa fa-file"> Special CSV Export</i></a>
                                            </td>
                                        </tr>
                                    <?php
                                    }

                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
    /**************************** DATE ***********************/
    $('#date_range').daterangepicker({
        autoclose: false,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });

    function formHandler(date){
    var menu = $("#menu").val();
    window.location.href = "<?php echo base_url()?>Con_workshop/workshop_sms/"+menu+"/"+date;
  }

    /**************************** SEND SMS ***********************/
    $(".btn_sms").click(function() {
        var menu = $("#menu").val();
        var id = $(this).val();

        swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Send  SMS!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_workshop/zoom_link_sms", {
                        delete: "data",
                        id: id
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal({
                                    title: "Sent!",
                                    text: "Successfully Sent!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/workshop_sms/" + menu;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    /**************************** REMINDER SMS ***********************/
    $(".btn_reminder").click(function() {
        var menu = $("#menu").val();
        var id = $(this).val();

        swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Send SMS!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_workshop/reminder_sms", {
                        delete: "data",
                        id: id
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal({
                                    title: "Sent!",
                                    text: "Successfully Sent!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/workshop_sms/" + menu;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    /**************************** FEEDBACK SMS ***********************/
    $(".btn_feedback").click(function() {
        var menu = $("#menu").val();
        var id = $(this).val();

        swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Send SMS!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_workshop/feedback_sms", {
                        delete: "data",
                        id: id
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal({
                                    title: "Sent!",
                                    text: "Successfully Sent!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/workshop_sms/" + menu;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    /**************************** BANK SMS ***********************/
    $(".btn_bank").click(function() {
        var menu = $("#menu").val();
        var id = $(this).val();

        swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Send SMS!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_workshop/bank_sms", {
                        delete: "data",
                        id: id
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal({
                                    title: "Sent!",
                                    text: "Successfully Sent!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/workshop_sms/" + menu;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });


    $(document).ready(function() {
        var col_len = '';
        $('.example22').DataTable({
            "aaSorting": [],

            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            dom: "Bfrtip",
            buttons: [

                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'excelHtml5',
                    header: true,


                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',

                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5],

                    }
                },

                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'

                    }
                },

                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    title: $('.download_label').html(),
                    customize: function(win) {
                        $(win.document.body)
                            .css('font-size', '10pt');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'colvis',
                    text: '<i class="fa fa-columns"></i>',
                    titleAttr: 'Columns',
                    title: $('.download_label').html(),
                    postfixButtons: ['colvisRestore']
                },
            ]
        });
    });
</script>