<?php
$menu = $this->uri->segment(3);
?>
<script>
  yr = $('#year').val();
</script>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1> <i class="fa fa-group"></i> Contact Search
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12 col-sm-6">
                <div class="row">
                  <form role="form" action="<?php echo site_url('Con_pre_registry/contact_search/') ?><?php echo $menu; ?>" method="post" class="">
                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                    <input type="hidden" name="acc_no" id="acc_no" value="<?php echo  $acc_no; ?>">
                    <input type="hidden" name="val" id="val" value="<?php echo  $val; ?>">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Search By Keyword</label>
                        <input type="text" name="search_text" class="form-control" placeholder="Search By Contact Number">
                        <span class="text-danger"><?php echo form_error('search_text'); ?></span>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <button type="submit" name="search" value="search_full" class="btn btn-primary btn-sm pull-right checkbox-toggle"><i class="fa fa-search"></i> Search</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php if (isset($resultlist)) { ?>
          <div class="box box-info">
            <div class="box-header ptbnull">
              <h3 class="box-title titlefix"><i class="fa fa-users"></i> Contact Details </i></h3>
              <div class="box-tools pull-right"> </div>
            </div>
            <div class="box-body table-responsive">
              <div class="download_label">Contact Details</div>
              <table class="table table-striped table-bordered table-hover example">
                <thead>
                  <tr>
                    <th></th>
                    <th>Status</th>
                    <th>Reg No</th>
                    <th>Contact No</th>
                    <th>Whatsapp</th>
                    <th>Chat</th>
                    <th>Name</th>
                    <th>Uploaded Date</th>
                    <th>Rejected Date</th>
                    <th>Operator</th>
                    <th>Agent Code</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $x = 0;
                  foreach ($resultlist as $student) {
                    $x++;
                    $color = '';

                    $dt = date("Y-m-d", strtotime($student['date']));
                    if (($dt == '1970-01-01') || ($dt == '1900-01-01')) {
                      $date = '';
                    } else {
                      $date = $dt;
                    }

                    $reject_dt = date("Y-m-d", strtotime($student['reject_date']));
                    if (($reject_dt == '1970-01-01') || $reject_dt == '1900-01-01' || $reject_dt == '0000-00-00' || $reject_dt == '0000-00-00 00:00:00' || $reject_dt == '-0001-11-30') {
                      $r_dt = '';
                    } else {
                      $r_dt = $reject_dt;
                    }

                    $reject_date = $student['agent_reject_date'];
                    if ($reject_date == '0000-00-00') {
                    } else {
                      $r_dt = $reject_date;
                    }

                    $re_reject_dt = date("Y-m-d", strtotime($student['reapplied_reject_date']));
                    if (($re_reject_dt == '1970-01-01') || $re_reject_dt == '1900-01-01' || $re_reject_dt == '0000-00-00' || $re_reject_dt == '0000-00-00 00:00:00' || $re_reject_dt == '-0001-11-30' || $re_reject_dt == '') {
                    } else {
                      $r_dt = $re_reject_dt;
                    }

                    $target = "target='_blank' ";

                    if ($student['feedback_reject'] == 0) {
                      if ($student['flag'] == 0) {
                        $status = "Pending Contacts";
                        $url = base_url() . "Con_call_center/index/179";
                      } else if ($student['flag'] == 1) {
                        $status = "Accepted";
                        $url = base_url() . "Con_pre_registry/index/196";
                      } else if ($student['flag'] == 2) {
                        $status = "Registered";
                        $url = base_url() . "Con_pre_registry/registered_list/197";
                      } else if ($student['flag'] == 3) {
                        $status = "Rejected";
                        $url = base_url() . "Con_Call_Report/reject_report/194";
                      } else if ($student['flag'] == 4) {
                        if ($student['pending_payment'] == 1) {
                          $status = "Class Follow Up";
                          $url = base_url() . "Con_zoom_upload/workshop_follow_up/200";
                        } else {
                          $status = "Workshop SMS";
                          $url = base_url() . "Con_workshop/view_details/202/" . $student['workshop'];
                        }
                      } else if ($student['flag'] == 5) {
                        $status = "Reapplied Rejected";
                        $url = base_url() . "Con_Contact_CSV/reapplied_reject_list/220";
                      } else {
                        $status = "";
                        $url = "#";
                      }
                    } else {
                      $status = "Workshop Rejected";
                      $url = base_url() . "Con_workshop/workshop_reject_report/205";
                    }

                    if ($student['direct_sale'] == 1) {
                      $status = "Direct Sale";
                      $url = base_url() . "Con_call_center/direct_sale_view/207";
                    }

                    if (($student['pending_payment'] == 1) and ($student['feedback_reject'] == 0) and ($student['flag'] != 3)) {
                      $status = "Class Follow Up";
                      $url = base_url() . "Con_zoom_upload/workshop_follow_up/200";
                    }

                    if ($student['flag'] == 5) {
                      $status = "Reapplied Rejected";
                      $url = base_url() . "Con_Contact_CSV/reapplied_reject_list/220";
                    }

                    if ($student['flag'] == 6 || $student['all_paid'] == '1') {
                      $status = "Account Created";
                      $url = base_url() . "Con_sales_report/account_creation/210";
                    }



                    if ($val == 13) {
                      if ($student['emp_accNo'] != $acc_no) {
                        $url = "#";
                        $target = "";
                        $color = "background-color: #fff8c7;";
                      } else {
                        $target = "target='_blank' ";
                        $color = "background-color: #d5efd4;";
                      }
                    }

                    $self_reg = $student['self_reg'];
                    $spl_cus = $student['spl_cus'];
                    if ($spl_cus == 1) {
                      $splCus = "background-color: red; color:#fff;";
                    } else {
                      $splCus = "";
                    }
                  ?>
                    <tr class="odd gradeX" style="<?php echo $color; ?>">
                      <td><?php echo $x; ?>.</td>
                      <td><a <?php echo $target; ?> href="<?php echo $url; ?>" class="btn btn-success btn-sm" title="<?php echo $status; ?>"><i class="fa fa-eye"> <?php echo $status; ?></i></a></td>
                      <td><?php echo $student['reg_no']; ?></td>
                      <td><?php echo $student['con_no']; ?></td>
                      <td><?php echo $student['whatsapp']; ?></td>
                      <td>
                        <?php
                        if ($student['tic_id'] == '' || $student['tic_id'] == '0') {
                        ?>
                          <a class="btn btn-default btn-xs btn_ticket" data-value="<?php echo $student['cid']; ?>" data-toggle="modal" data-target="#chat_open" title="Give Reminder" style="<?php echo $splCus; ?>">
                            <i class="fa fa-bell"></i>
                          </a>
                        <?php
                        } else { ?>
                          <a class="btn btn-default btn-xs btn_history" data-value="<?php echo $student['cid']; ?>" data-tic="<?php echo $student['tic_id']; ?>" data-owner="<?php echo $student['emp_accNo']; ?>" data-toggle="modal" data-target="#chat_history" title="Reply" style="<?php echo $splCus; ?>">
                            <i class="fa fa-comment"></i>
                          </a>
                        <?php } ?>
                      </td>
                      <td><?php echo $student['name']; ?></td>
                      <td><?php echo $date; ?></td>
                      <td><?php echo $r_dt; ?></td>
                      <td><?php echo $student['emp_name']; ?></td>
                      <td> <?php if ($self_reg == '1') { ?> WR<?php echo $student['agent_code'];
                                                          } ?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="chat_open" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title"> Add Reminder</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
          <div class="box-body">
            <input type="hidden" name="con_id" id="con_id" value="">

            <div class="form-group">
              <label for="email">Message</label><small class="req"> *</small>
              <textarea name="message" id="compose-textarea" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Send</button>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="chat_history" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="col-md-12" id="edit_data">
          <!-- DIRECT CHAT SUCCESS -->
          <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Chat Messages</h3>

            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="direct-chat-messages" style="height:500px;">
                <strong>
                  <p class="chat_contact_no"></p>
                </strong>
                <div id="chat_list">


                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <form action="javascript:void(0);" id="form_send">
                <input type="hidden" name="id_up" id="id_up">
                <input type="hidden" name="tic_id" id="tic_id">
                <div class="input-group">
                  <input type="text" name="reply_up" id="reply_up" placeholder="Type Message ..." class="form-control" required>
                  <span class="input-group-btn">
                    <button class="btn btn-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Send</button>
                  </span>
                </div>
              </form>
            </div><!-- /.box-footer-->
          </div>
          <!--/.direct-chat -->
        </div><!-- /.col -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close Chat</button>
      </div>
    </div>
  </div>
</div>


<script>
  /**************************** INSERT  ****************************/
  $('.btn_ticket').click(function() {
    var con_id = $(this).attr('data-value');
    $("#con_id").val(con_id);
  });

  $("#form_submit").on('submit', (function(e) {
    var menu = $("#menu").val();
    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/add_ticket",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'data exists') {
            swal({
              title: "",
              text: "Data Already Exists!",
              type: "warning",
              timer: 2000,
              showConfirmButton: false,
            });

          } else if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Added!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              location.reload();
            }, 2000);

          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /*********************************  TICKET ******************************/
  $("#form_send").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/send_reply",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal("Sent!", "Successfully Sent Message!", "success");
            location.reload();
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  $('.btn_history').click(function() {
    var con_id = $(this).attr('data-value');
    $("#id_up").val(con_id);
    var tic_id = $(this).attr('data-tic');
    $("#tic_id").val(tic_id);
    var owner = $(this).attr('data-owner');
    var acc_no = $('#acc_no').val();
    var val = $('#val').val();

    if ((acc_no == owner) || (val == 12 || val == 15)) {
      $('.btn_close').show();
    } else {
      $('.btn_close').hide();
    }


    $.post('<?php echo base_url() ?>Con_Call_Report/get_history', {
      'get_history': 'data',
      con_id: con_id
    }, function(data) {

      var Data = "";
      var reply_id = data.reply_id;


      if (data === undefined || data.length === 0 || data === null) {

        Data = '<div> <h4>This chat has no records!!</h4> </div>';
        $('#chat_list').html('').append(Data);

      } else {

        $.each(data.result, function(index, data) {
          var msg = "";
          var val = data.Val;
          var time = data.dt_time;

          var contact_no = data.con_no;
          var std_name = data.st_name;
          if (std_name == '') {
            var st_name = '';
          } else {
            var st_name = "Name: " + data.st_name + " | ";
          }
          var whatsapp = data.whatsapp;
          if (whatsapp == '') {
            var whatsapp = '';
          } else {
            var whatsapp = "Whatsapp: " + data.whatsapp;
          }
          $('.chat_contact_no').text(st_name + "\nContact No : " + contact_no + "\n | " + whatsapp);

          var user_type = '';
          var class_type = '';
          var user_name = '';

          if ((val == '12') || (val == '15')) {
            user_type = 'left';
            class_type = 'left';
            user_name = data.Name;
          } else {
            user_type = 'right';
            class_type = 'right';
            user_name = data.Name;
          }

          Data += '<div class="direct-chat-msg ' + user_type + '">';
          Data += '<div class="direct-chat-info clearfix">';
          Data += '<span class="direct-chat-name pull-' + class_type + '">' + user_name + ' &nbsp;</span>';
          Data += '<span class="direct-chat-timestamp pull-' + user_type + '">' + time + '&nbsp;</span>';
          Data += '</div>';
          Data += '<img class="direct-chat-img" src="<?php echo base_url(); ?>uploads/student_images/no_image.png" alt="message user image">';
          Data += '<div class="direct-chat-text">';
          Data += data.message;
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
        });

        $('#chat_list').html('').append(Data);
      }
    }, "json");
  });

  /******************************** CLOSE CHAT ***************************/
  $(".btn_close").click(function() {
    var tic_id = $("#tic_id").val();
    var id_up = $("#id_up").val();
    var menu = $("#menu").val();

    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, close it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_Call_Report/close_chat", {
            close_chat: "data",
            tic_id: tic_id,
            id_up: id_up
          }, function(data) {
            if ($.trim(data.status) === 'success') {
              swal("Closed", "Successfully Closed Chat!", "success");
              window.location.href = "<?php echo base_url() ?>Con_pre_registry/contact_search/" + menu;
            } else if ($.trim(data.status) === ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          }, "json");
        } else {
          swal("Cancelled", "", "error");
        }
      });

  });
</script>