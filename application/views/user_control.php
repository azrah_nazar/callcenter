<?php 
$menu = $this->uri->segment(3);
?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i> User Control</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
  
            <div class="col-md-12">             
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Users List</h3>
                    </div>
                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                    <div class="box-body ">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label">Users List</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th></th>
                                        <th>Stop</th>
                                        <th>Division</th>        
                                        <th>Profile</th>
                                        <th>Emp Name</th>
                                        <th>Acc No</th> 
                                        <th>User Name</th>
                                        <th>Contact No</th>
                                        <th class="text-right">Login History</th>
                                    </tr>
                                </thead>
                                <tbody>                                   

                                    <?php
                                    $count = 1;
                                     
                                    foreach($load_data as $value) {
                                        ?>
                                        <tr>
                                            <td class="mailbox-name"> <?php echo $count; ?>.</td>

                                            <td>
                                              <?php if($value->uid != 1) { ?>
                                                <button class="btn btn-sm btn-success btn-xs btn_lock" style="display:none" value="<?php echo $value->acc_no; ?>" id="lock_<?php echo $value->acc_no; ?>">Lock</button>
                                              <?php } ?>            
                                            </td>
                                            
                                            <td>
                                              <?php if($value->uid != 1) { ?>
                                                <button class="btn btn-sm btn-warning btn-xs btn_lock" style="display:none" value="<?php echo $value->acc_no; ?>" id="lock1_<?php echo $value->acc_no; ?>">Lock</button>
                                                <button class="btn btn-sm btn-danger btn-xs btn_unlock" style="display:none" value="<?php echo $value->acc_no; ?>" id="unlock_<?php echo $value->acc_no; ?>">Unlock</button>
                                              <?php } ?>
                                            </td>

                                            <td class="mailbox-name"> <?php echo $value->division; ?></td>
                                            <td class="mailbox-name"> <?php echo $value->profile; ?></td>
                                            <td class="mailbox-name"> <?php echo $value->Name; ?></td>
                                            <td>
                                              <i class="fa fa-circle text-yellow" id="cr1_<?php echo $value->acc_no; ?>" style="display:none"></i>
                                              <i class="fa fa-circle text-red" id="cr2_<?php echo $value->acc_no; ?>" style="display:none"></i> &nbsp;<?php echo $value->ID; ?>
                                            </td>
                                            <td class="mailbox-name"> <?php echo $value->acc_no; ?></td>
                                            <td class="mailbox-name"> <?php echo $value->Tp; ?></td>

                                            <td><a href="<?php echo base_url()?>Con_user_control/user_log/<?php echo $menu; ?>/<?php echo $value->acc_no; ?>" target="_new" class="btn btn-primary btn-xs">History</a></td>

                                             <script>
                                               <?php if($value->stt == 1) { ?>
                                                $("#lock1_<?php echo $value->acc_no; ?>").show();
                                                $("#cr1_<?php echo $value->acc_no; ?>").show();
                                                
                                              <?php } else if($value->stt == 2) { ?>
                                                $("#cr2_<?php echo $value->acc_no; ?>").show();
                                                $("#unlock_<?php echo $value->acc_no; ?>").show();
                                                
                                              <?php } else if($value->stt == 0) { ?>
                                                $("#lock_<?php echo $value->acc_no; ?>").show();
                                                
                                              <?php } ?>
                                            </script> 

                                        </tr>
                                        <?php
                                    $count++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
           
        </div> 

    </section>
</div>


 <script>
  $(".btn_lock").click(function(){ 
    var acc = $(this).val();    
    $.post( "<?php echo base_url()?>Con_user_control/update_stt", { update_stt: "data", acc : acc, flag : '2'}, function( data ) {
     if ($.trim(data) ==='success') {
      $("#lock_"+acc).hide();
      $("#unlock_"+acc).show();
      $("#lock1_"+acc).hide();
      $("#cr1_"+acc).hide();
      $("#cr2_"+acc).show();
    }
  });   
  });
  
  $(".btn_unlock").click(function(){ 
    var acc = $(this).val();    
    $.post( "<?php echo base_url()?>Con_user_control/update_stt", { update_stt: "data", acc : acc, flag : '0'}, function( data ) {
     if ($.trim(data) ==='success') {
      $("#lock_"+acc).show();
      $("#unlock_"+acc).hide();
      $("#lock1_"+acc).hide();
      $("#cr2_"+acc).hide();
      $("#cr1_"+acc).hide();      
    }
  });   
  });   
</script>