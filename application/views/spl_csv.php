<?php
$menu = $this->uri->segment(3);
$ws = $this->uri->segment(4);
$date = '';

if (empty($date)) {
    $dater = date('Y-m-d');
} else {
    $dater = $date;
}
?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1> <i class="fa fa-sitemap"></i> Export Special CSV
        <?php
            if ($con_cnt >= 1) {
            ?>
                <a href="https://contacts.google.com/" target="_blank" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-google"></i> Google Contacts</a>
            <?php } ?></h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if($con_cnt >=1){ ?>
                    <div class="box box-info">
                        <div class="box-body">
                            <form action="javascript:void(0);" method="post">
                                <div class="table-responsive ptt10">
                                    <div class="download_label"><?php echo $dater; ?>- Special CSV</div>
                                    <table class="table table-hover table-striped example22">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th style="display: none;">Group Membership</th>
                                                <th style="display: none;">Phone 1 - Type</th>
                                                <th>Phone 1 - Value</th>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($resultlist as $key => $value) {
                                                $dt = $value['w_date'];
                                                $ac_date = date_create($dt);
                                                $reg = $value['reg_no'];
                                                $reg_no = substr($reg, 3);
                                                $name = date_format($ac_date, "dM") .'WSC'. $reg_no;

                                                if(($value['con_no'] == $value['whatsapp'] )){
                                                    $con_no = $value['con_no'];
                                                }else if($value['whatsapp'] == ''){
                                                    $con_no = $value['con_no'];
                                                }else{
                                                    $con_no = $value['whatsapp'];
                                                }
                                            ?>
                                                <tr>
                                                    <td><?php echo $name; ?></td>
                                                    <td style="display: none;">Mobile</td>
                                                    <td style="display: none;">* myContacts ::: * starred</td>
                                                    <td><?php echo $con_no; ?></td>
                                                </tr>
                                            <?php
                                            }

                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php }else{ ?>
                    <div class="box box-info">
                        <div class="box-body">
                            <h4>No Special contacts available for the selected date!!</h4>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </section>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
    /**************************** DATE ***********************/
    $('#date_range').daterangepicker({
        autoclose: false,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });

    var date_format = 'yyyy-mm-dd';
    $(document).ready(function() {
        $(".date").datepicker({
            format: date_format,
            autoclose: true,
            todayHighlight: true
        });
    });

    $(document).ready(function() {
        var col_len = '';
        $('.example22').DataTable({
            "aaSorting": [],

            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            dom: "Bfrtip",
            buttons: [{
                extend: 'csvHtml5',
                text: '<i class="fa fa-file-text-o btn btn-lg btn-success csv_download"> CSV Download</i>',
                titleAttr: 'CSV',
                title: $('.download_label').html(),
                exportOptions: {
                    columns: [0, 1, 2, 3]
                }
            }, ]
        });
    });
</script>