<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
  $date1 = date("Y-m-d");
  $date2 = date("Y-m-d");
} else {
  $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
  $date1 = substr($daterange, 0, 10);
  $date2 = substr($daterange, 17, 20);
}
?>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1> <i class="fa fa-sitemap"></i> Invoice List
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">

        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

          </div>
          <div class="box-body">
            <form role="form" action="<?php echo site_url('Con_post_registry/full_payment/') ?><?php echo $menu. "/" . $date1 . ' - ' . $date2; ?>" method="post" class="">
              <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">

              <div class="col-md-12">
                <div class="form-group">
                  <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                  </div>
                </div>
              </div>

              <!-- <div class="col-sm-12">
                <div class="form-group">
                  <button type="submit" name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> Search</button>
                </div>
              </div> -->
            </form>
          </div>
        </div>

        <?php
        if (!empty($load_data)) { ?>
          <div class="box box-info">
            <div class="box-body">
              <form action="javascript:void(0);" method="post">
                <div class="table-responsive ptt10">
                  <table class="table table-hover table-striped example">
                    <thead>
                      <tr>
                        <th width="5%">#</th>
                        <th width="5%">Actions</th>
                        <th>WR No</th>
                        <th>CR No</th>
                        <th>Name</th>
                        <th>Contact No</th>
                        <th>Amount</th>
                        <th>Payment Method</th>
                        <th>Description</th>
                        <th>NIC</th>
                        <th>Job</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php
                      $count = 1;

                      foreach ($load_data as $value) {

                      ?>
                        <tr>
                          <td class="mailbox-name"> <?php echo $count; ?>.</td>
                          <td >
                            <a target="_blank" href="<?php echo base_url() ?>Con_post_registry/payment_details/<?php echo $value->inv_no; ?>/<?php echo $value->inv_id; ?>" class="btn btn-sm btn-success download_receipt" title="Receipt Preview"><i class="fa fa-download"></i></button>
                          </td>
                          <td class="mailbox-name"> <?php echo $value->reg_no; ?></td>
                          <td class="mailbox-name"> <?php echo $value->inv_no; ?></td>
                          <td class="mailbox-name"> <?php echo $value->name; ?></td>
                          <td class="mailbox-name"> <?php echo $value->con_no; ?></td>
                          <td class="mailbox-name"> <?php echo $value->tot_dep; ?></td>
                          <td class="mailbox-name"> <?php echo $value->bank; ?></td>
                          <td class="mailbox-name"> <?php echo $value->p_type; ?></td>
                          <td class="mailbox-name"> <?php echo $value->nic; ?></td>
                          <td class="mailbox-name"> <?php echo $value->job; ?></td>
                        </tr>
                      <?php
                        $count++;
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
</div>



<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
    /**************************** DATE ***********************/
    $('#date_range').daterangepicker({
        autoclose: false,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });

    function formHandler(date) {
        var menu = $("#menu").val();
        window.location.href = "<?php echo base_url() ?>Con_post_registry/full_payment/" + menu + "/" + date;
    }

  /**************************** ACCEPT / REJECT  ****************************/
  $(".btn_accept_reject").click(function() {
    var menu = $("#menu").val();
    bid = $(this).val();
    var res_flag = $(this).attr('data-res-flag');
    var reg = $(this).attr('data-reg');

    if (res_flag == 1) {
      txt = 'Accepted';
    } else {
      txt = "Rejected";
    }


    $.post("<?php echo base_url() ?>Con_post_registry/response_accept", {
        response_accept: "data",
        res_flag: res_flag,
        bid: bid,
        reg: reg
      },
      function(data) {
        if ($.trim(data.status) === 'success') {
          swal({
            title: 'Successfully ' + txt,
            text: '',
            type: "success",
            timer: 2000,
            showConfirmButton: false,
          });
          setTimeout(function() {
            location.reload();
          }, 2000);

        } else if ($.trim(data.status) === 'error') {
          swal({
            title: "",
            text: "Error!",
            type: "error",
            timer: 2000,
            showConfirmButton: false,
          });
          setTimeout(function() {
            location.reload();
          }, 2000);

        }
      }, "json");

  });
</script>