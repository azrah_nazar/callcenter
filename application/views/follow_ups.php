<?php
$menu = $this->uri->segment(3);
$num = $this->uri->segment(4);
?>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1>
      <i class="fa fa-id-card"></i> Pending Workshop Contacts - Total Count: <?php echo $pending_count['pending_count']; ?>
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">

      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Actions</h3>
          </div>
          <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
          <input type="hidden" name="num" id="num" value="<?php echo  $this->uri->segment(4); ?>">
          <form class="form-sample" id="form_con_no" action="javascript:void(0);">
            <div class="box-body">
              <div class="form-group" align="center">
                <label id="number" style="font-size: 30px;"></label><br>
                <label id="st_name" style="font-size: 30px;"></label>
              </div>
            </div>
            <div class="box-body">
              <div class="form-group response_set" align="center" style="font-size: 15px; display: none;">

                <button type="button" class="btn btn-sm btn-success btn_register" id="btn_register" data-tel='' data-reg="" title="Add Details"><i class="fa fa-user-plus"></i> Reserve Workshop</button> &nbsp;&nbsp;

                <button class="btn btn-danger btn-sm btn_reject" value="" data-con="" title="Reject"><i class="fa fa-close"> </i> Reject</button>&nbsp;&nbsp;

                <button class="btn btn-info btn-sm clz_followup" value="" title="Class Follow-up" style="background-color: #ff9d00; border-color: #fdb849"><i class="fa fa-user-plus"> </i> Class Follow-up</button>&nbsp;&nbsp;

                <button type="button" class="btn btn-info" id="btn_reminder" data-tel='' data-cid='' data-toggle="modal" data-target="#reminder_model" style="background-color:#aa67fd; border-color:#aa67fd;">Set Reminder</button>
                &emsp;&emsp;
                <br />
                <br />

                <button type="button" class="btn btn-sm btn-primary spl_cus cus1" data-flag="1" data-tel='' data-cid='' style="background-color:#1e88e5; border-color:#1e88e5;">Special Customer</button>

                <button type="button" class="btn btn-sm btn-primary spl_cus cus2" data-flag="2" data-tel='' data-cid='' style="background-color:#1e88e5; border-color:#1e88e5;">Regular Customer</button>
                &emsp;

                <button type="button" class="btn-sm btn btn-primary" id="btn_directSale" data-tel='' data-cid=''>Direct Sale</button>
                &emsp;&emsp;
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header ptbnull">
            <h3 class="box-title titlefix">Pending Workshop Contacts</h3>
          </div>
          <div class="box-body">
            <div class="table-responsive mailbox-messages">
              <div class="download_label"> Pending Workshop Contacts</div>
              <table class="table table-striped table-bordered table-hover example">
                <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Reg No</th>
                    <th>Contact No</th>
                    <th>Whatsapp No</th>
                    <th>Chat</th>
                    <th>Name</th>
                    <th>Accepted Date</th>
                    <th>Allocated Date</th>
                    <th>Job</th>
                  </tr>
                </thead>

                <tbody>

                  <?php
                  $count = 1;

                  foreach ($load_data as $value) {
                    //$splCus = "";

                    // $user_id = $value->user_id;
                    // if ($user_id == '1') {
                    //   $txt = "#fbf9b6";
                    // } else if ($user_id == '18') {
                    //   $txt = "#c0ffc2";
                    // } else if ($user_id == '23') {
                    //   $txt = "#c0d9ff";
                    // } else if ($user_id == '19') {
                    //   $txt = "#fbf9b6";
                    // } else {
                    //   $txt = "#fff";
                    // }

                    $user_id = $value->user_id;
                    $upload_type = $value->upload_type;
                    if ($upload_type == '') {
                      $txt = "#fff";
                    } else if ($upload_type == 'whatsapp') {
                      $txt = "#c0ffc2";
                    } else if ($upload_type == 'facebook') {
                      $txt = "#c0d9ff";
                    } else if ($upload_type == 'lead') {
                      $txt = "#fbf9b6";
                    } else {
                      $txt = "#fff";
                    }

                    $site_flag = $value->site_flag;
                    if ($site_flag == "1") {
                      $txt = "#cdba8e";
                    }

                    $spl_cus = $value->spl_cus;
                    if ($spl_cus == 1) {
                      $splCus = "background-color: red; color:#fff;";
                    } else {
                      $splCus = "";
                    }

                    $reminder_date = $value->reminder_date;
                    if ($reminder_date = "" || $reminder_date == '0000-00-00' || $reminder_date === NULL) {
                    } else {
                      $txt = "#d4b4fb";
                    }
                  ?>
                    <tr style="cursor: pointer; background-color:<?php echo $txt; ?>" onclick="getButtons('<?php echo $value->reg_no; ?>', '<?php echo $value->con_no; ?>', '<?php echo $value->name; ?>', '<?php echo $value->reminder_date; ?>', '<?php echo $value->id; ?>', '<?php echo $value->emp_accNo; ?>',  '<?php echo $value->tic_id; ?>', '<?php echo $spl_cus; ?>')">
                      <td> <?php echo $count; ?>.</td>
                      <td> <?php echo $value->reg_no; ?></td>
                      <td> <?php echo $value->con_no; ?></td>
                      <td> <?php echo $value->whatsapp; ?></td>
                      <th>
                        <?php
                        $reminder = $value->reminder_date;
                        if (($value->tic_id == '' || $value->tic_id == '0') && ($value->reminder_date == '0000-00-00' || empty($value->reminder_date))) {
                        ?>
                          <a class="btn btn-default btn-xs btn_ticket" data-value="<?php echo $value->id; ?>" data-toggle="modal" data-target="#chat_open" title="Give Reminder" style="<?php echo $splCus; ?>">
                            <i class="fa fa-bell"></i>
                          </a>
                        <?php
                        } else { ?>
                          <a class="btn btn-default btn-xs btn_history" data-value="<?php echo $value->id; ?>" data-tic="<?php echo $value->id; ?>" data-owner="<?php echo $value->emp_accNo; ?>" data-toggle="modal" data-target="#chat_history" title="Reply" style="<?php echo $splCus; ?>">
                            <i class="fa fa-comment"></i>
                          </a>
                        <?php } ?>
                      </th>
                      <td> <?php echo $value->name; ?></td>
                      <td> <?php echo $value->date1; ?></td>
                      <td> <?php echo $value->date; ?></td>
                      <td> <?php echo $value->job; ?></td>
                    </tr>
                  <?php
                    $count++;
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>


  </section>
</div>

<!-------------- Reminder Model----------->
<div class="modal fade" id="reminder_model" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title">Set Reminder</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_update2">
          <input type="hidden" name="id_up" id="id_up" class="id_up">
          <input type="hidden" name="cid_up" id="cid_up">
          <input type="hidden" name="tel_no" id="tel_no">

          <div class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label for="reminder_date">Reminder Date<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control datechk" name="reminder_date" id="reminder_date" placeholder="Enter ..." autocomplete="off" required value="<?php print(date("Y-m-d")); ?>">
              </div>

              <div class="form-group">
                <label for="r_time">Reminder Time<font color="#FF0000"><strong>*</strong></font></label>
                <div class='input-group timepicker'>
                  <input type='text' class="form-control" name="r_time" id="r_time" value="<?php echo date('h:i A'); ?>" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-time"></span>
                  </span>
                </div>
              </div>

              <div class="form-group">
                <label for="note">Note <small class="req"> *</small></label>
                <textarea name="note" id="note" class="form-control" rows="5" required> </textarea>
              </div>
            </div>
          </div>

          <!-- /.box-body -->
          <div class="box-footer">
            <button class="btn btn-danger" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="chat_open" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title"> Chat Messages</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
          <div class="box-body">
            <input type="hidden" name="con_id" id="cont_id" value="">

            <div class="form-group">
              <label for="email">Message</label><small class="req"> *</small>
              <textarea name="message" id="compose-textarea" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Send</button>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="chat_history" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="col-md-12" id="chat_his">
          <!-- DIRECT CHAT SUCCESS -->
          <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Chat Messages</h3>

            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="direct-chat-messages" style="height:500px;">
                <strong>
                  <p class="chat_contact_no"></p>
                </strong>
                <div id="chat_list">


                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <form action="javascript:void(0);" id="form_send">
                <input type="hidden" name="id_up" id="id_up2">
                <input type="hidden" name="tic_id" id="tic_id">
                <div class="input-group">
                  <input type="text" name="reply_up" id="reply_up" placeholder="Type Message ..." class="form-control" required>
                  <span class="input-group-btn">
                    <button class="btn btn-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Send</button>
                  </span>
                </div>
              </form>
            </div><!-- /.box-footer-->
          </div>
          <!--/.direct-chat -->
        </div><!-- /.col -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close Chat</button>
      </div>
    </div>
  </div>
</div>

<script>
  /**************************** DATE ***********************/
  var date_format = 'yyyy-mm-dd';
  $(document).ready(function() {

    $(".datechk").datepicker({
      format: date_format,
      autoclose: true,
      todayHighlight: true
    });

    $('.timepicker').datetimepicker({
      format: 'LT'
    });
  });

  /**************************** BUTTONS  ****************************/
  function getButtons(reg_no, telNo, name, reminder_date, id, emp_accno, tic_id, splCus) {
    $('#number').text(telNo);
    $('#st_name').text(name);

    $('.response_set').show();
    $('.btn_register').attr('data-tel', telNo);
    $('.btn_register').attr('data-reg', reg_no);
    $('.btn_reject').val(reg_no);
    $('.btn_reject').attr('data-con', telNo);
    $('.clz_followup').val(telNo);
    $('.spl_cus').attr('data-tel', telNo);
    $('.spl_cus').attr('data-cid', id);
    $('#btn_reminder').attr('data-tel', telNo);
    $('#btn_reminder').attr('data-cid', id);
    $('#btn_directSale').attr('data-tel', telNo);
    $('#btn_directSale').attr('data-cid', id);

    if (splCus == '0') {
      $('.cus1').show();
      $('.cus2').hide();
    } else {
      $('.cus2').show();
      $('.cus1').hide();
    }

    $(document).scrollTop(0);
  }

  /********************************* DIRECT SALE ******************************/
  $("#btn_directSale").click(function() {
    var menu = $("#menu").val();
    var tp = $(this).attr('data-tel');

    swal({
        title: "Are you sure you want to move to Direct Sale?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, Move to direct sale!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_call_center/direct_sale", {
            shuffle_op: "data",
            tp: tp
          }, function(data) {
            if ($.trim(data) === 'success') {
              swal({
                title: "Successful!",
                text: "Successfully moved to direct sale!",
                type: "success",
                timer: 2000,
                showConfirmButton: false,
              });
              setTimeout(function() {
                window.location.href = "<?php echo base_url() ?>Con_pre_registry/index/" + menu;
              }, 2000);

            } else if ($.trim(data) === 'error') {
              swal({
                title: "",
                text: "Error!",
                type: "error",
                timer: 2000,
                showConfirmButton: false,
              });
              setTimeout(function() {
                window.location.href = "<?php echo base_url() ?>Con_pre_registry/index/" + menu;
              }, 2000);

            }
          });
        } else {
          swal("Cancelled", "", "error");
        }
      });
  });

  /******************************** REJECT ***************************/
  $(".btn_reject").click(function() {
    var menu = $("#menu").val();
    var wid = $("#wid").val();
    var reg_num = $(this).val();
    var telNo = $(this).attr('data-con');

    swal({
        title: "Are you sure you want to Reject?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, reject it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_pre_registry/feedback_reject", {
            feedback_yes: "data",
            wid: wid,
            telNo: telNo
          }, function(data) {
            if ($.trim(data) == 'success') {
              swal({
                  title: "Rejected!",
                  text: "Successfully Rejected!",
                  type: "success",
                  confirmButtonText: "OK"
                },
                function(isConfirm) {
                  if (isConfirm) {
                    window.location.href = "<?php echo base_url() ?>Con_pre_registry/index/" + menu;
                  }
                });

            } else if ($.trim(data) == ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          });
        } else {
          swal("Cancelled", "", "error");
        }
      });

  });

  /******************************** REJECT ***************************/
  $(".clz_followup").click(function() {
    var menu = $("#menu").val();
    var wid = $("#wid").val();
    var telNo = $(this).val();

    swal({
        title: "Are you sure you want to send to Class Follow-up list?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, send it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_pre_registry/clz_followup", {
            feedback_yes: "data",
            wid: wid,
            telNo: telNo
          }, function(data) {
            if ($.trim(data) == 'success') {
              swal({
                  title: "Successful!",
                  text: "Successfully Added to follow up list!",
                  type: "success",
                  confirmButtonText: "OK"
                },
                function(isConfirm) {
                  if (isConfirm) {
                    window.location.href = "<?php echo base_url() ?>Con_pre_registry/index/" + menu;
                  }
                });

            } else if ($.trim(data) == ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          });
        } else {
          swal("Cancelled", "", "error");
        }
      });

  });

  /********************************* REMINDER ******************************/
  $("#btn_reminder").click(function() {
    var tel = $(this).attr('data-tel');
    var cid = $(this).attr('data-cid');
    $(".id_up").val(cid);
    $("#cid_up").val(cid);
    $("#tel_no").val(tel);
  });

  /********************************* Edit ******************************/
  $("#form_update2").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_pre_registry/update_reminder",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal({
                title: "Updated!",
                text: "Successfully Updated!",
                type: "success",
                confirmButtonText: "OK"
              },
              function(isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url() ?>Con_pre_registry/index/" + menu;
                }
              });
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /********************************* REGISTER ******************************/
  $(".btn_register").click(function() {
    var reg = $(this).attr('data-reg');

    window.open("<?php echo base_url() ?>register/" + reg);

  });

  /**************************** INSERT  ****************************/
  $('.btn_ticket').click(function() {
    var con_id = $(this).attr('data-value');
    $("#cont_id").val(con_id);
  });


  $("#form_submit").on('submit', (function(e) {
    var menu = $("#menu").val();
    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/add_ticket",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'data exists') {
            swal({
              title: "",
              text: "Data Already Exists!",
              type: "warning",
              timer: 2000,
              showConfirmButton: false,
            });

          } else if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Added!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              location.reload();
            }, 2000);

          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /*********************************  TICKET ******************************/
  $("#form_send").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/send_reply",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal("Sent!", "Successfully Sent Message!", "success");
            location.reload();;
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  $('.btn_history').click(function() {
    var con_id = $(this).attr('data-value');
    $("#id_up2").val(con_id);
    var tic_id = $(this).attr('data-tic');
    $("#tic_id").val(tic_id);
    var owner = $(this).attr('data-owner');
    var acc_no = $('#acc_no').val();
    var val = $('#val').val();

    if ((acc_no == owner) || (val == 12 || val == 15)) {
      $('.btn_close').show();
    } else {
      $('.btn_close').hide();
    }


    $.post('<?php echo base_url() ?>Con_Call_Report/get_history', {
      'get_history': 'data',
      con_id: con_id
    }, function(data) {
      $('.chat_contact_no').text('');

      var Data = "";
      var reply_id = data.reply_id;


      if (data === undefined || data.length === 0 || data === null) {

        Data = '<div> <h4>This chat has no records!!</h4> </div>';
        $('#chat_list').html('').append(Data);

      } else {

        $.each(data.result, function(index, data) {
          var msg = "";
          var val = data.Val;
          var time = data.dt_time;
          var contact_no = data.con_no;
          var std_name = data.st_name;
          if (std_name == '') {
            var st_name = '';
          } else {
            var st_name = "Name: " + data.st_name + " | ";
          }
          var whatsapp = data.whatsapp;
          if (whatsapp == '') {
            var whatsapp = '';
          } else {
            var whatsapp = "Whatsapp: " + data.whatsapp;
          }
          $('.chat_contact_no').text(st_name + "\nContact No : " + contact_no + "\n | " + whatsapp);

          var user_type = '';
          var class_type = '';
          var user_name = '';

          if ((val == '12') || (val == '15')) {
            user_type = 'left';
            class_type = 'left';
            user_name = data.Name;
          } else {
            user_type = 'right';
            class_type = 'right';
            user_name = data.Name;
          }

          Data += '<div class="direct-chat-msg ' + user_type + '">';
          Data += '<div class="direct-chat-info clearfix">';
          Data += '<span class="direct-chat-name pull-' + class_type + '">' + user_name + ' &nbsp;</span>';
          Data += '<span class="direct-chat-timestamp pull-' + user_type + '">' + time + '&nbsp;</span>';
          Data += '</div>';
          Data += '<img class="direct-chat-img" src="<?php echo base_url(); ?>uploads/student_images/no_image.png" alt="message user image">';
          Data += '<div class="direct-chat-text">';
          Data += data.message;
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
        });

        $('#chat_list').html('').append(Data);
      }
    }, "json");
  });

  /******************************** CLOSE CHAT ***************************/
  $(".btn_close").click(function() {
    var tic_id = $("#tic_id").val();
    var id_up = $("#id_up2").val();
    var menu = $("#menu").val();

    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, close it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_Call_Report/close_chat", {
            close_chat: "data",
            tic_id: tic_id,
            id_up: id_up
          }, function(data) {
            if ($.trim(data.status) === 'success') {
              swal("Closed", "Successfully Closed Chat!", "success");
              window.location.href = "<?php echo base_url() ?>Con_call_center/index/" + menu;

            } else if ($.trim(data.status) === ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          }, "json");
        } else {
          swal("Cancelled", "", "error");
        }
      });
  });

  /********************************* SPECIAL CUSTOMER ******************************/
  $(".spl_cus").click(function() {
    var menu = $("#menu").val();
    var tp = $(this).attr('data-tel');

    var flag = $(this).attr('data-flag');

    if (flag == 1) {
      txt = "Special";
    } else {
      txt = "Regular";
    }

    swal({
        title: "Are you sure you want to make a " + txt + " customer?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, make a " + txt + " customer!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_call_center/spl_customer", {
            shuffle_op: "data",
            tp: tp,
            flag: flag
          }, function(data) {
            if ($.trim(data) === 'success') {
              swal({
                title: "Successful!",
                text: "Successfully made a " + txt + " Customer!",
                type: "success",
                timer: 2000,
                showConfirmButton: false,
              });
              setTimeout(function() {
                window.location.href = "<?php echo base_url() ?>Con_pre_registry/index/" + menu;
              }, 2000);

            } else if ($.trim(data) === 'error') {
              swal({
                title: "",
                text: "Error!",
                type: "error",
                timer: 2000,
                showConfirmButton: false,
              });
              setTimeout(function() {
                window.location.href = "<?php echo base_url() ?>Con_pre_registry/index/" + menu;
              }, 2000);

            }
          });
        } else {
          swal("Cancelled", "", "error");
        }
      });
  });
</script>