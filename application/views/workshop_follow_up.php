<?php
$menu = $this->uri->segment(3);
?>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1> <i class="fa fa-sitemap"></i> Class Follow Up
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
        <?php
        if ($val == 12 || $val == 15) {
        ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12 col-sm-6">
                  <div class="row">
                    <form role="form" action="<?php echo site_url('Con_zoom_upload/workshop_follow_up/') ?><?php echo $menu . "/" ?>" method="post" class="">

                      <div class="form-group col-md-6">
                        <label for="agent">Agent</label>
                        <select id="agent" name="agent" class="form-control">
                          <option value="">Select</option>
                          <?php foreach ($load_agent as $agent) { ?>
                            <option value="<?php echo $agent->Acc_No ?>"><?php echo $agent->Name ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group col-md-6">
                        <label for="agent">Workshop</label>
                        <select id="workshop" name="workshop" class="form-control">
                          <option value="">Select</option>
                          <?php foreach ($load_workshop as $workshop) { ?>
                            <option value="<?php echo $workshop->id ?>"><?php echo $workshop->w_name . " (" . $workshop->w_date . " / " . date("h:i A", strtotime($workshop->w_time)) . ")"; ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="col-sm-12">
                        <div class="form-group">
                          <button name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle btn-search"><i class="fa fa-search"></i> Search</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Actions</h3>
          </div>
          <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
          <input type="hidden" name="num" id="num" value="<?php echo  $this->uri->segment(4); ?>">
          <form class="form-sample" id="form_con_no" action="javascript:void(0);">
            <div class="box-body">
              <div class="form-group" align="center">
                <label id="number" style="font-size: 30px;"></label><br>
                <label id="st_name" style="font-size: 30px;"></label>
              </div>
            </div>
            <div class="box-body">
              <div class="form-group response_set" align="center" style="font-size: 15px; display: none;">
                <button type="button" class="btn btn-sm btn-danger btn_reject" value="" data-con='' data-workshop=> <i class="fa fa-close"> Reject </i></button> &nbsp;&nbsp;

                <button type="button" class="btn btn-sm btn-info" id="btn_reminder" data-tel='' title="Set Reminder" data-toggle="modal" data-target="#reminder_model" style="display: none;"><i class="fa fa-clock-o"></i> Reminder</button> &nbsp;&nbsp;

                <button type="button" class="btn btn-sm btn-info btn_deposit" id='' data-reg="" data-deposit="" data-tel="" data-whatsapp="" data-name='' data-nic="" course-fee='' usdt-val="" usdt-marked="" title="Add Bank Deposit" data-toggle="modal" data-target="#deposit_model" style="background-color: #0a679f; border-color: #0a679f;"><i class="fa fa-money"></i> Bank Deposit</button> &nbsp;&nbsp;

              </div>
            </div>
          </form>

        </div>

        <?php if (isset($load_data)) { ?>
          <div class="box box-info">
            <div class="box-body">
              <form action="javascript:void(0);" method="post">
                <div class="table-responsive ptt10">
                  <table class="table table-hover table-striped example">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Reg No</th>
                        <th>Contact No</th>
                        <th>Whatsapp</th>
                        <th>Chat</th>
                        <?php
                        if ($val == 13) {
                        ?>
                          <th>Transfer Agent</th>
                        <?php } ?>
                        <th>Name</th>
                        <th>NIC</th>
                        <th>CR No</th>
                        <th>Accepted Date</th>
                        <th>Job</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php
                      $count = 1;

                      foreach ($load_data as $value) {
                        $p_flag = '111';

                        $spl_cus = $value->spl_cus;
                        if ($spl_cus == 1) {
                          $splCus = "background-color: red; color:#fff;";
                        } else {
                          $splCus = "";
                        }

                        $user_id = $value->user_id;
                        $usdt = $value->usdt_val;
                        $tot_dep = $value->deposits;
                        $pay_flag = $value->pay_flag;
                        $txt = '';

                        if ($usdt == '1') {
                          if ($tot_dep >= $course_fee) {
                            $txt = "#4caf5054"; //full payment
                            if ($p_flag == 0) {
                              // $dis = "display:none;";
                              $dis = "";
                            } else {
                              $dis = "";
                            }
                          } else if ($tot_dep == '') {
                            $txt = '';
                            $dis = '';
                          } else {
                            $txt = "#f3df0054"; //part payment
                            $dis = '';
                          }
                        } else {
                          if ($tot_dep == intval($course_fee) - intval(2500)) {
                            $txt = "#4caf5054"; //full payment
                            if ($p_flag == 0) {
                              // $dis = "display:none;";
                              $dis = "";
                            } else {
                              $dis = "";
                            }
                          } else if ($tot_dep == '') {
                            $txt = '';
                            $dis = "";
                          } else {
                            $txt = "#f3df0054"; //part payment
                            $dis = "";
                          }
                        }

                        // if ($pay_flag == 2) {
                        //   $txt = '#ff5722d1'; //payment rejected
                        //   $dis = "";
                        // }

                        // if ($user_id == '1') {
                        //   $txt = "#fbf9b6";
                        // } else if ($user_id == '18') {
                        //   $txt = "#c0ffc2";
                        // } else if ($user_id == '23') {
                        //   $txt = "#c0d9ff";
                        // } else if ($user_id == '19') {
                        //   $txt = "#fbf9b6";
                        // } else {
                        //   $txt = "#fff";
                        // }

                        // $site_flag = $value->site_flag;
                        // if ($site_flag == "1") {
                        //   $txt = "#cdba8e";
                        // }
                      ?>
                        <tr style="cursor: pointer; background-color:<?php echo $txt; ?> ;<?php echo $dis; ?>" onclick="getButtons('<?php echo $value->reg_no; ?>', '<?php echo $value->con_no; ?>', '<?php echo $value->workshop; ?>', '<?php echo $value->deposits; ?>', ' <?php echo $value->name; ?>', '<?php echo $value->id; ?>',  '<?php echo $value->whatsapp; ?>', '<?php echo $course_fee; ?>', '<?php echo $value->town; ?>', '<?php echo $value->usdt_val; ?>', '<?php echo $value->usdt_marked; ?>', '<?php echo $value->nic; ?>')">
                          <td class="mailbox-name"> <?php echo $count; ?>.</td>
                          <td class="mailbox-name"> <?php echo $value->reg_no; ?></td>
                          <td class="mailbox-name"> <?php echo $value->con_no; ?></td>
                          <td class="mailbox-name"> <?php echo $value->whatsapp; ?></td>
                          <th>
                            <?php
                            $reminder = $value->reminder_date;
                            if (($value->tic_id == '' || $value->tic_id == '0') && ($value->reminder_date == '0000-00-00' || empty($value->reminder_date))) {
                            ?>
                              <a class="btn btn-default btn-xs btn_ticket" data-value="<?php echo $value->id; ?>" data-toggle="modal" data-target="#chat_open" title="Give Reminder" style="<?php echo $splCus; ?>">
                                <i class="fa fa-bell"></i>
                              </a>
                            <?php
                            } else { ?>
                              <a class="btn btn-default btn-xs btn_history" data-value="<?php echo $value->id; ?>" data-tic="<?php echo $value->id; ?>" data-owner="<?php echo $value->emp_accNo; ?>" data-toggle="modal" data-target="#chat_history" title="Reply" style="<?php echo $splCus; ?>">
                                <i class="fa fa-comment"></i>
                              </a>
                            <?php } ?>
                          </th>
                          <?php
                          if ($val == 13) {
                          ?>
                            <td>
                              <a class="btn btn-success btn-xs btn_tansfer" data-value="<?php echo $value->id; ?>" data-toggle="modal" data-target="#transfer_agent" title="Transfer Agent">
                                <i class="fa fa-group"></i>
                              </a>
                            </td>
                          <?php } ?>
                          <td class="mailbox-name"> <?php echo $value->name; ?></td>
                          <td class="mailbox-name"> <?php echo $value->nic; ?></td>
                          <td class="mailbox-name"> <?php echo $value->inv_no; ?></td>
                          <td class="mailbox-name"> <?php echo $value->date1; ?></td>
                          <td class="mailbox-name"> <?php echo $value->job; ?></td>
                        </tr>
                      <?php
                        $count++;
                        // }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
</div>

<!-------------- Transfer Agent ----------->
<div class="modal fade" id="transfer_agent" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title"> Transfer Agent</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="agent_transfer" accept-charset="utf-8">
          <div class="box-body">
            <input type="hidden" name="con_id2" id="cont_id2" value="">

            <div class="form-group">
              <label for="agent">Agent</label>
              <select id="agent" name="agent" class="form-control" required>
                <option value="">Select</option>
                <?php foreach ($load_agent as $agent) { ?>
                  <option value="<?php echo $agent->Acc_No ?>"><?php echo $agent->Name ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Transfer</button>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<!-------------- Reminder Model----------->
<div class="modal fade" id="reminder_model" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title">Set Reminder</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_update">
          <input type="hidden" name="id_up" id="id_up">

          <div class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label for="reminder_date">Reminder Date<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control datechk" name="reminder_date" id="reminder_date" placeholder="Enter ..." autocomplete="off" required value="<?php print(date("Y-m-d")); ?>">
              </div>

              <div class="form-group">
                <label for="note">Note <small class="req"> *</small></label>
                <textarea name="note" id="note" class="form-control" rows="5" required> </textarea>
              </div>
            </div>
          </div>

          <!-- /.box-body -->
          <div class="box-footer">
            <button class="btn btn-danger" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-------------- BANK DEPOSIT Model----------->
<div class="modal fade" id="deposit_model" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-cls" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title">Add Bank Deposits</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_deposit">
          <input type="hidden" name="wr_num" id="wr_num">
          <input type="hidden" name="tot_deposit" id="tot_deposit">
          <input type="hidden" name="tot_course_fee" id="tot_course_fee">
          <input type="hidden" name="balance_amount" id="balance_amount">
          <input type="hidden" name="usdt_vall" id="usdt_vall">
          <input type="hidden" name="usdt_marked" id="usdt_marked">

          <div class="form-horizontal">
            <div class="box-body">
              <label class="pull-right balance_amt">Balance to be paid: LKR</label>
              <br>
              <div class="row">
                <div class="form-group col-md-12">
                  <label>Name<font color="#FF0000"><strong>*</strong></font></label>
                  <input type="text" class="form-control" name="name_ini" id="name_ini" placeholder="" autocomplete="off" required>
                </div>

                <div class="form-group col-md-12">
                  <label>NIC<font color="#FF0000"><strong>*</strong></font></label>
                  <input type="text" class="form-control" name="nic" id="nic" placeholder="" autocomplete="off" required>
                </div>
                <div class="form-group col-md-12">
                  <label>Contact No</label>
                  <input type="text" class="form-control" name="con_noo" id="con_noo" placeholder="" autocomplete="off" readonly>
                </div>
                <div class="form-group col-md-12">
                  <label>WhatsApp</label>
                  <input type="text" class="form-control" name="whatsappp" id="whatsappp" placeholder="" autocomplete="off" readonly>
                </div>
                <div class="form-group col-md-12">
                  <label>Town<font color="#FF0000"><strong>*</strong></font></label>
                  <input type="text" class="form-control" name="town" id="town" placeholder="" autocomplete="off" required>
                </div>
                <div class="form-group col-md-12" style="display: none;">
                  <label>Include USDT<small class="req"> *</small>&nbsp;&nbsp;&nbsp;</label>
                  <label class="radio-inline">
                    <input type="checkbox" name="is_usdt" id="is_usdt" value="1"> USDT
                    <input type="hidden" class="form-control" name="usdt_val" id="usdt_val">
                  </label>
                </div>
              </div>

              <div class="form-group">
                <label for="bank">Bank<font color="#FF0000"><strong>*</strong></font></label>
                <select id="bank" name="bank" class="form-control" required>
                  <option>BOC</option>
                  <option>Commercial</option>
                  <option>Crypto</option>
                  <option>Online Payment</option>
                  <option>KOKO</option>
                  <option>KOKO and Bank</option>
                  <option>Video Sales</option>
                </select>
              </div>

              <div class="form-group">
                <label for="p_type">Payment Type<font color="#FF0000"><strong>*</strong></font></label>
                <select id="p_type" name="p_type" class="form-control" required>
                </select>
              </div>

              <div class="form-group koko_pay" style="display: none;">
                <label>KOKO Amount<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control koko_amt" name="koko_amt" id="koko_amt" placeholder="Enter Amount" autocomplete="off" onkeypress="return isNumberKey(event, this)">
              </div>

              <div class="form-group koko_pay" style="display: none;">
                <label>Bank Amount<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control bank_amt" name="bank_amt" id="bank_amt" placeholder="Enter Amount" autocomplete="off" onkeypress="return isNumberKey(event, this)">
              </div>

              <div class="form-group">
                <label>Description</label>
                <input type="text" class="form-control" name="desc" id="desc" placeholder="" autocomplete="off" autofocus>
              </div>

              <div class="form-group">
                <label>Deposited Amount<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control manual" name="manual_amt" id="manual_amt" placeholder="Enter Amount" autocomplete="off" required onkeypress="return isNumberKey(event, this)">
              </div>

            </div>
          </div>

          <!-- /.box-body -->
          <div class="box-footer">
            <button class="btn btn-danger btn_update" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="chat_open" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title"> Chat Messages</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
          <div class="box-body">
            <input type="hidden" name="con_id" id="cont_id" value="">

            <div class="form-group">
              <label for="email">Message</label><small class="req"> *</small>
              <textarea name="message" id="compose-textarea" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Send</button>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="chat_history" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="col-md-12" id="chat_his">
          <!-- DIRECT CHAT SUCCESS -->
          <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Chat Messages</h3>

            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="direct-chat-messages" style="height:500px;">
                <strong>
                  <p class="chat_contact_no"></p>
                </strong>
                <div id="chat_list">


                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <form action="javascript:void(0);" id="form_send">
                <input type="hidden" name="id_up" id="id_up2">
                <input type="hidden" name="tic_id" id="tic_id">
                <div class="input-group">
                  <input type="text" name="reply_up" id="reply_up" placeholder="Type Message ..." class="form-control" required>
                  <span class="input-group-btn">
                    <button class="btn btn-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Send</button>
                  </span>
                </div>
              </form>
            </div><!-- /.box-footer-->
          </div>
          <!--/.direct-chat -->
        </div><!-- /.col -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close Chat</button>
      </div>
    </div>
  </div>
</div>

<script>
  /**************************** DATE ***********************/
  var date_format = 'yyyy-mm-dd';
  $(document).ready(function() {

    $(".datechk").datepicker({
      format: date_format,
      autoclose: true,
      todayHighlight: true
    });

    $('.timepicker').datetimepicker({
      format: 'LT'
    });
  });

  function isNumberKey(evt, obj) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
      if (charCode == 46) return false;
    if (charCode == 46) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  /**************************** BUTTONS  ****************************/
  function getButtons(reg_no, telNo, workshop, deposits, name, id, whatsapp, course_fee, town, usdt_vall, usdt_marked, nic) {
    $('#number').text(telNo);
    $('#st_name').text(name);

    $('.response_set').show();
    $('.btn_reject').val(reg_no);
    $('.btn_reject').attr('data-con', telNo);
    $('.btn_reject').attr('data-workshop', workshop);
    $('#btn_reminder').attr('data-tel', id);
    $('.btn_deposit').attr('data-reg', reg_no);
    $('.btn_deposit').attr('data-tel', telNo);
    $('.btn_deposit').attr('course-fee', course_fee);
    $('.btn_deposit').attr('usdt-val', usdt_vall);
    $('.btn_deposit').attr('usdt-marked', usdt_marked);
    $('#town').val(town);

    if (deposits == '' || ($(deposits).is(':empty'))) {
      dep = '0';
    } else {
      dep = deposits;
    }
    $('.btn_deposit').attr('data-deposit', dep);
    $('.btn_deposit').attr('data-name', $.trim(name));
    $('.btn_deposit').attr('data-nic', $.trim(nic));
    $('.btn_deposit').attr('data-whatsapp', whatsapp);

    $(document).scrollTop(0);
  }

  /******************************** REJECT ***************************/
  $(".btn_reject").click(function() {
    var menu = $("#menu").val();
    var wid = $(this).attr('data-workshop');
    var telNo = $(this).attr('data-con');
    var reg_num = $(this).val();

    swal({
        title: "Are you sure you want to Reject?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, reject it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_pre_registry/feedback_reject", {
            feedback_yes: "data",
            wid: wid,
            telNo: telNo
          }, function(data) {
            if ($.trim(data) == 'success') {
              swal({
                  title: "Rejected!",
                  text: "Successfully Rejected!",
                  type: "success",
                  confirmButtonText: "OK"
                },
                function(isConfirm) {
                  if (isConfirm) {
                    window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                  }
                });

            } else if ($.trim(data) == ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          });
        } else {
          swal("Cancelled", "", "error");
        }
      });

  });

  /********************************* REMINDER ******************************/
  $("#btn_reminder").click(function() {
    var id = $(this).attr('data-tel');
    $("#id_up").val(id);
  });

  /********************************* Edit ******************************/
  $("#form_update").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_call_center/update_reminder",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal({
                title: "Updated!",
                text: "Successfully Updated!",
                type: "success",
                confirmButtonText: "OK"
              },
              function(isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                }
              });
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));


  $("#koko_amt").focusout(function() {
    var dep_amt = $bank_amt = $koko_amt = 0;
    var bank_amt = $("#bank_amt").val();
    var koko_amt = $("#koko_amt").val();

    if (bank_amt == '') {
      bank_amt = 0;
    }
    if (koko_amt == '') {
      koko_amt = 0;
    }

    dep_amt += parseInt(bank_amt);
    dep_amt += parseInt(koko_amt);

    $("#manual_amt").val(dep_amt);
  });

  $("#bank_amt").focusout(function() {
    var dep_amt = $bank_amt = $koko_amt = 0;
    var bank_amt = $("#bank_amt").val();
    var koko_amt = $("#koko_amt").val();

    if (bank_amt == '') {
      bank_amt = 0;
    }
    if (koko_amt == '') {
      koko_amt = 0;
    }

    dep_amt += parseInt(bank_amt);
    dep_amt += parseInt(koko_amt);

    $("#manual_amt").val(dep_amt);
  });





  /********************************* BANK DEPOSIT ******************************/
  $("#form_deposit").on('submit', (function(e) {
    var menu = $("#menu").val();
    var dep_amt = $("#manual_amt").val();
    var desc = $("#desc").val();
    var bal_amt = $("#balance_amount").val();
    var course_fee = $('.btn_deposit').attr('course-fee');
    var p_type = $('#p_type').val();
    var bank_amt = $('#bank_amt').val();
    var koko_amt = $('#kok_amt').val();

    if (parseInt(dep_amt) != '0') {

      if (p_type == 'Advanced Payment') {
        if ($('#is_usdt').is(':checked')) {
          c_fee = $('#tot_course_fee').val();
          if (parseInt(dep_amt) >= parseInt(c_fee)) {
            swal("Please change amount to make an advanced payment!");
          } else {
            if (parseInt(dep_amt) > parseInt(bal_amt)) {
              swal("Please enter a valid amount! The deposited amount can't exceed the balance amount!");
            } else if (dep_amt != course_fee) {
              $('.btn_update').prop("disabled", true);

              e.preventDefault();
              $.ajax({
                url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {},
                success: function(data) {
                  $('#edit_data').modal('hide')
                  if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                  } else {
                    if ($.trim(data) === 'error') {
                      swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                      swal({
                          title: "Sucess!",
                          text: "Successfully Added Details!",
                          type: "success",
                          confirmButtonText: "OK"
                        },
                        function(isConfirm) {
                          if (isConfirm) {
                            window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                          }
                        });
                    }
                  }
                },
                error: function(e) {
                  alert("err2");
                }
              });

            } else {
              $('.btn_update').prop("disabled", true);
              e.preventDefault();
              $.ajax({
                url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {},
                success: function(data) {
                  $('#edit_data').modal('hide')
                  if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                  } else {
                    if ($.trim(data) === 'error') {
                      swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                      swal({
                          title: "Sucess!",
                          text: "Successfully Added Details!",
                          type: "success",
                          confirmButtonText: "OK"
                        },
                        function(isConfirm) {
                          if (isConfirm) {
                            window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                          }
                        });
                    }
                  }
                },
                error: function(e) {
                  alert("err2");
                }
              });
            }
          }
        } else {
          cc_fee = $('#tot_course_fee').val();
          c_fee = (parseInt(cc_fee) - parseInt(2500))
          if (parseInt(dep_amt) >= parseInt(c_fee)) {
            swal("Please change amount to make an advanced payment!");
          } else {
            if (parseInt(dep_amt) > parseInt(bal_amt)) {
              swal("Please enter a valid amount! The deposited amount can't exceed the balance amount!");
            } else if (dep_amt != course_fee) {
              $('.btn_update').prop("disabled", true);
              e.preventDefault();
              $.ajax({
                url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {},
                success: function(data) {
                  $('#edit_data').modal('hide')
                  if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                  } else {
                    if ($.trim(data) === 'error') {
                      swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                      swal({
                          title: "Sucess!",
                          text: "Successfully Added Details!",
                          type: "success",
                          confirmButtonText: "OK"
                        },
                        function(isConfirm) {
                          if (isConfirm) {
                            window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                          }
                        });
                    }
                  }
                },
                error: function(e) {
                  alert("err2");
                }
              });

            } else {
              $('.btn_update').prop("disabled", true);
              e.preventDefault();
              $.ajax({
                url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {},
                success: function(data) {
                  $('#edit_data').modal('hide')
                  if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                  } else {
                    if ($.trim(data) === 'error') {
                      swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                      swal({
                          title: "Sucess!",
                          text: "Successfully Added Details!",
                          type: "success",
                          confirmButtonText: "OK"
                        },
                        function(isConfirm) {
                          if (isConfirm) {
                            window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                          }
                        });
                    }
                  }
                },
                error: function(e) {
                  alert("err2");
                }
              });
            }
          }
        }
      } else if (p_type == 'Full Payment') {
        if ($('#is_usdt').is(':checked')) {
          c_fee = $('#tot_course_fee').val();
          if (parseInt(dep_amt) != parseInt(c_fee)) {
            swal("Please change amount to make a full payment!");
          } else {
            if (parseInt(dep_amt) > parseInt(bal_amt)) {
              swal("Please enter a valid amount! The deposited amount can't exceed the balance amount!");
            } else if (dep_amt != course_fee) {
              $('.btn_update').prop("disabled", true);
              e.preventDefault();
              $.ajax({
                url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {},
                success: function(data) {
                  $('#edit_data').modal('hide')
                  if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                  } else {
                    if ($.trim(data) === 'error') {
                      swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                      swal({
                          title: "Sucess!",
                          text: "Successfully Added Details!",
                          type: "success",
                          confirmButtonText: "OK"
                        },
                        function(isConfirm) {
                          if (isConfirm) {
                            window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                          }
                        });
                    }
                  }
                },
                error: function(e) {
                  alert("err2");
                }
              });

            } else {
              $('.btn_update').prop("disabled", true);
              e.preventDefault();
              $.ajax({
                url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {},
                success: function(data) {
                  $('#edit_data').modal('hide')
                  if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                  } else {
                    if ($.trim(data) === 'error') {
                      swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                      swal({
                          title: "Sucess!",
                          text: "Successfully Added Details!",
                          type: "success",
                          confirmButtonText: "OK"
                        },
                        function(isConfirm) {
                          if (isConfirm) {
                            window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                          }
                        });
                    }
                  }
                },
                error: function(e) {
                  alert("err2");
                }
              });
            }
          }
        } else {
          cc_fee = $('#tot_course_fee').val();
          c_fee = (parseInt(cc_fee) - parseInt(2500))
          if (parseInt(dep_amt) != parseInt(c_fee)) {
            swal("Please change amount to make a full payment!");
          } else {
            if (parseInt(dep_amt) > parseInt(bal_amt)) {
              swal("Please enter a valid amount! The deposited amount can't exceed the balance amount!");
            } else if (dep_amt != course_fee) {
              $('.btn_update').prop("disabled", true);
              e.preventDefault();
              $.ajax({
                url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {},
                success: function(data) {
                  $('#edit_data').modal('hide')
                  if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                  } else {
                    if ($.trim(data) === 'error') {
                      swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                      swal({
                          title: "Sucess!",
                          text: "Successfully Added Details!",
                          type: "success",
                          confirmButtonText: "OK"
                        },
                        function(isConfirm) {
                          if (isConfirm) {
                            window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                          }
                        });
                    }
                  }
                },
                error: function(e) {
                  alert("err2");
                }
              });

            } else {
              $('.btn_update').prop("disabled", true);
              e.preventDefault();
              $.ajax({
                url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {},
                success: function(data) {
                  $('#edit_data').modal('hide')
                  if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                  } else {
                    if ($.trim(data) === 'error') {
                      swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                      swal({
                          title: "Sucess!",
                          text: "Successfully Added Details!",
                          type: "success",
                          confirmButtonText: "OK"
                        },
                        function(isConfirm) {
                          if (isConfirm) {
                            window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                          }
                        });
                    }
                  }
                },
                error: function(e) {
                  alert("err2");
                }
              });
            }
          }
        }

      } else {
        if (parseInt(dep_amt) > parseInt(bal_amt)) {
          swal("Please enter a valid amount! The deposited amount can't exceed the balance amount!");
        } else if (dep_amt != bal_amt) {
          swal("Please enter a valid amount! The deposited amount and the balance amount must be equal!");
        } else if (dep_amt == course_fee) {
          $('.btn_update').prop("disabled", true);
          e.preventDefault();
          $.ajax({
            url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
              $('#edit_data').modal('hide')
              if ($.trim(data) === '') {
                swal("Oops...", "Something went wrong!", "warning");

              } else {
                if ($.trim(data) === 'error') {
                  swal("SQL Error!", "Please Try Again!", "warning");

                } else if ($.trim(data) === 'success') {
                  swal({
                      title: "Sucess!",
                      text: "Successfully Added Details!",
                      type: "success",
                      confirmButtonText: "OK"
                    },
                    function(isConfirm) {
                      if (isConfirm) {
                        window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                      }
                    });
                }
              }
            },
            error: function(e) {
              alert("err2");
            }
          });

        } else {
          $('.btn_update').prop("disabled", true);
          e.preventDefault();
          $.ajax({
            url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
              $('#edit_data').modal('hide')
              if ($.trim(data) === '') {
                swal("Oops...", "Something went wrong!", "warning");

              } else {
                if ($.trim(data) === 'error') {
                  swal("SQL Error!", "Please Try Again!", "warning");

                } else if ($.trim(data) === 'success') {
                  swal({
                      title: "Sucess!",
                      text: "Successfully Added Details!",
                      type: "success",
                      confirmButtonText: "OK"
                    },
                    function(isConfirm) {
                      if (isConfirm) {
                        window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                      }
                    });
                }
              }
            },
            error: function(e) {
              alert("err2");
            }
          });
        }
      }
    }
  }));

  /********************************* REGISTER ******************************/
  $("#btn_register").click(function() {
    var reg = $(this).attr('data-reg');

    window.open("<?php echo base_url() ?>register/" + reg);

  });

  /********************************* CLASS TIMETABLE ******************************/
  $(".btn_class").click(function() {
    var reg = $(this).attr('data-reg');

    window.open("<?php echo base_url() ?>Con_post_registry/class_register/" + reg);

  });

  /**************************** DEPOSIT ***********************/
  $(".btn_deposit").click(function() {
    var reg = $(this).attr('data-reg');
    var name = $(this).attr('data-name');
    var nic = $(this).attr('data-nic');
    var con_no = $(this).attr('data-tel');
    var whatsapp = $(this).attr('data-whatsapp');
    var tot_course_fee = $(this).attr('course-fee');
    var depo = $(this).attr('data-deposit');
    var usdt_val = $(this).attr('usdt-val');
    var usdt_marked = $(this).attr('usdt-marked');
    $('#wr_num').val(reg);
    $('#tot_course_fee').val(tot_course_fee);
    $('#name_ini').val($.trim(name));
    $('#nic').val($.trim(nic));
    $('#con_noo').val(con_no);
    $('#whatsappp').val(whatsapp);
    $('#usdt_vall').val(usdt_vall);
    $('#usdt_marked').val(usdt_marked);
    $('#manual_amt').val('');
    $('#desc').val('');
    $('#p_type').html("");

    if (usdt_val == 1) {
      $("#is_usdt").prop('checked', true);
      $("#usdt_val").val('1');
    } else {
      $("#is_usdt").prop('checked', false);
      $("#usdt_val").val('0');
    }

    var balance = 0;

    $.post("<?php echo base_url() ?>Con_zoom_upload/get_accepted_deposits", {
      reg: reg,
      usdt_val: usdt_val,
      usdt_marked: usdt_marked,
      depo: depo
    }, function(data) {
      $.each(data.result, function(index, data) {
        deposit = $.trim(data.totdep);
        if (deposit == '' || ($(deposit).is(':empty'))) {
          dep = '0';
        } else {
          dep = deposit;
        }
        $('.btn_deposit').attr('data-deposit', dep);
        depp = $('#tot_deposit').val(dep);

        if (usdt_marked == 1) {
          $("#is_usdt").attr("disabled", true);
        } else {
          $("#is_usdt").attr("disabled", false);
        }

        if (($.trim(dep) == '') || ($.trim(dep) == '0') || ($(dep).is(':empty'))) {
          deposit = '0';
          $("#manual_amt").attr("readonly", false);
        } else {
          deposit = parseInt(dep);
          $("#manual_amt").attr("readonly", true);
        }

        tot_course_fee = '17500';

        if (usdt_val == 1) {
          $("#is_usdt").prop('checked', true);
          $("#usdt_val").val('1');
          balance = parseInt(tot_course_fee) - parseInt(deposit);
          $("#manual_amt").val(balance);

          if (parseInt(deposit) < parseInt(tot_course_fee)) {
            $('.balance_amt').text("Balance to be paid: LKR " + balance);
            $('#balance_amount').val(balance);
          } else {
            $('.balance_amt').text("Balance to be paid: LKR " + balance);
            $('#balance_amount').val(balance);
          }
        } else {
          $("#usdt_val").val('0');
          balance = (parseInt(tot_course_fee) - parseInt(2500)) - parseInt(deposit);
          $("#manual_amt").val(balance);

          if (deposit <= tot_course_fee) {
            $('.balance_amt').text("Balance to be paid: LKR " + balance);
            $('#balance_amount').val(balance);
          } else {
            $('.balance_amt').text("Balance to be paid: LKR " + balance);
            $('#balance_amount').val(balance);
          }
        }

        p_data = '';
        if (parseInt(balance) == '17500' || parseInt(balance) == '15000') {
          p_data = '';
          $("#is_usdt").attr("disabled", false);
          p_data = "<option>Full Payment</option> <option>Advanced Payment</option>";
          $('#p_type').append(p_data);
        } else {
          p_data = '';
          $("#is_usdt").attr("disabled", true);
          p_data = "<option>Balance Payment</option>";
          $("#p_type").css("pointer-events", "none");
          $('#p_type').append(p_data);
        }

      });
    }, "json");
  });

  $(document).on('change', '#p_type', function(e) {
    p_type = $('#p_type').val();
    balance = $('#balance_amount').val();
    if(p_type == 'Balance Payment'){
      $("#manual_amt").attr("readonly", true);
      $("#manual_amt").val(balance);

    }else{
      if (($.trim(balance) == '') || ($.trim(balance) == '0') || ($(balance).is(':empty'))) {
        $("#manual_amt").attr("readonly", false);
      } else {
        if (parseInt(balance) == '17500' || parseInt(balance) == '15000') {
          $("#manual_amt").attr("readonly", false);
        } else {
          $("#manual_amt").attr("readonly", true);
        }
      }
    }
  });

  $(document).on('change', '#bank', function(e) {
    bank = $('#bank :selected').text();

    if (bank == 'KOKO') {
      $('.koko_pay').hide();
      $("#koko_amt").prop('required', false);
      $("#bank_amt").prop('required', false);
      if ($('#p_type').val() == 'Balance Payment') {
        $("#p_type").css("pointer-events", "none");
        $("#manual_amt").attr("readonly", true);
      } else {
        $("#p_type").css("pointer-events", "auto");
        $("#manual_amt").attr("readonly", false);
      }

    } else if (bank == 'KOKO and Bank') {
      if ($('#p_type').val() == 'Balance Payment') {
        $("#p_type").css("pointer-events", "none");
        $("#manual_amt").attr("readonly", true);
      } else {
        $("#p_type").css("pointer-events", "auto");
        $("#manual_amt").attr("readonly", false);
      }
      $('.koko_pay').show();
      $("#koko_amt").prop('required', true);
      $("#bank_amt").prop('required', true);

    } else if (bank == 'Video Sales') {
      $("#manual_amt").val('17500');
      $("#manual_amt").attr("readonly", true);
      $('#p_type').val('Full Payment');
      $("#p_type").css("pointer-events", "none");
      $('.koko_pay').hide();
      $("#koko_amt").prop('required', false);
      $("#bank_amt").prop('required', false);

    } else {
      if ($('#p_type').val() == 'Balance Payment') {
        $("#p_type").css("pointer-events", "none");
        $("#manual_amt").attr("readonly", true);
      } else {
        $("#p_type").css("pointer-events", "auto");
        $("#manual_amt").attr("readonly", false);
      }
      $('.koko_pay').hide();
      $("#koko_amt").prop('required', false);
      $("#bank_amt").prop('required', false);
    }
  });

  $(document).ready(function() {

    $('#is_usdt').change(function() {
      depp = $('#tot_deposit').val();
      if ($.trim(depp) == '' || $.trim(depp) == '0') {
        deposit = '0';
      } else {
        deposit = depp;
      }
      tot_course_fee = parseInt($('#tot_course_fee').val());
      if (parseInt(deposit) <= parseInt(tot_course_fee)) {
        if (this.checked) {
          $("#usdt_val").val('1');
          balance = tot_course_fee - deposit;
          $('.balance_amt').text("Balance to be paid: LKR " + balance);
          $('#balance_amount').val(balance);
          $('#manual_amt').val(balance);
        } else {
          $("#usdt_val").val('0');
          balance = (tot_course_fee - 2500) - deposit;
          $('.balance_amt').text("Balance to be paid: LKR " + balance);
          $('#balance_amount').val(parseInt(balance));
          $('#manual_amt').val(balance);
        }
      } else {
        $('.balance_amt').text("");
        $('#balance_amount').val('');
      }
    });
  });
  /**************************** SEND SMS ***********************/
  $(".btn_sms").click(function() {
    var menu = $("#menu").val();
    var reg = $(this).attr('data-reg');
    $('#wr_num').val(reg);

    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, Send  SMS!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_pre_registry/class_sms", {
            delete: "data",
            reg: reg
          }, function(data) {
            if ($.trim(data.status) === 'success') {
              swal({
                  title: "Sent!",
                  text: "Successfully Sent!",
                  type: "success",
                  confirmButtonText: "OK"
                },
                function(isConfirm) {
                  if (isConfirm) {
                    window.location.href = "<?php echo base_url() ?>Con_workshop/workshop_link/" + menu;
                  }
                });

            } else if ($.trim(data.status) === ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          }, "json");
        } else {
          swal("Cancelled", "", "error");
        }
      });

  });

  /**************************** INSERT  ****************************/
  $('.btn_ticket').click(function() {
    var con_id = $(this).attr('data-value');
    $("#cont_id").val(con_id);
  });


  $("#form_submit").on('submit', (function(e) {
    var menu = $("#menu").val();
    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/add_ticket",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'data exists') {
            swal({
              title: "",
              text: "Data Already Exists!",
              type: "warning",
              timer: 2000,
              showConfirmButton: false,
            });

          } else if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Added!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              location.reload();
            }, 2000);

          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));


  /*********************************  TICKET ******************************/
  $("#form_send").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/send_reply",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal("Sent!", "Successfully Sent Message!", "success");
            location.reload();
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  $('.btn_history').click(function() {
    var con_id = $(this).attr('data-value');
    $("#id_up2").val(con_id);
    var tic_id = $(this).attr('data-tic');
    $("#tic_id").val(tic_id);
    var owner = $(this).attr('data-owner');
    var acc_no = $('#acc_no').val();
    var val = $('#val').val();

    if ((acc_no == owner) || (val == 12 || val == 15)) {
      $('.btn_close').show();
    } else {
      $('.btn_close').hide();
    }


    $.post('<?php echo base_url() ?>Con_Call_Report/get_history', {
      'get_history': 'data',
      con_id: con_id
    }, function(data) {
      $('.chat_contact_no').text('');

      var Data = "";
      var reply_id = data.reply_id;


      if (data === undefined || data.length === 0 || data === null) {

        Data = '<div> <h4>This chat has no records!!</h4> </div>';
        $('#chat_list').html('').append(Data);

      } else {

        $.each(data.result, function(index, data) {
          var msg = "";
          var val = data.Val;
          var time = data.dt_time;
          var contact_no = data.con_no;
          var std_name = data.st_name;
          if (std_name == '') {
            var st_name = '';
          } else {
            var st_name = "Name: " + data.st_name + " | ";
          }
          var whatsapp = data.whatsapp;
          if (whatsapp == '') {
            var whatsapp = '';
          } else {
            var whatsapp = "Whatsapp: " + data.whatsapp;
          }
          $('.chat_contact_no').text(st_name + "\nContact No : " + contact_no + "\n | " + whatsapp);

          var user_type = '';
          var class_type = '';
          var user_name = '';

          if ((val == '12') || (val == '15')) {
            user_type = 'left';
            class_type = 'left';
            user_name = data.Name;
          } else {
            user_type = 'right';
            class_type = 'right';
            user_name = data.Name;
          }

          Data += '<div class="direct-chat-msg ' + user_type + '">';
          Data += '<div class="direct-chat-info clearfix">';
          Data += '<span class="direct-chat-name pull-' + class_type + '">' + user_name + ' &nbsp;</span>';
          Data += '<span class="direct-chat-timestamp pull-' + user_type + '">' + time + '&nbsp;</span>';
          Data += '</div>';
          Data += '<img class="direct-chat-img" src="<?php echo base_url(); ?>uploads/student_images/no_image.png" alt="message user image">';
          Data += '<div class="direct-chat-text">';
          Data += data.message;
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
        });

        $('#chat_list').html('').append(Data);
      }
    }, "json");
  });

  /******************************** CLOSE CHAT ***************************/
  $(".btn_close").click(function() {
    var tic_id = $("#tic_id").val();
    var id_up = $("#id_up2").val();
    var menu = $("#menu").val();

    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, close it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_Call_Report/close_chat", {
            close_chat: "data",
            tic_id: tic_id,
            id_up: id_up
          }, function(data) {
            if ($.trim(data.status) === 'success') {
              swal("Closed", "Successfully Closed Chat!", "success");
              location.reload();

            } else if ($.trim(data.status) === ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          }, "json");
        } else {
          swal("Cancelled", "", "error");
        }
      });
  });


  /**************************** AGENT TRANSFER  ****************************/
  $('.btn_tansfer').click(function() {
    var con_id = $(this).attr('data-value');
    $("#cont_id2").val(con_id);
  });

  $("#agent_transfer").on('submit', (function(e) {
    var menu = $("#menu").val();
    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_zoom_upload/agent_tranfer",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'data exists') {
            swal({
              title: "",
              text: "Data Already Exists!",
              type: "warning",
              timer: 2000,
              showConfirmButton: false,
            });

          } else if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Tansferred!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
            }, 2000);

          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));
</script>