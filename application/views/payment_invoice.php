<?php
date_default_timezone_set('Asia/Colombo');
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>VICTORY ACADEMY</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta http-equiv="Cache-control" content="no-cache">
  <meta name="theme-color" content="#424242" />
  <link href="<?php echo base_url(); ?>backend/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/style-main.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/jquery.mCustomScrollbar.min.css">
  <?php
  $this->load->view('layout/theme');
  ?>

  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/ionicons.min.css">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/morris/morris.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/colorpicker/bootstrap-colorpicker.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/custom_style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/datepicker/css/bootstrap-datetimepicker.css">
  <!--file dropify-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/dropify.min.css">
  <!--file nprogress-->
  <link href="<?php echo base_url(); ?>backend/dist/css/nprogress.css" rel="stylesheet">

  <link href="<?php echo base_url(); ?>backend/plugins/jQueryUI/jquery-ui.min.css" rel="stylesheet">

  <!--print table-->
  <link href="<?php echo base_url(); ?>backend/dist/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>backend/dist/datatables/css/buttons.dataTables.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>backend/dist/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet">
  <!--print table mobile support-->
  <link href="<?php echo base_url(); ?>backend/dist/datatables/css/responsive.dataTables.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>backend/dist/datatables/css/rowReorder.dataTables.min.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>backend/custom/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>backend/dist/js/moment.min.js"></script>
  <script src="<?php echo base_url(); ?>backend/datepicker/js/bootstrap-datetimepicker.js"></script>
  <script src="<?php echo base_url(); ?>backend/plugins/colorpicker/bootstrap-colorpicker.js"></script>
  <script src="<?php echo base_url(); ?>backend/datepicker/date.js"></script>
  <script src="<?php echo base_url(); ?>backend/dist/js/jquery-ui.min.js"></script>
  <script src="<?php echo base_url(); ?>backend/js/school-custom.js"></script>
  <script src="<?php echo base_url(); ?>backend/js/sstoast.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/js/jquery.mask.min.js"></script>


  <!-- fullCalendar -->
  <link rel="stylesheet" href="<?php echo base_url() ?>backend/calender/zabuto_calendar.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>backend/fullcalendar/dist/fullcalendar.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>backend/fullcalendar/dist/fullcalendar.print.min.css" media="print">
  <link rel="stylesheet" href="<?php echo base_url() ?>backend/sweet-alert/sweetalert2.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>backend/toggle/css/bootstrap-toggle.min.css">

  <script src="<?php echo base_url(); ?>backend/plugins/iCheck/icheck.min.js"></script>

  <style>
    .inv_text {
      font-family: Roboto-Bold;
      font-size: 15px;
      text-transform: uppercase;
      position: absolute;
    }

    .inv_text2 {
      font-family: Roboto-Bold;
      font-size: 12px;
      text-transform: uppercase;
      position: absolute;
    }
  </style>


</head>

<body class="hold-transition skin-blue fixed sidebar-collapse">



  <div class="wrapper">

    <header class="main-header" id="alert">
      <a href="#" class="logo">
        <span class="logo-mini">VA</span>
        <span class="logo-lg"><img src="<?php echo base_url(); ?>backend/images/logo.png" /></span>
      </a>
      <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>


      </nav>
    </header>

    <div class="content-wrapper" style="min-height: 946px;">
      <section class="content-header">
      </section>
      <!-- Main content -->
      <section class="content">
        <div class="row">



          <div class="col-md-12">
            <button id="print" class="btn btn-sm btn-primary pull-right btn_print" onclick="printContent('print-data');">Print <span class="glyphicon glyphicon-picture"></span></button>
            <div class="box-body container" id="print-data" style=" margin:0; position:relative; left:0; top:0; width: 210mm; height: 295mm; overflow: hidden; ">

              <!-- <img height="730px" width="475px" src="<?php //echo base_url() 
                                                          ?>images/refund_payment.jpg">
              <div class="inv_text" style="top: 289px; left: 60px;"><?php //echo $pay_details['name']; 
                                                                    ?></div>
              <div class="inv_text" style="top: 305px; left: 60px; color: #b6b6b6; font-size: 12px; "><?php //echo $pay_details['town']; 
                                                                                                      ?></div>
              <div class="inv_text" style="top: 289px; left: 400px; color: #b6b6b6;"><?php //echo $pay_details['inv_no']; 
                                                                                      ?></div>
              <div class="inv_text" style="top: 420px; left: 55px;">Rs. <?php //echo $pay_details['cr']; 
                                                                        ?></div>
              <div class="inv_text" style="top: 423px; left: 178px; font-size: 12px;">Class Enrollment Fee</div>
              <div class="inv_text" style="top: 420px; left: 386px;">Rs. <?php //echo $course_fee; 
                                                                          ?></div> -->

              <?php
              $amt = intval($pay_details['cr']);
              $c_fee = intval($course_fee);
              $usdt_val = $pay_details['usdt_val'];
              $reg_cnt = $pay_details2['reg_cnt'];

              if ($usdt_val == 1) {
                $course_fee = $course_fee;
              } else {
                $course_fee = intval($course_fee) - intval(2500);
              }

              if ($amt == $course_fee) {
              ?>
                <img height="730px" width="475px" src="<?php echo base_url() ?>images/enrollment_payment.jpeg">
                <div class="inv_text" style="top: 268px; left: 60px;"><?php echo $pay_details['name']; ?></div>
                <div class="inv_text" style="top: 285px; left: 60px; color: #b6b6b6; font-size: 12px; "><?php echo $pay_details['town']; ?></div>
                <div class="inv_text" style="top: 268px; left: 400px; color: #b6b6b6;"><?php echo $pay_details['inv_no']; ?></div>
                <div class="inv_text" style="top: 420px; left: 65px;">Rs. <?php echo $pay_details['cr']; ?></div>
                <div class="inv_text" style="top: 423px; left: 185px; font-size: 12px;">Class Enrollment Fee</div>
                <div class="inv_text" style="top: 420px; left: 395px;">Rs. <?php echo $course_fee; ?></div>
              <?php
              } else if ($reg_cnt >= 1) { ?>
                <img height="730px" width="475px" src="<?php echo base_url() ?>images/enrollment_payment.jpeg">
                <div class="inv_text" style="top: 268px; left: 60px;"><?php echo $pay_details['name']; ?></div>
                <div class="inv_text" style="top: 285px; left: 60px; color: #b6b6b6; font-size: 12px; "><?php echo $pay_details['town']; ?></div>
                <div class="inv_text" style="top: 268px; left: 400px; color: #b6b6b6;"><?php echo $pay_details['inv_no']; ?></div>
                <div class="inv_text" style="top: 420px; left: 65px;">Rs. <?php echo $pay_details['cr']; ?></div>
                <div class="inv_text" style="top: 423px; left: 185px; font-size: 12px;">Class Enrollment Fee</div>
                <div class="inv_text" style="top: 420px; left: 395px;">Rs. <?php echo $pay_details['cr']; ?></div>
              <?php } else {
                $balance = intval($course_fee) - intval($pay_details['tot']);
              ?>
                <img height="750px" width="475px" src="<?php echo base_url() ?>images/advance_payment.jpg">
                <div class="inv_text2" style="top: 285px; left: 60px;"><?php echo $pay_details['name']; ?></div>
                <div class="inv_text2" style="top: 300px; left: 60px; color: #b6b6b6; font-size: 12px; "><?php echo $pay_details['town']; ?></div>
                <div class="inv_text2" style="top: 285px; left: 400px; color: #b6b6b6;"><?php echo $pay_details['inv_no']; ?></div>
                <div class="inv_text2" style="top: 420px; left: 60px;">Rs. <?php echo $pay_details['cr']; ?></div>
                <div class="inv_text2" style="top: 420px; left: 147px; font-size: 11px;">Class Enrollment Fee</div>
                <div class="inv_text2" style="top: 420px; left: 287px;">Rs. <?php echo $course_fee; ?></div>
                <div class="inv_text2" style="top: 420px; left: 385px;">Rs. <?php echo $balance; ?></div>

              <?php  }
              ?>

            </div>
          </div>


        </div>
      </section>
    </div>
  </div>

  <script>
    function printContent(el) {
      var restorepage = $('body').html();
      var printcontent = $('#' + el).clone();
      $('body').empty().html(printcontent);
      window.print();
      $('body').html(restorepage);
    }
  </script>
</body>

</html>