<?php 
$menu = $this->uri->segment(3);
?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i> User Control</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
  
            <div class="col-md-12">             
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Users List</h3>
                    </div>
                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                    <div class="box-body ">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label">Users List</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <thead>
                                    <tr>
                                      <th>ID</th>
                                      <th>Main Project</th>
                                      <th>Activity</th>
                                      <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>                                   

                                    <?php
                                    $count = 1;
                                     
                                    foreach($load_data as $value) {
                                        ?>
                                        <tr>
                                            <td class="mailbox-name"> <?php echo $count; ?></td>
                                            <td class="mailbox-name"> <?php echo $value->prjct; ?></td>
                                            <td class="mailbox-name"> <?php echo $value->Panel; ?></td>
                                            <td class="mailbox-name"> <?php echo $value->Date; ?></td>
                                        </tr>
                                        <?php
                                    $count++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
           
        </div> 

    </section>
</div>


