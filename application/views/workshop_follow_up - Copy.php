<?php
$menu = $this->uri->segment(3);
?>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1> <i class="fa fa-sitemap"></i> Class Follow Up
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php
        if ($val == 12 || $val == 15) {
        ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12 col-sm-6">
                  <div class="row">
                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                    <form role="form" action="<?php echo site_url('Con_zoom_upload/workshop_follow_up/') ?><?php echo $menu . "/" ?>" method="post" class="">

                      <div class="form-group col-md-6">
                        <label for="agent">Agent</label>
                        <select id="agent" name="agent" class="form-control">
                          <option value="">Select</option>
                          <?php foreach ($load_agent as $agent) { ?>
                            <option value="<?php echo $agent->Acc_No ?>"><?php echo $agent->Name ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="form-group col-md-6">
                        <label for="agent">Workshop</label>
                        <select id="workshop" name="workshop" class="form-control">
                          <option value="">Select</option>
                          <?php foreach ($load_workshop as $workshop) { ?>
                            <option value="<?php echo $workshop->id ?>"><?php echo $workshop->w_name . " (" . $workshop->w_date . " / " . date("h:i A", strtotime($workshop->w_time)) . ")"; ?></option>
                          <?php } ?>
                        </select>
                      </div>

                      <div class="col-sm-12">
                        <div class="form-group">
                          <button name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle btn-search"><i class="fa fa-search"></i> Search</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php }
        if (isset($load_data)) { ?>
          <div class="box box-info">
            <div class="box-body">
              <form action="javascript:void(0);" method="post">
                <div class="table-responsive ptt10">
                  <table class="table table-hover table-striped example">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Registration No</th>
                        <th>Name</th>
                        <th>Workshop</th>
                        <th>Address</th>
                        <th class="pull-right">Actions</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php
                      $count = 1;

                      foreach ($load_data as $value) {
                      ?>
                        <tr>
                          <td class="mailbox-name"> <?php echo $count; ?>.</td>
                          <td class="mailbox-name"> <?php echo $value->reg_no; ?></td>
                          <td class="mailbox-name"> <?php echo $value->name; ?></td>
                          <td class="mailbox-name"> <?php echo $value->w_name; ?></td>
                          <td class="mailbox-name"> <?php echo $value->address; ?></td>
                          <td class="pull-right">
                            <?php
                            //if ($value->join_time == '') {
                            ?>
                              <!-- <button type="button" class="btn btn-sm btn-success" id="btn_register" data-tel='<?php //echo $value->con_no ?>' data-reg="<?php //echo $value->reg_no; ?>" title="Reassign Workshop "><i class="fa fa-user-plus"></i></button>
                              <button type="button" class="btn btn-sm btn-danger btn_accept_reject" data-tel='<?php //echo $value->con_no ?>' title="Reject" data-res-flag="3"><i class="fa fa-close"></i></button>

                              <button type="button" class="btn btn-sm btn-info" id="btn_reminder" data-tel='<?php //echo $value->con_no ?>' title="Set Reminder" data-toggle="modal" data-target="#reminder_model"><i class="fa fa-clock-o"></i></button> -->
                              <?php //} else {
                              // if ($value->deposits >= 10000) {
                              ?>
                                <!-- <button type="button" class="btn btn-sm btn-success btn_class" id="" data-tel='<?php //echo $value->con_no ?>' data-reg="<?php //echo $value->reg_no; ?>" title="Register for Class"><i class="fa fa-user-plus"></i></button> -->
                              <!-- <?php } ?> -->

                              <button type="button" class="btn btn-sm btn-danger btn_accept_reject" data-tel='<?php echo $value->con_no ?>' title="Reject" data-res-flag="3"><i class="fa fa-close"></i></button>
                              <button type="button" class="btn btn-sm btn-info" id="btn_reminder" data-tel='<?php echo $value->con_no ?>' title="Set Reminder" data-toggle="modal" data-target="#reminder_model"><i class="fa fa-clock-o"></i></button>
                              <button type="button" class="btn btn-sm btn-info btn_deposit" id="" data-reg='<?php echo $value->reg_no; ?>' data-deposit="<?php echo $value->deposits; ?>" data-tel='<?php echo $value->con_no ?>' title="Add Bank Deposit" data-toggle="modal" data-target="#deposit_model" style="background-color: #0a679f;
    border-color: #0a679f;"><i class="fa fa-money"></i></button>
                            <?php //} ?>
                          </td>
                        </tr>
                      <?php
                        $count++;
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
</div>

<!-------------- Reminder Model----------->
<div class="modal fade" id="reminder_model" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title">Set Reminder</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_update">
          <input type="hidden" name="id_up" id="id_up">

          <div class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label for="reminder_date">Reminder Date<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control datechk" name="reminder_date" id="reminder_date" placeholder="Enter ..." autocomplete="off" required value="<?php print(date("Y-m-d")); ?>">
              </div>

              <div class="form-group">
                <label for="note">Note <small class="req"> *</small></label>
                <textarea name="note" id="note" class="form-control" rows="5" required> </textarea>
              </div>
            </div>
          </div>

          <!-- /.box-body -->
          <div class="box-footer">
            <button class="btn btn-danger" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-------------- BANK DEPOSIT Model----------->
<div class="modal fade" id="deposit_model" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title">Add Bank Deposits</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_deposit">
          <input type="hidden" name="wr_num" id="wr_num">
          <input type="hidden" name="tot_deposit" id="tot_deposit">

          <div class="form-horizontal">
            <div class="box-body">
              <label class="pull-right balance_amt">Balance to be paid: LKR</label>
              <br>
              <div class="row">
                <div class="form-group col-md-12">
                  <label>Is USDT<small class="req"> *</small>&nbsp;&nbsp;&nbsp;</label>
                  <label class="radio-inline">
                    <input type="checkbox" name="is_usdt" id="is_usdt" value="1"> USDT
                    <input type="hidden" class="form-control" name="usdt_val" id="usdt_val">
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label>Description<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control" name="desc" id="desc" placeholder="Enter ..." autocomplete="off" required autofocus>
              </div>

              <div class="row">
                <div class="form-group col-md-12">
                  <label for="payment_type">Payment Type*&nbsp;&nbsp;&nbsp;</label>
                  <label class="radio-inline">
                    <input type="radio" name="payment_type" value="default" checked id="default"> Default
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="payment_type" value="manual" id="manual"> Manual
                  </label>
                </div>
              </div>

              <div class="form-group">
                <label>Deposited Amount<font color="#FF0000"><strong>*</strong></font></label>
                <select id="default_amt" name="default_amt" class="form-control default">
                  <option>5000</option>
                  <option>7500</option>
                  <option>12500</option>
                </select>
                <br>
                <input type="text" class="form-control manual" name="manual_amt" id="manual_amt" placeholder="Enter Amount" autocomplete="off" style="display: none;">
              </div>

            </div>
          </div>

          <!-- /.box-body -->
          <div class="box-footer">
            <button class="btn btn-danger" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  /**************************** PAYMENT TYPE  ****************************/
  $('input:radio[name="payment_type"]').change(function() {
    if ($(this).is(':checked')) {
      var value = $(this).val();
      if (value == "default") {
        $('.manual').hide();
        $('.default').show();
        $("#default_amt").prop('required', true);
        $("#manual_amt").prop('required', false);
      } else if (value == "manual") {
        $('.default').hide();
        $('.manual').show();
        $("#default_amt").prop('required', false);
        $("#manual_amt").prop('required', true);
      }

    }
  });
  /**************************** DATE ***********************/
  var date_format = 'yyyy-mm-dd';
  $(document).ready(function() {

    $(".datechk").datepicker({
      format: date_format,
      autoclose: true,
      todayHighlight: true
    });

    $('.timepicker').datetimepicker({
      format: 'LT'
    });
  });

  /**************************** ACCEPT / REJECT  ****************************/
  $(".btn_accept_reject").click(function() {
    var menu = $("#menu").val();
    var number = $(this).attr('data-tel');
    var res_flag = $(this).attr('data-res-flag')

    if (number == '') {
      swal("", "Please select the phone number.", "warning");
    } else {
      $.post("<?php echo base_url() ?>Con_call_center/response_accept", {
          response_accept: "data",
          number: number,
          res_flag: res_flag
        },
        function(data) {
          if ($.trim(data.status) === 'success') {
            swal({
              title: 'Successfully Rejected',
              text: '',
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              window.location.href = "<?php echo base_url() ?>Con_pre_registry/index/" + menu;;
            }, 2000);

          } else if ($.trim(data.status) === 'error') {
            swal({
              title: "",
              text: "Error!",
              type: "error",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
            }, 2000);

          }
        }, "json");
    }
  });

  /********************************* REMINDER ******************************/
  $("#btn_reminder").click(function() {
    var id = $(this).attr('data-tel');
    $("#id_up").val(id);
  });

  /********************************* Edit ******************************/
  $("#form_update").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_call_center/update_reminder",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal({
                title: "Updated!",
                text: "Successfully Updated!",
                type: "success",
                confirmButtonText: "OK"
              },
              function(isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                }
              });
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /********************************* BANK DEPOSIT ******************************/
  $("#form_deposit").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_post_registry/bank_deposit",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal({
                title: "Sucess!",
                text: "Successfully Added Details!",
                type: "success",
                confirmButtonText: "OK"
              },
              function(isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url() ?>Con_zoom_upload/workshop_follow_up/" + menu;
                }
              });
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /********************************* REGISTER ******************************/
  $("#btn_register").click(function() {
    var reg = $(this).attr('data-reg');

    window.open("<?php echo base_url() ?>register/" + reg);

  });

  /********************************* CLASS TIMETABLE ******************************/
  $(".btn_class").click(function() {
    var reg = $(this).attr('data-reg');

    window.open("<?php echo base_url() ?>Con_post_registry/class_register/" + reg);

  });

  /**************************** DEPOSIT ***********************/
  $(".btn_deposit").click(function() {
    var reg = $(this).attr('data-reg');
    var deposit = $(this).attr('data-deposit');
    $('#wr_num').val(reg);
    $('#tot_deposit').val(deposit);
    var balance = '';

    $.post("<?php echo base_url() ?>Con_zoom_upload/get_usdt", {
      reg: reg
    }, function(data) {
      $.each(data.result, function(index, data) {
        usdt_val = $.trim(data.usdt_val);
        if (usdt_val == 1) {
          $("#is_usdt").prop('checked', true);
          $("#usdt_val").val('1');
          balance = 12500 - deposit;
          if (deposit <= 12500) {
            $('.balance_amt').text("Balance to be paid: LKR " + balance);
          } else {
            $('.balance_amt').text("");
          }
        } else {
          $("#is_usdt").prop('checked', false);
          $("#usdt_val").val('0');
          balance = 10000 - deposit;
          if (deposit <= 12500) {
            $('.balance_amt').text("Balance to be paid: LKR " + balance);
          } else {
            $('.balance_amt').text("");
          }
        }
      });
    }, "json");

  });

  $('#is_usdt').change(function() {
    deposit = $('#tot_deposit').val();
    if (deposit <= 12500) {
      if (this.checked) {
        $("#usdt_val").val('1');
        balance = 12500 - deposit;
        $('.balance_amt').text("Balance to be paid: LKR " + balance);
      } else {
        $("#usdt_val").val('0');
        balance = 10000 - deposit;
        $('.balance_amt').text("Balance to be paid: LKR " + balance);
      }
    } else {
      $('.balance_amt').text("");
    }
  });
  /**************************** SEND SMS ***********************/
  $(".btn_sms").click(function() {
    var menu = $("#menu").val();
    var reg = $(this).attr('data-reg');
    $('#wr_num').val(reg);

    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, Send  SMS!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_pre_registry/class_sms", {
            delete: "data",
            reg: reg
          }, function(data) {
            if ($.trim(data.status) === 'success') {
              swal({
                  title: "Sent!",
                  text: "Successfully Sent!",
                  type: "success",
                  confirmButtonText: "OK"
                },
                function(isConfirm) {
                  if (isConfirm) {
                    window.location.href = "<?php echo base_url() ?>Con_workshop/workshop_link/" + menu;
                  }
                });

            } else if ($.trim(data.status) === ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          }, "json");
        } else {
          swal("Cancelled", "", "error");
        }
      });

  });
</script>