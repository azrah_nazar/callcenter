<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
    $date1 = date("Y-m-d");
    $date2 = date("Y-m-d");
} else {
    $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
    $date1 = substr($daterange, 0, 10);
    $date2 = substr($daterange, 17, 20);
}
?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-id-card"></i> Other Payment Report
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

                    </div>
                    <div class="box-body">
                        <form role="form" action="<?php echo site_url('Con_sales_report/personal_bank_report/') ?><?php echo $menu . "/" . $date1 . ' - ' . $date2; ?>" method="post" class="">
                            <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="col-sm-12">
                                <div class="form-group">
                                    <button type="submit" name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div> -->
                        </form>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Other Payment Report</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label"> Other Payment Report</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Crypto</th>
                                        <th>Online Payment</th>
                                        <th>KOKO</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    $count = 1;
                                    $crypto = $other = $koko = 0;

                                    foreach ($load_data as $value) {
                                    ?>
                                        <tr>
                                            <td> <?php echo $count; ?>.</td>
                                            <td> <?php echo $value->dt; ?></td>
                                            <td> <?php echo $value->Crypto; $crypto += $value->Crypto; ?></td>
                                            <td> <?php echo $value->online_payment; $other += $value->online_payment;  ?></td>
                                            <td> <?php echo $value->KOKO; $koko += $value->KOKO;  ?></td>
                                        </tr>
                                    <?php
                                        $count++;
                                    }
                                    ?>
                                    <tr>
                                        <td><b>Total</b></td>
                                        <td><b></b></td>
                                        <td><b><?php echo $crypto;?></b></td>
                                        <td><b><?php echo $other;?></b></td>
                                        <td><b><?php echo $koko;?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
    /**************************** DATE ***********************/
    $('#date_range').daterangepicker({
        autoclose: false,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });

    function formHandler(date) {
        var menu = $("#menu").val();
        window.location.href = "<?php echo base_url() ?>Con_sales_report/personal_bank_report/" + menu + "/" + date;
    }
   
</script>