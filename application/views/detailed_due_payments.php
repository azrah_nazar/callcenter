<?php
$menu = $this->uri->segment(3);

?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-id-card"></i> Due Payment Detailed Report
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Due Payment Detailed Report</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label"> Due Payment Detailed Report</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Agent</th>
                                        <th>Code</th>
                                        <th>No</th>
                                        <th>WR No</th>
                                        <th>TP</th>
                                        <th>Date</th>
                                        <th>Paid</th>
                                        <th>Due</th>
                                        <th>Month</th>
                                        <th>Count</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    $count = 1;
                                    $m_cnt = $tot_m_cnt = $due_tot = $due_tot_all = 0;

                                    $n_month = '';
                                    foreach ($load_data as $value) {
                                        $due = $value->due;
                                        if ($due > 0) {
                                            $ar_dt = $value->dt;

                                            $date = date('d', strtotime($ar_dt));
                                            $month = date('F', strtotime($ar_dt));
                                            if ($date <= '10') {
                                               $ac_month = date('F', strtotime(date($ar_dt) . " -1 month"));
                                            } else {
                                                $ac_month = $month;
                                            }
                                            $new_month = $ac_month;
                                            if ($count == 1) {
                                                $n_month = $new_month;
                                            }

                                            if ($n_month == $new_month) {
                                                $m_cnt += 1;
                                            } else {
                                    ?>
                                                <tr style="background: #d8d0d08f;">
                                                    <td></td>
                                                    <td style="font-weight: bold">Sub Total Count</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="font-weight: bold"><?php echo $due_tot;
                                                                                    $due_tot_all += $due_tot;
                                                                                    $due_tot = 0; ?></td>
                                                    <td style="font-weight: bold"><?php echo $n_month; ?></td>
                                                    <td style="font-weight: bold"><?php echo $m_cnt;
                                                                                    $tot_m_cnt += $m_cnt;
                                                                                    $m_cnt = 1; ?></td>
                                                </tr>
                                            <?php
                                            } ?>
                                            <tr>
                                                <td> <?php echo $count; ?>.</td>
                                                <td> <?php echo $value->Name; ?></td>
                                                <td> <?php echo substr($value->inv_no, 0, 3); ?></td>
                                                <td> <?php echo substr($value->inv_no, 3); ?></td>
                                                <td> <?php echo $value->reg_no; ?></td>
                                                <td> <?php echo $value->con_no; ?></td>
                                                <td> <?php echo $date . '-' . $month; ?></td>
                                                <td> <?php echo $value->deposits; ?></td>
                                                <td> <?php echo $value->due;
                                                        $due_tot += $value->due; ?></td>
                                                <td> <?php echo $new_month; ?></td>
                                                <td></td>
                                            </tr>
                                    <?php
                                            $count++;
                                            $n_month = $new_month;
                                        }
                                    }
                                    ?>
                                    <tr style="background: #d8d0d08f;">
                                        <td></td>
                                        <td style="font-weight: bold">Sub Total Count</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="font-weight: bold"><?php echo $due_tot;
                                                                        $due_tot_all += $due_tot;
                                                                        $due_tot = 0; ?></td>
                                        <td style="font-weight: bold"><?php echo $n_month; ?></td>
                                        <td style="font-weight: bold"><?php echo $m_cnt;
                                                                        $tot_m_cnt += $m_cnt;
                                                                        $m_cnt = 1; ?></td>
                                    </tr>
                                    <tr style="background: #b5fbba8f;">
                                        <td></td>
                                        <td style="font-weight: bold">Total Count</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="font-weight: bold"><?php echo $due_tot_all ?></td>
                                        <td></td>
                                        <td style="font-weight: bold"><?php echo $tot_m_cnt; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>