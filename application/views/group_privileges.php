<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i> Group Privilege</small></h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

          <div class="col-md-12">
              <div class="box box-primary">
                  <div class="box-header with-border">
                      <h3 class="box-title">Add Group Privilege</h3>
                  </div> 
                  <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                  <form role="form" id="form_submit" action="javascript:void(0);">
                      <div class="box-body">

                          <div class="form-group">
                            <label>Select Project:<font color="#FF0000"><strong>*</strong></font></label>
                            <select class="form-control txt_nav" name="pid" id="p_id" style="width: 100%;" required>
                              <?php
                              echo "<option ></option>"; 
                              foreach($project as $prj) {

                                $project = "";
                                if(!empty($project_id)) {
                                  if ($project_id == $prj->ID) {
                                    $project = "Selected";
                                  }
                                }

                                echo "<option ".$project." value=".$prj->ID.">".$prj->prjct."</option>"; 
                              }
                              ?>
                            </select>
                          </div>

                          <div class="form-group">
                            <label>Select Group Profile:<font color="#FF0000"><strong>*</strong></font></label>
                            <select class="form-control txt_nav" name="prof" id="prof_id" style="width: 100%;" required>
                              <?php
                              echo "<option ></option>"; 
                              foreach($profile as $prof) {

                                $profile = "";
                                if(!empty($profile_id)) {
                                  if ($profile_id == $prof->id) {
                                    $profile = "Selected";
                                  }
                                }

                                echo "<option ".$profile." value=".$prof->id.">".$prof->profile."</option>"; 
                              }
                              ?>
                            </select>
                          </div>
                      </div>
                      <div class="box-footer">
                          <button type="submit" class="btn btn-info">Filter</button>
                      </div>
                  </form>
                  
              </div>  
          </div> 

          <?php 
          if(!empty($project_id)) {
            $xx = 0;
            foreach($prjct_cat as $cat) { 
              $xx++; 
          ?>

          <div class="col-md-12">             
              <div class="box box-primary">
                  <div class="box-header ptbnull">
                      <h3 class="box-title titlefix"><?php echo $cat->cat; ?></h3>
                  </div>
                  <div class="box-body ">
                      <div class="table-responsive mailbox-messages">
                          <div class="download_label"><?php echo $cat->cat; ?></div>
                          <table class="table table-striped table-bordered table-hover example">
                              <thead>
                                  <tr>
                                      <th width="15%">ID</th>
                                      <th width="50%">Name</th>
                                      <th width="15%">Status</th>
                                      <th width="20%" class="text-right">Add / Remove</th>
                                  </tr>
                              </thead>
                              <tbody> 
                                <?php 
                                $privileges = array();
                                foreach ( $user_priv as $Items )
                                  $privileges[] = $Items->menu_id;

                                $yy = 1;
                                foreach($menu_lst as $m_value) { 

                                  $priv_val = 0;

                                  if ($cat->ID == $m_value->Cat_id) {

                                    if (in_array($m_value->ID, $privileges)){
                                      $priv_val = 1;
                                    }

                                  ?>                                  
                                <tr>
                                  <td> <?php echo $yy;?>.</td>
                                  <td> <?php echo $m_value->Caption; ?></td>
                                  
                                  <td>
                                    <?php 
                                      $img_add = "";
                                      $img_del = "";
                                      if ($priv_val == 0) {
                                        $img_del = "display:none";
                                      }else{ 
                                        $img_add = "display:none";
                                      } 
                                    ?>

                                    <img src="<?php echo base_url(); ?>uploads/w.png" width="20" class="img2<?php echo $m_value->ID; ?> img2" style="<?php echo $img_add; ?>">


                                    <img src="<?php echo base_url(); ?>uploads/r.png" width="20" class="img1<?php echo $m_value->ID; ?>" style="<?php echo $img_del; ?>">

                                   </td>

                                  <td>
                                    <?php 
                                    $btn_add = "";
                                    $btn_del = "";
                                    if ($priv_val == 0) {
                                      $btn_del = "display:none";
                                    }else{ 
                                      $btn_add = "display:none";
                                    } ?>

                                    <button class="btn btn-sm btn-success btn_add add<?php echo $m_value->ID; ?> add"  value="<?php echo $m_value->ID; ?>" style="<?php echo $btn_add; ?>"><i class="fa fa-plus"></i> &nbsp;ADD&nbsp;</button>


                                    <button class="btn btn-sm btn-danger btn_del remove<?php echo $m_value->ID; ?>" value="<?php echo $m_value->ID; ?>" style="<?php echo $btn_del; ?>"><i class="fa fa-trash-o"></i> &nbsp;REMOVE&nbsp;</button>

                                  </td>
                               
                              </tr>
                            <?php  $yy++;} } ?>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div> 

          <?php } } ?> 
        </div> 

    </section>
</div>

<script>    

  /**************************** INSERT  ****************************/
  var menu_id = $('#menu').val();

  $("#form_submit").on('submit',(function(e) {

    window.location.href = "<?php echo base_url()?>Con_group_privileges/index/"+menu_id+"/"+$("#p_id").val()+"/"+$("#prof_id").val();

  })); 

  
  /*************************** REMOVE PRIVILEGES *************************/ 
  $(".btn_del").click(function(){ 
    var prof = $("#prof_id").val();
    var menu = $(this).val();
    
    $.post( "<?php echo base_url()?>Con_group_privileges/remove_priv", { remove_priv: "data", menu : menu, prof: prof}, function( data ) {
      if($.trim(data.status) ==='success'){
        $(".img1"+menu).hide();
        $(".img2"+menu).show();
        $(".remove"+menu).hide();
        $(".add"+menu).show();
      }
    }, "json");
    
  });


  /**************************** ADD PRIVILEGES *************************/
  $(".btn_add").click(function(){ 
    var prof = $("#prof_id").val();
    var menu = $(this).val();
    
    $.post( "<?php echo base_url()?>Con_group_privileges/add_priv", { add_priv: "data", menu : menu, prof: prof}, function( data ) {
      if($.trim(data.status) === 'success'){
        $(".img1"+menu).show();
        $(".img2"+menu).hide();
        $(".remove"+menu).show();
        $(".add"+menu).hide();
      }
    }, "json");
    
  });
  
</script>

