<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i>Add Course</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

                <div class="col-md-3">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Course</h3>
                        </div>
                        <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>"> 
                        <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
                            <div class="box-body">


                                <div class="form-group" style="display:none" id="rep_cat_list">                 
                                  <label>Report Category:<font color="#FF0000"><strong>*</strong></font></label>

                                  <div id="report_list"></div>
                                </div>      

                                <div class="form-group">                 
                                  <label for="name">Course ID<font color="#FF0000"><strong>*</strong></font></label>
                                  <input type="text" class="form-control clr" name="course_id" id="course_id" placeholder="Enter ..." autocomplete="off" required>
                                </div>

                                <div class="form-group">                 
                                  <label for="url">Course Name<font color="#FF0000"><strong>*</strong></font></label>
                                  <input type="text" class="form-control clr" name="course_name" id="course_name" placeholder="Enter ..." autocomplete="off" required>
                                </div>

                                <div class="form-group">
                                  <label for="oid">Course Duration<font color="#FF0000"><strong>*</strong></font></label>
                                  <input type="text" class="form-control clr" name="course_duration" id="course_duration" placeholder="Enter ..." autocomplete="off" required>
                                </div>


                                  <div class="form-group">
                                  <label for="oid">Course Fee<font color="#FF0000"><strong>*</strong></font></label>
                                  <input type="text" class="form-control clr" name="course_fee" id="course_fee" placeholder="Enter ..." autocomplete="off" required>
                                </div>



                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>  
                </div>   
            <div class="col-md-9">             
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Course List</h3>
                    </div>
                    <div class="box-body ">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label">Course List</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th> ID </th>
                                        <th> Type </th>
                                    
                                        
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                   

                                    <?php
                                    $count = 0;
                                     
                                    foreach($load_data as $value) { $count++; 
                                        ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $value->id;?></td>
                                            <td><?php echo $value->type;?></td>
                                            
                                            
                                            <td class="mailbox-date pull-right">

                                                <button class="btn btn-default btn-xs btn_up" data-toggle="modal" data-target="#edit_data"   title="Edit">
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                                   
                                                <button class="btn btn-default btn-xs btn_del"  data-toggle="tooltip" title="Delete" >
                                                    <i class="fa fa-remove"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
           
        </div> 

    </section>
</div>

<div class="modal fade" id="edit_data" role="dialog">
    <div class="modal-dialog">       
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title text-center modal_title"> Edit Course</h4>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" id="form_update">
                  <input type="hidden" name="id_up" id="id_up">

                  <div class="form-horizontal">
                      <div class="box-body">
                          

                          <div class="form-group">                 
                            <label>Course ID:</label>
                            <input type="text" class="form-control" name="course_id_up" id="course_id_up" placeholder="Enter ..." autocomplete="off">
                          </div>

                          <div class="form-group">                 
                            <label>Course Name:</label>
                            <input type="text" class="form-control" name="course_name_up" id="course_name_up" placeholder="Enter ..." autocomplete="off">
                          </div>

                           <div class="form-group">
                             <label>Course Duration:</label>
                             <input type="text" class="form-control" name="course_duration_up" id="course_duration_up" placeholder="Enter ..." autocomplete="off">
                           </div>

                           <div class="form-group">
                             <label>Course Fee:</label>
                             <input type="text" class="form-control" name="course_fee_up" id="course_fee_up" placeholder="Enter ..." autocomplete="off">
                           </div>

                      </div>                   
                  </div>

                  <div class="box-footer"> 
                    <button class="btn btn-danger">UPDATE</button>
                  </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<script>
  /**************************** INSERT  ****************************/

   $("#form_submit").on('submit',(function(e) {
      var menu = $("#menu").val();

      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url()?>Con_Add_Course/add",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
        },
        success: function(data){
          if($.trim(data) === '') {
            swal("Oops...", "Something went wrong!", "warning");

          } else {
            if($.trim(data) ==='error')
            {
              swal("SQL Error!", "Please Try Again!", "warning");

            } else if($.trim(data) ==='order no exists') {
              swal("Exists!", "Order No already Exists!", "warning");
            } else if($.trim(data) ==='data exists') {
              $('#pid').focus();
              swal({
                title: "",
                text: "Menu List Already Exists!",
                type: "warning",
                timer: 2000,
                showConfirmButton: false,
              });           

            } else if($.trim(data) ==='success') {
              swal({
                title: "",
                text: "Successfully Added!",
                type: "success",
                timer: 2000,
                showConfirmButton: false,
              });

              $(".clr").val('');
              setTimeout(function() 
              {
                //window.location.href = "<?php //cho base_url()?>Con_menu_list";
              }, 2000);
              
            }
          }
        },
        error: function(e) 
        {
          alert("err2");
        }
      });
    }));

    /********************************* EDIT ******************************/

    $("#form_update").on('submit',(function(e) {
      var menu = $("#menu").val();

      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url()?>Con_Add_Course/update",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
        },
        success: function(data){
          $('#edit_data').modal('hide')
          if($.trim(data) === '') {
            swal("Oops...", "Something went wrong!", "warning");

          } else {
            if($.trim(data) ==='error')
            {
              swal("SQL Error!", "Please Try Again!", "warning");

            } else if($.trim(data) ==='data not exists') {
              swal("", "Menu Category Already Exist!", "warning");

            } else if($.trim(data) ==='success') {
              swal({
                title: "Updated!",
                text: "Successfully Updated!",
                type: "success",
                confirmButtonText: "OK"
              },
              function (isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url()?>Con_Add_Course/index/"+menu;
                }
              });
            }
          }      
        },
        error: function(e) 
        {
          alert("err2");
        }
      });
    }));

  /******************************** Remove ***************************/

  $(".btn_del").click(function(){
    var row_id = $(this).val();
    var menu = $("#menu").val();

    swal({
     title: "Are you sure?",
     text: "",
     type: "warning",
     showCancelButton: true,
     confirmButtonClass: "btn-danger",
     confirmButtonText: "Yes, delete it!",
     cancelButtonText: "No, cancel!",
     closeOnConfirm: false,
     closeOnCancel: false
   },
   function(isConfirm) {
     if (isConfirm) {
      $.post( "<?php echo base_url()?>Con_Add_Course/delete", {remove_data: "data", id : row_id}, function( data ) {   
       if($.trim(data.status) === 'success') {
         swal({
          title: "Deleted!",
          text: "Successfully Deleted!",
          type: "success",
          confirmButtonText: "OK"
        },
        function (isConfirm) {
          if (isConfirm) {
           window.location.href = "<?php echo base_url()?>Con_Add_Course/index/"+menu;
         }
       });

       } else if($.trim(data.status) ===' error') {
         swal("", "Error!", "warning");

       } else{
         swal("Oops...", "Something went wrong!", "warning");
       }
     }, "json");
    } else {
      swal("Cancelled", "", "error");
    }
  });

  }); 



  /**************************** LOAD FORM DATA ***********************/

  $(".btn_up").click(function(){
    var id = $(this).val();
    $("#id_up").val(id);

    $.post( "<?php echo base_url()?>Con_Add_Course/get_dataset", { get_dataset: "data"}, function( data ) {
      $.each(data.result, function (index, data) {
        $("#course_id_up").val(data.id);    
        $("#course_name_up").val(data.type); 
        $("#course_duration_up").val(data.course_duration);
        $("#course_fee_up").val(data.course_fee);
        // get_cat_list_up(data.p_id, data.Cat_id);
      });

    }, "json");   
  });

 

  </script> 