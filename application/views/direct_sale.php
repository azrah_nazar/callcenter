<?php
$menu = $this->uri->segment(3);
$num = $this->uri->segment(4);
?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-id-card"></i> Direct Sale
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Call Details</h3>
                    </div>
                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                    <input type="hidden" name="num" id="num" value="<?php echo  $this->uri->segment(4); ?>">
                    <form class="form-sample" id="form_con_no" action="javascript:void(0);">
                        <div class="box-body">
                            <div class="form-group" align="center">
                                <label id="number" style="font-size: 30px;"></label><br>
                                <label id="st_name" style="font-size: 30px;"></label>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group response_set" align="center" style="font-size: 15px; display: none;">

                                <button class="btn btn-success btn-sm btn_bank2" value="" title="Bank SMS"><i class="fa fa-bank"> Bank</i></button>&nbsp;&nbsp;

                                <button class="btn btn-danger btn-sm btn_accept_reject" data-tel='' data-res-flag="3" title="Reject"><i class="fa fa-close"> </i> Reject</button>&nbsp;&nbsp;

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Contact List</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label"> List</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Reg No</th>
                                        <th>Contact No</th>
                                        <th>Whatsapp No</th>
                                        <th>Chat</th>
                                        <th>Name</th>
                                        <th>Uploaded Date</th>
                                        <th>Bank SMS</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    $count = 1;

                                    foreach ($load_data as $value) {
                                        $up_date = $value->date;
                                        if ($up_date == '0000-00-00') {
                                            $uploaded_date = '';
                                        } else {
                                            $uploaded_date = $up_date;
                                        }

                                        if ($value->bank_sms == 1) {
                                            $bank = '<i class="fa fa-check"></i>';
                                        } else {
                                            $bank = '<i class="fa fa-close"></i>';
                                        }

                                        $spl_cus = $value->spl_cus;
                                        if ($spl_cus == 1) {
                                            $splCus = "background-color: red; color:#fff;";
                                        } else {
                                            $splCus = "";
                                        }
                                    ?>
                                        <tr style="cursor: pointer;">
                                            <td onclick="getTelNo('<?php echo $value->con_no; ?>', '<?php echo $value->name; ?>', '<?php echo $value->reg_no; ?>')"> <?php echo $count; ?>.</td>
                                            <td onclick="getTelNo('<?php echo $value->con_no; ?>', '<?php echo $value->name; ?>', '<?php echo $value->reg_no; ?>')"> <?php echo $value->reg_no; ?></td>
                                            <td onclick="getTelNo('<?php echo $value->con_no; ?>', '<?php echo $value->name; ?>', '<?php echo $value->reg_no; ?>')"> <?php echo $value->con_no; ?></td>
                                            <td onclick="getTelNo('<?php echo $value->con_no; ?>',  '<?php echo $value->name; ?>', '<?php echo $value->reg_no; ?>')"> <?php echo $value->whatsapp; ?></td>

                                            <td>
                                                <?php
                                                $reminder = $value->reminder_date;
                                                if (($value->tic_id == '' || $value->tic_id == '0') && ($value->reminder_date == '0000-00-00' || empty($value->reminder_date))) {
                                                ?>
                                                    <a class="btn btn-default btn-xs btn_ticket" data-value="<?php echo $value->id; ?>" data-toggle="modal" data-target="#chat_open" title="Give Reminder" style="<?php echo $splCus; ?>">
                                                        <i class="fa fa-bell"></i>
                                                    </a>
                                                <?php
                                                } else { ?>
                                                    <a class="btn btn-default btn-xs btn_history" data-value="<?php echo $value->id; ?>" data-tic="<?php echo $value->id; ?>" data-owner="<?php echo $value->emp_accNo; ?>" data-toggle="modal" data-target="#chat_history" title="Reply" style="<?php echo $splCus; ?>">
                                                        <i class="fa fa-comment"></i>
                                                    </a>
                                                <?php } ?>
                                            </td>

                                            <td onclick="getTelNo('<?php echo $value->con_no; ?>', '<?php echo $value->name; ?>', '<?php echo $value->reg_no; ?>')"> <?php echo $value->name; ?></td>
                                            <td> <?php echo $uploaded_date; ?></td>
                                            <td> <?php echo $bank; ?></td>
                                        </tr>
                                    <?php
                                        $count++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </section>
</div>


<div class="modal fade" id="chat_open" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title text-center modal_title"> Chat Messages</h4>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
                    <div class="box-body">
                        <input type="hidden" name="con_id" id="cont_id" value="">

                        <div class="form-group">
                            <label for="email">Message</label><small class="req"> *</small>
                            <textarea name="message" id="compose-textarea" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Send</button>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="chat_history" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-md-12" id="chat_his">
                    <!-- DIRECT CHAT SUCCESS -->
                    <div class="box box-primary direct-chat direct-chat-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Chat Messages</h3>

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="direct-chat-messages" style="height:500px;">
                                <strong>
                                    <p class="chat_contact_no"></p>
                                </strong>
                                <div id="chat_list">


                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <form action="javascript:void(0);" id="form_send">
                                <input type="hidden" name="id_up" id="id_up2">
                                <input type="hidden" name="tic_id" id="tic_id">
                                <div class="input-group">
                                    <input type="text" name="reply_up" id="reply_up" placeholder="Type Message ..." class="form-control" required>
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Send</button>
                                    </span>
                                </div>
                            </form>
                        </div><!-- /.box-footer-->
                    </div>
                    <!--/.direct-chat -->
                </div><!-- /.col -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close Chat</button>
            </div>
        </div>
    </div>
</div>



<script>
    /**************************** DATE ***********************/
    var date_format = 'yyyy-mm-dd';
    $(document).ready(function() {

        $(".datechk").datepicker({
            format: date_format,
            autoclose: true,
            todayHighlight: true
        });

        $('.timepicker').datetimepicker({
            format: 'LT'
        });
    });
    /**************************** LOAD TP  ****************************/
    function getTelNo(telNo, name, reg_no) {
        $('#number').text(telNo);
        $('#st_name').text(name);
        $('.response_set').show();
        $('.btn_accept_reject').attr('data-tel', telNo);
        $('.btn_bank2').val(reg_no);

        $(document).scrollTop(0);
    }

    /**************************** ACCEPT / REJECT  ****************************/
    $(".btn_accept_reject").click(function() {
        var menu = $("#menu").val();
        var number = $("#number").text();
        var res_flag = $(this).attr('data-res-flag');

        $(this).prop('disabled', true);
        var btn = $(this);
        btn.val(btn.data("loading-text"));
        setTimeout(function() {
            btn.val('Accept');
        }, 5000);

        if (res_flag == 1) {
            txt = "Successfully Accepted!";
            txt2 = "";
            tt = "Accept";
            link = "<?php echo base_url() ?>Con_call_center/direct_sale_view/" + menu;
        } else if (res_flag == 3) {
            txt = 'Successfully Rejected';
            txt2 = '';
            tt = 'Reject';
            link = "<?php echo base_url() ?>Con_call_center/direct_sale_view/" + menu;
        }

        if (number == '') {
            swal("", "Please select the phone number.", "warning");
        } else {
            if (res_flag == 3) {
                swal({
                        title: "Are you sure you want to " + tt + "?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, " + tt + "!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.post("<?php echo base_url() ?>Con_call_center/response_accept", {
                                response_accept: "data",
                                number: number,
                                res_flag: res_flag
                            }, function(data) {
                                if ($.trim(data.status) === 'success') {
                                    swal({
                                        title: txt,
                                        text: txt2,
                                        type: "success",
                                        timer: 2000,
                                        showConfirmButton: false,
                                    });
                                    setTimeout(function() {
                                        window.location.href = "<?php echo base_url() ?>Con_call_center/direct_sale_view/" + menu;
                                    }, 2000);

                                } else if ($.trim(data) === 'error') {
                                    swal({
                                        title: "",
                                        text: "Error!",
                                        type: "error",
                                        timer: 2000,
                                        showConfirmButton: false,
                                    });
                                    setTimeout(function() {
                                        window.location.href = "<?php echo base_url() ?>Con_call_center/direct_sale_view/" + menu;
                                    }, 2000);

                                }
                            }, "json");
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });
            }
        }
    });

    /**************************** INSERT  ****************************/
    $('.btn_ticket').click(function() {
        var con_id = $(this).attr('data-value');
        $("#cont_id").val(con_id);
    });

    $("#form_submit").on('submit', (function(e) {
        var menu = $("#menu").val();
        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>Con_Call_Report/add_ticket",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                } else {
                    if ($.trim(data) === 'error') {
                        swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'data exists') {
                        swal({
                            title: "",
                            text: "Data Already Exists!",
                            type: "warning",
                            timer: 2000,
                            showConfirmButton: false,
                        });

                    } else if ($.trim(data) === 'success') {
                        swal({
                            title: "",
                            text: "Successfully Added!",
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false,
                        });
                        setTimeout(function() {
                            location.reload();
                        }, 2000);

                    }
                }
            },
            error: function(e) {
                alert("err2");
            }
        });
    }));

    /*********************************  TICKET ******************************/
    $("#form_send").on('submit', (function(e) {
        var menu = $("#menu").val();

        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>Con_Call_Report/send_reply",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                $('#edit_data').modal('hide')
                if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                } else {
                    if ($.trim(data) === 'error') {
                        swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                        swal("Sent!", "Successfully Sent Message!", "success");
                        location.reload();
                    }
                }
            },
            error: function(e) {
                alert("err2");
            }
        });
    }));

    $('.btn_history').click(function() {
        var con_id = $(this).attr('data-value');
        $("#id_up2").val(con_id);
        var tic_id = $(this).attr('data-tic');
        $("#tic_id").val(tic_id);
        var owner = $(this).attr('data-owner');
        var acc_no = $('#acc_no').val();
        var val = $('#val').val();

        if ((acc_no == owner) || (val == 12 || val == 15)) {
            $('.btn_close').show();
        } else {
            $('.btn_close').hide();
        }


        $.post('<?php echo base_url() ?>Con_Call_Report/get_history', {
            'get_history': 'data',
            con_id: con_id
        }, function(data) {
            $('.chat_contact_no').text('');

            var Data = "";
            var reply_id = data.reply_id;


            if (data === undefined || data.length === 0 || data === null) {

                Data = '<div> <h4>This chat has no records!!</h4> </div>';
                $('#chat_list').html('').append(Data);

            } else {

                $.each(data.result, function(index, data) {
                    var msg = "";
                    var val = data.Val;
                    var time = data.dt_time;
                    var contact_no = data.con_no;
                    var std_name = data.st_name;
                    if (std_name == '') {
                        var st_name = '';
                    } else {
                        var st_name = "Name: " + data.st_name + " | ";
                    }
                    var whatsapp = data.whatsapp;
                    if (whatsapp == '') {
                        var whatsapp = '';
                    } else {
                        var whatsapp = "Whatsapp: " + data.whatsapp;
                    }
                    $('.chat_contact_no').text(st_name + "\nContact No : " + contact_no + "\n | " + whatsapp);

                    var user_type = '';
                    var class_type = '';
                    var user_name = '';

                    if ((val == '12') || (val == '15')) {
                        user_type = 'left';
                        class_type = 'left';
                        user_name = data.Name;
                    } else {
                        user_type = 'right';
                        class_type = 'right';
                        user_name = data.Name;
                    }

                    Data += '<div class="direct-chat-msg ' + user_type + '">';
                    Data += '<div class="direct-chat-info clearfix">';
                    Data += '<span class="direct-chat-name pull-' + class_type + '">' + user_name + ' &nbsp;</span>';
                    Data += '<span class="direct-chat-timestamp pull-' + user_type + '">' + time + '&nbsp;</span>';
                    Data += '</div>';
                    Data += '<img class="direct-chat-img" src="<?php echo base_url(); ?>uploads/student_images/no_image.png" alt="message user image">';
                    Data += '<div class="direct-chat-text">';
                    Data += data.message;
                    Data += '</div>';
                    Data += '</div>';
                    Data += '</div>';
                    Data += '</div>';
                });

                $('#chat_list').html('').append(Data);
            }
        }, "json");
    });

    /******************************** CLOSE CHAT ***************************/
    $(".btn_close").click(function() {
        var tic_id = $("#tic_id").val();
        var id_up = $("#id_up2").val();
        var menu = $("#menu").val();

        swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, close it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_Call_Report/close_chat", {
                        close_chat: "data",
                        tic_id: tic_id,
                        id_up: id_up
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal("Closed", "Successfully Closed Chat!", "success");
                            window.location.href = "<?php echo base_url() ?>Con_call_center/direct_sale_view/" + menu;

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    /**************************** BANK SMS ***********************/
    $(document).on('click', '.btn_bank2', function(e) {
        var menu = $("#menu").val();
        var reg_no = $(this).val();

        $(this).prop('disabled', true);
        var btn = $(this);
        btn.val(btn.data("loading-text"));
        setTimeout(function() {
            $('.btn_bank2').html('Sending..');
        }, 3000);

        swal({
                title: "Are you sure you want to send the Bank SMS?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Send  SMS!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $('.ajax-loader').css("visibility", "visible");
                    $.post("<?php echo base_url() ?>Con_workshop/individual_bank_sms", {
                        delete: "data",
                        reg_no: reg_no
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            $('.ajax-loader').css("visibility", "hidden");
                            swal({
                                    title: "Sent!",
                                    text: "Successfully Sent!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_call_center/direct_sale_view/" + menu;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            $('.ajax-loader').css("visibility", "hidden");
                            swal("", "Error!", "warning");

                        } else {
                            $('.ajax-loader').css("visibility", "hidden");
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                    window.location.href = "<?php echo base_url() ?>Con_call_center/direct_sale_view/" + menu;
                }
            });
    });
</script>