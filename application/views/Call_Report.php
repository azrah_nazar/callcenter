<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
  $date2 = date("Y-m-d");
  $date1 = date('Y-m-d', strtotime('-7 day', strtotime($date2)));
} else {
  $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
  $date1 = substr($daterange, 0, 10);
  $date2 = substr($daterange, 17, 23);
}
?>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1> <i class="fa fa-sitemap"></i> Call Summary Report
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

          </div>
          <div class="box-body">
            <form role="form" action="<?php echo site_url('Con_Call_Report/index/') ?><?php echo $menu . "/" . $date1 . ' - ' . $date2; ?>" method="post" class="">
              <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">

              <div class="col-md-6">
                <div class="form-group">
                  <label>Select Allocated Date:<font color="#FF0000"><strong>*</strong></font></label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                  </div>
                </div>
              </div>

              <?php if ($val == 12 || $val == 15) { ?>
                <div class="form-group col-md-6">
                  <label for="agent">Agent</label>
                  <select id="agent" name="agent" class="form-control">
                    <option value="">Select</option>
                    <?php foreach ($load_agent as $agent) { ?>
                      <option value="<?php echo $agent->Acc_No ?>" <?php if (set_value('agent') == $agent->Acc_No) echo "selected=selected"; ?>><?php echo $agent->Name ?></option>
                    <?php } ?>
                  </select>
                </div>
              <?php } ?>
              <div class="col-sm-12">
                <div class="form-group">
                  <button type="submit" name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> Search</button>
                </div>
              </div>
            </form>
          </div>
        </div>

        <div class="col-md-3" style="display: none;">
          <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center"><label id="user"></label></h3>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item listnoback">
                  <b>Registered Students </b><label id="register" class="pull-right text-aqua"></label>
                </li>
                <li class="list-group-item listnoback">
                  <b>No Answer </b><label id="accept" class="pull-right text-aqua"></label>
                </li>
                <li class="list-group-item">
                  <b>Rejected </b><label id="reject" class="pull-right text-aqua"></label>
                </li>
              </ul>
              <hr style="height:2px;border-width:0;color:gray;background-color:gray">

              <b>Total</b><label id="tot" class="pull-right text-aqua"></label>
            </div>
          </div>
        </div>
        <?php if (isset($list)) { ?>
          <div class="col-md-12">
            <div class="box box-info">
              <div class="box-body">

                <form action="javascript:void(0);" method="post">
                  <div class="table-responsive ptt10">
                    <table class="table table-hover table-striped example">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Agent</th>
                          <th>Name</th>
                          <th>Number</th>
                          <th>Called Date</th>
                          <th>Status</th>
                          <!-- <th class="pull-right">Action</th> -->
                      </thead>
                      <tbody>
                        <?php
                        $row_count = 1;

                        foreach ($list as $key => $value) {
                          $com = $value['comment'];
                          $st = $value['flag'];
                          $dt = $value['date1'];
                          if ($dt == "") {
                            $comment = '';
                          } else {
                            if ($com == '') {
                              if ($st == '3') {
                                $comment = 'Rejected';
                              } else {
                                $comment = 'Accepted';
                              }
                            } else {
                              $comment = $com;
                            }
                          }

                        ?>
                          <tr>
                            <td><?php echo $row_count . '.'; ?></td>
                            <td><?php echo $value['emp_name'] . ' ' . $value['surname']; ?></td>
                            <td><?php echo $value['name']; ?></td>
                            <td><?php echo $value['con_no']; ?></td>
                            <td><?php echo $value['date1']; ?></td>
                            <td><?php echo $comment; ?></td>
                            <!-- <td class="pull-right">
                              <?php
                              // if ($value['tic_id'] == '' || $value['tic_id'] == '0') {
                              //   if ($val == 12 || $val == 15) {

                              ?>
                                  <a class="btn btn-default btn-xs btn_ticket" data-value="<?php //echo $value['id']; 
                                                                                            ?>" data-toggle="modal" data-target="#chat_open" title="Give Reminder">
                                    <i class="fa fa-bell"></i>
                                  </a>
                                <?php //}
                                //} else { 
                                ?>
                                <a class="btn btn-default btn-xs btn_history" data-value="<?php //echo $value['id']; 
                                                                                          ?>" data-tic="<?php //echo $value['tic_id']; 
                                                                                                                                    ?>" data-toggle="modal" data-target="#chat_history" title="Reply">
                                  <i class="fa fa-comment"></i>
                                </a>
                              <?php //} 
                              ?>
                            </td> -->

                          </tr>
                        <?php
                          $row_count++;
                        }

                        ?>
                      </tbody>
                    </table>
                  </div>
                </form>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="chat_open" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title"> Chat Message</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
          <div class="box-body">
            <input type="hidden" name="con_id" id="con_id" value="">

            <div class="form-group">
              <label for="email">Message</label><small class="req"> *</small>
              <textarea name="message" id="compose-textarea" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Send</button>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="chat_history" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="col-md-12" id="edit_data">
          <!-- DIRECT CHAT SUCCESS -->
          <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Chat Messages</h3>

            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="direct-chat-messages" style="height:500px;">
                <strong>
                  <p class="chat_contact_no"></p>
                </strong>

                <div id="chat_list">


                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <form action="javascript:void(0);" id="form_send">
                <input type="hidden" name="id_up" id="id_up">
                <input type="hidden" name="tic_id" id="tic_id">
                <div class="input-group">
                  <input type="text" name="reply_up" id="reply_up" placeholder="Type Message ..." class="form-control" required>
                  <span class="input-group-btn">
                    <button class="btn btn-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Send</button>
                  </span>
                </div>
              </form>
            </div><!-- /.box-footer-->
          </div>
          <!--/.direct-chat -->
        </div><!-- /.col -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn_close">Close Chat</button>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
  /**************************** DATE ***********************/
  $('#date_range').daterangepicker({
    autoclose: false,
    todayBtn: true,
    pickerPosition: "bottom-left"
  });

  function formHandler(date) {
    var menu = $("#menu").val();
    window.location.href = "<?php echo base_url() ?>Con_Call_Report/index/" + menu + "/" + date;
  }

  // $.post("<?php //echo base_url() 
              ?>Con_Call_Report/get_data", {
  //   get_data: "data",
  // }, function(data) {
  //   $('#user').text(data.Name);
  //   $('#register').text(data.Registered);
  //   $('#accept').text(data.accept);
  //   $("#reject").text(data.Rejected);
  //   $("#tot").text(data.total);

  // }, "json");

  /**************************** INSERT  ****************************/
  $('.btn_ticket').click(function() {
    var con_id = $(this).attr('data-value');
    $("#con_id").val(con_id);
  });

  $("#form_submit").on('submit', (function(e) {
    var menu = $("#menu").val();
    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/add_ticket",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'data exists') {
            swal({
              title: "",
              text: "Bank Already Exists!",
              type: "warning",
              timer: 2000,
              showConfirmButton: false,
            });

          } else if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Added!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              window.location.href = "<?php echo base_url() ?>Con_Call_Report/index/" + menu;
            }, 2000);

          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /*********************************  TICKET ******************************/
  $("#form_send").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/send_reply",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal("Sent!", "Successfully Sent Message!", "success");
            location.reload();
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  $('.btn_history').click(function() {
    var con_id = $(this).attr('data-value');
    $("#id_up").val(con_id);
    var tic_id = $(this).attr('data-tic');
    $("#tic_id").val(tic_id);

    $.post('<?php echo base_url() ?>Con_Call_Report/get_history', {
      'get_history': 'data',
      con_id: con_id
    }, function(data) {
      $('.chat_contact_no').text('');
      var Data = "";
      var reply_id = data.reply_id;


      if (data === undefined || data.length === 0 || data === null) {

        Data = '<div> <h4>This chat has no records!!</h4> </div>';
        $('#chat_list').html('').append(Data);

      } else {

        $.each(data.result, function(index, data) {
          var msg = "";
          var val = data.Val;
          var time = data.dt_time;
          var contact_no = data.con_no;
          var std_name = data.st_name;
          if (std_name == '') {
            var st_name = '';
          } else {
            var st_name = "Name: " + data.st_name + " | ";
          }
          var whatsapp = data.whatsapp;
          if (whatsapp == '') {
            var whatsapp = '';
          } else {
            var whatsapp = "Whatsapp: " + data.whatsapp;
          }
          $('.chat_contact_no').text(st_name + "\nContact No : " + contact_no + "\n | " + whatsapp);

          var user_type = '';
          var class_type = '';
          var user_name = '';

          if ((val == '12') || (val == '15')) {
            user_type = 'left';
            class_type = 'left';
            user_name = data.Name;
          } else {
            user_type = 'right';
            class_type = 'right';
            user_name = data.Name;
          }

          Data += '<div class="direct-chat-msg ' + user_type + '">';
          Data += '<div class="direct-chat-info clearfix">';
          Data += '<span class="direct-chat-name pull-' + class_type + '">' + user_name + ' &nbsp;</span>';
          Data += '<span class="direct-chat-timestamp pull-' + user_type + '">' + time + '&nbsp;</span>';
          Data += '</div>';
          Data += '<img class="direct-chat-img" src="<?php echo base_url(); ?>uploads/student_images/no_image.png" alt="message user image">';
          Data += '<div class="direct-chat-text">';
          Data += data.message;
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
        });

        $('#chat_list').html('').append(Data);
      }
    }, "json");
  });

  /******************************** CLOSE CHAT ***************************/
  $(".btn_close").click(function() {
    var tic_id = $("#tic_id").val();
    var menu = $("#menu").val();

    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, close it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_Call_Report/close_chat", {
            close_chat: "data",
            tic_id: tic_id
          }, function(data) {
            if ($.trim(data.status) === 'success') {
              swal("Closed", "Successfully Closed Chat!", "success");
              window.location.href = "<?php echo base_url() ?>Con_Call_Report/index/" + menu;

            } else if ($.trim(data.status) === ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          }, "json");
        } else {
          swal("Cancelled", "", "error");
        }
      });

  });
</script>