<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1>
      <i class="fa fa-gears"></i>Add Branch</h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-3">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Branch</h3>
            </div>
            <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>"> 
            <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
              <div class="box-body"> 

                <div class="form-group">                 
                  <label for="name">Bank Name<font color="#FF0000"><strong>*</strong></font></label>
                  <!-- <input type="text" class="form-control clr" name="bank_name" id="bank_name" placeholder="Enter ..." autocomplete="off" required> -->
                  <select class="form-control" name="bank_name" id="bank_name" style="display: block;">
                    <?php
                    echo "<option></option>"; 
                    foreach($banks as $b) {
                      echo "<option value=".$b->id.">".$b->bank_name."</option>";
                    }
                    ?>
                  </select>
                </div>
                
                <input type="hidden" class="form-control" name="bank_id" id="bank_id" placeholder="Enter ..." autocomplete="off">


                <div class="form-group">                 
                  <label for="url">Branch Name<font color="#FF0000"><strong>*</strong></font></label>
                  <input type="text" class="form-control clr" name="branch_name" id="branch_name" placeholder="Enter ..." autocomplete="off" required>
                </div>

                <div class="form-group">                 
                  <label for="url">Branch Code<font color="#FF0000"><strong>*</strong></font></label>
                  <input type="text" class="form-control clr" name="code" id="code" placeholder="Enter ..." autocomplete="off" required>
                </div>



              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
            </form>
          </div>  
        </div>   
        <div class="col-md-9">             
          <div class="box box-primary">
            <div class="box-header ptbnull">
              <h3 class="box-title titlefix">Branch List</h3>
            </div>
            <div class="box-body ">
              <div class="table-responsive mailbox-messages">
                <div class="download_label">Branch List</div>
                <table class="table table-striped table-bordered table-hover example">
                  <thead>
                    <tr>
                      <th></th>
                      <th> Branch Name </th>
                      <th> Branch Code </th>
                      
                      
                      <th class="text-right">Action</th>
                    </tr>
                  </thead>
                  <tbody>                                   

                    <?php
                    $count = 0;
                    
                    foreach($load_data as $value) { $count++; 
                      ?>
                      <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $value->branch_name;?></td>
                        <td><?php echo $value->code;?></td>
                        
                        
                        <td class="mailbox-date pull-right">

                          <button class="btn btn-default btn-xs btn_up" data-toggle="modal" data-target="#edit_data"   title="Edit" value="<?php echo $value->id; ?>">
                            <i class="fa fa-pencil"></i>
                          </button>
                          
                          <button class="btn btn-default btn-xs btn_del"  data-toggle="tooltip" title="Delete" >
                            <i class="fa fa-remove"></i>
                          </button>
                        </td>
                      </tr>
                      <?php
                    }
                    
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div> 
        
      </div> 

    </section>
  </div>

  <!-- <div class="modal fade" id="edit_data" role="dialog">
    <div class="modal-dialog">       
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title title text-center modal_title"> Edit Bank</h4>
        </div>
        <div class="modal-body">
          <form action="javascript:void(0);" id="form_update">
            <input type="hidden" name="id_up" id="id_up">

            <div class="form-horizontal">
              <div class="box-body">


                <div class="form-group">                 
                  <label>Bank Name:</label>
                  <input type="text" class="form-control" name="bank_name_up" id="bank_name_up" placeholder="Enter ..." autocomplete="off">
                </div>

                <div class="form-group">                 
                  <label>Bank Code:</label>
                  <input type="text" class="form-control" name="code_up" id="code_up" placeholder="Enter ..." autocomplete="off">
                </div>

                
              </div>                   
            </div>

            <div class="box-footer"> 
              <button class="btn btn-danger">UPDATE</button>
            </div>

          </form>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div> -->


  <script>
    /**************************** INSERT  ****************************/

    $("#form_submit").on('submit',(function(e) {
      var menu = $("#menu").val();

      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url()?>Con_add_branch/add",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
        },
        success: function(data){
          if($.trim(data) === '') {
            swal("Oops...", "Something went wrong!", "warning");

          } else {
            if($.trim(data) ==='error')
            {
              swal("SQL Error!", "Please Try Again!", "warning");

            } else if($.trim(data) ==='order no exists') {
              swal("Exists!", "Already Exists!", "warning");
            } else if($.trim(data) ==='data exists') {
              $('#pid').focus();
              swal({
                title: "",
                text: "Already Exists!",
                type: "warning",
                timer: 2000,
                showConfirmButton: false,
              });           

            } else if($.trim(data) ==='success') {
              swal({
                title: "",
                text: "Successfully Added!",
                type: "success",
                timer: 2000,
                showConfirmButton: false,
              });

              $(".clr").val('');
              setTimeout(function() 
              {
                //window.location.href = "<?php //cho base_url()?>Con_menu_list";
              }, 2000);
              
            }
          }
        },
        error: function(e) 
        {
         swal({
          title: "Error!",
          text: "Try Again",
          type: "warning",
          timer: 2000,
          showConfirmButton: false,
        });
         setTimeout(function() 
         {
          window.location.href = "<?php echo base_url()?>Con_Contact_CSV/index/"+menu;
        }, 2000);
       }
     });
    }));

    /********************************* EDIT ******************************/

    $("#form_update").on('submit',(function(e) {
      var menu = $("#menu").val();

      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url()?>Con_add_branch/update",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
        },
        success: function(data){
          $('#edit_data').modal('hide')
          if($.trim(data) === '') {
            swal("Oops...", "Something went wrong!", "warning");

          } else {
            if($.trim(data) ==='error')
            {
              swal("SQL Error!", "Please Try Again!", "warning");

            } else if($.trim(data) ==='data not exists') {
              swal("", "Menu Category Already Exist!", "warning");

            } else if($.trim(data) ==='success') {
              swal({
                title: "Updated!",
                text: "Successfully Updated!",
                type: "success",
                confirmButtonText: "OK"
              },
              function (isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url()?>Con_add_branch/index/"+menu;
                }
              });
            }
          }      
        },
        error: function(e) 
        {
         swal({
          title: "Error!",
          text: "Try Again",
          type: "warning",
          timer: 2000,
          showConfirmButton: false,
        });
         setTimeout(function() 
         {
          window.location.href = "<?php echo base_url()?>Con_Contact_CSV/index/"+menu;
        }, 2000);
       }
     });
    }));

    /******************************** Remove ***************************/

    $(".btn_del").click(function(){
      var row_id = $(this).val();
      var menu = $("#menu").val();

      swal({
       title: "Are you sure?",
       text: "",
       type: "warning",
       showCancelButton: true,
       confirmButtonClass: "btn-danger",
       confirmButtonText: "Yes, delete it!",
       cancelButtonText: "No, cancel!",
       closeOnConfirm: false,
       closeOnCancel: false
     },
     function(isConfirm) {
       if (isConfirm) {
        $.post( "<?php echo base_url()?>Con_add_branch/delete", {remove_data: "data", id : row_id}, function( data ) {   
         if($.trim(data.status) === 'success') {
           swal({
            title: "Deleted!",
            text: "Successfully Deleted!",
            type: "success",
            confirmButtonText: "OK"
          },
          function (isConfirm) {
            if (isConfirm) {
              window.location.href = "<?php echo base_url()?>Con_add_branch/index/"+menu;
            }
          });

         } else if($.trim(data.status) ===' error') {
           swal("", "Error!", "warning");

         } else{
           swal("Oops...", "Something went wrong!", "warning");
         }
       }, "json");
      } else {
        swal("Cancelled", "", "error");
      }
    });

    }); 



    /**************************** LOAD FORM DATA ***********************/

    $(".btn_up").click(function(){
      // var bank_name = $("#bank_name").val();
      var id = $(this).val();
      // $("#bank_name_up").val(bank_name_up);

      // console.log(bank_name);
      $.post( "<?php echo base_url()?>Con_add_branch/get_dataset", { get_dataset: "data", id : id}, function( data ) {
        $.each(data.result, function (index, data) {

          $("#bank_name_up").val(data.bank_name);    
          $("#code_up").val(data.code); 
          
        });

      }, "json");   
    });

    

  </script>

  <script>
    $("#bank_name").change(function(){

     // var bank_name = $(this).val();
     var bank_name = $("#bank_name option:selected").text();
     var bank_id = $("#bank_name").val();
     $('#bank_id').val(bank_id);


    //  $.post('<?php echo base_url()?>Con_add_branch/get_bank_id', {'get_bank_id': 'data', bank_id: bank_id}, function (data) {
    //   $.each(data, function (index, data) {
    //     $('#bank_id').val(data.bank_id);
    //   });

    // }, "json");

  });

</script>