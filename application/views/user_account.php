<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1>
      <i class="fa fa-gears"></i> User Account
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">

      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add User Account</h3>
          </div>
          <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
          <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
            <div class="box-body">

              <div class="form-group">
                <label>Select Employee:<font color="#FF0000"><strong>*</strong></font></label>
                <select autofocus class="form-control txt_nav" name="emp_id" id="emp_id" style="width: 100%;" required>
                  <?php
                  echo "<option ></option>";
                  foreach ($staff as $emp) {
                    echo "<option value=" . $emp->ID . ">" . $emp->Name . "</option>";
                  }
                  ?>
                </select>
              </div>

              <div class="form-group">
                <label>Select Group Profile:<font color="#FF0000"><strong>*</strong></font></label>
                <select class="form-control txt_nav" name="prof_id" id="prof_id" style="width: 100%;" required>
                  <?php
                  echo "<option ></option>";
                  foreach ($profile as $prof) {
                    echo "<option value=" . $prof->id . ">" . $prof->profile . "</option>";
                  }
                  ?>
                </select>
              </div>

              <div class="form-group">
                <label>User Name:<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control" name="user_name" id="user_name" placeholder="Enter ..." autocomplete="off" required>
                <label id="username_error" class="text-red" for="user_name" style="display: none;">User Name Already Exists!</label>
              </div>

              <div class="form-group">
                <label>Password:<font color="#FF0000"><strong>*</strong></font></label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Enter ..." autocomplete="off" required>
              </div>
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-info pull-right">Save</button>
            </div>
          </form>
        </div>
      </div>
      <div class="col-md-8">
        <div class="box box-primary">
          <div class="box-header ptbnull">
            <h3 class="box-title titlefix">User Account List</h3>
          </div>
          <div class="box-body ">
            <div class="table-responsive mailbox-messages" style="overflow-y: scroll; overflow-x:scroll;">
              <div class="download_label">User Account List</div>
              <table class="table table-striped table-bordered table-hover example22">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Emp Name</th>
                    <th>Acc. No</th>
                    <th>User</th>
                    <th>Group Profile</th>
                    <th>Call Center ID</th>
                    <th>Agent Code</th>
                    <th class="text-right">Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                  $count = 0;

                  foreach ($load_data as $value) {
                    $count++;
                  ?>
                    <tr>
                      <td class="left"><?php echo $count; ?>.</td>
                      <td><?php echo $value->Name; ?></td>
                      <td><?php echo $value->acc_no; ?></td>
                      <td><?php echo $value->ID; ?></td>
                      <td><?php echo $value->profile; ?></td>
                      <td><?php echo $value->call_ID; ?></td>
                      <td><?php echo $value->agent_code; ?></td>
                      <td class="mailbox-date pull-right">

                        <button class="btn btn-default btn-xs btn_up" data-toggle="modal" data-target="#edit_data" value="<?php echo $value->acc_no; ?>" title="Edit">
                          <i class="fa fa-pencil"></i>
                        </button>
                      </td>
                    </tr>
                  <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>

  </section>
</div>

<div class="modal fade" id="edit_data" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title"> Edit User Account</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_update">
          <input type="hidden" name="id_up" id="id_up">

          <div class="form-horizontal">
            <div class="box-body">
              <div class="form-group">
                <label>Employee</label>
                <input type="text" class="form-control" id="emp_up" style="width: 100%;" required readonly>
              </div>

              <div class="form-group">
                <label>Account No</label>
                <input type="text" class="form-control" id="acc_up" style="width: 100%;" required readonly>
              </div>

              <div class="form-group">
                <label>Select Group Profile:<font color="#FF0000"><strong>*</strong></font></label>
                <select class="form-control" name="prof_id_up" id="prof_id_up" style="width: 100%;" required>
                  <?php
                  echo "<option ></option>";
                  foreach ($profile as $prof) {
                    echo "<option value=" . $prof->id . ">" . $prof->profile . "</option>";
                  }
                  ?>
                </select>
              </div>

              <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" id="user_up" placeholder="Enter ..." autocomplete="off" required readonly>
              </div>

              <div class="form-group">
                <label>Password:<font color="#FF0000"><strong>*</strong></font></label>
                <input type="password" class="form-control" name="password_up" id="password_up" placeholder="Enter ..." autocomplete="off" required>
              </div>

              <div class="form-group">
                <label>Call Center ID:</label>
                <input type="text" class="form-control" name="callID_up" id="callID_up" placeholder="Enter ..." autocomplete="off">
              </div>

              <div class="form-group">
                <label>Agent Code:</label>
                <input type="text" class="form-control" name="agent_code" id="agent_code" placeholder="Enter ..." autocomplete="off">
              </div>

            </div>
          </div>

          <!-- /.box-body -->
          <div class="box-footer">
            <button class="btn btn-danger" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Update</button>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>


<script>
  /******************** ONLY NUMERIC VALUES ********************/
  function isNumberKey(evt, obj) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
      if (charCode == 46) return false;
    if (charCode == 46) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  /******************** USERNAME EXISTS ********************/
  $('#user_name').keyup(function() {
    var user_name = $(this).val();

    var user_exist = true;

    $.post("<?php echo base_url() ?>Con_user_account/check_user", {
      check_user: "data",
      user_name: user_name
    }, function(data) {

      if ($.trim(data) === 'success') {
        $('#username_error').fadeOut();
        user_exist = false;
        $("#user_name").css("border", "1px solid #367fa9");
        $(':input[type="submit"]').prop('disabled', false);
      } else {
        $('#username_error').fadeIn();
        user_exist = true;
        $("#user_name").css("border", "1px solid #d73925");
        $(':input[type="submit"]').prop('disabled', true);
      }
    });
  });

  /******************** INSERT ********************/
  $("#form_submit").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_user_account/create_user",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        if ($.trim(data) === '') {
          swal("SQL Error!", "Please Try Again!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'data exists') {
            swal({
              title: "",
              text: "User Already Exists!",
              type: "warning",
              timer: 2000,
              showConfirmButton: false,
            });

          } else if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Added!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(
              function() {
                window.location.href = "<?php echo base_url() ?>Con_user_account/index/" + menu;
              }, 2000);

          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /********************************* Edit ******************************/

  $("#form_update").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_user_account/update",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'exists') {
            swal("", "Call Center ID Already Exists. Please Try Again!", "warning");

          } else if ($.trim(data) === 'agent code exists') {
            swal("", "Agent Code Already Exists. Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal({
                title: "Updated!",
                text: "Successfully Updated!",
                type: "success",
                confirmButtonText: "OK"
              },
              function(isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url() ?>Con_user_account/index/" + menu;
                }
              });
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /**************************** LOAD FORM DATA ***********************/

  $(".btn_up").click(function() {

    var id = $(this).val();
    $("#id_up").val(id);

    $.post("<?php echo base_url() ?>Con_user_account/get_dataset", {
      get_dataset: "data",
      id: id
    }, function(data) {
      $.each(data.result, function(index, data) {
        $("#emp_up").val(data.Name);
        $("#prof_id_up").val(data.Val);
        $("#user_up").val(data.ID);
        $("#acc_up").val(data.acc_no);
        $("#password_up").val(data.Pws);
        $("#callID_up").val(data.call_ID);
        $("#agent_code").val(data.agent_code);

        if (data.cash == "1") {
          $("#cashier_up").prop("checked", true);
        } else {
          $("#cashier_up").prop("checked", false);
        }

        if (data.parent_portal == "1") {
          $("#parent_up").prop("checked", true);
        } else {
          $("#parent_up").prop("checked", false);
        }
      });

    }, "json");

  });

  $(document).ready(function() {
    var col_len = '';
    $('.example22').DataTable({
      "aaSorting": [],

      rowReorder: {
        selector: 'td:nth-child(2)'
      },
      dom: "Bfrtip",
      buttons: [

        {
          extend: 'copyHtml5',
          text: '<i class="fa fa-files-o"></i>',
          titleAttr: 'Copy',
          title: $('.download_label').html(),
          exportOptions: {
            columns: ':visible'
          }
        },

        {
          extend: 'excelHtml5',
          header: true,


          text: '<i class="fa fa-file-excel-o"></i>',
          titleAttr: 'Excel',

          title: $('.download_label').html(),
          exportOptions: {
            columns: [0, 1, 2, 3, 4, 5, 6, 7],

          }
        },

        {
          extend: 'csvHtml5',
          text: '<i class="fa fa-file-text-o"></i>',
          titleAttr: 'CSV',
          title: $('.download_label').html(),
          exportOptions: {
            columns: ':visible'
          }
        },

        {
          extend: 'pdfHtml5',
          text: '<i class="fa fa-file-pdf-o"></i>',
          titleAttr: 'PDF',
          title: $('.download_label').html(),
          exportOptions: {
            columns: ':visible'

          }
        },

        {
          extend: 'print',
          text: '<i class="fa fa-print"></i>',
          titleAttr: 'Print',
          title: $('.download_label').html(),
          customize: function(win) {
            $(win.document.body)
              .css('font-size', '10pt');

            $(win.document.body).find('table')
              .addClass('compact')
              .css('font-size', 'inherit');
          },
          exportOptions: {
            columns: ':visible'
          }
        },

        {
          extend: 'colvis',
          text: '<i class="fa fa-columns"></i>',
          titleAttr: 'Columns',
          title: $('.download_label').html(),
          postfixButtons: ['colvisRestore']
        },
      ]
    });
  });
</script>