<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i> Workshop Student
            <button class="btn btn-info btn-md btn_reallocate pull-right" value="<?php echo  $this->uri->segment(4); ?>" title="Reallocate" data-toggle="modal" data-target="#edit_data" style="background-color: #bd5afb; border-color: #bd5afb"><i class="fa fa-star"> Reallocate Workshop</i></button>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Call Details</h3>
                    </div>
                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                    <input type="hidden" name="num" id="num" value="<?php echo  $this->uri->segment(4); ?>">
                    <form class="form-sample" id="form_con_no" action="javascript:void(0);">
                        <div class="box-body">
                            <div class="form-group" align="center">
                                <label id="number" style="font-size: 30px;"></label><br>
                                <label id="st_name" style="font-size: 30px;"></label>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group response_set" align="center" style="font-size: 15px; display: none;">

                                <button style="display: none;" class="btn btn-info btn-sm btn_sms2" value="" title="Send SMS"><i class="fa fa-mobile"> Workshop</i></button>

                                <button class="btn btn-info btn-sm btn_reminder2" value="" title="Reminder" style="background:#63c2f9; border-color:#63c2f9;" style="display: none;"><i class="fa fa-bell"> Reminder</i></button>

                                <button class="btn btn-info btn-sm btn_feedback2" value="" title="Feedback SMS" style="background-color: #f39c12; border-color: #f39c12;"><i class="fa fa-reply"> Yes / No</i></button>

                                <button class="btn btn-success btn-sm btn_bank2" value="" title="Bank SMS"><i class="fa fa-bank"> Bank</i></button>

                                <button class="btn btn-danger btn-sm btn_payment p1" value="" data-flag="1" title="Pending Payment"><i class="fa fa-money"> Pending Payment</i></button>

                                <button class="btn btn-danger btn-sm btn_payment p2" value="" data-flag="0" title="Reverse Payment" style="background-color: #f9998d; border-color: #f9998d;"><i class="fa fa-money"> Reverse Payment</i></button>

                                <button class="btn btn-success btn-sm btn_pending_all d1" value="" data-flag="1" title="Pending Allocation" style="background-color: #bd5afb; border-color: #bd5afb"><i class="fa fa-reply"> Pending Allocation</i></button>

                                <button class="btn btn-success btn-sm btn_pending_all d2" value="" data-flag="0" title="Pending Deallocation" style="background-color: #d99dff; border-color: #d99dff;"><i class="fa fa-reply"> Pending Deallocation</i></button>

                                <button class="btn btn-info btn-sm btn_reject" value="" title="Reject"><i class="fa fa-close"> Reject</i></button>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <div class="row">
            <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
            <input type="hidden" name="wid" id="wid" value="<?php echo  $this->uri->segment(4); ?>">

            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Workshop Students</h3>
                    </div>
                    <div class="box-body ">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label">Workshop Students</div>
                            <table class="table table-striped table-bordered table-hover example22">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Reg No </th>
                                        <th>Student Name </th>
                                        <th>Contact No</th>
                                        <th>Try</th>
                                        <th>Action</th>
                                        <th>Workshop SMS</th>
                                        <th>Reminder SMS</th>
                                        <th>Yes/No SMS</th>
                                        <th>Yes/No Status</th>
                                        <th>Bank SMS</th>
                                        <th>Chat</th>
                                        <th>Workshop Date</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $count = 0;

                                    foreach ($load_data as $value) {
                                        $count++;
                                        if ($value->sms_flag == 1) {
                                            $feedback = '<i class="fa fa-check"></i>';
                                        } else {
                                            $feedback = '<i class="fa fa-close"></i>';
                                        }

                                        if ($value->ws_reminder == 1) {
                                            $ws_reminder = '<i class="fa fa-check"></i>';
                                        } else {
                                            $ws_reminder = '<i class="fa fa-close"></i>';
                                        }

                                        if ($value->confirmation >= 1) {
                                            $confirmation = '<i class="fa fa-check"></i>';
                                        } else {
                                            $confirmation = '<i class="fa fa-close"></i>';
                                        }

                                        if ($value->confirmation == 3) {
                                            $confirmation_st = 'Yes';
                                        } else if ($value->confirmation == 2) {
                                            $confirmation_st = 'No';
                                        } else {
                                            $confirmation_st = '';
                                        }

                                        if ($value->bank_sms == 1) {
                                            $bank = '<i class="fa fa-check"></i>';
                                        } else {
                                            $bank = '<i class="fa fa-close"></i>';
                                        }

                                        if ($value->pending_payment == 1) {
                                            $pay = 'background-color: #fff8a2';
                                        } else {
                                            $pay = '';
                                        }

                                        $date = $value->w_date . ' ' . date("h:i A", strtotime($value->w_time));
                                        $currentDate = strtotime($date);
                                        $futureDate = $currentDate + (60 * 5);
                                        $formatDate = date("Y-m-d H:i:s", $futureDate);
                                        $nowDate = date("Y-m-d H:i:s");

                                        if ($formatDate <= $nowDate) {
                                            $txt = "display:none";
                                        } else {
                                            $txt = '';
                                        }
                                    ?>

                                        <tr style="<?php echo $pay; ?> cursor: pointer;" onclick="getButtons('<?php echo $value->reg_no; ?>', '<?php echo $value->con_no; ?>', '<?php echo $value->name; ?>', '<?php echo $value->pending_payment; ?>', '<?php echo $value->pending_reallocation; ?>', '<?php echo $txt; ?>')">
                                            <td><?php echo $count; ?>.</td>
                                            <td><?php echo $value->reg_no; ?></td>
                                            <td><?php echo $value->name; ?></td>
                                            <td><?php echo $value->con_no; ?></td>
                                            <td><?php if ($value->feedback_no == 0) {
                                                    echo "";
                                                } else {
                                                    echo $value->feedback_no;
                                                }; ?></td>
                                            <td class="mailbox-date pull-right">
                                                <button class="btn btn-info btn-sm btn_sms2" value="<?php echo $value->reg_no; ?>" title="Send SMS" style="<?php echo $txt; ?>"><i class="fa fa-mobile"> Workshop</i></button>
                                                <button class="btn btn-info btn-sm btn_reminder2" value="<?php echo $value->reg_no; ?>" title="Reminder" style="background:#63c2f9; border-color:#63c2f9; <?php echo $txt; ?>"><i class="fa fa-bell"> Reminder</i></button>
                                                <button class="btn btn-info btn-sm btn_feedback2" value="<?php echo $value->reg_no; ?>" title="Feedback SMS" style="background-color: #f39c12; border-color: #f39c12"><i class="fa fa-reply"> Yes / No</i></button>
                                                <button class="btn btn-success btn-sm btn_bank2" value="<?php echo $value->reg_no; ?>" title="Bank SMS"><i class="fa fa-bank"> Bank</i></button>
                                                <?php if ($value->pending_payment == 0) { ?>
                                                    <button class="btn btn-danger btn-sm btn_payment" value="<?php echo $value->reg_no; ?>" data-flag="1" title="Pending Payment"><i class="fa fa-money"> Pending Payment</i></button>
                                                <?php } else { ?>
                                                    <button class="btn btn-danger btn-sm btn_payment" value="<?php echo $value->reg_no; ?>" data-flag="0" title="Reverse Payment" style="background-color: #f9998d; border-color: #f9998d;"><i class="fa fa-money"> Reverse Payment</i></button>
                                                <?php } ?>
                                                <?php if ($value->pending_reallocation == 0) { ?>
                                                    <button class="btn btn-success btn-sm btn_pending_all" value="<?php echo $value->reg_no; ?>" data-flag="1" title="Pending Allocation" style="background-color: #bd5afb; border-color: #bd5afb"><i class="fa fa-reply"> Pending Allocation</i></button>
                                                <?php } else { ?>
                                                    <button class="btn btn-success btn-sm btn_pending_all" value="<?php echo $value->reg_no; ?>" data-flag="0" title="Pending Deallocation" style="background-color: #d99dff; border-color: #d99dff;"><i class="fa fa-reply"> Pending Deallocation</i></button>
                                                <?php } ?>
                                                <button class="btn btn-info btn-sm btn_reject" value="<?php echo $value->reg_no; ?>" title="Reject"><i class="fa fa-close"> Reject</i></button>
                                            </td>
                                            <td><?php echo $feedback; ?></td>
                                            <td><?php echo $ws_reminder; ?></td>
                                            <td><?php echo $confirmation; ?></td>
                                            <td><?php echo $confirmation_st; ?></td>
                                            <td><?php echo $bank; ?></td>
                                            <td>
                                                <?php
                                                if ($value->tic_id == '' || $value->tic_id == '0') {

                                                ?>
                                                    <a class="btn btn-default btn-xs btn_ticket" data-value="<?php echo $value->cid; ?>" data-toggle="modal" data-target="#chat_open" title="Give Reminder">
                                                        <i class="fa fa-bell"></i>
                                                    </a>
                                                <?php
                                                } else { ?>
                                                    <a class="btn btn-default btn-xs btn_history" data-value="<?php echo $value->cid; ?>" data-tic="<?php echo $value->tic_id; ?>" data-toggle="modal" data-target="#chat_history" title="Reply">
                                                        <i class="fa fa-comment"></i>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                            <td><?php if ($value->prev_wdate == '0000-00-00' || $value->prev_wdate == '') {
                                                    echo "";
                                                } else {
                                                    echo $value->prev_wdate;
                                                }; ?></td>
                                        </tr>
                                    <?php
                                    }

                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
</div>

<div class="modal fade" id="edit_data" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title text-center modal_title"> Reallocate Workshop</h4>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" id="form_update">
                    <input type="hidden" name="id_up" id="id_up">
                    <input type="hidden" name="wid_up" id="wid_up">

                    <div class="form-horizontal">
                        <div class="box-body">

                            <div class="form-group">
                                <label>Workshop:</label>
                                <select id="workshop" name="workshop" class="form-control" required autofocus>
                                    <option value="">Select</option>
                                    <?php foreach ($workshops as $workshop) { ?>
                                        <option value="<?php echo $workshop->id ?>"><?php echo $workshop->w_name . " (" . $workshop->w_date . " / " . date("h:i A", strtotime($workshop->w_time)) . ")"; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="box-footer">
                        <button class="btn btn-danger">REALLOCATE</button>
                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="chat_open" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title text-center modal_title"> Chat Messages</h4>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
                    <div class="box-body">
                        <input type="hidden" name="con_id" id="con_id" value="">

                        <div class="form-group">
                            <label for="email">Message</label><small class="req"> *</small>
                            <textarea name="message" id="compose-textarea" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Send</button>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="chat_history" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-md-12" id="edit_data2">
                    <!-- DIRECT CHAT SUCCESS -->
                    <div class="box box-primary direct-chat direct-chat-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Chat Messages</h3>

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="direct-chat-messages" style="height:500px;">
                                <strong>
                                    <p class="chat_contact_no"></p>
                                </strong>

                                <div id="chat_list">


                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <form action="javascript:void(0);" id="form_send">
                                <input type="hidden" name="id_up" id="cid_up">
                                <input type="hidden" name="tic_id" id="tic_id">
                                <div class="input-group">
                                    <input type="text" name="reply_up" id="reply_up" placeholder="Type Message ..." class="form-control" required>
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Send</button>
                                    </span>
                                </div>
                            </form>
                        </div><!-- /.box-footer-->
                    </div>
                    <!--/.direct-chat -->
                </div><!-- /.col -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn_close">Close Chat</button>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script>
    /**************************** BUTTONS  ****************************/
    function getButtons(reg_no, telNo, name, pd_pay, pd_reall, dp) {
        $('#number').text(telNo);
        $('#st_name').text(name);

        if (dp == '') {
            $('.btn_sms2').show();
            $('.btn_reminder2').show();
        } else {
            $('.btn_sms2').hide();
            $('.btn_reminder2').hide();
        }

        if (pd_pay == '0') {
            $('.p1').show();
            $('.p2').hide();
        } else {
            $('.p2').show();
            $('.p1').hide();
        }

        if (pd_reall == '0') {
            $('.d1').show();
            $('.d2').hide();
        } else {
            $('.d2').show();
            $('.d1').hide();
        }

        $('.response_set').show();
        $('.btn_sms2').val(reg_no);
        $('.btn_reminder2').val(reg_no);
        $('.btn_feedback2').val(reg_no);
        $('.btn_bank2').val(reg_no);
        $('.btn_payment').val(reg_no);
        $('.btn_pending_all').val(reg_no);
        $('.btn_reject').val(reg_no);
        $(document).scrollTop(0);
    }
    /**************************** INSERT  ****************************/
    $('.btn_ticket').click(function() {
        var con_id = $(this).attr('data-value');
        $("#con_id").val(con_id);
    });

    $("#form_submit").on('submit', (function(e) {
        var menu = $("#menu").val();
        var wid = $("#wid").val();

        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>Con_Call_Report/add_ticket",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                } else {
                    if ($.trim(data) === 'error') {
                        swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'data exists') {
                        swal({
                            title: "",
                            text: "Bank Already Exists!",
                            type: "warning",
                            timer: 2000,
                            showConfirmButton: false,
                        });

                    } else if ($.trim(data) === 'success') {
                        swal({
                            title: "",
                            text: "Successfully Added!",
                            type: "success",
                            timer: 2000,
                            showConfirmButton: false,
                        });
                        setTimeout(function() {
                            window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + '/' + wid;
                        }, 2000);

                    }
                }
            },
            error: function(e) {
                alert("err2");
            }
        });
    }));

    /*********************************  TICKET ******************************/
    $("#form_send").on('submit', (function(e) {
        var menu = $("#menu").val();
        var wid = $("#wid").val();

        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>Con_Call_Report/send_reply",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                $('#edit_data2').modal('hide')
                if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                } else {
                    if ($.trim(data) === 'error') {
                        swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                        swal({
                                title: "Sent!",
                                text: "Successfully Sent Message!",
                                type: "success",
                                confirmButtonText: "OK"
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + '/' + wid;
                                }
                            });
                    }
                }
            },
            error: function(e) {
                alert("err2");
            }
        });
    }));

    $('.btn_history').click(function() {
        var con_id = $(this).attr('data-value');
        $("#cid_up").val(con_id);
        var tic_id = $(this).attr('data-tic');
        $("#tic_id").val(tic_id);

        $.post('<?php echo base_url() ?>Con_Call_Report/get_history', {
            'get_history': 'data',
            con_id: con_id
        }, function(data) {

            var Data = "";
            var reply_id = data.reply_id;


            if (data === undefined || data.length === 0 || data === null) {

                Data = '<div> <h4>This chat has no records!!</h4> </div>';
                $('#chat_list').html('').append(Data);

            } else {

                $.each(data.result, function(index, data) {
                    var msg = "";
                    var val = data.Val;
                    var time = data.dt_time;
                    var contact_no = data.con_no;
                    var std_name = data.st_name;
                    if (std_name == '') {
                        var st_name = '';
                    } else {
                        var st_name = "Name: " + data.st_name + " | ";
                    }
                    var whatsapp = data.whatsapp;
                    if (whatsapp == '') {
                        var whatsapp = '';
                    } else {
                        var whatsapp = "Whatsapp: " + data.whatsapp;
                    }
                    $('.chat_contact_no').text(st_name + "\nContact No : " + contact_no + "\n | " + whatsapp);

                    var user_type = '';
                    var class_type = '';
                    var user_name = '';

                    if ((val == '12') || (val == '15')) {
                        user_type = 'left';
                        class_type = 'left';
                        user_name = data.Name;
                    } else {
                        user_type = 'right';
                        class_type = 'right';
                        user_name = data.Name;
                    }

                    Data += '<div class="direct-chat-msg ' + user_type + '">';
                    Data += '<div class="direct-chat-info clearfix">';
                    Data += '<span class="direct-chat-name pull-' + class_type + '">' + user_name + ' &nbsp;</span>';
                    Data += '<span class="direct-chat-timestamp pull-' + user_type + '">' + time + '&nbsp;</span>';
                    Data += '</div>';
                    Data += '<img class="direct-chat-img" src="<?php echo base_url(); ?>uploads/student_images/no_image.png" alt="message user image">';
                    Data += '<div class="direct-chat-text">';
                    Data += data.message;
                    Data += '</div>';
                    Data += '</div>';
                    Data += '</div>';
                    Data += '</div>';
                });

                $('#chat_list').html('').append(Data);
            }
        }, "json");
    });

    /******************************** CLOSE CHAT ***************************/
    $(".btn_close").click(function() {
        var tic_id = $("#tic_id").val();
        var menu = $("#menu").val();
        var wid = $("#wid").val();

        swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, close it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_Call_Report/close_chat", {
                        close_chat: "data",
                        tic_id: tic_id
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal({
                                    title: "Closed!",
                                    text: "Successfully Closed Chat!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + '/' + wid;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });

    });
    /**************************** SEND SMS ***********************/
    $(document).on('click', '.btn_sms2', function(e) {
        var menu = $("#menu").val();
        var wid = $("#wid").val();
        var reg_no = $(this).val();

        swal({
                title: "Are you sure you want to send the Workshop SMS?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Send  SMS!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_workshop/link_sms_individual", {
                        delete: "data",
                        reg_no: reg_no
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal({
                                    title: "Sent!",
                                    text: "Successfully Sent!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + "/" + wid;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    /**************************** REMINDER SMS ***********************/
    $(document).on('click', '.btn_reminder2', function(e) {
        var menu = $("#menu").val();
        var wid = $("#wid").val();
        var reg_no = $(this).val();

        swal({
                title: "Are you sure you want to send the Reminder SMS?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Send  SMS!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_workshop/individual_reminder", {
                        delete: "data",
                        reg_no: reg_no
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal({
                                    title: "Sent!",
                                    text: "Successfully Sent!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + "/" + wid;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    /**************************** FEEDBACK SMS ***********************/
    $(document).on('click', '.btn_feedback2', function(e) {
        var menu = $("#menu").val();
        var wid = $("#wid").val();
        var reg_no = $(this).val();

        swal({
                title: "Are you sure you want to send the Yes/No SMS?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Send  SMS!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_workshop/individual_feedback_sms", {
                        delete: "data",
                        reg_no: reg_no
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal({
                                    title: "Sent!",
                                    text: "Successfully Sent!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + "/" + wid;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    /**************************** BANK SMS ***********************/
    $(document).on('click', '.btn_bank2', function(e) {
        var menu = $("#menu").val();
        var wid = $("#wid").val();
        var reg_no = $(this).val();

        swal({
                title: "Are you sure you want to send the Bank SMS?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Send  SMS!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_workshop/individual_bank_sms", {
                        delete: "data",
                        reg_no: reg_no
                    }, function(data) {
                        if ($.trim(data.status) === 'success') {
                            swal({
                                    title: "Sent!",
                                    text: "Successfully Sent!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + "/" + wid;
                                    }
                                });

                        } else if ($.trim(data.status) === ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    }, "json");
                } else {
                    swal("Cancelled", "", "error");
                }
            });
    });

    /******************************** PENDING PAYMENT ***************************/
    $(".btn_payment").click(function() {
        var menu = $("#menu").val();
        var wid = $("#wid").val();
        var reg_num = $(this).val();
        var flag = $(this).attr('data-flag');

        swal({
                title: "Are you sure you want to proceed with the Pending Payment?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_pre_registry/pending_payment", {
                        pending_payment: "data",
                        wid: wid,
                        reg_num: reg_num,
                        flag: flag
                    }, function(data) {
                        if ($.trim(data) == 'success') {
                            swal({
                                    title: "Successful!",
                                    text: "Successfully Addded!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + "/" + wid;
                                    }
                                });

                        } else if ($.trim(data) == ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });

    });

    /******************************** PENDING REALLOCATION ***************************/
    $(".btn_pending_all").click(function() {
        var menu = $("#menu").val();
        var wid = $("#wid").val();
        var reg_num = $(this).val();
        var flag = $(this).attr('data-flag');

        swal({
                title: "Are you sure you want to add to Pending Reallocation?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_pre_registry/pending_reallocation", {
                        pending_payment: "data",
                        wid: wid,
                        reg_num: reg_num,
                        flag: flag
                    }, function(data) {
                        if ($.trim(data) == 'success') {
                            swal({
                                    title: "Successful!",
                                    text: "Successfully Addded!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + "/" + wid;
                                    }
                                });

                        } else if ($.trim(data) == ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });

    });

    /******************************** REJECT ***************************/
    $(".btn_reject").click(function() {
        var menu = $("#menu").val();
        var wid = $("#wid").val();
        var reg_num = $(this).val();

        swal({
                title: "Are you sure you want to Reject?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, reject it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo base_url() ?>Con_pre_registry/feedback_reject", {
                        feedback_yes: "data",
                        wid: wid,
                        reg_num: reg_num
                    }, function(data) {
                        if ($.trim(data) == 'success') {
                            swal({
                                    title: "Rejected!",
                                    text: "Successfully Rejected!",
                                    type: "success",
                                    confirmButtonText: "OK"
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + "/" + wid;
                                    }
                                });

                        } else if ($.trim(data) == ' error') {
                            swal("", "Error!", "warning");

                        } else {
                            swal("Oops...", "Something went wrong!", "warning");
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });

    });

    /********************************* EDIT ******************************/
    $('.btn_reallocate').click(function() {
        var wid = $("#wid").val();
        $("#wid_up").val(wid);
    });

    $("#form_update").on('submit', (function(e) {
        var menu = $("#menu").val();
        var wid = $("#wid").val();

        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url() ?>Con_pre_registry/reallocate_ws",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {},
            success: function(data) {
                $('#edit_data').modal('hide')
                if ($.trim(data) === '') {
                    swal("Oops...", "Something went wrong!", "warning");

                } else {
                    if ($.trim(data) === 'error') {
                        swal("SQL Error!", "Please Try Again!", "warning");

                    } else if ($.trim(data) === 'success') {
                        swal({
                                title: "Reallocated!",
                                text: "Successfully Reallocated!",
                                type: "success",
                                confirmButtonText: "OK"
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + "/" + wid;
                                }
                            });
                    }
                }
            },
            error: function(e) {
                swal({
                    title: "Error!",
                    text: "Try Again",
                    type: "warning",
                    timer: 2000,
                    showConfirmButton: false,
                });
                setTimeout(function() {
                    window.location.href = "<?php echo base_url() ?>Con_workshop/view_details/" + menu + "/" + wid;
                }, 2000);
            }
        });
    }));



    $(document).ready(function() {
        var col_len = '';
        $('.example22').DataTable({
            "aaSorting": [],
            "sScrollY": "350px",
            "sScrollX": "100%",
            "sScrollXInner": "110%",
            "bScrollCollapse": true,

            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            dom: "Bfrtip",
            buttons: [

                {
                    extend: 'copyHtml5',
                    text: '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'excelHtml5',
                    header: true,


                    text: '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',

                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5],

                    }
                },

                {
                    extend: 'csvHtml5',
                    text: '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    title: $('.download_label').html(),
                    exportOptions: {
                        columns: ':visible'

                    }
                },

                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>',
                    titleAttr: 'Print',
                    title: $('.download_label').html(),
                    customize: function(win) {
                        $(win.document.body)
                            .css('font-size', '10pt');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    },
                    exportOptions: {
                        columns: ':visible'
                    }
                },

                {
                    extend: 'colvis',
                    text: '<i class="fa fa-columns"></i>',
                    titleAttr: 'Columns',
                    title: $('.download_label').html(),
                    postfixButtons: ['colvisRestore']
                },
            ]
        });
    });
</script>