<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-key"></i> Change Password</h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <br/>
                    <form action="javascript:void(0);"  id="passwordform" name="passwordform" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">New Password<span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input required="required" class="form-control col-md-7 col-xs-12" id="new_pass" name="new_pass" placeholder="" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Confirm Password<span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="confirm_pass" name="confirm_pass" placeholder="" type="password"  value="<?php echo set_value('confirm_password'); ?>" class="form-control col-md-7 col-xs-12" >
                                <label id="pwd_error" class="text-danger" for="con_new_pwd" style="display: none;">Passwords donot match!</label>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <input type="submit" class="btn btn-info" value="Change Password"/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
  /********************************* Validation ******************************/
  var pass_validate = true;

  $('#confirm_pass').keyup(function () {
    var pass1 = $("#new_pass").val();
    var pass2 = $("#confirm_pass").val();
    var len = $("#confirm_pass").val().length;
    if (len !== 0) {
        if (pass1 === pass2) {
            $("#pwd_error").fadeOut();
            pass_validate = true;
            $("#confirm_pass").css("border","1px solid #d2d6de");
            $(':input[type="submit"]').prop('disabled', false);
        } else {
            $("#pwd_error").fadeIn();
            pass_validate = false;
            $("#confirm_pass").css("border","1px solid #d73925");
            $(':input[type="submit"]').prop('disabled', true);
        }
    } else {
        $("#pwd_error").fadeOut();
        pass_validate = false;
    }
  });

  $("#passwordform").on('submit',(function(e) {
    
    e.preventDefault();
    $.ajax({
    url: "<?php echo base_url()?>Con_my_profile/updatePassword",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
      cache: false,
    processData:false,
    beforeSend : function()
    {
    },
    success: function(data){
      if($.trim(data) === '') {
      swal("Oops...", "Something went wrong!", "warning");
      
      } else {
        if($.trim(data) ==='error')
        {
          swal("SQL Error!", "Please Try Again!", "warning");
        
        } else if($.trim(data) ==='success') {
        swal({
          title: "",
          text: "Successfully Updated Password!",
          type: "success",
          confirmButtonText: "OK"
          },
          function (isConfirm) {
          if (isConfirm) {
            window.location.href = "<?php echo base_url()?>Con_my_profile/changepass";
          }
        });
        }
      }      
      },
      error: function(e) 
      {
      alert("err2");
      }
     });
  }));
</script>
