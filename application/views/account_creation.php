<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
    $date1 = date("Y-m-d");
    $date2 = date("Y-m-d");
} else {
    $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
    $date1 = substr($daterange, 0, 10);
    $date2 = substr($daterange, 17, 20);
}
?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-id-card"></i> Account Creation
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

                    </div>
                    <div class="box-body">
                        <form role="form" action="<?php echo site_url('Con_sales_report/account_creation/') ?><?php echo $menu. "/" . $date1 . ' - ' . $date2; ?>" method="post" class="">
                            <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="col-sm-12">
                                <div class="form-group">
                                    <button type="submit" name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div> -->
                        </form>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Account Creation</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label"> Account Creation</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Invoice No</th>
                                        <th>Reg No</th>
                                        <th>CR No</th>
                                        <th>Name</th>
                                        <th>Contact No</th>
                                        <th>Deposited Amount</th>
                                        <th>Payment Method</th>
                                        <!-- <th>USDT</th> -->
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    $count = 1;

                                    foreach ($load_data as $value) {
                                        $usdt_val = $value->usdt_val;
                                        if($usdt_val == 1){
                                            $usdt = 'Yes';
                                            if($value->deposits != '17500'){
                                                $txt = 'display:none;';
                                            }else{
                                                $txt = '';
                                            }
                                        }else{
                                            $usdt = 'No';
                                            if($value->deposits != '15000'){
                                                $txt = 'display:none;';
                                            }else{
                                                $txt = '';
                                            }
                                        }

                                    ?>
                                        <tr style='<?php echo $txt; ?>'>
                                            <td> <?php echo $count; ?>.</td>
                                            <td> <?php echo $value->new_inv; ?></td>
                                            <td> <?php echo $value->reg_no; ?></td>
                                            <td> <?php echo $value->inv_no; ?></td>
                                            <td> <?php echo $value->name; ?></td>
                                            <td> <?php echo $value->con_no; ?></td>
                                            <td> <?php echo "LKR. " . $value->deposits; ?></td>
                                            <td> <?php echo $value->bank; ?></td>
                                            <!-- <td> <?php //echo $usdt; ?></td> -->
                                        </tr>
                                    <?php
                                        $count++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
    /**************************** DATE ***********************/
    $('#date_range').daterangepicker({
        autoclose: false,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });

    function formHandler(date) {
        var menu = $("#menu").val();
        window.location.href = "<?php echo base_url() ?>Con_sales_report/account_creation/" + menu + "/" + date;
    }
    
</script>