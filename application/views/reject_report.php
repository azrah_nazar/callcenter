<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
  $date1 = date("Y-m-01");
  $date2 = date("Y-m-d");;
} else {
  $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
  $date1 = substr($daterange, 0, 10);
  $date2 = substr($daterange, 17, 23);
}


?>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1> <i class="fa fa-sitemap"></i> Agent-wise Reject Report
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12 col-sm-6">
                <div class="row">
                  <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                  <form role="form" action="<?php echo site_url('Con_Call_Report/reject_report/') ?><?php echo $menu ."/".$date1.' - '.$date2; ?>" method="post" class="">

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                        </div>
                      </div>
                    </div>
                    <?php if ($val == 12 || $val == 15) { ?>
                      <div class="form-group col-md-6">
                        <label for="agent">Agent</label>
                        <select id="agent" name="agent" class="form-control">
                          <option value="">Select</option>
                          <?php foreach ($load_agent as $agent) { ?>
                            <option value="<?php echo $agent->Acc_No ?>" <?php if (set_value('agent') == $agent->Acc_No) echo "selected=selected"; ?>><?php echo $agent->Name ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    <?php } ?>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <button name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle btn-search"><i class="fa fa-search"></i> Search</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php if (isset($resultlist)) { ?>
          <div class="box box-info">
            <div class="box-body">
              <form action="javascript:void(0);" method="post">
                <div class="table-responsive ptt10">
                  <table class="table table-hover table-striped example">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Contact No</th>
                        <th>WhatsApp No</th>
                        <th>Chat</th>
                        <th>Transfer Agent</th>
                        <th>Name</th>
                        <th>Reject Date</th>
                    </thead>
                    <tbody>
                      <?php
                      $row_count = 1;

                      foreach ($resultlist as $key => $value) {
                        $spl_cus = $value['spl_cus'];
                        if ($spl_cus == 1) {
                          $splCus = "background-color: red; color:#fff;";
                        } else {
                          $splCus = "";
                        }

                        $reject_date = $value['agent_reject_date'];
                        if ($reject_date == '0000-00-00') {
                          $ag_reject_date = '';
                        } else {
                          $ag_reject_date = $reject_date;
                        }

                      ?>
                        <tr>
                          <td><?php echo $row_count . '.'; ?></td>
                          <td><?php echo $value['con_no']; ?></td>
                          <td><?php echo $value['whatsapp']; ?></td>
                          <td>
                            <?php
                            $reminder = $value['reminder_date'];

                            if (($value['tic_id'] == '' || $value['tic_id'] == '0') && ($value['reminder_date'] == '0000-00-00' || empty($value['reminder_date']))) {
                            ?>
                              <a class="btn btn-default btn-xs btn_ticket" data-value="<?php echo $value['id']; ?>" data-toggle="modal" data-target="#chat_open" title="Give Reminder" style="<?php echo $splCus; ?>">
                                <i class="fa fa-bell"></i>
                              </a>
                            <?php
                            } else { ?>
                              <a class="btn btn-default btn-xs btn_history" data-value="<?php echo $value['id']; ?>" data-tic="<?php echo $value['id']; ?>" data-owner="<?php echo $value['emp_accNo']; ?>" data-toggle="modal" data-target="#chat_history" title="Reply" style="<?php echo $splCus; ?>">
                                <i class="fa fa-comment"></i>
                              </a>
                            <?php } ?>
                          </td>
                          <td>
                            <a class="btn btn-success btn-xs btn_tansfer" data-value="<?php echo $value['id']; ?>" data-toggle="modal" data-target="#transfer_agent" title="Transfer Agent">
                              <i class="fa fa-group"></i>
                            </a>
                          </td>
                          <td><?php echo $value['name']; ?></td>
                          <td><?php echo $ag_reject_date; ?></td>
                        </tr>
                      <?php
                        $row_count++;
                      }

                      ?>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="chat_open" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title"> Chat Messages</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
          <div class="box-body">
            <input type="hidden" name="con_id" id="cont_id" value="">

            <div class="form-group">
              <label for="email">Message</label><small class="req"> *</small>
              <textarea name="message" id="compose-textarea" class="form-control" rows="5"></textarea>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Send</button>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="chat_history" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="col-md-12" id="chat_his">
          <!-- DIRECT CHAT SUCCESS -->
          <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Chat Messages</h3>

            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="direct-chat-messages" style="height:500px;">
                <strong>
                  <p class="chat_contact_no"></p>
                </strong>
                <div id="chat_list">


                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <form action="javascript:void(0);" id="form_send">
                <input type="hidden" name="id_up" id="id_up2">
                <input type="hidden" name="tic_id" id="tic_id">
                <div class="input-group">
                  <input type="text" name="reply_up" id="reply_up" placeholder="Type Message ..." class="form-control" required>
                  <span class="input-group-btn">
                    <button class="btn btn-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Send</button>
                  </span>
                </div>
              </form>
            </div><!-- /.box-footer-->
          </div>
          <!--/.direct-chat -->
        </div><!-- /.col -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close Chat</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="transfer_agent" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title"> Transfer Agent</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="agent_transfer" accept-charset="utf-8">
          <div class="box-body">
            <input type="hidden" name="con_id2" id="cont_id2" value="">

            <div class="form-group">
              <label for="agent">Agent</label>
              <select id="agent" name="agent" class="form-control" required>
                <option value="">Select</option>
                <?php foreach ($load_agent as $agent) { ?>
                  <option value="<?php echo $agent->Acc_No ?>"><?php echo $agent->Name ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Transfer</button>
          </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
  /**************************** DATE ***********************/
  $('#date_range').daterangepicker({
    autoclose: false,
    todayBtn: true,
    pickerPosition: "bottom-left"
  });

  function formHandler(date){
    var menu = $("#menu").val();
    window.location.href = "<?php echo base_url()?>Con_Call_Report/reject_report/"+menu+"/"+date;
  }

  /**************************** INSERT  ****************************/
  $('.btn_ticket').click(function() {
    var con_id = $(this).attr('data-value');
    $("#cont_id").val(con_id);
  });

  $("#form_submit").on('submit', (function(e) {
    var menu = $("#menu").val();
    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/add_ticket",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'data exists') {
            swal({
              title: "",
              text: "Data Already Exists!",
              type: "warning",
              timer: 2000,
              showConfirmButton: false,
            });

          } else if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Added!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              location.reload();
            }, 2000);

          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /*********************************  TICKET ******************************/
  $("#form_send").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/send_reply",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'success') {
            swal("Sent!", "Successfully Sent Message!", "success");
            location.reload();
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  $('.btn_history').click(function() {
    var con_id = $(this).attr('data-value');
    $("#id_up2").val(con_id);
    var tic_id = $(this).attr('data-tic');
    $("#tic_id").val(tic_id);
    var owner = $(this).attr('data-owner');
    var acc_no = $('#acc_no').val();
    var val = $('#val').val();

    if ((acc_no == owner) || (val == 12 || val == 15)) {
      $('.btn_close').show();
    } else {
      $('.btn_close').hide();
    }


    $.post('<?php echo base_url() ?>Con_Call_Report/get_history', {
      'get_history': 'data',
      con_id: con_id
    }, function(data) {
      $('.chat_contact_no').text('');

      var Data = "";
      var reply_id = data.reply_id;


      if (data === undefined || data.length === 0 || data === null) {

        Data = '<div> <h4>This chat has no records!!</h4> </div>';
        $('#chat_list').html('').append(Data);

      } else {

        $.each(data.result, function(index, data) {
          var msg = "";
          var val = data.Val;
          var time = data.dt_time;
          var contact_no = data.con_no;
          var std_name = data.st_name;
          if (std_name == '') {
            var st_name = '';
          } else {
            var st_name = "Name: " + data.st_name + " | ";
          }
          var whatsapp = data.whatsapp;
          if (whatsapp == '') {
            var whatsapp = '';
          } else {
            var whatsapp = "Whatsapp: " + data.whatsapp;
          }
          $('.chat_contact_no').text(st_name + "\nContact No : " + contact_no + "\n | " +whatsapp);

          var user_type = '';
          var class_type = '';
          var user_name = '';

          if ((val == '12') || (val == '15')) {
            user_type = 'left';
            class_type = 'left';
            user_name = data.Name;
          } else {
            user_type = 'right';
            class_type = 'right';
            user_name = data.Name;
          }

          Data += '<div class="direct-chat-msg ' + user_type + '">';
          Data += '<div class="direct-chat-info clearfix">';
          Data += '<span class="direct-chat-name pull-' + class_type + '">' + user_name + ' &nbsp;</span>';
          Data += '<span class="direct-chat-timestamp pull-' + user_type + '">' + time + '&nbsp;</span>';
          Data += '</div>';
          Data += '<img class="direct-chat-img" src="<?php echo base_url(); ?>uploads/student_images/no_image.png" alt="message user image">';
          Data += '<div class="direct-chat-text">';
          Data += data.message;
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
          Data += '</div>';
        });

        $('#chat_list').html('').append(Data);
      }
    }, "json");
  });

  /******************************** CLOSE CHAT ***************************/
  $(".btn_close").click(function() {
    var tic_id = $("#tic_id").val();
    var id_up = $("#id_up2").val();
    var menu = $("#menu").val();

    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, close it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_Call_Report/close_chat", {
            close_chat: "data",
            tic_id: tic_id, id_up:id_up
          }, function(data) {
            if ($.trim(data.status) === 'success') {
              swal("Closed", "Successfully Closed Chat!", "success");
              window.location.href = "<?php echo base_url() ?>Con_Call_Report/reject_report/" + menu;

            } else if ($.trim(data.status) === ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          }, "json");
        } else {
          swal("Cancelled", "", "error");
        }
      });
  });

  /**************************** AGENT TRANSFER  ****************************/
  $('.btn_tansfer').click(function() {
    var con_id = $(this).attr('data-value');
    $("#cont_id2").val(con_id);
  });

  $("#agent_transfer").on('submit', (function(e) {
    var menu = $("#menu").val();
    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_Call_Report/agent_tranfer",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'data exists') {
            swal({
              title: "",
              text: "Data Already Exists!",
              type: "warning",
              timer: 2000,
              showConfirmButton: false,
            });

          } else if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Tansferred!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });
            setTimeout(function() {
              window.location.href = "<?php echo base_url() ?>Con_Call_Report/reject_report/" + menu;
            }, 2000);

          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));
</script>