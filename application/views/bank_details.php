<?php
date_default_timezone_set('Asia/Colombo');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>VICTORY ACADEMY</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta name="theme-color" content="#424242" />
    <link href="<?php echo base_url(); ?>backend/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/style-main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/jquery.mCustomScrollbar.min.css">
    <?php
    $this->load->view('layout/theme');
    ?>

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/ionicons.min.css">

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/iCheck/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/morris/morris.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/colorpicker/bootstrap-colorpicker.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/custom_style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/datepicker/css/bootstrap-datetimepicker.css">
    <!--file dropify-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/dropify.min.css">
    <!--file nprogress-->
    <link href="<?php echo base_url(); ?>backend/dist/css/nprogress.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>backend/plugins/jQueryUI/jquery-ui.min.css" rel="stylesheet">

    <!--print table-->
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/buttons.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <!--print table mobile support-->
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/rowReorder.dataTables.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>backend/custom/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/dist/js/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/datepicker/js/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo base_url(); ?>backend/plugins/colorpicker/bootstrap-colorpicker.js"></script>
    <script src="<?php echo base_url(); ?>backend/datepicker/date.js"></script>
    <script src="<?php echo base_url(); ?>backend/dist/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/js/school-custom.js"></script>
    <script src="<?php echo base_url(); ?>backend/js/sstoast.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/js/jquery.mask.min.js"></script>


    <!-- fullCalendar -->
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/calender/zabuto_calendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/fullcalendar/dist/fullcalendar.print.min.css" media="print">
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/sweet-alert/sweetalert2.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/toggle/css/bootstrap-toggle.min.css">

    <script src="<?php echo base_url(); ?>backend/plugins/iCheck/icheck.min.js"></script>



</head>

<body class="hold-transition skin-blue fixed sidebar-collapse">



    <div class="wrapper">

        <header class="main-header" id="alert">
            <a href="#" class="logo">
                <span class="logo-mini">VA</span>
                <span class="logo-lg"><img src="<?php echo base_url(); ?>backend/images/logo.png" /></span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>


            </nav>
        </header>

        <div class="content-wrapper" style="min-height: 946px;">
            <section class="content-header">
                <h1>
                    <i class="fa fa-id-card"></i> Registration and Bank Deposit Details
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">

                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Registration Details</h3>
                            </div>
                            <form action="javascript:void(0);" id="form_update" accept-charset="utf-8">
                                <div class="box-body">
                                    <input type="hidden" name="reg_num" id="reg_num" value="<?php echo $this->uri->segment(2); ?>">
                                    <div class="form-group">
                                        <label>Contact Number:<font color="#FF0000"><strong>*</strong></font></label>
                                        <input type="text" class="form-control" name="con_no" id="con_no" style="width: 100%;" value="<?php echo $st_details['con_no']; ?>" readonly>
                                    </div>

                                    <div class="form-group">
                                        <label>Name:</label>
                                        <input type="text" class="form-control txt_nav" name="con_name" value="<?php echo $st_details['name']; ?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Address:</label>
                                        <input type="text" class="form-control txt_nav" name="con_adrz" value="<?php echo $st_details['address']; ?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Whatsapp Number:</label>
                                        <input type="text" class="form-control txt_nav" name="con_whatsapp" value="<?php echo $st_details['whatsapp']; ?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Email:</label>
                                        <input type="email" class="form-control txt_nav" name="con_email" value="<?php echo $st_details['email']; ?>">
                                    </div>

                                    <div class="form-group">
                                        <label>NIC:</label>
                                        <input type="text" class="form-control txt_nav" name="con_nic" value="<?php echo $st_details['nic']; ?>">
                                    </div>

                                    <button type="submit" class="btn btn-info pull-left">Update Details</button>

                                </div>

                            </form>


                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="box box-primary">
                            <div class="box-header ptbnull">
                                <h3 class="box-title titlefix">Bank Deposit Details</h3>
                            </div>
                            <div class="box-body ">
                                <p><strong>පාඨමාලාවන් සමඟ සම්බන්ධ වීමට බලාපොරොත්තු වන ඔබට අවශ්‍ය ගෙවීම් සම්බන්ධ විස්තර පහත දැක්වේ.</strong></p>

                                <div class="row" style="line-height: 1.8">
                                    <div class="col-md-12" style="padding-bottom: 12px;">
                                        <strong> Victory Academy PVT LTD </strong><br>
                                        Account number: 87514539 <br>
                                        Bank: Bank of Ceylon <br>
                                        Branch: Kurunegala <br>
                                        Branch Code: 009 <br>
                                    </div>
                                    <hr>

                                    <br>
                                    <br>
                                    <div class="col-md-12" style="padding-bottom: 12px;">
                                        <strong> Victory Academy PVT LTD </strong><br>
                                        Account number: 1000417553 <br>
                                        Bank: Commercial Bank <br>
                                        Branch: Kurunegala City Branch <br>
                                    </div>
                                </div>
                                <br />
                                <p><strong>Rs.12,500/-මුදල් බැර කිරීමෙන් පසුව ඔබගේ රිසිට් පත හෝ ඊමේල් පණිවුඩයෙහි ස්ක්‍රීන් ශොට් එකක් මෙම අංකයට වට්සැප් කරන්න.<?php echo $st_details['sinhala_name'] . ' ' . $st_details['Tp'] . ' / ' . $st_details['tpOffice'] ?></strong></p>

                            </div>
                        </div>
                    </div>
                </div>

        </div>


        <script src="<?php echo base_url(); ?>js/file-upload.js"></script>



        <script>
            function validatePhoneNumber(input_str) {
                var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

                return re.test(input_str);
            }

            var date_format = 'yyyy-mm-dd';
            $(document).ready(function() {
                $(".date").datepicker({
                    format: date_format,
                    autoclose: true,
                    endDate: '+0d',
                    todayHighlight: true
                });
            });


            /********************************* EDIT ******************************/

            $("#form_update").on('submit', (function(e) {
                var reg_num = $("#reg_num").val();

                e.preventDefault();
                $.ajax({
                    url: "<?php echo base_url() ?>Con_pre_registry/update_details",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {},
                    success: function(data) {
                        $('#edit_data').modal('hide')
                        if ($.trim(data) === '') {
                            swal("Oops...", "Something went wrong!", "warning");

                        } else {
                            if ($.trim(data) === 'error') {
                                swal("SQL Error!", "Please Try Again!", "warning");

                            } else if ($.trim(data) === 'data not exists') {
                                swal("", "Menu Category Already Exist!", "warning");

                            } else if ($.trim(data) === 'success') {
                                swal({
                                        title: "Updated!",
                                        text: "Successfully Updated!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },
                                    function(isConfirm) {
                                        if (isConfirm) {
                                            window.location.href = "<?php echo base_url() ?>bank-details/" + reg_num;
                                        }
                                    });
                            }
                        }
                    },
                    error: function(e) {
                        swal({
                            title: "Error!",
                            text: "Try Again",
                            type: "warning",
                            timer: 2000,
                            showConfirmButton: false,
                        });
                        setTimeout(function() {
                            window.location.href = "<?php echo base_url() ?>Con_Contact_CSV/index/" + menu;
                        }, 2000);
                    }
                });
            }));

            /******************************** Remove ***************************/

            $(".btn_select").click(function() {
                var ws_id = $(this).val();
                var reg_num = $("#reg_num").val();

                swal({
                        title: "Are you sure you want to reserve?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, reserve it!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.post("<?php echo base_url() ?>Con_pre_registry/reserve_ws", {
                                reserve_ws: "data",
                                ws_id: ws_id,
                                reg_num: reg_num
                            }, function(data) {
                                if ($.trim(data.status) === 'success') {
                                    swal({
                                            title: "Reserved!",
                                            text: "Successfully Reserved!",
                                            type: "success",
                                            confirmButtonText: "OK"
                                        },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                window.location.href = "<?php echo base_url() ?>register/" + reg_num;
                                            }
                                        });

                                } else if ($.trim(data.status) === ' error') {
                                    swal("", "Error!", "warning");

                                } else {
                                    swal("Oops...", "Something went wrong!", "warning");
                                }
                            }, "json");
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });

            });
        </script>