<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
    $date1 = date("Y-m-d");
    $date2 = date("Y-m-d");
} else {
    $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
    $date1 = substr($daterange, 0, 10);
    $date2 = substr($daterange, 17, 20);
}
?>

<style type="text/css">
    .error {
        color: red;
        size: 80%
    }

    .hidden {
        display: none;
    }
</style>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-id-card"></i> Reapplied Reject Contacts List
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

                    </div>
                    <div class="box-body">
                        <form role="form" action="<?php echo site_url('Con_Contact_CSV/reapplied_reject_list/') ?><?php echo $menu . "/" . $date1 . ' - ' . $date2; ?>" method="post" class="">
                            <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <?php if (isset($load_data)) { ?>
                    <div class="box box-primary">
                        <div class="box-header ptbnull">
                            <h3 class="box-title titlefix">Reapplied Reject Contacts Report</h3>
                        </div>
                        <div class="box-body ">
                            <div class="table-responsive mailbox-messages">
                                <div class="download_label"> Reapplied Reject Contacts List</div>
                                <table class="table table-striped table-bordered table-hover example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Contact Number</th>
                                            <th>Agent</th>
                                            <th>Status</th>
                                            <th>Chat</th>
                                            <th>Rejected Date</th>
                                            <th>Whatsapp</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $count = 1;

                                        foreach ($load_data as $value) {

                                            if ($value->feedback_reject == 0) {
                                                if ($value->status == 0) {
                                                    if ($value->Acc_No == '') {
                                                        $status = "Unallocated";
                                                    } else {
                                                        $status = "Pending Contacts";
                                                    }
                                                } else if ($value->status == 1) {
                                                    $status = "Accepted";
                                                } else if ($value->status == 2) {
                                                    $status = "Registered";
                                                } else if ($value->status == 3) {
                                                    $status = "Rejected";
                                                } else if ($value->status == 4) {
                                                    $status = "Workshop SMS";
                                                } else if ($value->status == 5) {
                                                    $status = "Reapplied Rejected";
                                                } else if ($value->status == 6) {
                                                    $status = "Account Created";
                                                } else {
                                                    $status = "";
                                                }
                                            } else {
                                                $status = "Workshop Rejected";
                                            }


                                            if ($value->status == 5) {
                                                $status = "Reapplied Rejected";
                                            } else if ($value->status == 6) {
                                                $status = "Account Created";
                                            }

                                            $spl_cus = $value->spl_cus;
                                            if ($spl_cus == 1) {
                                                $splCus = "background-color: red; color:#fff;";
                                            } else {
                                                $splCus = "";
                                            }
                                        ?>
                                            <tr>
                                                <td class="mailbox-name"> <?php echo $count; ?>.</td>
                                                <td class="mailbox-name"> <?php echo $value->con_no; ?></td>
                                                <td class="mailbox-name"> <?php echo $value->agent_name; ?></td>
                                                <td class="mailbox-name"> <?php echo $status; ?></td>
                                                <td>
                                                    <?php
                                                    if ($value->tic_id == '' || $value->tic_id == '0') {
                                                    ?>
                                                        <a class="btn btn-default btn-xs btn_ticket" data-value="<?php echo $value->cid; ?>" data-toggle="modal" data-target="#chat_open" title="Give Reminder" style="<?php echo $splCus; ?>">
                                                            <i class="fa fa-bell"></i>
                                                        </a>
                                                    <?php
                                                    } else { ?>
                                                        <a class="btn btn-default btn-xs btn_history" data-value="<?php echo $value->cid; ?>" data-tic="<?php echo $value->tic_id; ?>" data-owner="<?php echo $value->Acc_No; ?>" data-toggle="modal" data-target="#chat_history" title="Reply" style="<?php echo $splCus; ?>">
                                                            <i class="fa fa-comment"></i>
                                                        </a>
                                                    <?php } ?>
                                                </td>
                                                <td class="mailbox-name"> <?php echo $value->reapplied_reject_date; ?></td>
                                                <td class="mailbox-name"> <?php echo $value->whatsapp; ?></td>
                                            </tr>
                                        <?php
                                            $count++;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div>

        <div class="modal fade" id="chat_open" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title title text-center modal_title"> Add Reminder</h4>
                    </div>
                    <div class="modal-body">
                        <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
                            <div class="box-body">
                                <input type="hidden" name="con_id" id="con_id" value="">

                                <div class="form-group">
                                    <label for="email">Message</label><small class="req"> *</small>
                                    <textarea name="message" id="compose-textarea" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Send</button>
                            </div>
                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="chat_history" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="col-md-12" id="edit_data">
                            <!-- DIRECT CHAT SUCCESS -->
                            <div class="box box-primary direct-chat direct-chat-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Chat Messages</h3>

                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="direct-chat-messages" style="height:500px;">
                                        <strong>
                                            <p class="chat_contact_no"></p>
                                        </strong>
                                        <div id="chat_list">


                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                    <form action="javascript:void(0);" id="form_send">
                                        <input type="hidden" name="id_up" id="id_up">
                                        <input type="hidden" name="tic_id" id="tic_id">
                                        <div class="input-group">
                                            <input type="text" name="reply_up" id="reply_up" placeholder="Type Message ..." class="form-control" required>
                                            <span class="input-group-btn">
                                                <button class="btn btn-success" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Send</button>
                                            </span>
                                        </div>
                                    </form>
                                </div><!-- /.box-footer-->
                            </div>
                            <!--/.direct-chat -->
                        </div><!-- /.col -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close Chat</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- date-range-picker -->
        <script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

        <script>
            /**************************** DATE ***********************/
            $('#date_range').daterangepicker({
                autoclose: false,
                todayBtn: true,
                pickerPosition: "bottom-left"
            });

            function formHandler(date) {
                var menu = $("#menu").val();
                window.location.href = "<?php echo base_url() ?>Con_Contact_CSV/reapplied_reject_list/" + menu + "/" + date;
            }

            /**************************** INSERT  ****************************/
            $('.btn_ticket').click(function() {
                var con_id = $(this).attr('data-value');
                $("#con_id").val(con_id);
            });

            $("#form_submit").on('submit', (function(e) {
                var menu = $("#menu").val();
                e.preventDefault();
                $.ajax({
                    url: "<?php echo base_url() ?>Con_Call_Report/add_ticket",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {},
                    success: function(data) {
                        if ($.trim(data) === '') {
                            swal("Oops...", "Something went wrong!", "warning");

                        } else {
                            if ($.trim(data) === 'error') {
                                swal("SQL Error!", "Please Try Again!", "warning");

                            } else if ($.trim(data) === 'data exists') {
                                swal({
                                    title: "",
                                    text: "Data Already Exists!",
                                    type: "warning",
                                    timer: 2000,
                                    showConfirmButton: false,
                                });

                            } else if ($.trim(data) === 'success') {
                                swal({
                                    title: "",
                                    text: "Successfully Added!",
                                    type: "success",
                                    timer: 2000,
                                    showConfirmButton: false,
                                });
                                setTimeout(function() {
                                    location.reload();
                                }, 2000);

                            }
                        }
                    },
                    error: function(e) {
                        alert("No agent assigned to send message");
                    }
                });
            }));

            /*********************************  TICKET ******************************/
            $("#form_send").on('submit', (function(e) {
                var menu = $("#menu").val();

                e.preventDefault();
                $.ajax({
                    url: "<?php echo base_url() ?>Con_Call_Report/send_reply",
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {},
                    success: function(data) {
                        $('#edit_data').modal('hide')
                        if ($.trim(data) === '') {
                            swal("Oops...", "Something went wrong!", "warning");

                        } else {
                            if ($.trim(data) === 'error') {
                                swal("SQL Error!", "Please Try Again!", "warning");

                            } else if ($.trim(data) === 'success') {
                                swal("Sent!", "Successfully Sent Message!", "success");
                                location.reload();
                            }
                        }
                    },
                    error: function(e) {
                        alert("err2");
                    }
                });
            }));

            $('.btn_history').click(function() {
                var con_id = $(this).attr('data-value');
                $("#id_up").val(con_id);
                var tic_id = $(this).attr('data-tic');
                $("#tic_id").val(tic_id);
                var owner = $(this).attr('data-owner');
                var acc_no = $('#acc_no').val();
                var val = $('#val').val();

                if ((acc_no == owner) || (val == 12 || val == 15)) {
                    $('.btn_close').show();
                } else {
                    $('.btn_close').hide();
                }


                $.post('<?php echo base_url() ?>Con_Call_Report/get_history', {
                    'get_history': 'data',
                    con_id: con_id
                }, function(data) {
                    $('.chat_contact_no').text('');
                    var Data = "";
                    var reply_id = data.reply_id;


                    if (data === undefined || data.length === 0 || data === null) {

                        Data = '<div> <h4>This chat has no records!!</h4> </div>';
                        $('#chat_list').html('').append(Data);

                    } else {

                        $.each(data.result, function(index, data) {
                            var msg = "";
                            var val = data.Val;
                            var time = data.dt_time;
                            var contact_no = data.con_no;
                            var std_name = data.st_name;
                            if (std_name == '') {
                                var st_name = '';
                            } else {
                                var st_name = "Name: " + data.st_name + " | ";
                            }
                            var whatsapp = data.whatsapp;
                            if (whatsapp == '') {
                                var whatsapp = '';
                            } else {
                                var whatsapp = "Whatsapp: " + data.whatsapp;
                            }
                            $('.chat_contact_no').text(st_name + "\nContact No : " + contact_no + "\n | " + whatsapp);

                            var user_type = '';
                            var class_type = '';
                            var user_name = '';

                            if ((val == '12') || (val == '15')) {
                                user_type = 'left';
                                class_type = 'left';
                                user_name = data.Name;
                            } else {
                                user_type = 'right';
                                class_type = 'right';
                                user_name = data.Name;
                            }

                            Data += '<div class="direct-chat-msg ' + user_type + '">';
                            Data += '<div class="direct-chat-info clearfix">';
                            Data += '<span class="direct-chat-name pull-' + class_type + '">' + user_name + ' &nbsp;</span>';
                            Data += '<span class="direct-chat-timestamp pull-' + user_type + '">' + time + '&nbsp;</span>';
                            Data += '</div>';
                            Data += '<img class="direct-chat-img" src="<?php echo base_url(); ?>uploads/student_images/no_image.png" alt="message user image">';
                            Data += '<div class="direct-chat-text">';
                            Data += data.message;
                            Data += '</div>';
                            Data += '</div>';
                            Data += '</div>';
                            Data += '</div>';
                        });

                        $('#chat_list').html('').append(Data);
                    }
                }, "json");
            });

            /******************************** CLOSE CHAT ***************************/
            $(".btn_close").click(function() {
                var tic_id = $("#tic_id").val();
                var id_up = $("#id_up").val();
                var menu = $("#menu").val();

                swal({
                        title: "Are you sure?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, close it!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.post("<?php echo base_url() ?>Con_Call_Report/close_chat", {
                                close_chat: "data",
                                tic_id: tic_id,
                                id_up: id_up
                            }, function(data) {
                                if ($.trim(data.status) === 'success') {
                                    swal("Closed", "Successfully Closed Chat!", "success");
                                    location.reload();

                                } else if ($.trim(data.status) === ' error') {
                                    swal("", "Error!", "warning");

                                } else {
                                    swal("Oops...", "Something went wrong!", "warning");
                                }
                            }, "json");
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    });

            });

        </script>