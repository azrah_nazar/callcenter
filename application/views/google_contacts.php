<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
    $dater = '';
} else {
    $dater = $date;
}
?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1> <i class="fa fa-sitemap"></i> Export Contact
            <?php
            if ($con_cnt >= 1) {
            ?>
                <a href="https://contacts.google.com/" target="_blank" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-google"></i> Google Contacts</a>
            <?php } ?>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-6">
                                <div class="row">
                                    <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                                    <form role="form" action="<?php echo site_url('Con_Contact_CSV/google_contacts/') ?><?php echo $menu . "/" . $date; ?>" method="post" class="">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control txt_nav date" name="daterange" value="<?php echo $dater; ?>" onChange="formHandler($(this).val())">
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if($con_cnt >=1){ ?>
                    <div class="box box-info">
                        <div class="box-body">
                            <form action="javascript:void(0);" method="post">
                                <div class="table-responsive ptt10">
                                    <div class="download_label"><?php echo $dater; ?></div>
                                    <table class="table table-hover table-striped example22">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th style="display: none;">Group Membership</th>
                                                <th style="display: none;">Phone 1 - Type</th>
                                                <th>Phone 1 - Value</th>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($resultlist as $key => $value) {
                                                $dt = $value['date1'];
                                                $ac_date = date_create($dt);
                                                $reg_no = $value['reg_no'];
                                                $name = date_format($ac_date, "dM") . $reg_no;

                                                if(($value['con_no'] == $value['whatsapp'] )){
                                                    $con_no = $value['con_no'];
                                                }else if($value['whatsapp'] == ''){
                                                    $con_no = $value['con_no'];
                                                }else{
                                                    $con_no = $value['whatsapp'];
                                                }
                                            ?>
                                                <tr>
                                                    <td><?php echo $name; ?></td>
                                                    <td style="display: none;">Mobile</td>
                                                    <td style="display: none;">* myContacts ::: * starred</td>
                                                    <td><?php echo $con_no; ?></td>
                                                </tr>
                                            <?php
                                            }

                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php }else{ ?>
                    <div class="box box-info">
                        <div class="box-body">
                            <h4>No contacts available for the selected date!!</h4>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </section>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
    /**************************** DATE ***********************/
    $('#date_range').daterangepicker({
        autoclose: false,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });

    var date_format = 'yyyy-mm-dd';
    $(document).ready(function() {
        $(".date").datepicker({
            format: date_format,
            autoclose: true,
            todayHighlight: true
        });
    });

    function formHandler(date) {
        var menu = $('#menu').val();
        window.location.href = "<?php echo base_url() ?>Con_Contact_CSV/google_contacts/" + menu + "/" + date;
    }

    $(document).ready(function() {
        var col_len = '';
        $('.example22').DataTable({
            "aaSorting": [],

            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            dom: "Bfrtip",
            buttons: [{
                extend: 'csvHtml5',
                text: '<i class="fa fa-file-text-o btn btn-lg btn-success csv_download"> CSV Download</i>',
                titleAttr: 'CSV',
                title: $('.download_label').html(),
                exportOptions: {
                    columns: [0, 1, 2, 3]
                }
            }, ]
        });
    });

    // function csv_download() {
    //     window.open("https://contacts.google.com/");
    // }
</script>