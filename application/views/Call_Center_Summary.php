<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
  $date1 = date("Y-m-01");
  $date2 = date("Y-m-d");
} else {
  $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
  $date1 = substr($daterange, 0, 10);
  $date2 = substr($daterange, 17, 23);
}
?>

<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1> <i class="fa fa-sitemap"></i> Call Center Summary Report
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">

        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

          </div>
          <div class="box-body">
            <form role="form" action="<?php echo site_url('Con_Call_Center_Summary/index/') ?><?php echo $menu. "/" . $date1 . ' - ' . $date2; ?>" method="post" class="">
              <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">

              <div class="col-md-12">
                <div class="form-group">
                  <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <?php if (isset($list)) { ?>
          <div class="box box-info">
            <div class="box-body">
              <form action="javascript:void(0);" method="post">
                <div class="table-responsive ptt10">
                  <table class="table table-hover table-striped example">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Registered</th>
                        <th>Accepted</th>
                        <th>Rejected</th>
                        <!-- <th>Total</th> -->
                    </thead>
                    <tbody>
                      <?php
                      $row_count = 1;

                      foreach ($list as $key => $value) {
                        $tot = $value['reg'] + $value['accept'] + $value['reject'];

                      ?>
                        <tr>
                          <td><?php echo $row_count . '.'; ?></td>
                          <td><?php echo $value['Name']; ?></td>
                          <td><?php echo $value['reg']; ?></td>
                          <td><?php echo $value['accept']; ?></td>
                          <td><?php echo $value['reject']; ?></td>
                          <!-- <td><?php //echo $tot; 
                                    ?></td> -->

                        </tr>
                      <?php
                        $row_count++;
                      }

                      ?>
                    </tbody>
                  </table>
                </div>
              </form>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </section>
</div>

<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
<script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

<script>
  /**************************** DATE ***********************/
  $('#date_range').daterangepicker({
    autoclose: false,
    todayBtn: true,
    pickerPosition: "bottom-left"
  });

  function formHandler(date) {
    var menu = $("#menu").val();
    window.location.href = "<?php echo base_url() ?>Con_Call_Center_Summary/index/" + menu + "/" + date;
  }
</script>