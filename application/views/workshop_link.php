<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1>
      <i class="fa fa-gears"></i> Workshop Link
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">

      <div class="col-md-3">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Add Workshop Link</h3>
          </div>
          <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
          <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
            <div class="box-body">

              <div class="form-group">
                <label for="workshop">Workshop<font color="#FF0000"><strong>*</strong></font></label>
                <select id="workshop" name="workshop" class="form-control" required autofocus>
                  <option value="">Select</option>
                  <?php foreach ($workshops as $workshop) { ?>
                    <option value="<?php echo $workshop->id ?>"><?php echo $workshop->w_name . " (" . $workshop->w_date . " / " . date("h:i A", strtotime($workshop->w_time)). ")"; ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label>Zoom Link<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control" name="zoom_link" id="zoom_link" placeholder="Enter ..." autocomplete="off" required>
              </div>

            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-info pull-right">Add Link</button>
            </div>
          </form>
        </div>
      </div>
      <div class="col-md-9">
        <div class="box box-primary">
          <div class="box-header ptbnull">
            <h3 class="box-title titlefix">Workshop Links</h3>
          </div>
          <div class="box-body ">
            <div class="table-responsive mailbox-messages" style="overflow-y: scroll; overflow-x:scroll;">
              <div class="download_label">Workshop Links</div>
              <table class="table table-striped table-bordered table-hover example22">
                <thead>
                  <tr>
                    <th>#</th>
                    <th> Workshop </th>
                    <th> Link </th>
                    <th> Date / Time </th>
                    <th class="text-right">Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                  $count = 0;

                  foreach ($load_data as $value) {
                    $count++;
                  ?>
                    <tr>
                      <td><?php echo $count; ?></td>
                      <td><?php echo $value->w_name; ?></td>
                      <td><?php echo $value->zoom_link; ?></td>
                      <td><?php echo $value->w_date . '/' . date("h:i A", strtotime($value->w_time)); ?></td>
                      <td class="mailbox-date pull-right">
                        <button class="btn btn-default btn-xs btn_up" data-toggle="modal" data-target="#edit_data" value="<?php echo $value->id; ?>" title="Edit">
                          <i class="fa fa-pencil"></i>
                        </button>

                        <button class="btn btn-default btn-xs btn_del" data-toggle="tooltip" title="Delete" value="<?php echo $value->id; ?>">
                          <i class="fa fa-remove"></i>
                        </button>
                      </td>
                    </tr>
                  <?php
                  }

                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>

  </section>
</div>

<div class="modal fade" id="edit_data" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title title text-center modal_title"> Edit Workshop Link</h4>
      </div>
      <div class="modal-body">
        <form action="javascript:void(0);" id="form_update">
          <input type="hidden" name="id_up" id="id_up">

          <div class="form-horizontal">
            <div class="box-body">

              <div class="form-group">
                <label>Workshop<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control" name="workshop_up" id="workshop_up" placeholder="Enter ..." autocomplete="off" required readonly>
              </div>

              <div class="form-group">
                <label>Zoom Link<font color="#FF0000"><strong>*</strong></font></label>
                <input type="text" class="form-control" name="zoom_link_up" id="zoom_link_up" placeholder="Enter ..." autocomplete="off" required>
              </div>

            </div>
          </div>

          <div class="box-footer">
            <button class="btn btn-danger">UPDATE</button>
          </div>

        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<script>
  function isNumberKey(evt, obj) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
      if (charCode == 46) return false;
    if (charCode == 46) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
  /**************************** INSERT  ****************************/

  $("#form_submit").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_workshop/add_workshop_link",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'data exists') {
            swal({
              title: "",
              text: "Data Already Exists!",
              type: "warning",
              timer: 2000,
              showConfirmButton: false,
            });

          } else if ($.trim(data) === 'success') {
            swal({
              title: "",
              text: "Successfully Added!",
              type: "success",
              timer: 2000,
              showConfirmButton: false,
            });

            $(".clr").val('');
            setTimeout(function() {
              window.location.href = "<?php echo base_url() ?>Con_workshop/workshop_link/" + menu;
            }, 2000);

          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /********************************* EDIT ******************************/

  $("#form_update").on('submit', (function(e) {
    var menu = $("#menu").val();

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url() ?>Con_workshop/update_link",
      type: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function() {},
      success: function(data) {
        $('#edit_data').modal('hide')
        if ($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if ($.trim(data) === 'error') {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if ($.trim(data) === 'data not exists') {
            swal("", "Data Already Exist!", "warning");

          } else if ($.trim(data) === 'success') {
            swal({
                title: "Updated!",
                text: "Successfully Updated!",
                type: "success",
                confirmButtonText: "OK"
              },
              function(isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url() ?>Con_workshop/workshop_link/" + menu;
                }
              });
          }
        }
      },
      error: function(e) {
        alert("err2");
      }
    });
  }));

  /******************************** Remove ***************************/

  $(".btn_del").click(function() {
    var row_id = $(this).val();
    var menu = $("#menu").val();

    swal({
        title: "Are you sure?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.post("<?php echo base_url() ?>Con_workshop/delete_link", {
            remove_data: "data",
            id: row_id
          }, function(data) {
            if ($.trim(data.status) === 'success') {
              swal({
                  title: "Deleted!",
                  text: "Successfully Deleted!",
                  type: "success",
                  confirmButtonText: "OK"
                },
                function(isConfirm) {
                  if (isConfirm) {
                    window.location.href = "<?php echo base_url() ?>Con_workshop/workshop_link/" + menu;
                  }
                });

            } else if ($.trim(data.status) === ' error') {
              swal("", "Error!", "warning");

            } else {
              swal("Oops...", "Something went wrong!", "warning");
            }
          }, "json");
        } else {
          swal("Cancelled", "", "error");
        }
      });

  });

  /**************************** LOAD FORM DATA ***********************/
  $(".btn_up").click(function() {
    var id = $(this).val();
    $("#id_up").val(id);

    $.post("<?php echo base_url() ?>Con_workshop/get_dataset", {
      get_dataset: "data",
      id: id
    }, function(data) {
      $.each(data.result, function(index, data) {
        $("#zoom_link_up").val(data.zoom_link);
        $("#workshop_up").val(data.w_name + " (" + data.w_date + " / " + data.w_time + ")");
      });

    }, "json");
  });

  $(document).ready(function() {
    var col_len = '';
    $('.example22').DataTable({
      "aaSorting": [],

      rowReorder: {
        selector: 'td:nth-child(2)'
      },
      dom: "Bfrtip",
      buttons: [

        {
          extend: 'copyHtml5',
          text: '<i class="fa fa-files-o"></i>',
          titleAttr: 'Copy',
          title: $('.download_label').html(),
          exportOptions: {
            columns: ':visible'
          }
        },

        {
          extend: 'excelHtml5',
          header: true,


          text: '<i class="fa fa-file-excel-o"></i>',
          titleAttr: 'Excel',

          title: $('.download_label').html(),
          exportOptions: {
            columns: [0, 1, 2, 3, 4],

          }
        },

        {
          extend: 'csvHtml5',
          text: '<i class="fa fa-file-text-o"></i>',
          titleAttr: 'CSV',
          title: $('.download_label').html(),
          exportOptions: {
            columns: ':visible'
          }
        },

        {
          extend: 'pdfHtml5',
          text: '<i class="fa fa-file-pdf-o"></i>',
          titleAttr: 'PDF',
          title: $('.download_label').html(),
          exportOptions: {
            columns: ':visible'

          }
        },

        {
          extend: 'print',
          text: '<i class="fa fa-print"></i>',
          titleAttr: 'Print',
          title: $('.download_label').html(),
          customize: function(win) {
            $(win.document.body)
              .css('font-size', '10pt');

            $(win.document.body).find('table')
              .addClass('compact')
              .css('font-size', 'inherit');
          },
          exportOptions: {
            columns: ':visible'
          }
        },

        {
          extend: 'colvis',
          text: '<i class="fa fa-columns"></i>',
          titleAttr: 'Columns',
          title: $('.download_label').html(),
          postfixButtons: ['colvisRestore']
        },
      ]
    });
  });
</script>