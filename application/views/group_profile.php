<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-gears"></i> Group Profile</small></h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">

                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Group Profile</h3>
                        </div> 
                        <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                        <form action="javascript:void(0);" id="form_submit" accept-charset="utf-8">
                            <div class="box-body">

                                <div class="form-group">                 
                                  <label>Profile Name:<font color="#FF0000"><strong>*</strong></font></label>
                                  <input autofocus type="text" class="form-control" name="group_name" id="group_name" placeholder="Enter ..." autocomplete="off" required>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>  
                </div>   
            <div class="col-md-8">             
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix"> Group Profile List</h3>
                    </div>
                    <div class="box-body ">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label"> Group Profile List</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <thead>
                                    <tr>
                                        <th> Group Profile</th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>                                   

                                    <?php
                                    $count = 1;
                                     
                                    foreach($load_data as $value) {
                                        ?>
                                        <tr>
                                            <td class="mailbox-name"> <?php echo $value->profile; ?></td>
                                            <td class="mailbox-date pull-right">

                                                <button class="btn btn-default btn-xs btn_up" data-toggle="modal" data-target="#edit_data" value="<?php echo $value->id; ?>"  title="Edit">
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    $count++;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
           
        </div> 

    </section>
</div>

<div class="modal fade" id="edit_data" role="dialog">
    <div class="modal-dialog">       
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title text-center modal_title"> Edit Section</h4>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" id="form_update">
                  <input type="hidden" name="id_up" id="id_up">

                  <div class="form-horizontal">
                      <div class="box-body">
                          <div class="form-group">                 
                            <label>Group Profile:<font color="#FF0000"><strong>*</strong></font></label>
                            <input type="text" class="form-control" name="group_name_up" id="group_name_up" placeholder="Enter ..." autocomplete="off" required>
                          </div>
                      </div>                   
                  </div>

                  <!-- /.box-body -->
                  <div class="box-footer"> 
                    <button class="btn btn-danger" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Update</button>
                  </div>
                </form>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<script>
  $(function() {
    $(':input[required=""],:input[required]').bind('focusout', function() {
      if ($(this).val() == "") {
        $(this).css("border-color", "red");
      } else {
        $(this).css("border-color", "#ccc");
      }
    });

    $('.txt_nav:first').focus();
    var $inp = $('.txt_nav');
    $inp.bind('keydown', function(e) {
      var n = $inp.length;
      var key = e.which;
      if (key == 13) {
        e.preventDefault();
        var nxtIdx = $inp.index(this) + 1;
        if(nxtIdx < n) {
          $('.txt_nav')[nxtIdx].focus();
        }
        else
        {
         $('.txt_nav')[nxtIdx-1].blur();
         $('#submit_btn').focus();
       }
     }
   });

    $( "#submit_btn" ).keypress(function( event ) {
      if ( event.which == 13 ) {
        req_input_check();     
      }
    });         

  });

  function req_input_check() {
    $(':input[required=""],:input[required]').bind('keypress', function(event){
      if ( event.which == 13 ) {
        if ($(this).val() == ""){
          $(this).css("border-color", "red");
        } else {
          $(this).css("border-color", "#ccc");
          $('#form_submit').submit();
        }
      }
    });
  }

  /**************************** INSERT  ****************************/

   $("#form_submit").on('submit',(function(e) {
      var menu = $("#menu").val();

      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url()?>Con_group_profile/add",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
        },
        success: function(data){
          if($.trim(data) === '') {
            swal("Oops...", "Something went wrong!", "warning");

          } else {
            if($.trim(data) ==='error')
            {
              swal("SQL Error!", "Please Try Again!", "warning");

            } else if($.trim(data) ==='data exists') {
              $('#group_name').focus();
              swal({
                title: "",
                text: "Group Profile Already Exists!",
                type: "warning",
                timer: 2000,
                showConfirmButton: false,
              });           

            } else if($.trim(data) ==='success') {
              swal({
                title: "",
                text: "Successfully Added!",
                type: "success",
                timer: 2000,
                showConfirmButton: false,
              });
              setTimeout(function() 
              {
                window.location.href = "<?php echo base_url()?>Con_group_profile/index/"+menu;
              }, 2000);
              
            }
          }
        },
        error: function(e) 
        {
          alert("err2");
        }
      });
    }));

    /********************************* Edit ******************************/

    $("#form_update").on('submit',(function(e) {
      var menu = $("#menu").val();

      e.preventDefault();
      $.ajax({
        url: "<?php echo base_url()?>Con_group_profile/update",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
        },
        success: function(data){
          $('#edit_data').modal('hide')
          if($.trim(data) === '') {
            swal("Oops...", "Something went wrong!", "warning");

          } else {
            if($.trim(data) ==='error')
            {
              swal("SQL Error!", "Please Try Again!", "warning");

            } else if($.trim(data) ==='data exists') {
              swal("", "Group Profile Already Exist!", "warning");

            } else if($.trim(data) ==='success') {
              swal({
                title: "Updated!",
                text: "Successfully Updated!",
                type: "success",
                confirmButtonText: "OK"
              },
              function (isConfirm) {
                if (isConfirm) {
                  window.location.href = "<?php echo base_url()?>Con_group_profile/index/"+menu+"/#update_details";
                }
              });
            }
          }      
        },
        error: function(e) 
        {
          alert("err2");
        }
      });
    }));  

  /******************************** Remove ***************************/

  $(".btn_del").click(function(){
    var menu = $("#menu").val();
    var row_id = $(this).val();

    swal({
     title: "Are you sure?",
     text: "",
     type: "warning",
     showCancelButton: true,
     confirmButtonClass: "btn-danger",
     confirmButtonText: "Yes, delete it!",
     cancelButtonText: "No, cancel!",
     closeOnConfirm: false,
     closeOnCancel: false
   },
   function(isConfirm) {
     if (isConfirm) {
      $.post( "<?php echo base_url()?>Con_group_profile/delete", {remove_data: "data", id : row_id}, function( data ) {   
       if($.trim(data.status) === 'success') {
         swal({
          title: "Deleted!",
          text: "Successfully Deleted!",
          type: "success",
          confirmButtonText: "OK"
        },
        function (isConfirm) {
          if (isConfirm) {
           window.location.href = "<?php echo base_url()?>Con_group_profile/index/"+menu+"/#update_details";
         }
       });

       } else if($.trim(data.status) ===' error') {
         swal("", "Error!", "warning");

       } else{
         swal("Oops...", "Something went wrong!", "warning");
       }
     }, "json");
    } else {
      swal("Cancelled", "", "error");
    }
  });

  });

  /**************************** Load Form Data ***********************/

  $(".btn_up").click(function(){
    var id = $(this).val();
    $("#id_up").val(id);

    $.post( "<?php echo base_url()?>Con_group_profile/get_dataset", { get_dataset: "data", id : id}, function( data ) {
      $.each(data.result, function (index, data) {
        $("#group_name_up").val(data.profile);
      });

    }, "json");   
  });

  </script> 
