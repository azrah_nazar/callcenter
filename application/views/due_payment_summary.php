<?php
$menu = $this->uri->segment(3);

?>
<div class="content-wrapper" style="min-height: 946px;">
    <section class="content-header">
        <h1>
            <i class="fa fa-id-card"></i> Due Payment Summary Report
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header ptbnull">
                        <h3 class="box-title titlefix">Due Payment Summary Report</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive mailbox-messages">
                            <div class="download_label"> Due Payment Summary Report</div>
                            <table class="table table-striped table-bordered table-hover example">
                                <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Count</th>
                                        <th>Total Due Payment</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php
                                    $count = 1;
                                    $tot_cnt = $tot_due = 0;
                                    foreach ($load_data as $value) {
                                    ?>
                                        <tr>
                                            <td> <?php echo $count; ?>.</td>
                                            <td> <?php echo $value->agent_code; ?></td>
                                            <td> <?php echo $value->Name; ?></td>
                                            <td> <?php echo $value->count; $tot_cnt+= $value->count; ?></td>
                                            <td> LKR <?php echo $value->tot_due; $tot_due += $value->tot_due; ?></td>
                                           
                                        </tr>
                                    <?php
                                        $count++;
                                    }
                                    ?>

                                        <tr>
                                            <th>Totals</th>
                                            <th></th>
                                            <th> </th>
                                            <th> <?php echo $tot_cnt;?></th>
                                            <th> LKR <?php echo $tot_due; ?></th>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>
