<style>
    .absent {
        background-color: #FA2601;
    }
    .present {
        background-color: #27AB00;
    }
    .holiday {
        background-color: #a7a7a7;
    }
</style>

<div class="content-wrapper" style="min-height: 946px;">
  <div class="row">  
    <div class="col-md-12">
      <section class="content-header" style="padding-right: 0;">
        <h1>
          <i class="fa fa-group"></i> My Profile</small></h1>
          
        </section>
      </div>
    
    <!-- /.control-sidebar -->
  </div>
<input type="hidden" name="acc_no" id="acc_no" value="<?php echo $staff['Acc_No']; ?>">
<input type="hidden" name="cashier" id="cashier" value="<?php echo $user_details->cash; ?>">
  <section class="content">
    <div class="row">
      <div class="col-md-4">             
        <div class="box box-primary">
          <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="<?php if(!empty($staff['prof_pic'])){ echo base_url() .$staff['prof_pic']; }else { echo base_url() ."uploads/student_images/no_image.png"; } ?>" alt="User profile picture">
            <h3 class="profile-username text-center"><?php echo $staff['Name'] ?></h3>
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item listnoback">
                <b>Employee No</b> <a class="pull-right text-aqua"><?php echo $staff['Acc_No']; ?></a>
                <input type="hidden" name="acc_no"  id="acc_no" value="<?php echo $staff['Acc_No']; ?>">
              </li>
              <li class="list-group-item listnoback">
                <b>Designation</b> <a class="pull-right text-aqua"><?php echo $staff['desig']; ?></a>
              </li>
              <li class="list-group-item listnoback">
                <b>Department</b> <a class="pull-right text-aqua"><?php echo strtolower($staff['division']); ?></a>
              </li>
              <li class="list-group-item listnoback">
                <b>Contact No</b> <a class="pull-right text-aqua"><?php echo strtolower($staff['Tp']); ?></a>
              </li>
              <li class="list-group-item listnoback">
                <b>NIC</b> <a class="pull-right text-aqua"><?php echo strtolower($staff['Nic']); ?></a>
              </li>
              <li class="list-group-item listnoback">
                <b>Email</b> <a class="pull-right text-aqua"><?php echo strtolower($staff['email']); ?></a>
              </li>
            </ul>
          </div>
        </div>

        
      </div>
</section>
</div>

<div class="modal fade" id="opening_bal_modal" role="dialog">
    <div class="modal-dialog">       
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title title text-center modal_title"> Opening Balance</h4>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0);" id="form_update">
                  <input type="hidden" name="acc_up" id="acc_up" value="<?php echo $staff['Acc_No']; ?>">

                  <div class="form-horizontal">
                      <div class="box-body">
                          <div class="form-group">                 
                            <label>Enter Opening Balance:<font color="#FF0000"><strong>*</strong></font></label>
                            <input type="text" class="form-control" name="open_bal" id="open_bal" placeholder="Enter ..." autocomplete="off" required onkeypress="return isNumberKey(event, this)">
                          </div>
                      </div>                   
                  </div>

                  <!-- /.box-body -->
                  <div class="box-footer"> 
                    <button class="btn btn-danger" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">Shift In</button>
                  </div>
                </form>
                
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button> -->
            </div>
        </div>
    </div>
</div>


<script>
  function isNumberKey(evt, obj) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
      if (charCode == 46) return false;
    if (charCode == 46) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  $(document).ready(function() {
    var desig = $('#cashier').val();
    var acc_no = $('#acc_no').val();

    if(desig == '1'){

      $.post( "<?php echo base_url()?>Con_my_profile/check_login", { check_login: "data", acc_no : acc_no}, function( data ) {
        $.each(data.result, function (index, data) {
          var status = data.ont;
          
          if(status == '0'){
            $('#opening_bal_modal').modal({
              backdrop: 'static',
              keyboard: true, 
              show: true
            });
          }
        });

      }, "json");
    }
  });

  /********************************* Edit ******************************/

  $("#form_update").on('submit',(function(e) {

    e.preventDefault();
    $.ajax({
      url: "<?php echo base_url()?>Con_my_profile/update",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      beforeSend : function()
      {
      },
      success: function(data){
        $('#opening_bal_modal').modal('hide')
        if($.trim(data) === '') {
          swal("Oops...", "Something went wrong!", "warning");

        } else {
          if($.trim(data) ==='error')
          {
            swal("SQL Error!", "Please Try Again!", "warning");

          } else if($.trim(data) ==='success') {
            swal({
              title: "Updated!",
              text: "Opening Balance Successfully Updated!",
              type: "success",
              confirmButtonText: "OK"
            },
            function (isConfirm) {
              if (isConfirm) {
                window.location.href = "<?php echo base_url()?>Con_my_profile/index";
              }
            });
          }
        }      
      },
      error: function(e) 
      {
        alert("err2");
      }
    });
  }));


</script>

