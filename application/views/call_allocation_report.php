<?php
$menu = $this->uri->segment(3);
$date = $this->uri->segment(4);

if (empty($date)) {
  $date1 = date("Y-m-01");
  $date2 = date("Y-m-d");
} else {
  $daterange = htmlspecialchars(trim($date), ENT_QUOTES, 'UTF-8');
  $date1 = substr($daterange, 0, 10);
  $date2 = substr($daterange, 17, 23);
}
?>

<style type="text/css">
  .error {
    color: red;
    size: 80%
  }

  .hidden {
    display: none;
  }
</style>
<div class="content-wrapper" style="min-height: 946px;">
  <section class="content-header">
    <h1>
      <i class="fa fa-id-card"></i> Call Allocation Report
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> Select Criteria </h3>

          </div>
          <div class="box-body">
            <form role="form" action="<?php echo site_url('Con_Contact_Allocation/call_allocation_report/') ?><?php echo $menu . "/" . $date1 . ' - ' . $date2; ?>" method="post" class="">
              <input type="hidden" name="menu" id="menu" value="<?php echo  $this->uri->segment(3); ?>">

              <div class="col-md-6">
                <div class="form-group">
                  <label>Select Date:<font color="#FF0000"><strong>*</strong></font></label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" autocomplete="off" name="date_range" class="form-control pull-right date_range" id="date_range" value="<?php echo $date1 . ' - ' . $date2; ?>" onChange="formHandler($(this).val())">
                  </div>
                </div>
              </div>

              <div class="form-group col-md-6">
                <label for="agent">Agent</label>
                <select id="agent" name="agent" class="form-control">
                  <option value="">Select</option>
                  <?php foreach ($load_agent as $agent) { ?>
                    <option value="<?php echo $agent->Acc_No ?>" <?php if (set_value('agent') == $agent->Acc_No) echo "selected=selected"; ?>><?php echo $agent->Name ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-sm-12">
                <div class="form-group">
                  <button type="submit" name="search" value="search_filter" class="btn btn-primary pull-right btn-sm checkbox-toggle"><i class="fa fa-search"></i> Search</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <?php if (isset($load_data)) { ?>
          <div class="box box-primary">
            <div class="box-header ptbnull">
              <h3 class="box-title titlefix">Call Allocation Report</h3>
            </div>
            <div class="box-body ">
              <div class="table-responsive mailbox-messages">
                <div class="download_label"> Call Allocation List</div>
                <table class="table table-striped table-bordered table-hover example">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Agent</th>
                      <th>Name</th>
                      <th>Contact Number</th>
                      <!-- <th>Upload ID</th> -->
                      <th>Uploaded By</th>
                      <th>Uploaded Date</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                    $count = 1;

                    foreach ($load_data as $value) {
                      $birthday = $value->dob;
                      if ($birthday == '0000-00-00') {
                        $dob = '';
                      } else {
                        $dob = $birthday;
                      }
                    ?>
                      <tr>
                        <td class="mailbox-name"> <?php echo $count; ?>.</td>
                        <td class="mailbox-name"> <?php echo $value->agent_name; ?></td>
                        <td class="mailbox-name"> <?php echo $value->name; ?></td>
                        <td class="mailbox-name"> <?php echo $value->con_no; ?></td>
                        <!-- <td class="mailbox-name"> <?php //echo $value->upload_id; 
                                                        ?></td> -->
                        <td class="mailbox-name"> <?php echo $value->username; ?></td>
                        <td class="mailbox-name"> <?php echo $value->date; ?></td>
                      </tr>
                    <?php
                      $count++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>

    </div>


    <!-- date-range-picker -->
    <script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/plugins/daterangepicker_n/daterangepicker.js"></script>

    <script>
      /**************************** DATE ***********************/
      $('#date_range').daterangepicker({
        autoclose: false,
        todayBtn: true,
        pickerPosition: "bottom-left"
      });

      function formHandler(date) {
        var menu = $("#menu").val();
        window.location.href = "<?php echo base_url() ?>Con_Contact_Allocation/call_allocation_report/" + menu + "/" + date;
      }
    </script>