<?php
date_default_timezone_set('Asia/Colombo');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>VICTORY ACADEMY</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta name="theme-color" content="#424242" />
    <link href="<?php echo base_url(); ?>backend/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/style-main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/jquery.mCustomScrollbar.min.css">
    <?php
    $this->load->view('layout/theme');
    ?>

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/ionicons.min.css">

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/iCheck/all.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/morris/morris.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/colorpicker/bootstrap-colorpicker.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/custom_style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/datepicker/css/bootstrap-datetimepicker.css">
    <!--file dropify-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/dist/css/dropify.min.css">
    <!--file nprogress-->
    <link href="<?php echo base_url(); ?>backend/dist/css/nprogress.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>backend/plugins/jQueryUI/jquery-ui.min.css" rel="stylesheet">

    <!--print table-->
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/buttons.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <!--print table mobile support-->
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>backend/dist/datatables/css/rowReorder.dataTables.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>backend/custom/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/dist/js/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/datepicker/js/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo base_url(); ?>backend/plugins/colorpicker/bootstrap-colorpicker.js"></script>
    <script src="<?php echo base_url(); ?>backend/datepicker/date.js"></script>
    <script src="<?php echo base_url(); ?>backend/dist/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>backend/js/school-custom.js"></script>
    <script src="<?php echo base_url(); ?>backend/js/sstoast.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>backend/dist/js/jquery.mask.min.js"></script>


    <!-- fullCalendar -->
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/calender/zabuto_calendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/fullcalendar/dist/fullcalendar.print.min.css" media="print">
    <link rel="stylesheet" href="<?php echo base_url() ?>backend/sweet-alert/sweetalert2.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>backend/toggle/css/bootstrap-toggle.min.css">

    <script src="<?php echo base_url(); ?>backend/plugins/iCheck/icheck.min.js"></script>



</head>

<body class="hold-transition skin-blue fixed sidebar-collapse">



    <div class="wrapper">

        <header class="main-header" id="alert">
            <a href="#" class="logo">
                <span class="logo-mini">VA</span>
                <span class="logo-lg"><img src="<?php echo base_url(); ?>backend/images/logo.png" /></span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>


            </nav>
        </header>

        <div class="content-wrapper" style="min-height: 946px;">
            <section class="content-header">

            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="box box-primary">
                            <div class="box-header ptbnull">
                                <h3 class="box-title titlefix">Workshop Feedback</h3>
                            </div>
                            <div class="box-body">
                                <?php if($st_details['confirmation'] == 1 || $st_details['confirmation'] == 0){?>
                                <p style="font-size: 18px;">ඔබ <?php echo $st_details['w_date']?> දින <?php echo date("h:i A", strtotime($st_details['w_time'])); ?> පැවැත්වූ අපගේ හදුන්වාදීමේ වැඩසටහනට සහභාගි වූවා නම් Yes Button යොදන්න.
වැඩසටහනට සහභාගි වීමට නොහැකි වූවා නම් No Button යොදන්න.</p><br />

                                <div class="col-md-4 col-md-offset-4">
                                    <button class="btn btn-success btn-lg btn_yes" data-toggle="tooltip" title="Yes" value="<?php echo $st_details['wid']?>" data-reg="<?php echo $st_details['reg_no']?>" data-con="<?php echo $st_details['con_no']?>">
                                        <i class="fa fa-check"> YES</i>
                                    </button>&nbsp;&nbsp;

                                    <button class="btn btn-danger btn-lg btn_no" data-toggle="tooltip" title="No" value="<?php echo $st_details['wid']?>" data-reg="<?php echo $st_details['reg_no']?>" data-con="<?php echo $st_details['con_no']?>">
                                        <i class="fa fa-close"> NO</i>
                                    </button>
                                </div>
                                <?php }else{ ?>
                                    <div class="col-md-12" style="text-align: center;">
                                    <p ><strong>Victory Academy වෙත ඔබව සාදරයෙන් පිලිගනිමු..! <br/><br/>අපගේ පාඨමාලාවට සහභාගි වීම සම්බන්ධව ඔබ විසින් දැනටමත් අප වෙත ඔබගේ ප්‍රතිචාරය ලබා දී ඇත.<br/> <br/>ස්තුති...</strong></p>
                                    <?php }?>
                                    </div>


                            </div>
                        </div>
                    </div>
                </div>

        </div>


        <script>
        /******************************** FEEDBACK YES ***************************/
        $(".btn_yes").click(function() {
            var ws_id = $(this).val();
            var reg_num = $(this).attr('data-reg');
            var telNo = $(this).attr('data-con');

            swal({
                    title: "Are you sure you want to accept?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, accept it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.post("<?php echo base_url() ?>Con_pre_registry/feedback_yes", {
                            feedback_yes: "data",
                            ws_id: ws_id,
                            telNo: telNo
                        }, function(data) {
                            if ($.trim(data) == 'success') {
                                swal({
                                        title: "Accepted!",
                                        text: "Successfully Accepted!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },
                                    function(isConfirm) {
                                        if (isConfirm) {
                                            window.location.href = "<?php echo base_url() ?>feedback/" + reg_num;
                                        }
                                    });

                            } else if ($.trim(data) == ' error') {
                                swal("", "Error!", "warning");

                            } else {
                                swal("Oops...", "Something went wrong!", "warning");
                            }
                        });
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });

        });

        /******************************** FEEDBACK NO ***************************/
        $(".btn_no").click(function() {
            var ws_id = $(this).val();
            var reg_num = $(this).attr('data-reg');
            var telNo = $(this).attr('data-con');

            swal({
                    title: "Are you sure you want to reject?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, reject it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.post("<?php echo base_url() ?>Con_pre_registry/feedback_no", {
                            feedback_yes: "data",
                            ws_id: ws_id,
                            telNo: telNo
                        }, function(data) {
                            if ($.trim(data) == 'success') {
                                swal({
                                        title: "Rejected!",
                                        text: "Successfully Rejected!",
                                        type: "success",
                                        confirmButtonText: "OK"
                                    },
                                    function(isConfirm) {
                                        if (isConfirm) {
                                            window.location.href = "<?php echo base_url() ?>feedback/" + reg_num;
                                        }
                                    });

                            } else if ($.trim(data) == ' error') {
                                swal("", "Error!", "warning");

                            } else {
                                swal("Oops...", "Something went wrong!", "warning");
                            }
                        });
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });

        });
        </script>