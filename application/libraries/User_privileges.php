<?php 

$data = array();

if($this->session->has_userdata('username'))
{
	$acc_no = strip_tags($this->session->userdata('acc_no'));
	$user = strip_tags($this->session->userdata('username'));
	$user_id = strip_tags($this->session->userdata('id'));
	$Val = strip_tags($this->session->userdata('Val'));
	$emp_id = strip_tags($this->session->userdata('emp'));
	$u_shift_id = strip_tags($this->session->userdata('u_shift_id'));

	$user_details = $this->My_profile_model->user_details($user);
	$data['user_details'] = $user_details;

	$category = $this->Common_model->categories($user_id, $Val);
	$data['category'] = $category;

	$menu_list = $this->Common_model->menu_list($user_id, $Val);
	$data['menu_list'] = $menu_list;

	$msg_count = $this->Call_Report_Model->message_count($acc_no, $Val);
	$this->session->set_userdata('msg_count', $msg_count);

	$unread_msg = $this->Call_Report_Model->unread_msg($acc_no, $Val);
	$data['unread_msg'] = $unread_msg;

	// $data['acc_no'] = $acc_no;
	// $data['val'] = $Val;

	$user_access = $this->Common_model->user_access($menu, $acc_no);

	$activity = $user_access['Caption'];
	$dt = date("Y-m-d H:i:s");
	$time = date("H:i:s");

	if (!empty($user_access)) {
		$this->Common_model->insert_log($activity, $dt, $time, $acc_no);
		return $data;

	}else{
		redirect('Con_unauthorized', 'refresh');
	}

}else{
	redirect('Con_login', 'refresh');
}



?>