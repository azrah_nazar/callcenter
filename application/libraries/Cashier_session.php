<?php 

if($this->session->has_userdata('username')) {
	$cashier = strip_tags($this->session->userdata('cash'));

	if($cashier == '1'){
		$this->user = strip_tags($this->session->userdata('username'));
		setcookie('cashLogin', $this->user, time() + (3600 * 12), "/"); // 3600 = 1 hr
	}else{
		$this->user = '';
	}	
	

} else {
	$this->user = '';
}

if(isset($_COOKIE['cashLogin'])) {
	$this->Cashier = $_COOKIE['cashLogin'];
	
} else {
	if(empty($this->user)) {
		$this->Cashier = '';
	
	} else {
		if(isset($_COOKIE['cashLogin'])) {
			$this->Cashier = $_COOKIE['cashLogin'];
		
		} else {
			$this->Cashier = '';
		}
	}
}

if(empty($this->Cashier)) {
	$this->acc_no = '';
	$this->user_id = '';
	$this->Val = '';
	$this->userCash = '';
	$this->userShift = '';
	$this->userOnt = '';
	$this->userDiv = '';

} else {
	$logData = $this->Payment_model->getUserDetails($this->Cashier);
	$this->acc_no = $logData['acc_no'];
	$this->user_id = $logData['uid'];
	$this->Val = $logData['Val'];
	$this->userCash = $logData['cash'];
	$this->userShift = $logData['Shift'];
	$this->userOnt = $logData['ont'];
	$this->userDiv = $logData['DV'];
}

?>