<?php 
   class Main_category_model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 

      /******************** LOAD DATA ********************/  
      public function load_data(){
         $this->db->select('*');
         $this->db->from('Stock_Book');
         $this->db->order_by('ID');
         $query = $this->db->get();

         return $query->result();
      } 

      /******************** INSERT ********************/ 
      public function insert($data) {
         $result = $this->db->get_where("Stock_Book",array('Name'=>$data['Name']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            if ($this->db->insert("Stock_Book", $data)) {
               return "success";
            }else{
               return "error";
            }
             
         }else{
            return "data exists";
         }
      }

     

      public function get_dataset($id){
         $this->db->select('*');
         $this->db->from('Stock_Book');
         $this->db->where('ID', $id);
         $query = $this->db->get();

         return $query->result();
      }

      /******************** UPDATE ********************/
      public function update($data, $id){
         $result = $this->db->get_where("Stock_Book",array( 'Name'=>$data['Name']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            $this->db->set($data);
            $this->db->where("ID", $id);
            if ($this->db->update("Stock_Book", $data)) {
               return "success"; 
            }else{
               return "error";
            }
         }else{
            return "data exists";
         }
      }

      /******************** DELETE ********************/
      public function delete($data){
         if ($this->db->delete("Stock_Book", "ID = ".$data['ID'])) {
            return true; 
         }else{
            return "error";
         }
      }


      
   } 
?> 