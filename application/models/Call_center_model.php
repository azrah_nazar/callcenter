<?php
class call_center_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function get_data()
	{
		$sql = "SELECT * FROM con_list where flag ='0' OR flag = '2' OR flag = '3' ORDER BY flag ASC LIMIT 1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function get_process_list()
	{
		$sql = "SELECT * FROM con_list where emp_accNo = '' OR emp_accNo IS NULL";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function update_number($number)
	{
		$sql = "UPDATE con_list SET flag='0' where flag='1'";
		$query = $this->db->query($sql);
		if ($query) {
			$sql1 = "UPDATE con_list SET flag='1' WHERE con_no='$number'";
			$query = $this->db->query($sql1);
		}
	}


	public function load_contact_list($emp)
	{
		$date = date('Y-m-d');
		$sql = "SELECT * FROM con_list where emp_accNo='$emp' AND (flag = 0) AND direct_sale<>'1'  AND reapplied_reject<>'1' ORDER BY  reminder_date ASC, date DESC";
		$query = $this->db->query($sql);

		return $query->result();
	}

	// flag = 1 => accept
	// flag = 2 => registered
	// flag = 3 => reject
	// flag = 4 => workshop accept

	public function response_update($number, $res_flag)
	{
		if ($res_flag == 1) {
			$comment =  "Accepted";
			$tt =  "success";
			$agent_reject_date = '';
			$direct_sale = '';
		} else if ($res_flag == 3) {
			$comment =  "Rejected";
			$tt =  "success2";
			$direct_sale = '';
			$agent_reject_date = date('Y-m-d');
		} else {
			$comment =  "";
			$tt =  "";
			$agent_reject_date = '';
			$direct_sale = '';
		}

		$date = date('Y-m-d');
		$query = $this->db->query("UPDATE con_list SET flag='$res_flag', comment='$comment', date1='$date', agent_reject_date='$agent_reject_date', direct_sale='$direct_sale' WHERE con_no='$number';");
		if ($query) {
			return $tt;
		} else {
			return "error";
		}
	}

	/******************** UPDATE ********************/
	public function update_reminder($data, $id)
	{
		$this->db->set($data);
		$this->db->where("con_no", $id);
		if ($this->db->update("con_list", $data)) {
			return "success";
		} else {
			return "error";
		}
	}

	/******************** SHUFFLE ********************/
	public function shuffle_op($tp)
	{
		$sql = "INSERT INTO shuffle_log (con_no, date, flag, user_id, comment, date1, name, address, whatsapp, job, dob, emp_accNo, reminder_date) SELECT con_no, date, flag, user_id, comment, date1, name, address, whatsapp, job, dob, emp_accNo, reminder_date FROM con_list WHERE con_no = '$tp' LIMIT 1";
		if ($this->db->query($sql)) {
			if ($this->db->query("UPDATE con_list SET emp_accNo='', flag='0', date='', workshop='', self_reg='0', confirmation='', bank_sms='', ws_reminder='', feedback_no='', feedback_reject='', pending_payment='', pending_reallocation='', spl_cus='', tic_id='', reminder_date='' WHERE con_no='$tp'")) {

				$this->db->select('id');
				$this->db->from('con_list');
				$this->db->where('con_no', $tp);
				$query = $this->db->get();
				$row = $query->row();
				$con_id = $row->id;
				$this->db->query("DELETE FROM reminders WHERE con_id='$con_id'");
				return "success";
			} else {
				return "error";
			}
		} else {
			return "error";
		}
	}

	/******************** CALL ALLOCATION ********************/
	public function call_allocation($desig = NULL, $acc_no)
	{
		$st = 'success';
		$sql = "SELECT * FROM con_list where emp_accNo = '' OR emp_accNo IS NULL";
		$query = $this->db->query($sql);
		$rowcount = $query->num_rows();

		$sqle = "SELECT * FROM Usert where call_ID > 0 ORDER BY call_ID ASC";
		$querye = $this->db->query($sqle);
		$ucount = $querye->num_rows();

		$this->db->select('iterate_no');
		$this->db->from('invoice_no');
		$query = $this->db->get();
		$row = $query->row();
		$new_it_no = $row->iterate_no;

		$this->db->select('con_upload');
		$this->db->from('invoice_no');
		$qry = $this->db->get();
		$rw = $qry->row();
		$con_upload = $rw->con_upload;

		if ($rowcount != 0) {
			if ($ucount > 0) {
				while ($rowcount > 0) {
					$new_it_no += 1;

					$this->db->select('acc_no');
					$this->db->from('Usert');
					$this->db->where('call_ID', $new_it_no);
					$this->db->where('is_present', '1');
					$query2 = $this->db->get();
					$ss = $query2->num_rows();

					if ($ss == 0) {
						while ($new_it_no <= $ucount) {
							if ($new_it_no >= $ucount) {
								$new_it_no = 0;
							}
							$new_it_no += 1;
							$this->db->select('acc_no');
							$this->db->from('Usert');
							$this->db->where('call_ID', $new_it_no);
							$this->db->where('is_present', '1');
							$query3 = $this->db->get();
							$sss = $query3->num_rows();
							if ($sss == 1) {
								break;
							}
							//echo $new_it_no."\n";
						}
						$row3 = $query3->row();
						$empid = $row3->acc_no;
					} else {
						$row2 = $query2->row();
						$empid = $row2->acc_no;
					}

					$sql1 = "SELECT * FROM con_list where emp_accNo = '' OR emp_accNo IS NULL ORDER BY id ASC";
					$query1 = $this->db->query($sql1);
					$row_res = $query1->row_array();
					$id = $row_res['id'];
					$cn = $row_res['con_no'];
					$wa = $row_res['whatsapp'];
					$now = date('Y-m-d H:i:s');

					$sqlu = $this->db->query("UPDATE con_list SET emp_accNo='$empid', upload_id = '$con_upload', date='$now' WHERE id='$id'");
					if (!$sqlu) {
						$st = 'error';
					} else {
						$this->db->query("UPDATE con_list_temp SET emp_accNo='$empid' WHERE con_no='$cn' OR whatsapp='$wa'");
						if ($new_it_no >= $ucount) {
							$new_it_no = 0;
						}
						$this->db->query("UPDATE invoice_no SET iterate_no='$new_it_no'");
					}

					$rowcount--;
				}
				$nw_con_upload = 1;
				$this->db->query("UPDATE invoice_no SET con_upload='$nw_con_upload'");
			} else {
				$st = 'empty';
			}
		} else {
			$st = 'empty';
		}
		return $st;
	}

	/************************** WELCOME SMS *************************/
	public function welcome_sms($mobile_no)
	{
		$this->db->select('reg_no');
		$this->db->from('invoice_no');
		$query = $this->db->get();
		$row = $query->row();

		$this->db->select('agent_code, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice');
		$this->db->from('con_list');
		$this->db->join('Usert', 'con_list.emp_accNo = Usert.acc_no');
		$this->db->join('Employee', 'Employee.Acc_No = Usert.acc_no');
		$this->db->where("con_list.con_no", $mobile_no);
		$qry = $this->db->get();
		$rw = $qry->row();
		$agent_code = $rw->agent_code;
		$emp_name = $rw->name;
		$emp_tp = $rw->Tp;
		$sinhala_name = $rw->sinhala_name;
		$emp_tp2 = " / " . $rw->tpOffice;

		$reg = $row->reg_no;
		$reg_no = sprintf('%03d', $reg);
		$reg_num = "WR" . $agent_code . $reg;

		$nw_reg = $reg + 1;

		$this->db->query("UPDATE con_list SET reg_no='$reg_num' WHERE con_no='$mobile_no'");
		$this->db->query("UPDATE invoice_no SET reg_no='$nw_reg'");

		// $message = "මෙම ලින්ක් එක ඔස්සේ නොමිලේ පවත්වනු ලබන වැඩමුළුවට අවස්ථාවක් වෙන්කරවා ගන්න.\nhttps://callcenter.victoryacademylk.com/register/" . trim($reg_num) . "\n" . $sinhala_name . " " . $emp_tp . " " . $emp_tp2;

		// $data['message'] = trim($message);
		// $count = ceil(strlen($message) / 160);

		// $url = 'https://smsserver.textorigins.com/Send_sms?';

		// $success_count = 0;

		// $data_sms = array(
		// 	'src' => 'CYCLOMAX257', //Mask
		// 	'email' => 'newvictoryacademy@gmail.com ', //User Email
		// 	'pwd' => 'Vic66556', //User Password
		// 	'msg' => $message, //Message
		// 	'dst' => $mobile_no
		// ); //Phone Number

		// $msg = http_build_query($data_sms);

		// $url .= $msg;

		// $ch = curl_init($url);

		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// curl_setopt($ch, CURLOPT_HEADER, false);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// $result_set = curl_exec($ch);

		// $now = date('Y-m-d H:i:s');
		// if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
		// 	$success_count++;
		// 	$this->db->query("INSERT INTO sms_log(operator, sms_date, message, msg_type, reg_num, con_no) VALUES('$emp_name', '$now', '$message', 'Accepted Message', '$reg_num', '$mobile_no')");
		// }


		// $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		// curl_close($ch);


		// $result['count'] = $success_count;
		// $result['status'] = '1601';
		// $result['message'] = 'Successfully Sent';

		//echo json_encode($result);

	}

	/******************** GET DATASET ********************/
	public function get_dataset($id)
	{
		$this->db->select('*');
		$this->db->from('con_list');
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->result();
	}

	/******************** UPDATE ********************/
	public function update($data, $id)
	{
		$new_tel = $data['con_no'];
		$new_whatsapp = $data['whatsapp'];

		$result = $this->db->query("SELECT * FROM con_list WHERE id!='$id' AND ((con_list.con_no = '$new_tel' OR ((con_list.whatsapp = '$new_tel' OR con_list.whatsapp = '0$new_tel') AND con_list.whatsapp != '')) OR (con_list.con_no = '$new_whatsapp' OR con_list.con_no = '0$new_whatsapp'  OR ((con_list.whatsapp = '$new_whatsapp' OR con_list.whatsapp = '0$new_whatsapp')  AND con_list.whatsapp != '')))");
		$rowcount = $result->num_rows();
		if ($rowcount == 0) {

			$this->db->set($data);
			$this->db->where("id", $id);
			if ($this->db->update("con_list", $data)) {
				return "success";
			} else {
				return "error";
			}
		} else {
			return "data not exists";
		}
	}

	/******************** DIRECT SALE ********************/
	public function direct_sale($tp)
	{
		$this->db->select('reg_no');
		$this->db->from('invoice_no');
		$query = $this->db->get();
		$row = $query->row();

		$this->db->select('agent_code, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice');
		$this->db->from('con_list');
		$this->db->join('Usert', 'con_list.emp_accNo = Usert.acc_no');
		$this->db->join('Employee', 'Employee.Acc_No = Usert.acc_no');
		$this->db->where("con_list.con_no", $tp);
		$qry = $this->db->get();
		$rw = $qry->row();
		$agent_code = $rw->agent_code;

		$reg = $row->reg_no;
		$reg_num = "WR" . $agent_code . $reg;

		$nw_reg = $reg + 1;
		$today = date('Y-m-d');

		$this->db->query("UPDATE con_list SET reg_no='$reg_num', date1='$today' WHERE con_no='$tp'");
		$this->db->query("UPDATE invoice_no SET reg_no='$nw_reg'");

		$this->db->set('direct_sale', '1');
		$this->db->where("con_no", $tp);
		if ($this->db->update("con_list")) {
			return "success";
		} else {
			return "error";
		}
	}

	/******************** DIRECT SALE LIST ********************/
	public function direct_sale_list($emp)
	{
		$date = date('Y-m-d');
		$sql = "SELECT * FROM con_list where emp_accNo='$emp' AND direct_sale='1' AND feedback_reject<>'1' AND flag<>'3' AND con_list.all_paid <> '1' ORDER BY  reminder_date ASC, reminder_note ASC, date DESC";
		$query = $this->db->query($sql);

		return $query->result();
	}

	/******************** SPECIAL CUSTOMER********************/
	public function spl_customer($tp, $cus)
	{
		$this->db->set('spl_cus', $cus);
		$this->db->where("con_no", $tp);
		if ($this->db->update("con_list")) {
			return "success";
		} else {
			return "error";
		}
	}

}
