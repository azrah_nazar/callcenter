<?php 
class Staff_registration_model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   /******************** LOAD DATA ********************/
   public function load_data($id){
      if($id == ''){
         return "";
      }else{
         $this->db->select('*');
         $this->db->from('employee_details');
         $this->db->where('emp_id', $id);
         $user = $this->db->get();

         return $user->result();
      }
   }

   /******************** INSERT ********************/
   public function add($data, $emp_id) {

      if($emp_id == '' || $emp_id === NULL){
         $this->db->select('PR_Code');
         $this->db->from('account_numbers');
         $query = $this->db->get();

         $row = $query->row();

         $pr = $row->PR_Code;
         $PR_Code = $pr + 1;
         $emp_name = $data['Name'];

         $sql2 = "UPDATE account_numbers SET PR_Code='$PR_Code'";
         $this->db->query($sql2);

         $acc_no = '4-8-9-'.$PR_Code;

         $data['Acc_No'] = $acc_no;
         $data['payout_date'] = '';

         $result = $this->db->get_where("Employee",array('Nic'=>$data['Nic']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            if ($this->db->insert("Employee", $data)) {
               $sql_p = "INSERT INTO account_posting (P_Code, PName, B_Code, T_Code, H_Code, L_Code) VALUES ('$PR_Code', '$emp_name', '4', '8', '9', '$acc_no')";
               $this->db->query($sql_p);

               return "success";
            }else{
               return "error";
            }

         }else{
            $this->db->set($data);
            $this->db->where("ID", $emp_id);
            if ($this->db->update("Employee", $data)) {
               return "updated";
            }
         }
      }else{
         $this->db->set($data);
         $this->db->where("ID", $emp_id);
         if ($this->db->update("Employee", $data)) {
            return "updated";
         }
      }
   }


   public function load_bank(){
      $this->db->select('*');
      $this->db->from('bank');

      $query = $this->db->get();

      return $query->result();
   }

   public function load_branch(){
      $this->db->select('*');
      $this->db->from('branch');

      $query = $this->db->get();

      return $query->result();
   }


   public function load_bank_code($bank_id){
      $this->db->select('*');
      $this->db->from('bank');
      $this->db->where("id", $bank_id);

      $query = $this->db->get();

      return $query->result();
   }

   public function load_branch_code($branch_id){
      $this->db->select('*');
      $this->db->from('branch');

      // $array = array('id' => $branch_id, 'bank_id' => $bank_id);

      $this->db->where("id", $branch_id);
      // $this->db->where($array);

      $query = $this->db->get();

      return $query->result();
   }

   public function get_branch($bank_id){
      $this->db->select('*');
      $this->db->from('branch');
      $this->db->where('bank_id', $bank_id);
      // $this->db->order_by('cat');
      $query = $this->db->get();

      return $query->result();
   }
} 
?> 