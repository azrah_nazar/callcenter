<?php
class Mapping_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /******************** LOAD MALE OPERATORS ********************/
    public function load_males()
    {
        $this->db->select('Employee.*');
        $this->db->from('Employee');
        $this->db->where('gender', 'Male');
        $this->db->where('mapped_to', '');
        $this->db->where('desig', '4');
        $user = $this->db->get();

        return $user->result();
    }

    /******************** LOAD FEMALE OPERATORS ********************/
    public function load_females()
    {
        $query = $this->db->query("SELECT Employee.Name, Employee.Acc_No FROM Employee WHERE Employee.gender = 'Female' AND Employee.desig='4'
        AND Employee.Acc_No NOT IN (SELECT Employee.mapped_to FROM Employee WHERE Employee.mapped_to!='')");

        return $query->result();
    }

    /******************** LOAD DATA ********************/
    public function load_data()
    {
        $this->db->select('Employee.Name AS male, Employee.Acc_No AS male_acc, emp2.Name AS female');
        $this->db->from('Employee');
        $this->db->join('Employee AS emp2', 'Employee.mapped_to = emp2.Acc_No');

        $user = $this->db->get();

        return $user->result();
    }

    /******************** INSERT ********************/
    public function insert($data)
    {
        $male = $data['male'];
        $female = $data['female'];
        if ($this->db->query("UPDATE Employee SET mapped_to='$female' WHERE Acc_No='$male'")) {
            return "success";
        } else {
            return "error";
        }
    }

    /******************** reset ********************/
    public function reset($Acc_No)
    {
        if ($this->db->query("UPDATE Employee SET mapped_to='' WHERE Acc_No='$Acc_No'")) {
            return "success";
        } else {
            return "error";
        }
    }

    /******************** BACKDATE ********************/
    public function backdate($no_of_days)
    {
        if ($this->db->query("UPDATE backdate SET no_of_days='$no_of_days'")) {
            return "success";
        } else {
            return "error";
        }
    }

    /******************** LOAD BACK DATE ********************/
    public function get_backdate()
    {
        $this->db->select('backdate.*');
        $this->db->from('backdate');
        $query = $this->db->get();

        return $query->row_array();
    }
     
}
