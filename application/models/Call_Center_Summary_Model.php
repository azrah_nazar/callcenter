<?php 
class Call_Center_Summary_Model extends CI_Model {
	
   function __construct() { 
      parent::__construct(); 
   } 

   public function details($date1, $date2) {
      $sql = "SELECT Employee.Name, COUNT((case when (con_list.date1 BETWEEN '$date1' AND '$date2') then 1 end)) AS `accept`, COUNT((case when (date(con_list.date) BETWEEN '$date1' AND '$date2') then 1 end)) AS reg, COUNT((case when (date(con_list.reject_date) BETWEEN '$date1' AND '$date2') then 1 end)) AS reject
      FROM
      con_list
      Inner Join Employee ON con_list.emp_accNo = Employee.Acc_No
      Group by Employee.Name";
      $query = $this->db->query($sql);

      return $query->result_array();
   }


}
?>