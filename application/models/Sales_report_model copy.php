<?php
class Sales_report_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /******************** ACCOUNT CREATION ********************/
    public function account_creation($date1 = NULL, $date2 = NULL, $agent = NULL)
    {
        $this->db->select('con_list.name, con_list.reg_no, con_list.whatsapp, con_list.con_no, workshops.w_name, con_list.spl_cus, con_list.workshop, SUM(bank_deposits.cr) AS deposits, bank_deposits.is_usdt, bank_deposits.flag AS pay_flag, con_list.reminder_date, con_list.tic_id, con_list.id, con_list.emp_accNo, con_list.spl_cus, con_list.date1, con_list.job, con_list.site_flag, con_list.user_id, con_list.town, bank_deposits.inv_no,  con_list.usdt_val, con_list.usdt_marked, MAX(`bank_deposits`.`ac_rej_date`) AS dt');
        $this->db->from('con_list');
        $this->db->join('workshops', 'con_list.workshop = workshops.id', 'LEFT OUTER');
        $this->db->join('bank_deposits', 'con_list.reg_no = bank_deposits.reg_no', 'LEFT OUTER');
        // if ($agent != '') {
        //     $this->db->where('con_list.emp_accNo', $agent);
        // }
        
        $this->db->where('bank_deposits.flag', '1');
        $this->db->group_start();
        $this->db->where('con_list.pending_payment', '1');
        $this->db->or_where('con_list.direct_sale', '1');
        $this->db->group_end();
        $this->db->having("SUM(bank_deposits.cr) >= '15000' ");
        $this->db->having("MAX(`bank_deposits`.`ac_rej_date`) BETWEEN '" . $date1 . " 00:00:00' AND '" . $date2 . " 23:59:59'");

        $this->db->group_by('con_list.name, con_list.reg_no, con_list.whatsapp, con_list.reminder_date, con_list.tic_id, con_list.id, con_list.emp_accNo, con_list.spl_cus, con_list.date1, con_list.job, con_list.site_flag, con_list.user_id');
        $this->db->order_by('bank_deposits.ac_rej_date', 'DESC');

        $query = $this->db->get();

        return $query->result();
    }


    /******************** REFUND LIST ********************/
    public function refund_list($date1 = NULL, $date2 = NULL, $agent)
    {
        $this->db->select('con_list.reg_no, con_list.con_no, con_list.whatsapp, bank_deposits.inv_no, con_list.name, refunds.ref_amt, refunds.ref_date, refunds.id AS ref_id');
        $this->db->from('con_list');
        $this->db->join('bank_deposits', 'con_list.reg_no = bank_deposits.reg_no');
        $this->db->join('refunds', 'con_list.reg_no = refunds.reg_no');

        $this->db->where("DATE(refunds.ref_date) BETWEEN '" . $date1 . "' AND '" . $date2 . "'");
        $this->db->where("bank_deposits.flag <>", "2");
        $this->db->group_by('con_list.reg_no, con_list.con_no, con_list.whatsapp, bank_deposits.inv_no, con_list.name');

        $query = $this->db->get();

        return $query->result();
    }

    public function get_refund_details($inv_no, $ref_id)
	{
		$sql = "SELECT con_list.name, con_list.town, bank_deposits.inv_no, refunds.ref_amt FROM
		con_list
		Inner Join bank_deposits ON con_list.reg_no = bank_deposits.reg_no
		Inner Join refunds ON bank_deposits.reg_no = refunds.reg_no WHERE bank_deposits.inv_no='$inv_no' AND refunds.id='$ref_id'";
		$query = $this->db->query($sql);

		return $query->row_array();
	}

    /******************** DAILY SALES LIST ********************/
    public function daily_sales($date)
    {
        if (empty($date)) {
            $date1 = $date2  = date('Y-m-d');
        } else {
            $date1 = $date2 = $date;
        }
        $this->db->select('Usert.agent_code,Employee.Name, Count(bank_deposits.reg_no) AS pay_cnt');
        $this->db->from('Usert');
        $this->db->join('Employee', 'Usert.acc_no = Employee.Acc_No');
        $this->db->join('con_list', 'con_list.emp_accNo = Usert.acc_no');
        $this->db->join('bank_deposits', 'bank_deposits.reg_no = con_list.reg_no');

        $this->db->group_start();
        $this->db->where('Usert.Val', '13');
        $this->db->where('bank_deposits.flag <>', '2');
        $this->db->where('bank_deposits.p_type', 'Full Payment');
        $this->db->or_where('bank_deposits.p_type', 'Advanced Payment');
        $this->db->group_end();

        $this->db->where("DATE(ac_rej_date) BETWEEN '" . $date1 . "' AND '" . $date2 . "'");
        $this->db->group_by('Usert.agent_code, Employee.Name');
        $this->db->order_by('Count(bank_deposits.reg_no)', 'DESC');

        $query = $this->db->get();

        return $query->result();
    }

    /******************** TOTAL SALES LIST ********************/
    public function total_sales($dte)
    {
        if (empty($dte)) {
            $dt = date('Y-m-d');
        } else {
            $dt = $dte;
        }
        $datee = date("d", strtotime($dt));
        $mon = date("m", strtotime($dt));
        if ($datee > '10') {
            $date1 = date('Y-'.$mon.'-11');
            $date2 = $dt;
        
            $last_month_name = date("F", strtotime($dt));
        } else {
            $last_month = date("m", strtotime('-1 month', strtotime($dt)));
            $date1 = date('Y-' . $last_month . '-11');
            $date2 = $dt;
        }

        $this->db->select('Usert.agent_code,Employee.Name, Count(bank_deposits.reg_no) AS pay_cnt');
        $this->db->from('Usert');
        $this->db->join('Employee', 'Usert.acc_no = Employee.Acc_No');
        $this->db->join('con_list', 'con_list.emp_accNo = Usert.acc_no');
        $this->db->join('bank_deposits', 'bank_deposits.reg_no = con_list.reg_no');

        $this->db->group_start();
        $this->db->where('bank_deposits.flag <>', '2');
        $this->db->where('Usert.Val', '13');
        $this->db->where('bank_deposits.p_type', 'Full Payment');
        $this->db->or_where('bank_deposits.p_type', 'Advanced Payment');
        $this->db->group_end();

        $this->db->where("DATE(ac_rej_date) BETWEEN '" . $date1 . "' AND '" . $date2 . "'");
        $this->db->group_by('Usert.agent_code, Employee.Name');
        $this->db->order_by('Count(bank_deposits.reg_no)', 'DESC');
        $this->db->order_by('Usert.agent_code', 'ASC');

        $query = $this->db->get();

        return $query->result();
    }

    /******************** PERSONAL BANK REPORT ********************/
    public function personal_bank_report($date1 = NULL, $date2 = NULL)
    {
        $query = $this->db->query("SELECT DATE(bank_deposits.ac_rej_date) AS dt, SUM(IF(bank = 'Sampath', bank_deposits.cr, NULL)) AS Sampath, SUM(IF(bank = 'Peoples', bank_deposits.cr, NULL)) AS Peoples, SUM(IF(bank = 'Crypto', bank_deposits.cr, NULL)) AS Crypto,  SUM(IF(bank = 'Online Payment', bank_deposits.cr, NULL)) AS online_payment, SUM(IF(bank = 'KOKO', bank_deposits.cr, NULL)) AS KOKO
      FROM bank_deposits WHERE DATE(ac_rej_date) BETWEEN '$date1' AND '$date2' AND flag='1' AND (bank = 'KOKO' OR bank = 'Peoples' OR bank = 'Crypto' OR bank = 'Online Payment') GROUP BY DATE(bank_deposits.ac_rej_date)");
        return $query->result();
    }

    /******************** DUE PAYMENT SUMMARY REPORT ********************/
    public function due_payment_summary()
    {
        $query = $this->db->query("SELECT agent_code, Name, COUNT(inv_no) AS count, SUM(due) AS tot_due FROM due_payment WHERE due > 0 GROUP BY agent_code ORDER BY SUM(due) DESC");
        return $query->result();
    }

    /******************** DETAILED DUE PAYMENT REPORT ********************/
    public function detailed_due_payments()
    {
        $query = $this->db->query("SELECT con_list.reg_no, Usert.agent_code, Employee.Name, bank_deposits.inv_no, con_list.con_no, bank_deposits.is_usdt, SUM(bank_deposits.cr) AS deposits, DATE(bank_deposits.ac_rej_date) AS dt,
        IF(is_usdt = '1', 17500 - SUM(bank_deposits.cr), 15000 - SUM(bank_deposits.cr)) AS due 
        FROM Usert Inner Join Employee ON Usert.acc_no = Employee.Acc_No Inner Join con_list ON Employee.Acc_No = con_list.emp_accNo Inner Join bank_deposits ON con_list.reg_no = bank_deposits.reg_no WHERE Usert.Val =  '13' AND bank_deposits.flag = '1'
        GROUP BY bank_deposits.reg_no
        ORDER BY bank_deposits.ac_rej_date ASC");
        return $query->result();
    }

    /******************** REJECT LIST ********************/
    public function reject_list($date1 = NULL, $date2 = NULL)
    {
        $this->db->select('con_list.name, con_list.reg_no, con_list.whatsapp, con_list.con_no, workshops.w_name, con_list.spl_cus, con_list.workshop, bank_deposits.cr AS deposits, bank_deposits.is_usdt, bank_deposits.flag AS pay_flag, con_list.reminder_date, con_list.tic_id, con_list.id, con_list.emp_accNo, con_list.spl_cus, con_list.date1, con_list.job, con_list.site_flag, con_list.user_id, con_list.town, bank_deposits.inv_no,  con_list.usdt_val, con_list.usdt_marked, DATE(bank_deposits.ac_rej_date) AS ac_rej_date');
        $this->db->from('con_list');
        $this->db->join('workshops', 'con_list.workshop = workshops.id', 'LEFT OUTER');
        $this->db->join('bank_deposits', 'con_list.reg_no = bank_deposits.reg_no', 'LEFT OUTER');
        
        $this->db->where("DATE(bank_deposits.ac_rej_date) BETWEEN '" . $date1 . "' AND '" . $date2 . "'");
        $this->db->where('bank_deposits.flag', '2');
        $this->db->group_start();
        $this->db->where('con_list.pending_payment', '1');
        $this->db->or_where('con_list.direct_sale', '1');
        $this->db->group_end();

        $this->db->group_by('con_list.name, con_list.reg_no, con_list.whatsapp, con_list.reminder_date, con_list.tic_id, con_list.id, con_list.emp_accNo, con_list.spl_cus, con_list.date1, con_list.job, con_list.site_flag, con_list.user_id');
        $this->db->order_by('bank_deposits.ac_rej_date', 'DESC');

        $query = $this->db->get();

        return $query->result();
    }
}
