<?php 
class Add_course_model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   /******************** LOAD DATA ********************/
   public function load_data(){
      $this->db->select('*');
      $this->db->from('course');
      $result = $this->db->get();

      return $result->result();
   }



   /******************** INSERT ********************/
   public function insert($data) {

      if ($this->db->insert("course", $data)) {
         return "success";
      }else{
         return "error";
      }

   }

   /******************** GET DATASET ********************/
   public function get_dataset($id){
      $this->db->select('*');
      $this->db->from('course');
      $this->db->where('id', $id);
      $query = $this->db->get();

      return $query->result();
   }

   /******************** UPDATE ********************/
   public function update($data, $id){
      $result = $this->db->get_where("course",array('course_name'=>$data['course_name'], 'course_fee'=>$data['course_fee']));

      if($result->num_rows() == 0) {
         $row = $result->row();

         $this->db->set($data);
         $this->db->where("id", $id);
         if ($this->db->update("course", $data)) {
            return "success"; 
         }else{
            return "error";
         }
      }else{
         return "data not exists";
      }
   }

   /******************** DELETE ********************/
   public function delete($data){
      if ($this->db->delete("course", "id = ".$data['id'])) {
         return true; 
      }else{
         return "error";
      }
   }





} 
?> 