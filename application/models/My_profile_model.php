<?php 
class My_profile_model extends CI_Model {
	
   function __construct() { 
      parent::__construct(); 
   } 

   /******************** LOAD DETAILS ********************/
   public function user_details($user) {
      $this->db->select('Employee.Name, Employee.Acc_No, Usert.ID AS username, Employee.address, Employee.email, Employee.prof_pic, Employee.desig, Usert.Val, Usert.cash, Usert.parent_portal');
      $this->db->from(' Employee ');
      $this->db->join('Usert', 'Employee.ID = Usert.emp');
      $this->db->where('Usert.ID', $user);
      $details = $this->db->get()->row();

      return $details;
   }

   /******************** UPDATE ********************/
   public function update($data, $id){
      $this->db->set($data);
      $this->db->where("id", $id);
      if ($this->db->update("users", $data)) {
         return true; 
      }
   }

   /******************** CHECK PASSWORD ********************/
   public function check_pwd($username, $old_pass) {
      if($username && $old_pass) {
         $sql = "SELECT * FROM users WHERE user_name = ?";
         $query = $this->db->query($sql, array($username));

         if($query->num_rows() == 1) {
            $result = $query->row_array();

            $hash_password = password_verify($old_pass, $result['password']);
            if($hash_password === true) {
               return "pwd match";   
            }
            else {
               return false;
            }
         }
         else {
            return false;
         }
      }
   }

   /******************** UPDATE BALANCE ********************/
   public function update_balance($data, $uname){
      $in_date = $data['date'];
      $in_time = $data['time'];
      $acc = $data['user_n'];

      $this->db->select('shift_id')->from('Invoice_No');
      $query = $this->db->get();
      $shft = $query->row_array();

      $shift_id = $shft['shift_id'];
      $data['shift'] = $shift_id;

      $new_shift = $shft['shift_id']+1;
      $this->db->update('Invoice_No', array('shift_id' => $new_shift));

      if($this->db->insert('shift_start', $data)) {  
         $this->db->query("INSERT INTO Shift (Emp_Code, In_Date, in_time, shift_id, login) VALUES('$acc', '$in_date', '$in_time', '$shift_id', '$uname')");

         $this->db->query("UPDATE Usert SET Shift='$shift_id', ont='1' WHERE acc_no='$acc'");

         return "success";
      }else{
         return "error";
      }
   }

   /******************** CHECK LGIN ********************/
   public function check_login($acc_no){
      $this->db->select('*');
      $this->db->from('Usert');
      $this->db->where('acc_no', $acc_no);
      $query = $this->db->get();

      return $query->result();
   }

   /******************** UPDATE PASSOWRD ********************/
   public function updatePassword($data, $acc_no){
      $this->db->set($data);
      $this->db->where("acc_no", $acc_no);
      if ($this->db->update("Usert", $data)) {
         return true; 
      }
   }


}