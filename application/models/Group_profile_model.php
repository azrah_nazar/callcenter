<?php 
   class Group_profile_model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 

      /******************** LOAD DATA ********************/
      public function load_data(){
         $this->db->select('*');
         $this->db->from('Group_Prof');
         $user = $this->db->get();

         return $user->result();
      }

      /******************** INSERT ********************/
      public function insert($data) {
         $result = $this->db->get_where("Group_Prof",array('profile'=>$data['profile']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            if ($this->db->insert("Group_Prof", $data)) {
               return "success";
            }else{
               return "error";
            }
             
         }else{
            return "data exists";
         }
      }

      /******************** GET DATASET ********************/
      public function get_dataset($id){
         $this->db->select('*');
         $this->db->from('Group_Prof');
         $this->db->where('id', $id);
         $query = $this->db->get();

         return $query->result();
      }

      /******************** UPDATE ********************/
      public function update($data, $id){
         $result = $this->db->get_where("Group_Prof",array('profile'=>$data['profile']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            $this->db->set($data);
            $this->db->where("id", $id);
            if ($this->db->update("Group_Prof", $data)) {
               return "success"; 
            }else{
               return "error";
            }
         }else{
            return "data exists";
         }
      }

      /******************** DELETE ********************/
      public function delete($data){
         if ($this->db->delete("Group_Prof", "id = ".$data['id'])) {
            return true; 
         }else{
            return "error";
         }
      }

      
   } 
?> 