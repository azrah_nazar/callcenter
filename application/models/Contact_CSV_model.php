<?php
class Contact_CSV_model extends CI_Model
{

   function __construct()
   {
      parent::__construct();
   }


   public function insert_csv($fp, $user_id, $upload_type, $filename, $upload_typ)
   {
      //$st = 'success';
      $ress =  $this->db->query("SELECT * FROM con_list_temp WHERE csv_name='$filename'");
      $rwss = $ress->num_rows();
      if ($rwss == 0) {
         while (($value = fgetcsv($fp, 1000, ",")) !== FALSE) {
            $st_name   = $value[0];
            $st_adrz  = $value[1];
            $st_tel  = $value[2];
            $st_whtz = $value[3];
            $st_job = $value[4];
            $st_dob = $value[5];

            //echo $st_name.'<br>';

            if (!empty($st_tel)) {
               $st_tel = htmlspecialchars(strip_tags(trim($st_tel)), ENT_QUOTES, 'UTF-8');
               $st_name = htmlspecialchars(strip_tags(trim($st_name)), ENT_QUOTES, 'UTF-8');
               $st_adrz = htmlspecialchars(strip_tags(trim($st_adrz)), ENT_QUOTES, 'UTF-8');
               $st_whtz = htmlspecialchars(strip_tags(trim($st_whtz)), ENT_QUOTES, 'UTF-8');
               $st_job = htmlspecialchars(strip_tags(trim($st_job)), ENT_QUOTES, 'UTF-8');
               $st_dob = htmlspecialchars(strip_tags(trim($st_dob)), ENT_QUOTES, 'UTF-8');
               $upload_type = $upload_typ;

               $first_num = substr($st_tel, 0, 1);
               if ($first_num == '0') {
                  $st_tel = $st_tel;
               } else {
                  if ($first_num == '') {
                     $st_tel = '';
                  } else {
                     $st_tel = '0' . $st_tel;
                  }
               }

               $wa_num = substr($st_whtz, 0, 1);
               if ($wa_num == '0') {
                  $st_whtz = $st_whtz;
               } else {
                  if ($wa_num == '') {
                     $st_whtz = '';
                  } else {
                     $st_whtz = '0' . $st_whtz;
                  }
               }

               $data = array(
                  'con_no' => $st_tel,
                  'name' => $st_name,
                  'address' => $st_adrz,
                  'whatsapp' => $st_whtz,
                  'job' => $st_job,
                  'dob' => $st_dob,
                  'user_id' => $user_id,
                  'date' => date("Y-m-d H:i:s"),
                  'csv_name' => $filename,
                  'upload_type' => $upload_typ,
                  'flag' => 0
               );



               if ($this->db->insert("con_list_temp", $data)) {

                  $this->db->query("UPDATE con_list_temp SET flag = '1' WHERE flag='0'");

                  $this->db->query("UPDATE con_list_temp AS R
                  INNER JOIN con_list AS P 
                        ON R.con_no = P.con_no 
                  SET R.emp_accNo = P.emp_accNo, 
                     R.date = P.date, 
                     R.status = P.flag,
                     R.flag = 2,
                     R.feedback_reject = P.feedback_reject
                  WHERE R.flag='1' AND P.con_no <> ''");

                  $this->db->query("UPDATE con_list_temp AS R
                  INNER JOIN con_list AS P 
                        ON R.con_no = P.whatsapp 
                  SET R.emp_accNo = P.emp_accNo, 
                     R.date = P.date, 
                     R.status = P.flag,
                     R.flag = 2,
                     R.feedback_reject = P.feedback_reject
                  WHERE R.flag='1' AND P.whatsapp <>''");

                  $this->db->query("UPDATE con_list_temp AS R
                  INNER JOIN con_list AS P 
                        ON R.whatsapp = P.whatsapp 
                  SET R.emp_accNo = P.emp_accNo, 
                     R.date = P.date, 
                     R.status = P.flag,
                     R.flag = 2,
                     R.feedback_reject = P.feedback_reject
                  WHERE R.flag='1'  AND P.whatsapp <>''");

                  $this->db->query("UPDATE con_list_temp AS R
                  INNER JOIN con_list AS P 
                        ON R.whatsapp = P.con_no 
                  SET R.emp_accNo = P.emp_accNo, 
                     R.date = P.date, 
                     R.status = P.flag,
                     R.flag = 2,
                     R.feedback_reject = P.feedback_reject
                  WHERE R.flag='1' AND P.con_no<>''");


                  $this->db->query("INSERT INTO con_list (con_no, name, address, whatsapp, job, dob, user_id, date, upload_type)
                     SELECT con_no, name, address, whatsapp, job, dob, user_id, date, upload_type
                     FROM con_list_temp
                     WHERE flag = 1 AND (con_no NOT IN (SELECT con_no FROM con_list) AND con_no NOT IN (SELECT whatsapp FROM con_list)) AND (whatsapp NOT IN (SELECT con_no FROM con_list) AND whatsapp NOT IN (SELECT whatsapp FROM con_list WHERE whatsapp <> ''))");

                  $this->db->query("UPDATE con_list_temp SET flag='2' WHERE flag = 1 AND con_no NOT IN (SELECT con_no FROM con_list)");

                  $this->db->query("UPDATE con_list_temp SET flag='3' WHERE flag = 1 AND con_no IN (SELECT con_no FROM con_list)");

                  // $this->db->query("DELETE FROM con_list_temp WHERE flag='2'");
                  $st = 'success';
               } else {
                  $st = 'error';
               }
            }
         }
      } else {
         $st = 'file exists';
      }
      fclose($fp);
      return $st;
   }

   /******************** ALLOCATE ********************/
   public function allocate()
   {
      $this->db->query("UPDATE con_list_temp AS R
                  INNER JOIN con_list AS P 
                        ON R.con_no = P.con_no 
                  SET R.emp_accNo = P.emp_accNo, 
                     R.date = P.date, 
                     R.status = P.flag,
                     R.flag = 2,
                     R.feedback_reject = P.feedback_reject
                  WHERE R.flag='1' AND P.con_no <> ''");

      $this->db->query("UPDATE con_list_temp AS R
                  INNER JOIN con_list AS P 
                        ON R.con_no = P.whatsapp 
                  SET R.emp_accNo = P.emp_accNo, 
                     R.date = P.date, 
                     R.status = P.flag,
                     R.flag = 2,
                     R.feedback_reject = P.feedback_reject
                  WHERE R.flag='1' AND P.whatsapp <>''");

      $this->db->query("UPDATE con_list_temp AS R
                  INNER JOIN con_list AS P 
                        ON R.whatsapp = P.whatsapp 
                  SET R.emp_accNo = P.emp_accNo, 
                     R.date = P.date, 
                     R.status = P.flag,
                     R.flag = 2,
                     R.feedback_reject = P.feedback_reject
                  WHERE R.flag='1'  AND P.whatsapp <>''");

      $this->db->query("UPDATE con_list_temp AS R
                  INNER JOIN con_list AS P 
                        ON R.whatsapp = P.con_no 
                  SET R.emp_accNo = P.emp_accNo, 
                     R.date = P.date, 
                     R.status = P.flag,
                     R.flag = 2,
                     R.feedback_reject = P.feedback_reject
                  WHERE R.flag='1' AND P.con_no<>''");


      $this->db->query("INSERT INTO con_list (con_no, name, address, whatsapp, job, dob, user_id, date)
                     SELECT con_no, name, address, whatsapp, job, dob, user_id, date
                     FROM con_list_temp
                     WHERE flag = 1 AND con_no NOT IN (SELECT con_no FROM con_list)");

      $this->db->query("UPDATE con_list_temp SET flag='2' WHERE flag = 1 AND con_no NOT IN (SELECT con_no FROM con_list)");

      $this->db->query("UPDATE con_list_temp SET flag='3' WHERE flag = 1 AND con_no IN (SELECT con_no FROM con_list)");

      // $this->db->query("DELETE FROM con_list_temp WHERE flag='2'");
      $st = 'success';
      return $st;
   }

   /******************** con_no EXISTS ********************/
   public function con_no_check($con_no)
   {
      $query = $this->db->query("SELECT * FROM con_list WHERE con_no = '$con_no'");

      $rowcount = $query->num_rows();
      if ($rowcount == 0) {
         return "success";
      } else {
         return "con_no exists";
      }
   }

   public function search_con_no()
   {
      $keyword = strip_tags(trim($_REQUEST['term']));

      $sugg_json = array();
      $json_row = array();
      $key = preg_replace('/\s+/', ' ', $keyword);

      $sql = "SELECT con_no FROM con_list WHERE con_no LIKE '%$key%' GROUP BY con_no ORDER BY con_no";
      $query = $this->db->query($sql);
      $telArr = $query->result_array();

      foreach ($telArr as $recResult) {
         $json_row["value"] = $recResult['con_no'];
         $json_row["label"] = $recResult['con_no'];
         array_push($sugg_json, $json_row);
      }

      $jsonOutput = json_encode($sugg_json, JSON_UNESCAPED_SLASHES);
      print $jsonOutput;
   }


   /******************** GET DATASET ********************/
   public function get_dataset($con_no)
   {
      $this->db->select('*');
      $this->db->from('con_list');
      $this->db->where('con_no', $con_no);
      $query = $this->db->get();

      return $query->result_array();
   }

   /******************** INSERT ********************/
   public function add($data)
   {
      $con_no = $data['con_no'];
      $whatsapp = $data['whatsapp'];

      $first_num = substr($whatsapp, 0, 1);
      if ($first_num == '0') {
         $whatsapp = $whatsapp;
      } else {
         if ($first_num == '') {
            $whatsapp = '';
         } else {
            $whatsapp = '0' . $whatsapp;
         }
      }

      $data['whatsapp'] = $whatsapp;

      $aa = "SELECT * FROM con_list WHERE (con_list.con_no = '$con_no'  OR con_list.con_no = '0$con_no'  OR con_list.whatsapp = '$con_no' OR con_list.whatsapp = '0$con_no')";
      if (!empty($whatsapp)) {
         $aa .= " AND (con_list.con_no = '$whatsapp' OR con_list.con_no = '0$whatsapp' OR con_list.whatsapp = '$whatsapp' OR con_list.whatsapp = '0$whatsapp' OR con_list.whatsapp = '')";
      }
      $aa .= " AND con_list.flag = '5'";
      $resulta = $this->db->query($aa);
      $rowcounta = $resulta->num_rows();

      if ($rowcounta == 0) {

         $ss = "SELECT * FROM con_list WHERE (con_list.con_no = '$con_no'  OR con_list.con_no = '0$con_no'  OR con_list.whatsapp = '$con_no' OR con_list.whatsapp = '0$con_no')";
         if (!empty($whatsapp)) {
            $ss .= " OR (con_list.con_no = '$whatsapp' OR con_list.con_no = '0$whatsapp' OR con_list.whatsapp = '$whatsapp' OR con_list.whatsapp = '0$whatsapp' )";
         }

         $result = $this->db->query($ss);
         $rowcount = $result->num_rows();

         if ($rowcount == 0) {
            if ($this->db->insert("con_list", $data)) {
               return "success";
            } else {
               return "error";
            }
         } else {
            $xx = "SELECT Employee.Name, Employee.surname FROM Employee JOIN con_list ON Employee.Acc_No=con_list.emp_accNo  WHERE (con_list.con_no = '$con_no'  OR con_list.con_no = '0$con_no'  OR con_list.whatsapp = '$con_no' OR con_list.whatsapp = '0$con_no')";
            if (!empty($whatsapp)) {
               $xx .= " OR (con_list.con_no = '$whatsapp' OR con_list.con_no = '0$whatsapp' OR con_list.whatsapp = '$whatsapp' OR con_list.whatsapp = '0$whatsapp')";
            }
            $query = $this->db->query($xx);

            //$query = $this->db->query("SELECT Employee.Name, Employee.surname FROM Employee JOIN con_list ON Employee.Acc_No=con_list.emp_accNo WHERE (con_list.con_no = '$con_no' OR ((con_list.whatsapp = '$con_no' OR con_list.whatsapp = '0$con_no') AND con_list.whatsapp != '')) OR (con_list.con_no = '$whatsapp' OR con_list.con_no = '0$whatsapp' OR ((con_list.whatsapp = '$whatsapp' OR con_list.whatsapp = '0$whatsapp') AND con_list.whatsapp != '' ))");

            $row = $query->row();
            $rowcnt = $query->num_rows();
            if ($rowcnt == 0) {
               $op_name = " unallocated operator";
            } else {
               $op_name = $row->Name . ' ' . $row->surname;
            }

            return $op_name;
         }
      } else {
         return "rejected";
      }
   }

   public function load_data()
   {
      $sql = "SELECT * FROM con_list WHERE (flag = 0 OR flag = 1) AND upload_id = (select max(upload_id) from con_list)";
      $query = $this->db->query($sql);

      return $query->result();
   }

   public function load_data2($date1 = NULL, $date2 = NULL, $agent)
   {
      $txt = '';

      if ($agent != '') {
         $txt =  " AND con_list.emp_accNo = '$agent'";
      }

      $query = $this->db->query("SELECT con_list.name, con_list.con_no, con_list.address, con_list.whatsapp, con_list.job, con_list.dob, Employee.Name AS agent_name, con_list.date, con_list.user_id, con_list.upload_id, Usert.ID AS username
      FROM
      con_list
      Inner Join Employee ON con_list.emp_accNo = Employee.Acc_No
      Left Outer Join Usert ON con_list.User_id = Usert.uid
      WHERE DATE(con_list.date) BETWEEN '$date1'  AND '$date2' AND self_reg != '1' " . $txt . "ORDER BY date DESC");

      return $query->result();
   }

   public function load_operator()
   {
      $sql = "SELECT Usert.is_present, Employee.Name, Usert.ID FROM Usert INNER JOIN Employee ON Usert.acc_no = Employee.Acc_No WHERE Val = '13' AND stt!='2'";
      $query = $this->db->query($sql);

      return $query->result();
   }

   public function map_operator()
   {
      $sql = "SELECT Usert.is_present, Employee.Name, Employee.gender, Employee.Acc_No, Usert.ID FROM Usert INNER JOIN Employee ON Usert.acc_no = Employee.Acc_No WHERE Val = '13' AND stt!='2' AND gender='Female'";
      $query = $this->db->query($sql);

      return $query->result();
   }

   public function mapped_operator($acc_no)
   {
      $sql = "SELECT Employee.mapped_to, emp2.Name FROM Usert INNER JOIN Employee ON Usert.acc_no = Employee.Acc_No LEFT OUTER JOIN Employee AS emp2 ON Employee.mapped_to = emp2.Acc_No WHERE Val = '13' AND stt!='2' AND Employee.gender='Male' AND Employee.Acc_No='$acc_no'";

      $query = $this->db->query($sql);

      return $query->row_array();
   }

   /******************** MAP OPERATOR ********************/
   public function mapping_operator($data, $acc_no)
   {
      $this->db->set($data);
      $this->db->where("Acc_No", $acc_no);
      if ($this->db->update("Employee", $data)) {
         return "success";
      } else {
         return "error";
      }
   }

   /******************** DEACTIVATE OPERATOR ********************/
   public function pause_operator($data, $id)
   {
      $this->db->set($data);
      $this->db->where("ID", $id);
      if ($this->db->update("Usert", $data)) {
         return "success";
      } else {
         return "error";
      }
   }

   /******************** GOOGLE CONTACTS ********************/
   public function google_contacts($dater, $acc_no)
   {
      $this->db->select('*');
      $this->db->from('con_list');
      $this->db->where('date1', $dater);
      $this->db->where('emp_accNo', $acc_no);
      $this->db->where('reg_no !=', '');
      $query = $this->db->get();

      return $query->result_array();
   }

   /******************** GOOGLE CONTACTS COUNT ********************/
   public function google_contacts_count($dater, $acc_no)
   {
      $this->db->select('*');
      $this->db->from('con_list');
      $this->db->where('date1', $dater);
      $this->db->where('emp_accNo', $acc_no);
      $this->db->where('reg_no !=', '');
      $query = $this->db->get();
      $rowcount = $query->num_rows();

      return $rowcount;
   }

   /******************** INSERTED CONTACTS ********************/
   public function inserted_contacts($date1 = NULL, $date2 = NULL)
   {
      $query = $this->db->query("SELECT con_list_temp.name, con_list_temp.con_no, con_list_temp.address, con_list_temp.whatsapp, con_list_temp.job, con_list_temp.dob, Employee.Name AS agent_name, con_list_temp.date, con_list_temp.user_id, con_list_temp.csv_name
      FROM
      con_list_temp
      LEFT OUTER JOIN Employee ON con_list_temp.emp_accNo = Employee.Acc_No
      WHERE DATE(con_list_temp.upload_date) BETWEEN '$date1'  AND '$date2' ORDER BY csv_name");

      return $query->result();
   }

   /******************** REAPPLIED CONTACTS ********************/
   public function reapplied_contacts($date1 = NULL, $date2 = NULL, $agent)
   {
      if ($agent == '') {
         $ag = '';
         $reapply_rej = '';
      } else {
         $ag = " AND con_list.emp_accNo ='$agent' ";
         $reapply_rej = " AND con_list_temp.reapplied_reject <>'1' ";
      }
      $query = $this->db->query("SELECT con_list_temp.name, con_list_temp.con_no, con_list_temp.whatsapp, Employee.Name AS agent_name, con_list_temp.date, con_list_temp.user_id, con_list_temp.csv_name, con_list.feedback_reject, con_list.flag AS status, con_list.id AS cid, con_list_temp.id AS cid_temp, con_list.tic_id, con_list.spl_cus, con_list_temp.emp_accNo, Employee.Acc_No, con_list.direct_sale, con_list.pending_payment
      FROM
      con_list_temp
      Inner Join con_list ON con_list_temp.con_no = con_list.con_no
      Inner Join Employee ON con_list.emp_accNo = Employee.Acc_No
      WHERE DATE(con_list_temp.upload_date) BETWEEN '$date1'  AND '$date2' AND con_list_temp.flag='2' AND con_list.flag<>'5'  " . $reapply_rej . $ag . " GROUP BY  con_list_temp.con_no,  con_list_temp.whatsapp, con_list_temp.csv_name ORDER BY csv_name ");

      return $query->result();
   }

   /************************** REAPPLIED REJECT  *************************/
   public function reapplied_reject($cid, $con_no)
   {
      $now = date('Y-m-d H:i:s');
      if ($this->db->query("UPDATE con_list_temp SET reapplied_reject='1', reapplied_reject_date='$now', status='5' WHERE con_no='$con_no'")) {

         $this->db->query("UPDATE con_list SET reapplied_reject='1', reapplied_reject_date='$now', flag='5', direct_sale='0' WHERE con_no='$con_no' OR whatsapp='$con_no'");
         return "success";
      } else {
         return "error";
      }
   }


   /******************** REAPPLIED REJECT CONTACTS LIST ********************/
   public function reapplied_reject_list($date1 = NULL, $date2 = NULL, $agent)
   {
      if ($agent == '') {
         $ag = '';
      } else {
         $ag = " AND con_list.emp_accNo ='$agent'";
      }
      $query = $this->db->query("SELECT con_list_temp.name, con_list_temp.con_no, con_list_temp.whatsapp, Employee.Name AS agent_name, con_list_temp.user_id, con_list_temp.feedback_reject, con_list_temp.flag, con_list.flag AS status, con_list.id AS cid, con_list_temp.id AS cid_temp, con_list.tic_id, con_list.spl_cus, con_list.emp_accNo, Employee.Acc_No, con_list.reapplied_reject_date
      FROM
      con_list_temp
      Inner Join con_list ON con_list_temp.con_no = con_list.con_no
      Inner Join Employee ON con_list.emp_accNo = Employee.Acc_No 
      WHERE DATE(con_list.reapplied_reject_date) BETWEEN '$date1'  AND '$date2' AND con_list.reapplied_reject='1' " . $ag . " GROUP BY con_list.con_no, con_list.whatsapp ORDER BY csv_name");

      return $query->result();
   }


   /******************** GET CSV ********************/
   public function get_csv($date1, $date2)
   {
      $query = $this->db->query("SELECT DISTINCT(csv_name) AS csv_name FROM con_list_temp WHERE DATE(upload_date) BETWEEN '$date1' AND '$date2'");

      return $query->result();
   }

   /******************** DELETE CSV ********************/
   public function delete_csv($data)
   {
      $csv = $data['csv_name'];
      if ($this->db->query("DELETE con_list FROM con_list LEFT JOIN con_list_temp ON con_list.con_no = con_list_temp.con_no OR con_list.whatsapp = con_list_temp.whatsapp
      where con_list_temp.csv_name='$csv'")) {
         if ($this->db->delete("con_list_temp", "csv_name = '" . $csv . "'")) {
            return "success";
         } else {
            return "error";
         }
      } else {
         return "error";
      }
   }

   /******************** AGENT TRANSFER ********************/
   public function transfer_operator($acc_no, $mapped_operator, $backdate_days)
   {
      $monthsBack = date('y-m-d', strtotime('-' . $backdate_days . ' days'));
      $today =  date('Y-m-d');

      $query = $this->db->query("SELECT * FROM con_list WHERE emp_accNo='$mapped_operator' AND DATE(reject_date) <='$monthsBack' AND con_list.feedback_reject = '1' AND con_list.pending_payment <> '1' AND  ws_reject_retrieve <> '1' ");

      foreach ($query->result_array() as $row) {
         $con_no = $row['con_no'];

         $this->db->query("UPDATE con_list SET transferred_to='$acc_no', transferred_date='$today', ws_reject_retrieve='1' WHERE con_no='$con_no'");
      }
      return "success";
   }

   /******************** AGENT TRANSFER ********************/
   public function transfer_operator_ws($mapped_operator, $backdate_days, $acc_no)
   {
      $monthsBack = date('y-m-d', strtotime('-' . $backdate_days . ' days'));
      $today =  date('Y-m-d');

      $query = $this->db->query("SELECT * FROM con_list WHERE emp_accNo='$mapped_operator' AND DATE(date) <='$monthsBack' AND con_list.feedback_reject <> '1' AND con_list.pending_payment <> '1' AND ws_sms_retrieve <> '1'");

      foreach ($query->result_array() as $row) {
         $con_no = $row['con_no'];

         $this->db->query("UPDATE con_list SET transferred_to='$acc_no', transferred_date='$today', ws_sms_retrieve = '1' WHERE con_no='$con_no'");
      }
      return "success";
   }

   /******************** REAPPLIED REJECT RETRIEVE ********************/
   public function reapplied_reject_retrieve($acc_no, $mapped_operator, $backdate_days)
   {
      $monthsBack = date('Y-m-d', strtotime('-' . $backdate_days . ' days'));
      $today =  date('Y-m-d');

      $query = $this->db->query("SELECT * FROM con_list_temp WHERE emp_accNo='$mapped_operator' AND DATE(reapplied_reject_date) <='$today' AND reapplied_reject = '1' ");

      foreach ($query->result_array() as $row) {
         $con_no = $row['con_no'];

         $this->db->query("UPDATE con_list_temp SET retrieved_by='$acc_no', retrieved_date='$today', reapplied_rejected_retrieve = '1' WHERE con_no='$con_no'");

         $this->db->query("UPDATE con_list SET retrieved_by='$acc_no', retrieved_date='$today', reapplied_rejected_retrieve = '1' WHERE con_no='$con_no' ");
      }
      return "success";
   }
}
