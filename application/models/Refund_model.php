<?php
class Refund_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function load_data($search_text)
    {
        $this->db->select('con_list.reg_no, con_list.con_no, SUM(bank_deposits.cr) as deposits, con_list.whatsapp, bank_deposits.inv_no, con_list.name');
        $this->db->from('con_list');
        $this->db->join('bank_deposits', 'con_list.reg_no = bank_deposits.reg_no');

        $this->db->where('bank_deposits.flag', '1');

        $this->db->group_start();
        $this->db->or_where('con_list.reg_no', $search_text);
        $this->db->or_where('con_list.con_no', $search_text);
        $this->db->or_where('con_list.whatsapp', $search_text);
        $this->db->or_where('bank_deposits.inv_no', $search_text);
        $this->db->group_end();

        $this->db->group_by('con_list.reg_no, con_list.con_no, con_list.whatsapp, bank_deposits.inv_no, con_list.name');

        $query = $this->db->get();

        return $query->result();
    }

    /******************** GET DATASET ********************/
    public function get_dataset($reg_no){
        $this->db->select('SUM(ref_amt) AS tot_refunded');
        $this->db->from('refunds');
        $this->db->where('reg_no', $reg_no);
        $query = $this->db->get();

        return $query->result();
     }

    /******************** INSERT ********************/
    public function insert($data)
    {

        if ($this->db->insert("refunds", $data)) {
            return "success";
        } else {
            return "error";
        }
    }
}
