<?php 
class Common_model extends CI_Model {
	
   function __construct() { 
      parent::__construct(); 
   } 


   /******************** LOAD CATEGORIES ********************/
   public function categories($user_id, $val){
      $this->db->select('cat, Cat_id, icon');
      $this->db->from('User_priv_qq');
      $this->db->where('val', $val);
      $this->db->where('user_id', $user_id);
      $this->db->group_by('cat, Cat_id, icon');
      $this->db->order_by('cat');
      $query = $this->db->get();

      return $query->result();
   }

   /******************** LOAD MENU LIST ********************/
   public function menu_list($user_id, $Val){
      $this->db->select('Cat_id, Form, Caption, menu_id');
      $this->db->from('User_priv_qq');
      $this->db->where('val', $Val);
      $this->db->where('user_id', $user_id);
      $this->db->where('p_id', '8');
      $this->db->order_by('subcat_id', 'ASC');
      $query = $this->db->get();

      return $query->result();
      
   }

   /******************** LOAD PRIVILEDGED CATEGORIES ********************/
   public function priv_categories($menu){
      $this->db->select('Form, Caption');
      $this->db->from('User_priv_qq');
      $this->db->where('menu_id', $menu);
      $query = $this->db->get();

      return $query->result();
      
   }

   /******************** USER ACCESS  ********************/
   public function user_access($menu, $acc_no) {
      if($menu && $acc_no) {
         $sql = "SELECT * FROM User_priv_qq WHERE acc_no = ? AND menu_id=? AND stt=1";
         $query = $this->db->query($sql, array($acc_no, $menu));

         if($query->num_rows() == 1) {
            $result = $query->row_array();
            
            return $result;   
         }
         else {
            return false;
         }
      }
   }
   /******************** USER LOG ********************/
   public function insert_log($activity, $dt, $time, $acc_no) {

      $sql = "INSERT INTO A_Log (Usr, Panel, Date, Tym, p_id) VALUES('$acc_no', '$activity', '$dt', '$time', 8)";
      $query = $this->db->query($sql);
      
   }





}