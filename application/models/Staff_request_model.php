<?php 
   class Staff_request_model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 

      /******************** LOAD DATA ********************/
      public function load_data(){
         $query = $this->db->query("SELECT location.location_name, Staff_request.ID AS ID, Staff_request.Location, Staff_request.Emp_name, Staff_request.Reqed_by, Staff_request.Req_date, Staff_request.Flag, location.id FROM location INNER JOIN Staff_request ON location.id = Staff_request.Location WHERE Staff_request.Flag=0");

         return $query->result();
      }

       /********************  DATA INSERT ********************/
         public function Staff_request_model($data,$approved,$acc_no,$date) {
         $result = $this->db->get_where("Staff_request",array('Location'=>$data['Location'],'Emp_name'=>$data['Emp_name'],'Reqed_by'=>$data['Reqed_by'],'Req_date'=>$data['Req_date']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

              // $this->db->select('*')->from('Staff_request');
              // $query = $this->db->get();
              // $inv_q = $query->row_array(); 

            if ($this->db->insert("Staff_request", $data)) {
              // $xx = "UPDATE Staff_request SET Flag=1,Approved_by='$acc_no',Approved_date='$date' WHERE ID='$approved'";
              // $x = $this->db->query($xx);
               return "success";
            }else{
               return "error";
            }
             
         }else{
            return "data exists";
         }
      }

   /******************** GET EMPLOYEE NAME ********************/
      public function get_employee($location) {   
         // $query = $this->db->query("SELECT Employee.location, Staff_request.Location AS Expr2, Staff_request.Emp_name, Employee.Name, Employee.surname, Employee.empno FROM Employee INNER JOIN Staff_request ON Employee.location = Staff_request.Location WHERE (Staff_request.Location = '$location')");
         $query=$this->db->query("SELECT ID, empno, Acc_No, Name,  surname, location FROM Employee WHERE(location = '$location')");
      return $query->result_array();
    }

        /******************** DELETE ********************/
      public function delete_category($data){
         if ($this->db->delete("Staff_request", "ID = ".$data['id'])) {
            return true; 
         }else{
            return "error";
         }
      }

    /******************** GET DATA ********************/
          public function get_data($id){
         $this->db->select('Staff_request.Location, Staff_request.Emp_name');
         $this->db->from('Staff_request');
         $this->db->where('ID', $id);
         $query = $this->db->get();

         return $query->result();
      }

         /******************** UPDATE ********************/
      public function update_form($data, $id){

         $this->db->set($data);
         $this->db->where("ID", $id);
         if ($this->db->update("Staff_request")) {
            return "success"; 
         }else{
            return "error";
         }
         
      }

/////////////////////////////////LOG////////////////////////////////////////////////////////////////////////////

    /******************** LOAD ALL REQUESTED DATA ********************/
     public function load_all_data(){
         $query = $this->db->query("SELECT location.location_name, Staff_request.ID, Staff_request.Location, Staff_request.Emp_name, Staff_request.Reqed_by, Staff_request.Req_date, Staff_request.Flag, location.id AS Expr1, Staff_request.Approved_by, Staff_request.Approved_date FROM location INNER JOIN Staff_request ON location.id = Staff_request.Location");

         return $query->result();
      }

      /******************** TERMINATE STUDENT LIST ********************/
      // public function teminate_student_list(){
      //    $query = $this->db->query("SELECT  clearance_request.index_no, Student_Reg.Name_Ini, clearance_request.requested_date, clearance_request.id AS req_id, clearance_request.ref_no, Student_Reg.is_active, clearance_category.category FROM clearance_request INNER JOIN Student_Reg ON clearance_request.index_no = Student_Reg.Admission INNER JOIN clearance_category ON clearance_request.cl_cat = clearance_category.id
      //    WHERE (Student_Reg.st_status <> 1) AND (Student_Reg.st_status <> 9) AND (clearance_request.flag <> 7)");

      //    return $query->result();
      // }

      /******************** TERMINATION LOG ********************/
      // public function termination_log(){
      //    $query = $this->db->query("SELECT clearance_request.index_no, Student_Reg.Name_Ini, clearance_request.ref_no, Student_Reg.is_active, Employee.Name AS req_by, Employee.surname AS req_by_surname, clearance_request.requested_date, clearance_request.cancel_reason, clearance_request.req_cancelled_date, Employee_1.Name, Employee_1.surname, Student_Reg.ac_year, clearance_request.remark, clearance_category.category, clearance_cat_map.type FROM clearance_request INNER JOIN Student_Reg ON clearance_request.index_no = Student_Reg.Admission INNER JOIN clearance_category ON clearance_request.cl_cat = clearance_category.id INNER JOIN clearance_cat_map ON clearance_category.id = clearance_cat_map.category LEFT OUTER JOIN Employee ON clearance_request.requested_by = Employee.Acc_No LEFT OUTER JOIN Employee AS Employee_1 ON clearance_request.cancelled_by = Employee_1.Acc_No GROUP BY clearance_request.index_no, Student_Reg.Name_Ini, clearance_request.ref_no, Student_Reg.is_active, clearance_request.cancel_reason, clearance_request.req_cancelled_date, Employee_1.Name, Employee_1.surname, Student_Reg.ac_year, Employee.Name, Employee.surname, clearance_request.requested_date, clearance_request.remark, clearance_category.category, clearance_cat_map.type");

      //    return $query->result();
      // }

      // public function TerminationLogSearch($searchterm) {

      //    $query = $this->db->query("SELECT clearance_request.index_no, Student_Reg.Name_Ini, clearance_request.ref_no, Student_Reg.is_active, clearance_request.cancel_reason, clearance_request.req_cancelled_date, Employee.Name, Employee.surname, Student_Reg.ac_year FROM clearance_request INNER JOIN Student_Reg ON clearance_request.index_no = Student_Reg.Admission INNER JOIN  Employee ON clearance_request.cancelled_by = Employee.Acc_No
      //    WHERE(clearance_request.flag = 7)  AND (Student_Reg.Name_Ini LIKE '%$searchterm%' ESCAPE '!') OR
      //    (clearance_request.flag = 7) AND (Student_Reg.Admission LIKE '%$searchterm%' ESCAPE '!') OR
      //    (clearance_request.flag = 7) AND (clearance_request.ref_no LIKE '%$searchterm%' ESCAPE '!') OR
      //    (clearance_request.flag = 7) AND (Student_Reg.Name LIKE '%$searchterm%' ESCAPE '!') 
      //    GROUP BY clearance_request.index_no, Student_Reg.Name_Ini, clearance_request.ref_no, Student_Reg.is_active, clearance_request.cancel_reason,  clearance_request.req_cancelled_date, Employee.Name, Employee.surname, Student_Reg.ac_year
      //    ORDER BY clearance_request.index_no");
         
      //    return $query->result();
      // }

      /******************** GET DATASET ********************/
      // public function get_clearance_category($type, $grade_id){
      //    $this->db->select('clearance_cat_map.id, clearance_cat_map.type, clearance_category.category, clearance_cat_map.id AS cat_id, clearance_category.id AS cid');
      //    $this->db->from('clearance_cat_map');
      //    $this->db->join('clearance_category', 'clearance_cat_map.category = clearance_category.id');
      //    if($grade_id != ''){
      //       $this->db->join('Grade', 'clearance_cat_map.location = Grade.location');
      //       $this->db->where('Grade', $grade_id);
      //    }
      //    $this->db->where('type', $type);
      //    $query = $this->db->get();

      //    return $query->result();
      // }

      /******************** GET DATASET ********************/
      // public function get_clearance_cat($data, $type, $grade){
      //    $type = $type;
      //    $indx = $data['index_no'];
      //    $txt = "";

      //    $q1 = $this->db->query("SELECT * FROM Grade WHERE Grade = '$grade'");
      //    $r1= $q1->row_array();

      //    $location = $r1['location'];
      //    if($type == "Past Student"){
      //       $txt = " AND clearance_cat_map.location= '$location'";
      //    }else{
      //       $txt = "";
      //    }

      //    $query = $this->db->query("SELECT clearance_cat_map.id, clearance_cat_map.type, clearance_category.category FROM clearance_cat_map INNER JOIN clearance_category ON clearance_cat_map.category = clearance_category.id WHERE (clearance_cat_map.type = '$type'" .$txt.")");

      //    foreach ($query->result_array() as $row)
      //    {
      //       $data['cl_cat'] = $row['id'];
      //       $cl_cat = $data['cl_cat'];

      //       $result = $this->db->query("SELECT * FROM clearance_request WHERE index_no='$indx' AND cl_cat='$cl_cat' AND flag!=7");
      //       $rowcount = $result->num_rows();

      //       $this->db->select('LS, STC, TMP, STS, INCT')->from('Invoice_No');
      //       $query = $this->db->get();
      //       $row= $query->row_array();

      //       if($type == "Past Student"){
      //          $prefix = "LS"; 
      //          $ref = $row['LS'];
      //          $new_ref = $row['LS']+1;
      //       }else if($type == "Studentship Cancellation"){
      //          $prefix = "STC";
      //          $ref = $row['STC'];
      //          $new_ref = $row['STC']+1;
      //       }else if($type == "Temporary Leaving"){
      //          $prefix = "TMP";
      //          $ref = $row['TMP'];
      //          $new_ref = $row['TMP']+1;
      //       }else if($type == "Suspend"){
      //          $prefix = "STS";
      //          $ref = $row['STS'];
      //          $new_ref = $row['STS']+1;
      //       }else if($type == "Inactive Pool"){
      //          $prefix = "INCT";
      //          $ref = $row['INCT'];
      //          $new_ref = $row['INCT']+1;
      //       }
            

      //       $qry = $this->db->query("SELECT * FROM clearance_request WHERE index_no='$indx' AND flag!=7 AND ref_no LIKE '%LS%'");
      //       $rowcount2 = $qry->num_rows();
      //       $row2= $qry->row_array();

      //       $rf = $row2['ref_no'];

      //       if ($rowcount2 == 0) {
      //          $ref_id = $prefix.'/'.date('Y').'/'.$ref;
      //          $data['ref_no'] = $ref_id;
      //          $this->db->update('Invoice_No', array($prefix => $new_ref));
      //       }else{
      //          $data['ref_no'] = $rf;
      //       }

      //       if ($rowcount == 0) {
      //          $this->db->query("UPDATE clearance_request SET flag='7' where index_no='$indx' AND ref_no NOT LIKE'%LS%'");

      //          if ($this->db->insert("clearance_request", $data)) {
      //             $this->db->query("UPDATE Student_Reg SET is_active='2', st_status='9' where Admission='$indx'");
      //             $txt = "success";
      //          }else{
      //             $txt = "error";
      //          }
                
      //       }else{
      //             $txt =  "data exists";
      //       }
         
      //    }
      // return $txt;
      // }


      /******************** TERMINATE STUDENT ********************/
      // public function insert($data) {
      //    $indx = $data['index_no'];
      //    $cl_cat = $data['cl_cat'];
      //    $result =$this->db->query("SELECT * FROM clearance_request WHERE index_no='$indx' AND cl_cat='$cl_cat' AND flag!=7");
      //    $rowcount = $result->num_rows();

      //    $cl_cat = $data['cl_cat'];

      //    $this->db->select('LS, STC, TMP, STS, INCT')->from('Invoice_No');
      //    $query = $this->db->get();
      //    $row= $query->row_array();


      //    $qq = $this->db->query("SELECT * FROM clearance_cat_map WHERE category='$cl_cat'");
      //    $rw = $qq->row_array();

      //    $type = $rw['type'];

      //    if($type == "Past Student"){
      //       $prefix = "LS"; 
      //       $ref = $row['LS'];
      //       $new_ref = $row['LS']+1;
      //    }else if($type == "Studentship Cancellation"){
      //       $prefix = "STC";
      //       $ref = $row['STC'];
      //       $new_ref = $row['STC']+1;
      //    }else if($type == "Temporary Leaving"){
      //       $prefix = "TMP";
      //       $ref = $row['TMP'];
      //       $new_ref = $row['TMP']+1;
      //    }else if($type == "Suspend"){
      //       $prefix = "STS";
      //       $ref = $row['STS'];
      //       $new_ref = $row['STS']+1;
      //    }else if($type == "Inactive Pool"){
      //       $prefix = "INCT";
      //       $ref = $row['INCT'];
      //       $new_ref = $row['INCT']+1;
      //    }else{
      //       $prefix  = '';
      //       $ref = '';
      //       $new_ref = '';
      //    }

      //    $qry = $this->db->query("SELECT * FROM clearance_request WHERE index_no='$indx' AND cl_cat='$cl_cat' AND flag!=7");
      //    $rowcount2 = $qry->num_rows();
      //    $row2= $qry->row_array();

      //    $rf = $row2['ref_no'];

      //    if ($rowcount2 == 0) {
      //       $ref_id = $prefix.'/'.date('Y').'/'.$ref;
      //       $data['ref_no'] = $ref_id;
      //       $this->db->update('Invoice_No', array($prefix => $new_ref));
      //    }else{
      //       $data['ref_no'] = $rf;
      //    }

      //    if ($rowcount == 0) {

      //       if ($this->db->insert("clearance_request", $data)) {
      //          return "success";
      //       }else{
      //          return "error";
      //       }
             
      //    }else{
      //       $qqq = $this->db->query("SELECT * FROM clearance_request WHERE index_no='$indx' AND cl_cat='$cl_cat'");
      //       $rww = $qqq->row_array();

      //       $flag = $rww['flag'];

      //       if($flag == 0){
      //          return "data exists";
      //       }else{
      //          return "cleared";
      //       }
      //    }
      // }



      /******************** RE-REQUEST ********************/
      // public function rerequest($data) {
      //    $indx = $data['index_no'];
      //    $result = $this->db->get_where("clearance_request",array('index_no'=>$indx, 'cl_cat'=>$data['cl_cat']));
      //    $rowcount = $result->num_rows();

      //    $cl_cat = $data['cl_cat'];

      //    $this->db->select('LS, STC, TMP, STS, INCT')->from('Invoice_No');
      //    $query = $this->db->get();
      //    $row= $query->row_array();


      //    $qq = $this->db->query("SELECT * FROM clearance_cat_map WHERE category='$cl_cat'");
      //    $rw = $qq->row_array();

      //    $type = $rw['type'];

      //    if($type == "Past Student"){
      //       $prefix = "LS"; 
      //       $ref = $row['LS'];
      //       $new_ref = $row['LS']+1;
      //    }else if($type == "Studentship Cancellation"){
      //       $prefix = "STC";
      //       $ref = $row['STC'];
      //       $new_ref = $row['STC']+1;
      //    }else if($type == "Temporary Leaving"){
      //       $prefix = "TMP";
      //       $ref = $row['TMP'];
      //       $new_ref = $row['TMP']+1;
      //    }else if($type == "Suspend"){
      //       $prefix = "STS";
      //       $ref = $row['STS'];
      //       $new_ref = $row['STS']+1;
      //    }else if($type == "Inactive Pool"){
      //       $prefix = "INCT";
      //       $ref = $row['INCT'];
      //       $new_ref = $row['INCT']+1;
      //    }else{
      //       $prefix  = '';
      //       $ref = '';
      //       $new_ref = '';
      //    }
         
      //    $qry = $this->db->query("SELECT * FROM clearance_request WHERE index_no='$indx'");
      //    $rowcount2 = $qry->num_rows();
      //    $row2= $qry->row_array();

      //    $rf = $row2['ref_no'];

      //    if ($rowcount2 == 0) {
      //       $ref_id = $prefix.'/'.date('Y').'/'.$ref;
      //       $data['ref_no'] = $ref_id;
      //       $this->db->update('Invoice_No', array($prefix => $new_ref));
      //    }else{
      //       $data['ref_no'] = $rf;
      //    }

      //    if ($this->db->insert("clearance_request", $data)) {
      //       return "success";
      //    }else{
      //       return "error";
      //    }
      // }

      /******************** UPDATE ********************/
      // public function update($data, $index_no, $cl_cat){

      //    $this->db->set($data);
      //    $this->db->where("index_no", $index_no);
      //    $this->db->where("cl_cat", $cl_cat);
      //    if ($this->db->update("clearance_request", $data)) {
      //       return "success"; 
      //    }else{
      //       return "error";
      //    }
         
      // }

   

      /******************** DELETE ********************/
      // public function delete($data, $index_no){

      //    $sql = $this->db->query("SELECT * FROM clearance_request WHERE index_no='$index_no' AND flag!=0 AND flag<7");
      //    $rowcount = $sql->num_rows();

      //    if ($rowcount == 0) {
      //       $this->db->query("UPDATE Student_Reg SET is_active='1', st_status='0' WHERE Admission='$index_no'");
            
      //       $this->db->set($data);
      //       $this->db->where("index_no", $index_no);
      //       $this->db->where("flag<", 7);
      //       if($this->db->update("clearance_Request", $data)){
      //          return "success"; 
      //       }else{
      //          return "error";
      //       }
      //    }else{
      //       return "cleared";
      //    }
      // }

      /******************** MY CLEARANCE LIST ********************/
      // public function my_clearance($acc_no){
      //    $query = $this->db->query("SELECT clearance_request.index_no, Student_Reg.Name_Ini, clearance_cat_map.type, clearance_category.category, Employee.Name AS res_person, clearance_request.requested_date,  clearance_request.leaving_date, clearance_request.id AS req_id, clearance_request.ref_no, clearance_cat_map.id AS cat_id,  clearance_request.req_flag FROM clearance_cat_map INNER JOIN clearance_category ON clearance_cat_map.category = clearance_category.id INNER JOIN Employee ON clearance_cat_map.res_person = Employee.Acc_No INNER JOIN
      //    clearance_request INNER JOIN Student_Reg ON clearance_request.index_no = Student_Reg.Admission ON clearance_cat_map.id = clearance_request.cl_cat WHERE (Employee.Acc_No = '$acc_no') AND (clearance_request.flag = 0)");

      //    return $query->result();
      // }

      /******************** CLEARANCE UPDATE ********************/
      // public function clearance_update($data, $id){

      //    $this->db->set($data);
      //    $this->db->where("id", $id);
      //    if ($this->db->update("clearance_request", $data)) {
      //       return "success"; 
      //    }else{
      //       return "error";
      //    }
      // }

      /******************** CLEARANCE STATUS********************/
      // public function clearance_status(){
      //    $sql = "EXEC [dbo].[clearance_status]";
      //    $query = $this->db->query($sql);
      //    return $query->result();
      // }

      /******************** APPLY LEAVING ********************/
      // public function apply_leaving($data, $index_no){
      //    $flag = $data['flag'];

      //    $this->db->set($data);
      //    $this->db->where("index_no", $index_no);
      //    $this->db->where("flag<", 7);
      //    if ($this->db->update("clearance_request", $data)) {

      //       if(($flag == '4') || ($flag == '5')){
      //          $this->db->query("UPDATE Student_Reg SET is_active='2', st_status='1' WHERE Admission='$index_no'");
      //       }
      //       return "success"; 
      //    }else{
      //       return "error";
      //    }
      // }

      /******************** REQUEST ACCESS********************/
      // public function request_access($data, $index_no, $cat_id){
         
      //    $this->db->set($data);
      //    $this->db->where("index_no", $index_no);
      //    if($cat_id != ""){
      //       $this->db->where("cl_cat", $cat_id);
      //    }
      //    if ($this->db->update("clearance_request", $data)) {
      //       return "success"; 
      //    }else{
      //       return "error";
      //    }
      // }

      /******************** LEAVING REQUEST ********************/
      // public function leaving_request(){
      //    $query = $this->db->query("SELECT clearance_request.index_no, Student_Reg.Name_Ini, Grade.Caption,  Student_Reg.Class, clearance_request.leaving_req_date, clearance_request.req_flag, clearance_request.leaving_date
      //    FROM clearance_request INNER JOIN
      //    Student_Reg ON clearance_request.index_no = Student_Reg.Admission INNER JOIN
      //    Grade ON Student_Reg.Grade = Grade.Grade
      //    WHERE(clearance_request.flag = 3)
      //    GROUP BY clearance_request.index_no, Student_Reg.Name_Ini,  Student_Reg.Class, Grade.Caption, clearance_request.leaving_req_date, clearance_request.req_flag, clearance_request.leaving_date");

      //    return $query->result();
      // }

      /******************** LEAVING RECORD ********************/
      // public function leaving_record(){
      //    $query = $this->db->query("SELECT clearance_request.index_no, Student_Reg.Name_Ini, Grade.Caption,  Student_Reg.Class, clearance_request.leaving_req_date, clearance_request.leaving_date, clearance_request.prepared_date, clearance_request.collected_date
      //             FROM clearance_request INNER JOIN
      //             Student_Reg ON clearance_request.index_no = Student_Reg.Admission INNER JOIN
      //             Grade ON Student_Reg.Grade = Grade.Grade
      //             WHERE(clearance_request.flag = 4 OR clearance_request.flag = 5)
      //             GROUP BY clearance_request.index_no, Student_Reg.Name_Ini,  Student_Reg.Class, Grade.Caption, clearance_request.leaving_req_date, clearance_request.leaving_date, clearance_request.prepared_date, clearance_request.collected_date");

      //    return $query->result();
      // }

      /******************** REQUEST CONTROL ********************/
	   // public function request_control(){
    //      $query = $this->db->query("SELECT clearance_request.index_no, Student_Reg.Name_Ini, clearance_cat_map.type, clearance_category.category, Employee.Name AS res_person, clearance_request.requested_date, clearance_request.id AS req_id,  clearance_request.ref_no, clearance_request.cl_cat, clearance_request.req_type, clearance_request.req_flag
    //         FROM clearance_cat_map INNER JOIN
    //         clearance_category ON clearance_cat_map.category = clearance_category.id INNER JOIN
    //         clearance_request INNER JOIN
    //         Student_Reg ON clearance_request.index_no = Student_Reg.Admission ON clearance_cat_map.id = clearance_request.cl_cat INNER JOIN Employee ON clearance_cat_map.res_person = Employee.Acc_No WHERE (clearance_request.req_flag <> 0)");

    //      return $query->result();
    //   }

       /******************** TERMINATION REQUEST  ********************/
	   // public function get_termination_requests(){
    //      $query = $this->db->query("SELECT clearance_request.id AS req_id, clearance_request.ref_no, clearance_request.index_no, Student_Reg.Name_Ini, clearance_cat_map.type, clearance_category.category, clearance_request.requested_date, Student_Reg.ID AS st_id, clearance_request.finance_flag, clearance_request.eff_date, clearance_request.cancel_reason, clearance_request.remark, clearance_category.id AS cat_id, clearance_request.cancellation_date, clearance_request.suspend_date  FROM clearance_request INNER JOIN  Student_Reg ON clearance_request.index_no = Student_Reg.Admission INNER JOIN clearance_category ON clearance_request.cl_cat = clearance_category.id INNER JOIN clearance_cat_map ON clearance_category.id = clearance_cat_map.category WHERE (clearance_request.finance_flag = 1) OR (clearance_request.finance_flag = 2)");

    //      return $query->result();
    //   }
      
      /******************** TERMINATION POOL SEARCH ********************/
      // public function load_termination($status = NULL){
      //    $flag = '';
      //    if($status == 2){
      //       $flag = 8;
      //    }else if($status == 3 ){
      //       $flag = 9;
      //    }else if($status == 4 ){
      //       $flag = 10;
      //    }else if($status == 5 ){
      //       $flag = 11;
      //    }else if($status == 6 ){
      //       $flag = 12;
      //    }

      //    if($status =='1'){

      //       $query = $this->db->query("SELECT Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active,  clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date FROM Student_Reg INNER JOIN clearance_request ON Student_Reg.Admission = clearance_request.index_no INNER JOIN Grade ON Student_Reg.Grade = Grade.Grade WHERE (clearance_request.flag = 4 OR clearance_request.flag = 5) AND (clearance_request.flag <> 7) GROUP BY Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active, clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date ORDER BY clearance_request.flag DESC ");
      //    }else if($status =='9'){
      //       $query = $this->db->query("SELECT Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active,  clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date FROM Student_Reg INNER JOIN clearance_request ON Student_Reg.Admission = clearance_request.index_no INNER JOIN Grade ON Student_Reg.Grade = Grade.Grade WHERE (clearance_request.flag = 0 OR clearance_request.flag = 1 OR clearance_request.flag = 2 OR clearance_request.flag = 3) AND (clearance_request.flag <> 7) GROUP BY Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active, clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date ORDER BY clearance_request.flag DESC ");
      //    }else{
      //       $query = $this->db->query("SELECT Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active, clearance_request.cl_cat, clearance_request.requested_date, clearance_request.ref_no, clearance_request.replied_date, clearance_request.flag, clearance_request.remark, clearance_request.cancellation_date, clearance_request.upload1, clearance_request.upload2, clearance_request.upload3, clearance_request.upload4, clearance_request.tmp_from, clearance_request.tmp_to, clearance_request.suspend_date, clearance_request.leaving_req_date, clearance_request.leaving_reason, clearance_request.leaving_date, clearance_request.prepared_date, clearance_request.collected_date, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.id as clearance_id
      //    FROM Student_Reg INNER JOIN clearance_request ON Student_Reg.Admission = clearance_request.index_no LEFT OUTER JOIN Grade ON Student_Reg.Grade = Grade.Grade
      //    WHERE(clearance_request.flag = $flag) AND finance_flag<>1");
      //    }
         
      //    return $query->result_array();
         
      // }
   
      // public function TerminationSearchKeyword($searchterm, $status) {
      //    $flag = '';
      //    if($status == 2){
      //       $flag = 8;
      //    }else if($status == 3 ){
      //       $flag = 9;
      //    }else if($status == 4 ){
      //       $flag = 10;
      //    }else if($status == 5 ){
      //       $flag = 11;
      //    }else if($status == 6 ){
      //       $flag = 12;
      //    }

      //    if($status =='1'){
      //       $query = $this->db->query("SELECT Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active,  clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date FROM Student_Reg INNER JOIN clearance_request ON Student_Reg.Admission = clearance_request.index_no INNER JOIN Grade ON Student_Reg.Grade = Grade.Grade
      //       WHERE(Student_Reg.st_status = '$status')  AND (Student_Reg.Name_Ini LIKE '%$searchterm%' ESCAPE '!') AND (clearance_request.flag = 4 OR clearance_request.flag = 5) AND (clearance_request.flag <> 7) OR
      //       (Student_Reg.st_status = '$status') AND (Student_Reg.Admission LIKE '%$searchterm%' ESCAPE '!') AND (clearance_request.flag = 4 OR clearance_request.flag = 5) AND (clearance_request.flag <> 7) OR
      //       (Student_Reg.st_status = '$status') AND (clearance_request.ref_no LIKE '%$searchterm%' ESCAPE '!')AND (clearance_request.flag = 4 OR clearance_request.flag = 5) AND (clearance_request.flag <> 7) OR
      //       (Student_Reg.st_status = '$status') AND (Student_Reg.Name LIKE '%$searchterm%' ESCAPE '!') AND (clearance_request.flag = 4 OR clearance_request.flag = 5) AND (clearance_request.flag <> 7) 
      //       GROUP BY Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active, clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date ORDER BY Student_Reg.Admission, Grade.Caption");
      //    }else if($status =='9'){
      //       $query = $this->db->query("SELECT Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active,  clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date FROM Student_Reg INNER JOIN clearance_request ON Student_Reg.Admission = clearance_request.index_no INNER JOIN Grade ON Student_Reg.Grade = Grade.Grade
      //       WHERE(Student_Reg.st_status = '$status')  AND (Student_Reg.Name_Ini LIKE '%$searchterm%' ESCAPE '!') AND (clearance_request.flag = 0 OR clearance_request.flag = 1 OR clearance_request.flag = 2 OR clearance_request.flag = 3) AND (clearance_request.flag <> 7) OR
      //       (Student_Reg.st_status = '$status') AND (Student_Reg.Admission LIKE '%$searchterm%' ESCAPE '!') AND (clearance_request.flag = 0 OR clearance_request.flag = 1 OR clearance_request.flag = 2 OR clearance_request.flag = 3) AND (clearance_request.flag <> 7) OR
      //       (Student_Reg.st_status = '$status') AND (clearance_request.ref_no LIKE '%$searchterm%' ESCAPE '!')AND (clearance_request.flag = 0 OR clearance_request.flag = 1 OR clearance_request.flag = 2 OR clearance_request.flag = 3) AND (clearance_request.flag <> 7) OR
      //       (Student_Reg.st_status = '$status') AND (Student_Reg.Name LIKE '%$searchterm%' ESCAPE '!') AND (clearance_request.flag = 0 OR clearance_request.flag = 1 OR clearance_request.flag = 2 OR clearance_request.flag = 3) AND (clearance_request.flag <> 7) 
      //       GROUP BY Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active, clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date ORDER BY Student_Reg.Admission, Grade.Caption");
      //    }else{
      //       $query = $this->db->query("SELECT Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active, clearance_request.cl_cat, clearance_request.requested_date, clearance_request.ref_no, clearance_request.replied_date, clearance_request.flag, clearance_request.remark, clearance_request.cancellation_date, clearance_request.upload1, clearance_request.upload2, clearance_request.upload3, clearance_request.upload4, clearance_request.tmp_from, clearance_request.tmp_to, clearance_request.suspend_date, clearance_request.leaving_req_date, clearance_request.leaving_reason, clearance_request.leaving_date, clearance_request.prepared_date, clearance_request.collected_date, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.id as clearance_id
      //    FROM Student_Reg INNER JOIN clearance_request ON Student_Reg.Admission = clearance_request.index_no LEFT OUTER JOIN Grade ON Student_Reg.Grade = Grade.Grade
      //    WHERE(clearance_request.flag = $flag)  AND (Student_Reg.Name_Ini LIKE '%$searchterm%' ESCAPE '!') OR
      //    (clearance_request.flag = $flag) AND (Student_Reg.Admission LIKE '%$searchterm%' ESCAPE '!') OR
      //    (clearance_request.flag = $flag) AND (clearance_request.ref_no LIKE '%$searchterm%' ESCAPE '!') OR
      //    (clearance_request.flag = $flag) AND (Student_Reg.Name LIKE '%$searchterm%' ESCAPE '!') 
      //    ORDER BY Student_Reg.Admission, Grade.Caption");
      //    }
         
      //    return $query->result_array();
      // }

      // public function TerminationByClassSection($grade_id = null, $status = NULL) {
      //    $flag = '';
      //    if($status == 2){
      //       $flag = 8;
      //    }else if($status == 3 ){
      //       $flag = 9;
      //    }else if($status == 4 ){
      //       $flag = 10;
      //    }else if($status == 5 ){
      //       $flag = 11;
      //    }else if($status == 6 ){
      //       $flag = 12;
      //    }

      //    if($status =='1'){
      //       $query = $this->db->query("SELECT Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active,  clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date
      //       FROM Student_Reg INNER JOIN clearance_request ON Student_Reg.Admission = clearance_request.index_no INNER JOIN Grade ON Student_Reg.Grade = Grade.Grade
      //       WHERE((Student_Reg.st_status = $status) AND (Student_Reg.Grade = '$grade_id') AND (clearance_request.flag = 4 OR clearance_request.flag = 5) AND (clearance_request.flag <> 7)) GROUP BY Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active, clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date ORDER BY Student_Reg.Admission, Grade.Caption");
         
      //    }else if($status =='9'){
      //       $query = $this->db->query("SELECT Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active,  clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date
      //       FROM Student_Reg INNER JOIN clearance_request ON Student_Reg.Admission = clearance_request.index_no INNER JOIN Grade ON Student_Reg.Grade = Grade.Grade
      //       WHERE((Student_Reg.st_status = $status) AND (Student_Reg.Grade = '$grade_id') AND (clearance_request.flag = 0 OR clearance_request.flag = 1 OR clearance_request.flag = 2 OR clearance_request.flag = 3) AND (clearance_request.flag <> 7)) GROUP BY Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active, clearance_request.ref_no, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.flag, clearance_request.leaving_date ORDER BY Student_Reg.Admission, Grade.Caption");
      //    }else{
      //       $query = $this->db->query("SELECT Student_Reg.Name_Ini, Grade.Caption, Student_Reg.Class, Student_Reg.is_active, clearance_request.cl_cat, clearance_request.requested_date, clearance_request.ref_no, clearance_request.replied_date, clearance_request.flag, clearance_request.remark, clearance_request.cancellation_date, clearance_request.upload1, clearance_request.upload2, clearance_request.upload3, clearance_request.upload4, clearance_request.tmp_from, clearance_request.tmp_to, clearance_request.suspend_date, clearance_request.leaving_req_date, clearance_request.leaving_reason, clearance_request.leaving_date, clearance_request.prepared_date, clearance_request.collected_date, clearance_request.req_flag, clearance_request.req_type, Student_Reg.Admission, Student_Reg.ID, clearance_request.ac_year, clearance_request.id as clearance_id
      //       FROM Student_Reg INNER JOIN clearance_request ON Student_Reg.Admission = clearance_request.index_no INNER JOIN Grade ON Student_Reg.Grade = Grade.Grade
      //       WHERE((clearance_request.flag = $flag) AND (Student_Reg.Grade = '$grade_id'))
      //       ORDER BY Student_Reg.Admission, Grade.Caption");
      //    }

      //    return $query->result_array();
      // }

      /******************** GET DATASET ********************/
      // public function get_dataset($id){
      //    $this->db->select('Student_Reg.Admission, Student_Reg.Name_Ini, Student_Reg.Class, Student_Reg.ID AS st_id, Grade.Caption, clearance_cat_map.type, clearance_category.category, clearance_request.remark, clearance_request.cancellation_date, clearance_request.tmp_from, clearance_request.tmp_to, clearance_request.suspend_date, clearance_request.leaving_req_date, clearance_request.leaving_reason, clearance_request.leaving_date, clearance_request.prepared_date, clearance_request.collected_date');
      //    $this->db->from('clearance_request');
      //    $this->db->join('Student_Reg', 'clearance_request.index_no = Student_Reg.Admission');
      //    $this->db->join('Grade', 'Student_Reg.Grade = Grade.Grade');
      //    $this->db->join('clearance_cat_map', 'clearance_request.cl_cat = clearance_cat_map.category ');
      //    $this->db->join('clearance_category', ' clearance_cat_map.category = clearance_category.id');
      //    $this->db->where('clearance_request.id', $id);
      //    $query = $this->db->get();

      //    return $query->result();
      // }

     
   } 
?> 