<?php 
   class Menu_list_model extends CI_Model {
   
      function __construct() { 
         parent::__construct(); 
      } 

      /******************** LOAD DATA ********************/
      public function load_data(){
         $this->db->select('Form_List.Caption, Form_List.Name, Form_List.Category, Form_List.subcat_id, Form_List.R_Category, Form_List.ID AS form_list_id, Prjct.prjct');
         $this->db->from('Form_List');
         $this->db->join('Prjct', 'Form_List.p_id = Prjct.ID');
         $this->db->order_by('Prjct.prjct', 'asc');
         $user = $this->db->get();

         return $user->result();
      }

      /******************** GET CATEGORY ********************/
      public function get_category($id){
         $this->db->select('*');
         $this->db->from('prjct_cat');
         $this->db->where('p_id', $id);
         $this->db->order_by('cat');
         $query = $this->db->get();

         return $query->result();
      }

      /******************** INSERT ********************/
      public function insert($data) {
         $result = $this->db->get_where("Form_List",array('Caption'=>$data['Caption'], 'Cat_id'=>$data['Cat_id'], 'p_id'=>$data['p_id']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            $result2 = $this->db->get_where("Form_List",array('subcat_id'=>$data['subcat_id'], 'Cat_id'=>$data['Cat_id'], 'p_id'=>$data['p_id']));
            $rowcount2 = $result2->num_rows();
            if ($rowcount2 == 0) {

               if ($this->db->insert("Form_List", $data)) {
                  return "success";
               }else{
                  return "error";
               }
            }else{
               return "order no exists";
            }
             
         }else{
            return "data exists";
         }
      }

      /******************** GET DATASET ********************/
      public function get_dataset($id){
         $this->db->select(' Form_List.Caption, Form_List.Name, Form_List.Category, Form_List.subcat_id, Form_List.R_Category, Form_List.ID AS form_list_id, prjct_cat.icon, Form_List.p_id, Form_List.Cat_id');
         $this->db->from('Form_List');
         $this->db->join('prjct_cat', 'Form_List.Cat_id = prjct_cat.ID');
         $this->db->where('Form_List.ID', $id);
         $query = $this->db->get();

         return $query->result();
      }

      /******************** UPDATE ********************/
      public function update($data, $id){
         $result = $this->db->get_where("prjct_cat",array('ID'=>$data['Cat_id']));

         if($result->num_rows() == 1) {
            $row = $result->row();
            
            $this->db->set($data);
            $this->db->where("id", $id);
            if ($this->db->update("Form_List", $data)) {
               return "success"; 
            }else{
               return "error";
            }
         }else{
            return "data not exists";
         }
      }

      /******************** DELETE ********************/
      public function delete($data){
         if ($this->db->delete("Form_List", "id = ".$data['id'])) {
            return true; 
         }else{
            return "error";
         }
      }

      
   } 
?> 