<?php 
   class Staff_list_model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 

      /******************** LOAD DATA ********************/
      public function load_data(){
         $this->db->select('Employee.Acc_No, Employee.Name, Staff_Division.division, designation.desig, Employee.Nic, Employee.Tp, Employee.prof_pic, Employee.ID');
         $this->db->from('Employee');
         $this->db->join('designation', 'Employee.desig = designation.ID');
         $this->db->join('Staff_Division', 'Employee.Division = Staff_Division.id');
         $load_data = $this->db->get();

         return $load_data->result();
      }

      /******************** GET DATASET ********************/
      public function get_dataset($id){
         $this->db->select('*');
         $this->db->from('Employee');
         $this->db->where('ID', $id);
         $query = $this->db->get();

         return $query->result();
      }

      /******************** GET DATASET ********************/
      public function get_dataset2($acc_no){
         $this->db->select('*');
         $this->db->from('Employee');
         $this->db->where('Acc_No', $acc_no);
         $query = $this->db->get();

         return $query->result();
      }

      /******************** STAFF DETAILS********************/
      public function staff_details($acc_no){
         $this->db->select('Employee.ID,Employee.Acc_No, Employee.Name, Staff_Division.division, Employee.desig AS desig_id, designation.desig, Employee.Nic, Employee.Tp, Employee.prof_pic, Employee.ID, Employee.email');
         $this->db->from('Employee');
         $this->db->join('designation', 'Employee.desig = designation.ID');
         $this->db->join('Staff_Division', 'Employee.Division = Staff_Division.id');
         $this->db->where('Acc_No', $acc_no);
         $query = $this->db->get();

         return $query->row_array();
      }


      /******************** UPDATE ********************/
      public function update($data, $id){
         $result = $this->db->get_where("Employee",array('bank'=>$data['bank']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            $this->db->set($data);
            $this->db->where("ID", $id);
            if ($this->db->update("Employee", $data)) {
               return "success"; 
            }else{
               return "error";
            }
         }else{
            return "data exists";
         }
      }

      /******************** DELETE ********************/
      public function delete($data){
         if ($this->db->delete("Employee", "ID = ".$data['id'])) {
            return true; 
         }else{
            return "error";
         }
      }


      public function search() {

      }


      /******************** SEARCH BY ROLE ********************/
      public function searchByRole($role = null) {
         $this->db->select('Employee.Acc_No, Employee.Name, Staff_Division.division, designation.desig, Employee.Nic, Employee.Tp, Employee.prof_pic, Employee.ID');
         $this->db->from('Employee');
         $this->db->join('designation', 'Employee.desig = designation.ID');
         $this->db->join('Staff_Division', 'Employee.Division = Staff_Division.id');
         $this->db->where('Employee.desig', $role);
         $this->db->order_by('Employee.desig', 'ASC');
           
         $query = $this->db->get();
           return $query->result();
      }

      /******************** SEARCH BY KEYWORD ********************/
      public function searchKeyword($searchterm) {
         $this->db->select('Employee.Acc_No, Employee.Name, Staff_Division.division, designation.desig, Employee.Nic, Employee.Tp, Employee.prof_pic, Employee.ID');
         $this->db->from('Employee');
         $this->db->join('designation', 'Employee.desig = designation.ID');
         $this->db->join('Staff_Division', 'Employee.Division = Staff_Division.id');
         
         $this->db->like('Name', $searchterm);
         $this->db->or_like('Staff_Division.division', $searchterm);
         $this->db->or_like('designation.desig', $searchterm);
         $this->db->or_like('Nic', $searchterm);
         $this->db->or_like('Tp', $searchterm);
           
         $this->db->order_by('Employee.Name', 'ASC');
         $this->db->order_by('Staff_Division.division', 'ASC');
         $this->db->order_by('designation.desig', 'ASC');
         
         $query = $this->db->get();
         
         return $query->result();
      }
      
   } 
?> 