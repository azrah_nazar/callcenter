<?php
class Workshop_model extends CI_Model
{

   function __construct()
   {
      parent::__construct();
   }

   /******************** LOAD DATA ********************/
   public function load_data()
   {
      $this->db->select("*");
      $this->db->from('workshops');
      $this->db->order_by('w_date', 'DESC');
      $user = $this->db->get();

      return $user->result();
   }

   /******************** INSERT ********************/
   public function insert($data)
   {
      $result = $this->db->get_where("workshops", array('w_name' => $data['w_name'], 'w_date' => $data['w_date'], 'w_time' => $data['w_time']));

      if ($result->num_rows() == 0) {

         if ($this->db->insert("workshops", $data)) {
            return "success";
         } else {
            return "error";
         }
      } else {
         return "data exists";
      }
   }

   /******************** GET DATASET ********************/
   public function get_dataset($id)
   {
      $this->db->select('*');
      $this->db->from('workshops');
      $this->db->where('id', $id);
      $query = $this->db->get();

      return $query->result();
   }

   /******************** GET DATASET ********************/
   public function get_ws_students($id, $acc_no)
   {
      $this->db->select('workshops.*, con_list.name, con_list.reg_no, con_list.con_no, con_list.sms_flag, con_list.confirmation, con_list.bank_sms, con_list.ws_reminder, con_list.pending_payment, con_list.pending_reallocation, feedback_no, con_list.prev_wdate, con_list.tic_id, con_list.id AS cid, con_list.spl_cus, con_list.job');
      $this->db->from('workshops');
      $this->db->join('con_list', 'workshops.id = con_list.workshop');
      $this->db->where('workshops.id', $id);
      $this->db->where('con_list.feedback_reject<>', '1');
      $this->db->where('con_list.reapplied_reject<>', '1');
      $this->db->where('con_list.emp_accNo', $acc_no);
      $this->db->order_by('con_list.confirmation', 'DESC');
      $query = $this->db->get();

      return $query->result();
   }

   /******************** WORKSHOP********************/
   public function wshop($wid)
   {
      $this->db->select('*');
      $this->db->from('workshops');
      $this->db->where('id', $wid);
      $query = $this->db->get();

      return $query->row_array();
   }

   /******************** UPDATE ********************/
   public function update($data, $id)
   {
      $result = $this->db->get_where("workshops", array('w_name' => $data['w_name'], 'w_date' => $data['w_date'], 'w_time' => $data['w_time']));

      if ($result->num_rows() == 0) {
         $row = $result->row();

         $this->db->set($data);
         $this->db->where("id", $id);
         if ($this->db->update("workshops", $data)) {
            return "success";
         } else {
            return "error";
         }
      } else {
         return "data not exists";
      }
   }

   /******************** DELETE ********************/
   public function delete($data)
   {
      if ($this->db->delete("workshops", "id = " . $data['id'])) {
         return "success";
      } else {
         return "error";
      }
   }

   /******************** FUTURE WORKSHOPS ********************/
   public function next_ws()
   {
      $today = date('Y-m-d');
      $this->db->select("*");
      $this->db->from('workshops');
      $this->db->where('w_date >=', $today);
      $this->db->order_by('id');
      $user = $this->db->get();

      return $user->result();
   }

   /******************** FUTURE WORKSHOPS ********************/
   public function next_ws2()
   {
      $today = date('Y-m-d');
      $now = date('Y-m-d H:i:00');
      $this->db->limit(1);
      $this->db->select('w_time');
      $this->db->from('workshops');
      $this->db->where('w_date >=', $today);
      $this->db->where('w_time >=', $now);
      $query = $this->db->get();

      $row = $query->row();
      if ($query->num_rows() == 0) {
         $w_time = "H:i:s";
      } else {
         $w_time = $row->w_time;
      }

      $new_time = date('Y-m-d H:i:00', strtotime('+5 minutes', strtotime($w_time)));
      $this->db->limit(1);
      $this->db->select("*");
      $this->db->from('workshops');
      $this->db->where('w_date >=', $today);
      $this->db->where('w_time <=', $new_time);
      $this->db->where('w_time >=', $now);
      $user = $this->db->get();

      return $user->result();
   }

   /******************** ZOOM LINK ********************/
   public function load_zoom_link()
   {
      $today = date('Y-m-d');
      $last_week = date('Y-m-d', strtotime('-7 day', strtotime($today)));

      $this->db->select('*');
      $this->db->from('workshops');
      $this->db->where('zoom_link !=', '');
      $this->db->where('w_date>=', $last_week);
      $query = $this->db->get();

      return $query->result();
   }

   /******************** ZOOM LINK ********************/
   public function load_zoom_link2($date1, $date2)
   {
      $this->db->select('*');
      $this->db->from('workshops');
      $this->db->where('zoom_link !=', '');
      $this->db->where("w_date BETWEEN '$date1 00:00:00'  AND '$date2 23:59:59'");
      $this->db->order_by("w_date", 'DESC');
      $query = $this->db->get();

      return $query->result();
   }

   /******************** LAST WORKSHOP********************/
   public function last_workshop()
   {
      $this->db->select('*');
      $this->db->from('workshops');
      $this->db->where('w_date !=', '');
      $this->db->order_by("id", 'DESC');
      $query = $this->db->get();

      return $query->row_array();
   }

   /******************** ADD LINK ********************/
   public function add_workshop_link($data, $id)
   {
      $this->db->set($data);
      $this->db->where("id", $id);
      if ($this->db->update("workshops")) {
         return "success";
      } else {
         return "error";
      }
   }

   /******************** UPDATE LINK ********************/
   public function update_link($data, $id)
   {
      $this->db->set($data);
      $this->db->where("id", $id);
      if ($this->db->update("workshops", $data)) {
         return "success";
      } else {
         return "error";
      }
   }

   /************************** ZOOM LINK SMS *************************/
   public function zoom_link_sms($w_id, $acc_no)
   {
      $this->db->select('zoom_link, w_date, w_time');
      $this->db->from('workshops');
      $this->db->where('id', $w_id);
      $qry = $this->db->get();
      $rw = $qry->row();
      $zoom_link = $rw->zoom_link;
      $w_date = $rw->w_date;
      $w_time = date("h:i A", strtotime($rw->w_time));


      $this->db->select('con_list.con_no, con_list.reg_no, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice');
      $this->db->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('workshop', $w_id);
      $this->db->where('Employee.Acc_No', $acc_no);
      $this->db->where('sms_flag', '0');

      $query = $this->db->get();
      foreach ($query->result_array() as $row) {
         $con_no = $row['con_no'];
         $reg_no = $row['reg_no'];
         $emp_name = $row['name'];
         $emp_tp = $row['Tp'];
         $sinhala_name = $row['sinhala_name'];
         $emp_tp2 = ' / ' . $row['tpOffice'];

         $message = "අපගේ වැඩසටහනට සම්බන්ධ වීමට Zoom link එක පහතින්." . $w_date . " / " . $w_time . "\n" . $zoom_link;
         $data['message'] = trim($message);
         $count = ceil(strlen($message) / 160);

         $url = 'https://smsserver.textorigins.com/Send_sms?';

         $success_count = 0;

         $data_sms = array(
            'src' => 'CYCLOMAX257', //Mask
            'email' => 'newvictoryacademy@gmail.com ', //User Email
            'pwd' => 'Vic66556', //User Password
            'msg' => $message, //Message
            'dst' => $con_no
         ); //Phone Number

         $msg = http_build_query($data_sms);

         $url .= $msg;

         $ch = curl_init($url);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HEADER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

         $result_set = curl_exec($ch);

         if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
            $success_count++;
            $this->db->query("UPDATE con_list SET sms_flag= '1' WHERE con_no='$con_no'");
         }

         $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);

         $result['count'] = $success_count;
         $result['status'] = '1601';
         $result['message'] = 'Successfully Sent';
      }
      return "success";
   }

   /************************** ZOOM LINK INDIVIDUAL SMS *************************/
   public function link_sms_individual($reg_no, $acc_no)
   {

      $this->db->select('con_list.con_no, con_list.reg_no, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice, con_list.workshop');
      $this->db->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      if ($acc_no !== null || $acc_no != '') {
         $this->db->where('Employee.Acc_No', $acc_no);
      }
      $this->db->where('con_list.reg_no', $reg_no);

      $query = $this->db->get();
      foreach ($query->result_array() as $row) {

         $w_id = $row['workshop'];

         $this->db->select('zoom_link, w_date, w_time');
         $this->db->from('workshops');
         $this->db->where('id', $w_id);
         $qry = $this->db->get();
         $rw = $qry->row();
         $zoom_link = $rw->zoom_link;
         $w_date = $rw->w_date;
         $w_time = date("h:i A", strtotime($rw->w_time));

         $con_no = $row['con_no'];
         $reg_no = $row['reg_no'];
         $emp_tp = $row['Tp'];
         $sinhala_name = $row['sinhala_name'];
         $emp_tp2 = ' / ' . $row['tpOffice'];

         $message = "අපගේ වැඩසටහනට සම්බන්ධ වීමට Zoom link එක පහතින්." . $w_date . " / " . $w_time . "\n" . $zoom_link;
         $data['message'] = trim($message);
         $count = ceil(strlen($message) / 160);

         $url = 'https://smsserver.textorigins.com/Send_sms?';

         $success_count = 0;

         $data_sms = array(
            'src' => 'CYCLOMAX257', //Mask
            'email' => 'newvictoryacademy@gmail.com ', //User Email
            'pwd' => 'Vic66556', //User Password
            'msg' => $message, //Message
            'dst' => $con_no
         ); //Phone Number

         $msg = http_build_query($data_sms);

         $url .= $msg;

         $ch = curl_init($url);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HEADER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

         $result_set = curl_exec($ch);

         if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
            $success_count++;
            $this->db->query("UPDATE con_list SET sms_flag= '1' WHERE con_no='$con_no'");
         }

         $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);

         $result['count'] = $success_count;
         $result['status'] = '1601';
         $result['message'] = 'Successfully Sent';
      }
      return "success";
   }

   /************************** REMINDER SMS *************************/
   public function reminder_sms($w_id, $acc_no)
   {
      $this->db->select('zoom_link, w_date, w_time');
      $this->db->from('workshops');
      $this->db->where('id', $w_id);
      $qry = $this->db->get();
      $rw = $qry->row();
      $w_date = $rw->w_date;
      $w_time = date("h:i A", strtotime($rw->w_time));

      $this->db->select('con_list.con_no, con_list.reg_no, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice');
      $this->db->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('workshop', $w_id);
      $this->db->where('Employee.Acc_No', $acc_no);
      $this->db->where('con_list.ws_reminder', '0');

      $query = $this->db->get();
      foreach ($query->result_array() as $row) {
         $con_no = $row['con_no'];
         $reg_no = $row['reg_no'];
         $emp_tp = $row['Tp'];
         $sinhala_name = $row['sinhala_name'];
         $emp_tp2 = ' / ' . $row['tpOffice'];

         $message = "නොමිලේ පවත්වන හඳුන්වාදීමේ වැඩසටහන සුළු මොහොතකින් (" . $w_time . ") ආරම්භ වන බවට කාරුණික දැනුම්දීමයි.";

         $data['message'] = trim($message);
         $count = ceil(strlen($message) / 160);

         $url = 'https://smsserver.textorigins.com/Send_sms?';

         $success_count = 0;

         $data_sms = array(
            'src' => 'CYCLOMAX257', //Mask
            'email' => 'newvictoryacademy@gmail.com ', //User Email
            'pwd' => 'Vic66556', //User Password
            'msg' => $message, //Message
            'dst' => $con_no
         ); //Phone Number

         $msg = http_build_query($data_sms);

         $url .= $msg;

         $ch = curl_init($url);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HEADER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

         $result_set = curl_exec($ch);

         if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
            $success_count++;
            $this->db->query("UPDATE con_list SET ws_reminder= '1' WHERE con_no='$con_no'");
         }

         $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);

         $result['count'] = $success_count;
         $result['status'] = '1601';
         $result['message'] = 'Successfully Sent';
      }
      return "success";
   }

   /************************** INDIVIDUAL REMINDER SMS *************************/
   public function individual_reminder($reg_no, $acc_no)
   {
      $this->db->select('con_list.con_no, con_list.reg_no, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice, con_list.workshop');
      $this->db->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('Employee.Acc_No', $acc_no);
      $this->db->where('con_list.reg_no', $reg_no);

      $query = $this->db->get();
      foreach ($query->result_array() as $row) {

         $w_id = $row['workshop'];

         $this->db->select('zoom_link, w_date, w_time');
         $this->db->from('workshops');
         $this->db->where('id', $w_id);
         $qry = $this->db->get();
         $rw = $qry->row();
         $w_date = $rw->w_date;
         $w_time = date("h:i A", strtotime($rw->w_time));

         $con_no = $row['con_no'];
         $reg_no = $row['reg_no'];
         $emp_tp = $row['Tp'];
         $sinhala_name = $row['sinhala_name'];
         $emp_tp2 = ' / ' . $row['tpOffice'];

         $message = "නොමිලේ පවත්වන හඳුන්වාදීමේ වැඩසටහන සුළු මොහොතකින් (" . $w_time . ") ආරම්භ වන බවට කාරුණික දැනුම්දීමයි.";
         $data['message'] = trim($message);
         $count = ceil(strlen($message) / 160);

         $url = 'https://smsserver.textorigins.com/Send_sms?';

         $success_count = 0;

         $data_sms = array(
            'src' => 'CYCLOMAX257', //Mask
            'email' => 'newvictoryacademy@gmail.com ', //User Email
            'pwd' => 'Vic66556', //User Password
            'msg' => $message, //Message
            'dst' => $con_no
         ); //Phone Number

         $msg = http_build_query($data_sms);

         $url .= $msg;

         $ch = curl_init($url);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HEADER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

         $result_set = curl_exec($ch);

         if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
            $success_count++;
            $this->db->query("UPDATE con_list SET ws_reminder= '1' WHERE con_no='$con_no'");
         }

         $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);

         $result['count'] = $success_count;
         $result['status'] = '1601';
         $result['message'] = 'Successfully Sent';
      }
      return "success";
   }

   /************************** FEEDBACK SMS *************************/
   public function feedback_sms($w_id, $acc_no)
   {
      $this->db->select('zoom_link, w_date, w_time');
      $this->db->from('workshops');
      $this->db->where('id', $w_id);
      $qry = $this->db->get();
      $rw = $qry->row();
      $w_date = $rw->w_date;
      $w_time = date("h:i A", strtotime($rw->w_time));

      $this->db->select('con_list.con_no, con_list.reg_no, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice');
      $this->db->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('workshop', $w_id);
      $this->db->where('Employee.Acc_No', $acc_no);
      $this->db->where('con_list.confirmation', '0');

      $query = $this->db->get();
      foreach ($query->result_array() as $row) {
         $con_no = $row['con_no'];
         $reg_no = $row['reg_no'];
         $emp_tp = $row['Tp'];
         $sinhala_name = $row['sinhala_name'];
         $emp_tp2 = ' / ' . $row['tpOffice'];

         $message = "අප විසින් " . $w_date . " / " . $w_time . " පැවැත්වූ වැඩසටහනට ඔබ සහභාගිවූවා නම් link එක click කර තහවුරු කරන්න. \nhttps://callcenter.victoryacademylk.com/feedback/" . $reg_no;

         $data['message'] = trim($message);
         $count = ceil(strlen($message) / 160);

         $url = 'https://smsserver.textorigins.com/Send_sms?';

         $success_count = 0;

         $data_sms = array(
            'src' => 'CYCLOMAX257', //Mask
            'email' => 'newvictoryacademy@gmail.com ', //User Email
            'pwd' => 'Vic66556', //User Password
            'msg' => $message, //Message
            'dst' => $con_no
         ); //Phone Number

         $msg = http_build_query($data_sms);

         $url .= $msg;

         $ch = curl_init($url);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HEADER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

         $result_set = curl_exec($ch);

         if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
            $success_count++;
            $this->db->query("UPDATE con_list SET confirmation= '1' WHERE con_no='$con_no'");
         }

         $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);

         $result['count'] = $success_count;
         $result['status'] = '1601';
         $result['message'] = 'Successfully Sent';
      }
      return "success";
   }

   /************************** INDIVIDUAL FEEDBACK SMS *************************/
   public function individual_feedback_sms($reg_no, $acc_no)
   {
      $this->db->select('con_list.con_no, con_list.reg_no, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice, con_list.workshop');
      $this->db->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('Employee.Acc_No', $acc_no);
      $this->db->where('con_list.reg_no', $reg_no);

      $query = $this->db->get();
      foreach ($query->result_array() as $row) {

         $w_id = $row['workshop'];

         $this->db->select('zoom_link, w_date, w_time');
         $this->db->from('workshops');
         $this->db->where('id', $w_id);
         $qry = $this->db->get();
         $rw = $qry->row();
         $w_date = $rw->w_date;
         $w_time = date("h:i A", strtotime($rw->w_time));

         $con_no = $row['con_no'];
         $reg_no = $row['reg_no'];
         $emp_tp = $row['Tp'];
         $sinhala_name = $row['sinhala_name'];
         $emp_tp2 = ' / ' . $row['tpOffice'];

         $message = "අප විසින් " . $w_date . " / " . $w_time . " පැවැත්වූ වැඩසටහනට ඔබ සහභාගිවූවා නම් link එක click කර තහවුරු කරන්න. \nhttps://callcenter.victoryacademylk.com/feedback/" . $reg_no;
         $data['message'] = trim($message);
         $count = ceil(strlen($message) / 160);

         $url = 'https://smsserver.textorigins.com/Send_sms?';

         $success_count = 0;

         $data_sms = array(
            'src' => 'CYCLOMAX257', //Mask
            'email' => 'newvictoryacademy@gmail.com ', //User Email
            'pwd' => 'Vic66556', //User Password
            'msg' => $message, //Message
            'dst' => $con_no
         ); //Phone Number

         $msg = http_build_query($data_sms);

         $url .= $msg;

         $ch = curl_init($url);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HEADER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

         $result_set = curl_exec($ch);

         if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
            $success_count++;
            $this->db->query("UPDATE con_list SET confirmation= '1' WHERE con_no='$con_no'");
         }

         $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);

         $result['count'] = $success_count;
         $result['status'] = '1601';
         $result['message'] = 'Successfully Sent';
      }
      return "success";
   }

   /************************** CONFIRMATION  *************************/
   public function confirmation($telNo)
   {
      // 0 => default
      // 1 => sms sent
      // 2 => no
      // 3 => yes
      if ($this->db->query("UPDATE con_list SET confirmation='3' WHERE con_no='$telNo'")) {
         return "success";
      } else {
         return "error";
      }
   }

   /************************** CONFIRMATION  *************************/
   public function confirmation_no($telNo)
   {
      if ($this->db->query("UPDATE con_list SET confirmation='2' WHERE con_no='$telNo'")) {
         return "success";
      } else {
         return "error";
      }
   }

   /************************** CONFIRMATION REJECT  *************************/
   public function confirmation_reject($telNo)
   {
      $now = date('Y-m-d H:i:s');
      if ($this->db->query("UPDATE con_list SET feedback_reject='1', reject_date='$now', pending_payment='0', direct_sale='0' WHERE con_no='$telNo'")) {
         return "success";
      } else {
         return "error";
      }
   }

   /************************** CONFIRMATION REJECT  *************************/
   public function clz_followup($telNo)
   {
      if ($this->db->query("UPDATE con_list SET pending_payment='1' WHERE con_no='$telNo'")) {
         return "success";
      } else {
         return "error";
      }
   }

   /************************** PENDING PAYMENT *************************/
   public function pending_payment($telNo, $flag)
   {
      if ($this->db->query("UPDATE con_list SET pending_payment='$flag', feedback_reject='0', reject_date='' WHERE con_no='$telNo'")) {
         return "success";
      } else {
         return "error";
      }
   }

   /************************** PENDING REALLOCATION *************************/
   public function pending_reallocation($telNo, $flag)
   {
      if ($this->db->query("UPDATE con_list SET pending_reallocation='$flag', direct_sale='0' WHERE con_no='$telNo'")) {
         return "success";
      } else {
         return "error";
      }
   }


   /************************** BANK SMS *************************/
   public function bank_sms($w_id, $acc_no)
   {
      $this->db->select('con_list.con_no, con_list.reg_no, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice');
      $this->db->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('bank_sms', '0');
      $this->db->where('workshop', $w_id);
      $this->db->where('emp_accNo', $acc_no);

      $query = $this->db->get();
      foreach ($query->result_array() as $row) {
         $con_no = $row['con_no'];
         $reg_no = $row['reg_no'];
         $emp_tp = $row['Tp'];
         $sinhala_name = $row['sinhala_name'];
         $emp_tp2 = ' / ' . $row['tpOffice'];

         $message = "පාඨමාලාවට සහභාගි වීමට ලියාපදිංචි වීමේ විස්තර මෙම link එකෙන් ලබාගන්න. https://callcenter.victoryacademylk.com/bank-details/" . $reg_no . "\n" . $sinhala_name . " " . $emp_tp . " " . $emp_tp2;

         $data['message'] = trim($message);
         $count = ceil(strlen($message) / 160);

         $url = 'https://smsserver.textorigins.com/Send_sms?';

         $success_count = 0;

         $data_sms = array(
            'src' => 'CYCLOMAX257', //Mask
            'email' => 'newvictoryacademy@gmail.com ', //User Email
            'pwd' => 'Vic66556', //User Password
            'msg' => $message, //Message
            'dst' => $con_no
         ); //Phone Number

         $msg = http_build_query($data_sms);

         $url .= $msg;

         $ch = curl_init($url);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HEADER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

         $result_set = curl_exec($ch);

         if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
            $success_count++;
            $this->db->query("UPDATE con_list SET bank_sms= '1' WHERE con_no='$con_no'");
         }

         $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);

         $result['count'] = $success_count;
         $result['status'] = '1601';
         $result['message'] = 'Successfully Sent';
      }
      return "success";
   }

   /************************** INDIVIDUAL BANK SMS *************************/
   public function individual_bank_sms($reg_no, $acc_no)
   {
      $this->db->select('con_list.con_no, con_list.reg_no, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice, con_list.workshop');
      $this->db->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('Employee.Acc_No', $acc_no);
      $this->db->where('con_list.reg_no', $reg_no);

      $query = $this->db->get();
      foreach ($query->result_array() as $row) {

         $con_no = $row['con_no'];
         $reg_no = $row['reg_no'];
         $emp_tp = $row['Tp'];
         $sinhala_name = $row['sinhala_name'];
         $emp_tp2 = ' / ' . $row['tpOffice'];

         // $message = "පාඨමාලාවට සහභාගි වීමට ලියාපදිංචි වීමේ විස්තර මෙම link එකෙන් ලබාගන්න. https://callcenter.victoryacademylk.com/bank-details/" . $reg_no . "\n" . $sinhala_name . " " . $emp_tp . " " . $emp_tp2;
         $message = "සුබ පැතුම්.\nඅප ආයතනයේ ශිෂ්‍යත්වය සදහා ඔබ සුදුසු කම් ලබා ඇත. ශිෂ්‍යත්වයට අදාළ ගාස්තුව රු.17500/- ගෙවා ඔබගේ Invoice පත ලබා ගන්න. ඔබට ලඟම ඇති පාඨමාලා දිනය වෙන් කර ගැනීමට අප ඔබට සහය වනු ඇත. ඔබගේ ලියාපදිංචි අංකය ".$reg_no;
         $data['message'] = trim($message);
         $count = ceil(strlen($message) / 160);

         $url = 'https://smsserver.textorigins.com/Send_sms?';

         $success_count = 0;

         $data_sms = array(
            'src' => 'CYCLOMAX257', //Mask
            'email' => 'newvictoryacademy@gmail.com ', //User Email
            'pwd' => 'Vic66556', //User Password
            'msg' => $message, //Message
            'dst' => $con_no
         ); //Phone Number

         $msg = http_build_query($data_sms);

         $url .= $msg;

         $ch = curl_init($url);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HEADER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

         $result_set = curl_exec($ch);

         if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
            $success_count++;
            $this->db->query("UPDATE con_list SET bank_sms= '1' WHERE con_no='$con_no'");
         }

         $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);

         $result['count'] = $success_count;
         $result['status'] = '1601';
         $result['message'] = 'Successfully Sent';
      }
      return "success";
   }

   /************************** ZOOM LINK SMS *************************/
   public function reallocate_ws($w_id, $acc_no, $prev_wid)
   {
      $this->db->select('zoom_link, w_date, w_time');
      $this->db->from('workshops');
      $this->db->where('id', $w_id);
      $qry = $this->db->get();
      $rw = $qry->row();
      $zoom_link = $rw->zoom_link;
      $w_date = $rw->w_date;
      $w_time = date("h:i A", strtotime($rw->w_time));

      $today = date('Y-m-d H:i:s');

      $this->db->select('w_date');
      $this->db->from('workshops');
      $this->db->where('id', $prev_wid);
      $qry2 = $this->db->get();
      $rw2 = $qry2->row();
      $w_date2 = $rw2->w_date;

      $this->db->query("UPDATE con_list SET workshop='$w_id', sms_flag='0', confirmation='0', bank_sms='0', ws_reminder='0', pending_reallocation='0', prev_wdate='$w_date2', feedback_no = feedback_no+1, pending_payment = 0 WHERE pending_reallocation='1' AND feedback_no<3 AND emp_accNo='$acc_no' AND workshop='$prev_wid'");

      //$this->db->query("UPDATE con_list SET feedback_reject = '1', reject_date='$today' WHERE pending_payment='0' AND feedback_no>=3 AND emp_accNo='$acc_no' AND workshop='$prev_wid'");

      $this->db->query("UPDATE con_list SET feedback_reject = '1', reject_date='$today', workshop='', sms_flag='0', confirmation='0', bank_sms='0', ws_reminder='0', pending_reallocation='0', feedback_no='0' WHERE pending_payment='0' AND emp_accNo='$acc_no' AND workshop='$prev_wid'");

      $this->db->select('con_list.con_no, con_list.reg_no, Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice');
      $this->db->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('workshop', $w_id);
      $this->db->where('Employee.Acc_No', $acc_no);
      $this->db->where('sms_flag', '0');

      $query = $this->db->get();
      foreach ($query->result_array() as $row) {
         $con_no = $row['con_no'];
         $reg_no = $row['reg_no'];
         $emp_name = $row['name'];
         $emp_tp = $row['Tp'];
         $sinhala_name = $row['sinhala_name'];
         $emp_tp2 = ' / ' . $row['tpOffice'];

         $message = "අපගේ වැඩසටහනට සම්බන්ධ වීමට Zoom link එක පහතින්." . $w_date . " / " . $w_time . "\n" . $zoom_link;

         $data['message'] = trim($message);
         $count = ceil(strlen($message) / 160);

         $url = 'https://smsserver.textorigins.com/Send_sms?';

         $success_count = 0;

         $data_sms = array(
            'src' => 'CYCLOMAX257', //Mask
            'email' => 'newvictoryacademy@gmail.com ', //User Email
            'pwd' => 'Vic66556', //User Password
            'msg' => $message, //Message
            'dst' => $con_no
         ); //Phone Number

         $msg = http_build_query($data_sms);

         $url .= $msg;

         $ch = curl_init($url);

         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_HEADER, false);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

         $result_set = curl_exec($ch);

         if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
            $success_count++;
            $this->db->query("UPDATE con_list SET sms_flag= '1' WHERE con_no='$con_no'");
         }

         $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         curl_close($ch);

         $result['count'] = $success_count;
         $result['status'] = '1601';
         $result['message'] = 'Successfully Sent';
      }
      return "success";
   }

   /************************** WORKSHOP REJECT REPORT *************************/
   public function workshop_reject_report($agent = null, $date1 = NULL, $date2 = NULL)
   {
      $this->db->select('con_list.*, Employee.Name as emp_name')->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('con_list.feedback_reject', 1);
      $this->db->where('con_list.pending_payment <>', 1);
      $this->db->where('con_list.reapplied_reject <>', 1);
      $this->db->where("con_list.reject_date BETWEEN '$date1 " . "00:00:00 '" . " AND '$date2 23:59:59'");

      if ($agent != '') {
         $this->db->where('emp_accNo', $agent);
      }

      $this->db->order_by('date', 'DESC');

      $query = $this->db->get();
      return $query->result_array();
   }

   /************************** REJOIN PENDING  *************************/
   public function rejoin_pending($reg_no)
   {
      if ($this->db->query("UPDATE con_list SET flag='0', comment='', reminder_date='', workshop='', sms_flag='0', confirmation='0', bank_sms='0', ws_reminder='0', pending_reallocation='0', feedback_reject='', reject_date='' WHERE reg_no='$reg_no'")) {
         return "success";
      } else {
         return "error";
      }
   }

   /************************** NEW WORKSHOP *************************/
   public function new_workshop($telNo)
   {
      $today = date('Y-m-d');
      if ($this->db->query("UPDATE con_list SET flag='1', reminder_date='', workshop='', sms_flag='0', confirmation='0', bank_sms='0', ws_reminder='0', pending_reallocation='0', feedback_reject='', reject_date='', date1='$today', spl_cus='0' WHERE con_no='$telNo'")) {
         return "success";
      } else {
         return "error";
      }
   }

   /************************** MALE WORKSHOP REJECT REPORT *************************/
   public function male_workshop_reject($date1 = NULL, $date2 = NULL, $acc_no = NULL, $mapped_op)
   {
      $this->db->select('con_list.*, Employee.Name as emp_name')->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('con_list.feedback_reject', 1);
      $this->db->where('con_list.pending_payment <>', 1);
      $this->db->where('con_list.direct_sale <>', 1);
      $this->db->where('con_list.ws_reject_retrieve', 1);
      $this->db->where('con_list.emp_accNo', $mapped_op);
      $this->db->where('con_list.transferred_to', $acc_no);
      $this->db->where('con_list.emp_accNo <>', $acc_no);
      $this->db->where("con_list.reject_date BETWEEN '$date1 " . "00:00:00 '" . " AND '$date2 23:59:59'");

      $this->db->order_by('date', 'DESC');

      $query = $this->db->get();
      return $query->result_array();
   }

   /************************** MAXIMUM TRANSFER DATE *************************/
   public function max_trans_date($acc_no = NULL)
   {
      $this->db->select('con_list.*, MAX(con_list.transferred_date) AS max_date')->from('con_list');
      $this->db->where('con_list.transferred_to', $acc_no);

      $this->db->order_by('transferred_to', 'DESC');

      $query = $this->db->get();
      return $query->row_array();
   }

   /************************** PENDING CONTACTS *************************/
   public function pending_contacts($data, $telNo)
   {
      $this->db->set($data);
      $this->db->where("con_no", $telNo);
      if ($this->db->update("con_list")) {
         return "success";
      } else {
         return "error";
      }
   }

   /******************** GET MALE SMS STUDENTS ********************/
   public function get_male_ws_students($mapped_op, $acc_no, $date1, $date2)
   {
      $this->db->select('workshops.*, con_list.name, con_list.reg_no, con_list.con_no, con_list.sms_flag, con_list.confirmation, con_list.bank_sms, con_list.ws_reminder, con_list.pending_payment, con_list.pending_reallocation, feedback_no, con_list.prev_wdate, con_list.tic_id, con_list.id AS cid, con_list.spl_cus');
      $this->db->from('workshops');
      $this->db->join('con_list', 'workshops.id = con_list.workshop');
      $this->db->where('con_list.feedback_reject<>', '1');
      $this->db->where('con_list.pending_payment <>', 1);
      $this->db->where('con_list.direct_sale <>', 1);
      $this->db->where('con_list.emp_accNo', $mapped_op);
      $this->db->where('con_list.transferred_to', $acc_no);
      $this->db->where('con_list.emp_accNo <>', $acc_no);
      $this->db->where("con_list.date BETWEEN '$date1 " . "00:00:00 '" . " AND '$date2 23:59:59'");
      $this->db->where('con_list.ws_sms_retrieve', '1');
      $this->db->order_by('con_list.confirmation', 'DESC');
      $query = $this->db->get();

      return $query->result();
   }

   /************************** MALE REAPPLED REJECT REPORT *************************/
   public function male_reapplied_reject($date1 = NULL, $date2 = NULL, $acc_no = NULL, $mapped_op = NULL)
   {
      $query = $this->db->query("SELECT con_list_temp.name, con_list.con_no, con_list.whatsapp, Employee.Name AS agent_name, con_list_temp.date, con_list_temp.user_id, con_list_temp.csv_name, con_list_temp.feedback_reject, con_list_temp.flag, con_list.flag AS status, con_list.id AS cid, con_list_temp.id AS cid_temp, con_list.tic_id, con_list.spl_cus, con_list_temp.emp_accNo, Employee.Acc_No, con_list.reapplied_reject_date
       FROM
      con_list_temp
      Inner Join Employee ON con_list_temp.emp_accNo = Employee.Acc_No
      Inner Join con_list ON con_list_temp.con_no = con_list.con_no
      WHERE DATE(con_list.retrieved_date) BETWEEN '$date1'  AND '$date2' AND con_list.reapplied_reject ='1' AND con_list.reapplied_rejected_retrieve='1'  AND con_list.retrieved_by='$acc_no' AND con_list.emp_accNo='$mapped_op' AND con_list.flag <> '0' GROUP BY  con_list.con_no,  con_list.whatsapp ORDER BY csv_name ");

      return $query->result_array();
   }

   /******************** SPECIAL CSV ********************/
   public function spl_csv($acc_no, $ws)
   {
      $this->db->select('con_list.*, workshops.w_date');
      $this->db->from('con_list');
      $this->db->join('workshops', 'con_list.workshop = workshops.id');
      $this->db->where('workshop', $ws);
      $this->db->where('emp_accNo', $acc_no);
      $this->db->where('spl_cus', '1');
      $query = $this->db->get();

      return $query->result_array();
   }

   /******************** SPECIAL CSV COUNT ********************/
   public function spl_csv_count($acc_no, $ws)
   {
      $this->db->select('*');
      $this->db->from('con_list');
      $this->db->where('workshop', $ws);
      $this->db->where('emp_accNo', $acc_no);
      $this->db->where('spl_cus', '1');
      $query = $this->db->get();
      $rowcount = $query->num_rows();

      return $rowcount;
   }
}
