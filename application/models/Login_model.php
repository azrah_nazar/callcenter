<?php 

class Login_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	/* 
		This function checks if the email and password matches with the database
	*/
	public function login($username, $password) {
		if($username && $password) {
			$sql = "SELECT * FROM Usert WHERE ID = ? AND Pws=?";
			$query = $this->db->query($sql, array($username, $password));

			if($query->num_rows() == 1) {
				$result = $query->row_array();

				$sql2 = "SELECT Usert.*, Employee.gender FROM Usert INNER JOIN Employee ON Usert.acc_no = Employee.Acc_No WHERE Usert.ID = ? AND Usert.Pws=? AND Usert.stt!=2";
				$query2 = $this->db->query($sql2, array($username, $password));

				if($query2->num_rows() == 1) {
					$result2 = $query2->row_array();
					
					return $result2;	
				}else {
					return "access denied";
				}
				
			}
			else {
				return false;
			}
		}
	}

	/******************** USER LOG ********************/
      public function user_log($data) {

        if ($this->db->insert("A_Log", $data)) {

        	//update employee status
        	$acc_no = $data['Usr'];
        	
        	$this->db->set('stt', 1);
        	$this->db->where('acc_no', $acc_no);
        	if ($this->db->update("Usert")) {
        	   return "success"; 
        	}else{
        	   return "error";
        	}

        }else{
           return "error";
        }
      }
}