<?php 
   class Group_privileges_model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 


      /******************** PROJECT CATEGORY ********************/
      public function get_pro_cat() {
         $this->db->select('*');
         $this->db->from('prjct_cat');
         $this->db->order_by('cat', 'ASC');
         $query = $this->db->get();

         return $query->result();
         
      }

      /********************  MENU LIST ********************/
      public function get_cat(){
         $this->db->select('*');
         $this->db->from('Form_List');
         $this->db->order_by("Caption", "ASC");
         $query = $this->db->get();

         $row = $query->row();

         return $query->result();
      }

      /******************** MAIN CATEGORY ********************/
      public function menu_list($pr_id) {
         $query = "SELECT * FROM Form_List WHERE p_id = '$pr_id'";

         $result = $this->db->query($query);

         return $result->result();
      }

      /******************** GET PRIVILEGES********************/
      public function user_priv($profile) {
         $this->db->select('*');
         $this->db->from('User_priv');
         $this->db->where('grp_id', $profile);
         $this->db->order_by("menu_id", "ASC");
         $query = $this->db->get();

         $row = $query->row();

         return $query->result();

      }

      /******************** MAIN CATEGORY ********************/
      public function get_category($pid) {
         $query = "SELECT * FROM prjct_cat WHERE p_id = '$pid'";

         $result = $this->db->query($query)->result_array();

         return $result;
      }

      /******************** MAIN CATEGORY ********************/
      public function get_menu_list($pr_id, $cat_id) {
         $query = "SELECT * FROM Form_List WHERE p_id = '$pr_id' AND Cat_id='$cat_id'";

         $result = $this->db->query($query)->result_array();

         return $result;
      }

      /******************** GET PRIVILEGES********************/
      public function get_priv($grp_id, $menu_id) {

         $query = "SELECT * FROM User_priv WHERE grp_id = '$grp_id' AND menu_id='$menu_id'";

         $result = $this->db->query($query)->result_array();

         return $result;
      }
      
     

      /********************  ADD PRIVILEGES ********************/
      public function add_priv($data){
         $result = $this->db->get_where("User_priv",array('grp_id'=>$data['grp_id'], 'menu_id'=>$data['menu_id']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            if ($this->db->insert("User_priv", $data)) {
               return "success";
            }else{
               return "error";
            }
         }else{
            return "data exists";
         }
      }

      /********************  REMOVE PRIVILEGES ********************/
      public function remove_Priv($data){
         if ($this->db->delete("User_priv", array('grp_id'=>$data['grp_id'],'menu_id'=>$data['menu_id']))) {
            return "success"; 
         }else{
            return "error";
         }
      }



      
   } 
?> 