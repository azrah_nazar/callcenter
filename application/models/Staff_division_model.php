<?php 
   class Staff_division_model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 

      /******************** LOAD DATA ********************/
      public function load_data(){
         $this->db->select('*');
         $this->db->from('Staff_Division');
         // $this->db->order_by('division');
         $user = $this->db->get();

         return $user->result();
      }

      /******************** INSERT ********************/
      public function insert($data) {
         $result = $this->db->get_where("Staff_Division",array('division'=>$data['division']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            if ($this->db->insert("Staff_Division", $data)) {
               return "success";
            }else{
               return "error";
            }
             
         }else{
            return "data exists";
         }
      }

      /******************** GET DATASET ********************/
      public function get_dataset($id){
         $this->db->select('*');
         $this->db->from('Staff_Division');
         $this->db->where('id', $id);
         $query = $this->db->get();

         return $query->result();
      }

      /******************** UPDATE ********************/
      public function update($data, $id){
         $result = $this->db->get_where("Staff_Division",array('division'=>$data['division']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            $this->db->set($data);
            $this->db->where("id", $id);
            if ($this->db->update("Staff_Division", $data)) {
               return "success"; 
            }else{
               return "error";
            }
         }else{
            return "data exists";
         }
      }

      /******************** DELETE ********************/
      public function delete($data){
         if ($this->db->delete("Staff_Division", "id = ".$data['id'])) {
            return true; 
         }else{
            return "error";
         }
      }

      
   } 
?> 