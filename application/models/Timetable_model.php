<?php 
class Timetable_model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   /******************** LOAD DATA ********************/
   public function load_data(){
      $this->db->select('Employee.Name, course.course_name, class_timetable.c_date, class_timetable.c_time, class_timetable.no_slot, class_timetable.id');
      $this->db->from('class_timetable');
      $this->db->join('Employee', 'class_timetable.lecturer = Employee.Acc_No');
      $this->db->join('course', 'class_timetable.course = course.id');
      $result = $this->db->get();

      return $result->result();
   }



   /******************** INSERT ********************/
   public function insert($data) {

      if ($this->db->insert("class_timetable", $data)) {
         return "success";
      }else{
         return "error";
      }

   }

   /******************** GET DATASET ********************/
   public function get_dataset($id){
      $this->db->select('Employee.Name, Employee.Acc_No, course.course_name, course.id as c_id, class_timetable.c_date, class_timetable.c_time, class_timetable.no_slot, class_timetable.id');
      $this->db->from('class_timetable');
      $this->db->join('Employee', 'class_timetable.lecturer = Employee.Acc_No');
      $this->db->join('course', 'class_timetable.course = course.id');
      $this->db->where('class_timetable.id', $id);
      $query = $this->db->get();

      return $query->result();
   }

   /******************** UPDATE ********************/
   public function update($data, $id){


      $this->db->set($data);
      $this->db->where("id", $id);
      if ($this->db->update("class_timetable", $data)) {
         return "success"; 
      }else{
         return "error";
      }
      
   }

   /******************** DELETE ********************/
   public function delete($data){
      if ($this->db->delete("class_timetable", "id = ".$data['id'])) {
         return true; 
      }else{
         return "error";
      }
   }

   /******************** FUTURE CLASSES ********************/
   public function next_classes()
   {
      $today = date('Y-m-d');
      $this->db->select("class_timetable.*, course.course_name");
      $this->db->from('class_timetable');
      $this->db->join('course', 'class_timetable.course = course.id');
      $this->db->where('c_date >', $today);
      $user = $this->db->get();

      return $user->result();
   }





} 
?> 