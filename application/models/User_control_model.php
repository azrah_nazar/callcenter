<?php 
   class User_control_model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 

      /******************** LOAD DATA ********************/
      public function load_data(){
         $this->db->select('Usert.ID, Usert.uid, Group_Prof.profile, Staff_Division.division, Usert.acc_no, Usert.stt, Employee.Name, Employee.Tp, 
                      Usert.Val');
         $this->db->from('Usert');
         $this->db->join('Employee', 'Usert.emp = Employee.ID');
         $this->db->join('Staff_Division', 'Employee.Division = Staff_Division.id');
         $this->db->join('Group_Prof', 'Usert.Val = Group_Prof.id');
         $user = $this->db->get();

         return $user->result();
      }

      /******************** UPDATE ********************/
      public function update($data, $id){

         $this->db->set($data);
         $this->db->where("acc_no", $id);
         if ($this->db->update("Usert", $data)) {
            return "success"; 
         }else{
            return "error";
         }
      }

      /******************** USER LOG ********************/
      public function user_log($acc_no){
         $this->db->select('prjct.prjct, A_Log.Panel, A_Log.Date, A_Log.Usr, A_Log.ID');
         $this->db->from('A_Log');
         $this->db->join('prjct', 'A_Log.p_id = prjct.ID');
         $this->db->where('A_Log.Usr', $acc_no);
         $this->db->order_by('A_Log.ID ', 'DESC');
         $user = $this->db->get();

         return $user->result();
      }
      
   } 
?> 