<?php 
   class Add_bank_model extends CI_Model {
   
      function __construct() { 
         parent::__construct(); 
      } 

      /******************** LOAD DATA ********************/
      public function load_data(){
         $this->db->select('*');
         $this->db->from('bank');
         $user = $this->db->get();

         return $user->result();
      }

     

      /******************** INSERT ********************/
      public function insert($data) {
         
         if ($this->db->insert("bank", $data)) {
            return "success";
         }else{
            return "error";
         }
           
      }

      /******************** GET DATASET ********************/
      public function get_dataset($id){
         $this->db->select('*');
         $this->db->from('bank');
         $this->db->where('id', $id);
         $query = $this->db->get();

         return $query->result();
      }

      /******************** UPDATE ********************/
      public function update($data, $id){
         $result = $this->db->get_where("bank",array('id'=>$data['id']));

         if($result->num_rows() == 1) {
            $row = $result->row();
            
            $this->db->set($data);
            $this->db->where("id", $id);
            if ($this->db->update("bank", $data)) {
               return "success"; 
            }else{
               return "error";
            }
         }else{
            return "data not exists";
         }
      }

      /******************** DELETE ********************/
      public function delete($data){
         if ($this->db->delete("bank", "id = ".$data['id'])) {
            return true; 
         }else{
            return "error";
         }
      }


     

      
   }
