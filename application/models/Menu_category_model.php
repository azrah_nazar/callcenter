<?php 
   class Menu_category_model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 

      /******************** LOAD PROJECTS ********************/
      public function load_project(){
         $this->db->select('*');
         $this->db->from('Prjct');
         $user = $this->db->get();

         return $user->result();
      }

      /******************** LOAD DATA ********************/
      public function load_data(){
         $this->db->select('prjct_cat.ID, prjct_cat.cat, prjct_cat.icon, Prjct.prjct');
         $this->db->from('Prjct');
         $this->db->join('prjct_cat', 'Prjct.ID = prjct_cat.p_id');
         $this->db->order_by("prjct_cat.cat", "asc");
         $user = $this->db->get();

         return $user->result();
      }

      /******************** INSERT ********************/
      public function insert($data) {
         $result = $this->db->get_where("prjct_cat",array('cat'=>$data['cat'], 'p_id'=>$data['p_id']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            if ($this->db->insert("prjct_cat", $data)) {
               return "success";
            }else{
               return "error";
            }
             
         }else{
            return "data exists";
         }
      }

      /******************** GET DATASET ********************/
      public function get_dataset($id){
         $this->db->select('*');
         $this->db->from('prjct_cat');
         $this->db->where('id', $id);
         $query = $this->db->get();

         return $query->result();
      }

      /******************** UPDATE ********************/
      public function update($data, $id){
         $result = $this->db->get_where("prjct_cat",array('cat'=>$data['cat'], 'p_id'=>$data['p_id'], 'icon'=>$data['icon']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            $this->db->set($data);
            $this->db->where("id", $id);
            if ($this->db->update("prjct_cat", $data)) {
               return "success"; 
            }else{
               return "error";
            }
         }else{
            return "data exists";
         }
      }


      
   } 
?> 