<?php
class Call_Report_Model extends CI_Model
{

   function __construct()
   {
      parent::__construct();
   }

   public function load_details($userdata, $date1, $date2, $val, $agent)
   {
      if ($val == '12' || $val == '15') {
         $txt = '';
      } else {
         $txt = " AND emp_accNo = '$userdata'";
      }
      if ($agent != '') {
         $txt .= " AND emp_accNo = '$agent'";
      }
      $sql = "SELECT con_list.*, Employee.Name AS emp_name, Employee.surname from con_list INNER JOIN Employee ON 
         con_list.emp_accNo = Employee.Acc_No where con_list.date BETWEEN '$date1 00:00:00'  AND '$date2 23:59:59'" . $txt . " ORDER BY tic_id DESC";
      $query = $this->db->query($sql);

      return $query->result_array();
   }



   public function details($userdata, $val)
   {
      if ($val == '12' || $val == '15') {
         $txt = '';
      } else {
         $txt = " where emp_accNo = '$userdata'";
      }
      $sql = "SELECT * from con_list " . $txt;
      $query = $this->db->query($sql);
      $register = 0;
      $reject = 0;
      $accept = 0;
      foreach ($query->result_array() as $res) {
         $flag = $res['flag'];
         if ($flag == '1') {
            $accept = $accept + 1;
         } else if ($flag == '2' || $flag == '4') {
            $register = $register + 1;
         } else if ($flag == '3') {
            $reject = $reject + 1;
         }
      }
      if ($val == '12' || $val == '15') {
         $txt2 = '';
      } else {
         $txt2 = " where uid = $userdata";
      }
      $qury = $this->db->query("SELECT Employee.Name FROM Usert join Employee ON Usert.acc_no = Employee.Acc_No " . $txt2);
      $row = $qury->row();
      $name = $row->Name;

      $total = $register + $reject + $accept;

      $result = array('Name' => $name, 'Registered' => $register, 'Rejected' => $reject, 'accept' => $accept, 'total' => $total);

      return $result;
   }

   /******************** LOAD AGENT ********************/
   public function load_agent()
   {
      $this->db->select('Employee.*, Usert.Val');
      $this->db->from('Employee');
      $this->db->join('Usert', 'Employee.Acc_No = Usert.acc_no');
      $this->db->where('Usert.Val', '13');
      $this->db->where('Usert.stt <>', '2');
      $this->db->order_by('Employee.Acc_No');
      $query = $this->db->get();

      return $query->result();
   }

   public function load_agent_except_me($emp)
   {
      $this->db->select('Employee.*, Usert.Val');
      $this->db->from('Employee');
      $this->db->join('Usert', 'Employee.Acc_No = Usert.acc_no');
      $this->db->where('Usert.Val', '13');
      $this->db->where('Usert.stt <>', '2');
      $this->db->where('Usert.acc_no <>', $emp);
      $this->db->order_by('Employee.Acc_No');
      $query = $this->db->get();

      return $query->result();
   }

   public function agent_reject_report($agent = null, $date1 = NULL, $date2 = NULL)
   {
      $this->db->select('con_list.*, Employee.Name as emp_name')->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where('con_list.flag', 3);
      $this->db->where("con_list.agent_reject_date BETWEEN '$date1'" . " AND '$date2'");

      if ($agent != '') {
         $this->db->where('emp_accNo', $agent);
      }

      $this->db->order_by('agent_reject_date', 'DESC');

      $query = $this->db->get();
      return $query->result_array();
   }

   /******************** REMINDER REPORT ********************/
   public function reminder_report($agent = null, $date1 = NULL, $date2 = NULL)
   {
      $this->db->select('con_list.*, Employee.Name as emp_name')->from('con_list');
      $this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
      $this->db->where("con_list.reminder_date BETWEEN '$date1 " . "00:00:00 '" . " AND '$date2 23:59:59'");

      if ($agent != '') {
         $this->db->where('emp_accNo', $agent);
      }

      $this->db->order_by('date', 'DESC');

      $query = $this->db->get();
      return $query->result_array();
   }

   /******************** RESET REMINDER ********************/
   public function reset_reminder($data, $id)
   {
      $this->db->set($data);
      $this->db->where("con_no", $id);
      if ($this->db->update("con_list", $data)) {
         return "success";
      } else {
         return "error";
      }
   }


   /******************** GET HISTORY********************/
   public function get_history($id)
   {
      $today = date('Y-m-d');

      $query = $this->db->query("SELECT `reminders`.*, con_list.con_no, con_list.whatsapp, con_list.name AS st_name, `Usert`.`Val`, `Employee`.`Name`, `Employee`.`surname` FROM `reminders` JOIN `Usert` ON `reminders`.`sender` = `Usert`.`acc_no` JOIN `Employee` ON `Usert`.`acc_no` = `Employee`.`Acc_No` JOIN con_list ON reminders.con_id = con_list.id WHERE (`con_id` = '$id') ORDER BY `reminders`.`dt_time`");

      return $query->result();
   }

   /******************** ADD TICKET ********************/
   public function add_ticket($data)
   {
      $con_id = $data['con_id'];
      $sender = $data['sender'];
      $this->db->select('emp_accNo');
      $this->db->from('con_list');
      $this->db->where('id', $con_id);
      $query = $this->db->get();

      $row = $query->row();

      $data['receiver'] = $row->emp_accNo;
      $receiver = $data['receiver'];

      if ($sender == $receiver) {
         $data['sender_flag'] = 0;
      } else {
         $data['sender_flag'] = 1;
         $data['flag'] = 1;
      }

      if ($this->db->insert("reminders", $data)) {
         $insert_id = $this->db->insert_id();

         $qry = $this->db->query("SELECT * FROM reminders WHERE reply_id = '$insert_id'");
         $row = $qry->row_array();
         $reply_id = $data['reply_id'];

         if ($reply_id == '') {
            $this->db->query("UPDATE reminders SET reply_id = '$insert_id' WHERE  id='$insert_id'");
         } else {
            $this->db->query("UPDATE reminders SET reply_id = '$reply_id' WHERE  id='$insert_id'");
         }
         $this->db->query("UPDATE con_list SET tic_id = '$insert_id' WHERE  id='$con_id'");

         return "success";
      } else {
         return "error";
      }
   }
   /******************** SEND REPLY ********************/
   public function send_reply($data)
   {
      $con_id = $data['con_id'];
      $sender = $data['sender'];

      $this->db->select('emp_accNo');
      $this->db->from('con_list');
      $this->db->where('id', $con_id);
      $query = $this->db->get();

      $row = $query->row();

      $data['receiver'] = $row->emp_accNo;
      $receiver = $data['receiver'];

      if ($sender == $receiver) {
         $data['sender_flag'] = 0;
      } else {
         $data['sender_flag'] = 1;
         $data['flag'] = 1;
      }

      if ($this->db->insert("reminders", $data)) {
         $this->db->query("UPDATE reminders SET sender_flag= '3' WHERE sender_flag = '2' AND sender='$sender' AND con_id='$con_id'");
         return "success";
      } else {
         return "error";
      }
   }

   /******************** UPDATE DETAILS ********************/
   public function update_flag($data, $id)
   {
      $this->db->set($data);
      $this->db->where("reply_id", $id);
      $this->db->where("flag<>", '2');
      if ($this->db->update("reminders", $data)) {
         $this->db->query("UPDATE reminders SET sender_flag= '2' WHERE sender_flag = '1'AND reply_id='$id'");
         return "success";
      } else {
         return "error";
      }
   }

   /******************** CLOSE CHAT ********************/
   public function close_chat($data, $id, $accNo, $id_up)
   {
      $today = date('Y-m-d');
      // $this->db->set($data);
      // $this->db->where("tic_id", $id);
      // if ($this->db->update("con_list", $data)) {

      $this->db->query("UPDATE reminders SET flag= '3' WHERE reply_id='$id' AND DATE(dt_time)<= '$today' AND receiver='$accNo'");

      $this->db->query("UPDATE reminders SET sender_flag= '2' WHERE sender_flag='1' AND DATE(dt_time)<= '$today' AND receiver='$accNo' AND con_id='$id_up'");
      $xxx = "UPDATE reminders SET sender_flag= '3' WHERE (sender_flag='2') AND con_id='$id_up' AND sender='$accNo'";
      $this->db->query($xxx);
      return "success";
      // } else {
      //    return "error";
      // }
   }

   /************************** GET MESSAGE COUNT *************************/
   public function message_count($acc_no, $val)
   {
      $today = date('Y-m-d H:i:s');
      $msg_cnt = 0;

      $qry2 = $this->db->query(("SELECT COUNT(id) AS msg_cnt FROM reminders WHERE (receiver='$acc_no' AND flag='1') OR (sender='$acc_no' AND sender_flag='2')  AND dt_time <=  '$today'"));
      $row = $qry2->row();

      $msg_cnt = $row->msg_cnt;

      return $msg_cnt;
   }

   /************************** UNREAD MESSAGES *************************/
   public function unread_msg($acc_no, $val)
   {
      $today = date('Y-m-d H:i:s');

      $qry2 = $this->db->query(("SELECT reminders.*, con_list.con_no FROM reminders INNER JOIN con_list ON reminders.con_id=con_list.id WHERE (receiver='$acc_no' AND reminders.flag='1') OR (sender='$acc_no' AND sender_flag='2')  AND dt_time <=  '$today'"));
      return $qry2->result();
   }

   /************************** TIMELY NOTI *************************/
   public function regular_noti($acc_no, $val)
   {
      $today = date('Y-m-d H:i:s');
      $currentDate = strtotime($today);
      $futureDate = $currentDate + (60 * 10);
      $nxt_noti = date("Y-m-d H:i:s", $futureDate);

      $qry2 = $this->db->query(("SELECT reminders.*, con_list.con_no FROM reminders INNER JOIN con_list ON reminders.con_id=con_list.id WHERE receiver='$acc_no' AND reminders.flag='1' AND (dt_time >= '$today' AND  dt_time <= '$nxt_noti' ) AND (sender='$acc_no')"));
      return $qry2->result();
   }

   /******************** AGENT TRANSFER ********************/
   public function agent_tranfer($data, $id)
   {
      $query = $this->db->query("SELECT reg_no FROM con_list where id='$id'");
      $row = $query->row();
      $regNo = $row->reg_no;
      $accNo = $data['emp_accNo'];

      $query1 = $this->db->query("SELECT agent_code FROM Usert where acc_no='$accNo'");
      $row1 = $query1->row();
      $agent_code = $row1->agent_code;

      if ($regNo == '') {
         $this->db->select('reg_no');
         $this->db->from('invoice_no');
         $query2 = $this->db->get();
         $row2 = $query2->row();

         $reg = $row2->reg_no;
         $new_reg = "WR" . $agent_code . $reg;

         $nw_reg = $reg + 1;

         $this->db->query("UPDATE invoice_no SET reg_no='$nw_reg'");
      }else{
         $rg = substr($regNo, 3);
         $new_reg = 'WR' . $agent_code . $rg; 
      }
      
      $data['reg_no'] = $new_reg;
      $data['self_reg'] = '0';

      $this->db->set($data);
      $this->db->where("id", $id);
      if ($this->db->update("con_list")) {
         return "success";
      } else {
         return "error";
      }
   }
}
