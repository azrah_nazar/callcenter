<?php
class Pre_registry_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	/************************** FOLLOW UPS *************************/
	public function load_follow_up($emp)
	{
		$date = date('Y-m-d');
		$now = date('Y-m-d H:i:s');
		$last_week =  date('Y-m-d', strtotime($now. ' - 7 days'));
		$sql = "SELECT * FROM con_list where emp_accNo='$emp' AND (flag = 1) AND (reminder_date IS NULL OR reminder_date='0000-00-00' OR reminder_date <='$date') AND (workshop ='' OR workshop IS NULL OR workshop='0') AND feedback_reject!='1' AND pending_payment<>'1' AND reapplied_reject<>'1' AND direct_sale<>'1' ORDER BY  reminder_date ASC, date1 DESC";
		$query = $this->db->query($sql);

		return $query->result();
	}

	/************************** PENDING COUNT *************************/
	public function pending_count($emp)
	{
		$date = date('Y-m-d');
		$sql = "SELECT COUNT(reg_no) AS pending_count FROM con_list where emp_accNo='$emp' AND (flag = 1) AND (reminder_date IS NULL OR reminder_date='0000-00-00' OR reminder_date <='$date') AND (workshop ='' OR workshop IS NULL OR workshop='0') AND feedback_reject!='1' AND pending_payment<>'1' AND direct_sale<>'1' ORDER BY  reminder_date ASC, date1 DESC";
		$query = $this->db->query($sql);

		return $query->row_array();
	}

	/************************** CRON JOB *************************/
	public function cron_job()
	{
		$date = date('Y-m-d');
		$now = date('Y-m-d H:i:s');
		$last_week =  date('Y-m-d', strtotime($now. ' - 4 days'));
		$sql = "SELECT * FROM con_list where (flag = 1) AND (reminder_date IS NULL OR reminder_date='0000-00-00' OR reminder_date <='$date') AND (workshop ='' OR workshop IS NULL) AND feedback_reject!='1' AND pending_payment!='1' ORDER BY date1 DESC";
		$query = $this->db->query($sql);

		foreach ($query->result_array() as $row) {
			$con_no = $row['con_no'];

			$this->db->query("UPDATE con_list SET feedback_reject='1', reject_date='$now', pending_payment='0' WHERE con_no='$con_no' AND date1 <= '$last_week' AND pending_payment!='1' AND direct_sale!='1'");
		}

		return $query->result();
	}

	/************************** STUDENT DETAILS *************************/
	public function get_st_ws_details($reg)
	{
		$this->db->select('con_list.*, Employee.sinhala_name, Employee.Tp, Employee.tpOffice, workshops.w_time,workshops.w_date, workshops.id AS wid');
		$this->db->from('con_list');
		$this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
		$this->db->join('workshops', 'con_list.workshop = workshops.id');
		$this->db->where('reg_no', $reg);
		$query = $this->db->get();

		return $query->row_array();
	}

	/************************** STUDENT WS DETIALS *************************/
	public function get_st_details($reg)
	{
		$this->db->select('con_list.*, Employee.sinhala_name, Employee.Tp, Employee.tpOffice');
		$this->db->from('con_list');
		$this->db->join('Employee', 'con_list.emp_accNo = Employee.Acc_No');
		$this->db->where('reg_no', $reg);
		$query = $this->db->get();

		return $query->row_array();
	}

	/************************** PAYMENT DETAILS *************************/
	public function get_pay_details($inv_no, $inv_id)
	{
		$sql = "SELECT con_list.reg_no, con_list.usdt_val, con_list.town, bank_deposits.descrip, SUM(bank_deposits.cr) AS tot, bank_deposits.cr, con_list.name, bank_deposits.inv_no, bank_deposits.bank, bank_deposits.inv_img, bank_deposits.deposit_date, bank_deposits.p_type, bank_deposits.new_inv, bank_deposits.koko_amt, bank_deposits.bank_amt FROM con_list INNER JOIN bank_deposits ON con_list.reg_no=bank_deposits.reg_no WHERE inv_no='$inv_no' AND bank_deposits.id='$inv_id' AND bank_deposits.flag <> '2' GROUP BY bank_deposits.reg_no";
		$query = $this->db->query($sql);

		return $query->row_array();
	}

	/************************** PAYMENT DETAILS *************************/
	public function get_pay_details2($inv_no, $inv_id)
	{
		$sql = "SELECT con_list.reg_no, con_list.town, bank_deposits.descrip, SUM(bank_deposits.cr) AS tot, bank_deposits.cr, con_list.name, bank_deposits.inv_no, COUNT(bank_deposits.reg_no) AS reg_cnt FROM con_list INNER JOIN bank_deposits ON con_list.reg_no=bank_deposits.reg_no WHERE inv_no='$inv_no' AND bank_deposits.id<'$inv_id' AND bank_deposits.flag <> '2' GROUP BY bank_deposits.reg_no";
		$query = $this->db->query($sql);

		return $query->row_array();
	}

	/******************** UPDATE ********************/
	public function update_details($data, $reg_no, $con_no)
	{
		$this->db->set($data);
		$this->db->where("reg_no", $reg_no);
		$this->db->where("con_no", $con_no);
		if ($this->db->update("con_list")) {
			return "success";
		} else {
			return "error";
		}
	}

	/************************** FOLLOW UPS *************************/
	public function registered_list($emp)
	{
		$date = date('Y-m-d');

		if ($emp == '') {
			$txt = '';
		} else {
			$txt = " AND emp_accNo = '$emp'";
		}
		$sql = "SELECT con_list.*, Employee.Name AS op_name FROM con_list JOIN Employee ON con_list.emp_accNo = Employee.Acc_No where (con_list.flag = 4) AND (reminder_date IS NULL OR reminder_date <='$date') AND workshop !='' AND feedback_reject!='1'" . $txt;
		$query = $this->db->query($sql);

		return $query->result_array();
	}

	/************************** COMMON SMS *************************/
	public function common_sms($reg_no, $msg)
	{
		$this->db->select('con_no');
		$this->db->from('con_list');
		$this->db->where('reg_no', $reg_no);
		$query = $this->db->get();
		$row = $query->row();
		$mobile_no = $row->con_no;

		$message =  $msg;
		$data['message'] = trim($message);
		$count = ceil(strlen($message) / 160);

		$url = 'https://smsserver.textorigins.com/Send_sms?';

		$success_count = 0;

		$data_sms = array(
			'src' => 'CYCLOMAX257', //Mask
			'email' => 'newvictoryacademy@gmail.com ', //User Email
			'pwd' => 'Vic66556', //User Password
			'msg' => $message, //Message
			'dst' => $mobile_no
		); //Phone Number

		$msg = http_build_query($data_sms);

		$url .= $msg;

		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result_set = curl_exec($ch);

		if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
			$success_count++;
		}


		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);


		$result['count'] = $success_count;
		$result['status'] = '1601';
		$result['message'] = 'Successfully Sent';

		//echo json_encode($result);

	}


	/******************** BANK DEPOSIT ********************/
	public function bank_deposit($data, $is_usdt, $reg_no, $name_ini, $town, $nic, $telNo)
	{	
		$query = $this->db->query("SELECT Employee.Name, Employee.Acc_No, Usert.agent_code, Usert.Val, con_list.con_no FROM con_list JOIN Employee ON con_list.emp_accNo = Employee.Acc_No JOIN Usert ON con_list.emp_accNo = Usert.acc_no  WHERE con_list.reg_no='$reg_no'");

		$p_type = $data['p_type'];
		$new_inv = '';
		if($p_type == 'Full Payment' || $p_type == 'Balance Payment'){
			$q =$this->db->query("SELECT new_inv FROM invoice_no");
			$r = $q->row();
			$new_inv = $r->new_inv;
		}


		$row = $query->row();
        $emp_name = $row->Name;
        $emp_acc = $row->Acc_No;
        $agent_code = $row->agent_code;
        $desig = $row->Val;
        $con_no = $row->con_no;

		$data['emp_acc'] = $emp_acc;
		$data['emp_name'] = $emp_name;
		$data['agent_code'] = $agent_code;
		$data['desig'] = $desig;
		$data['con_no'] = $con_no;
		$data['new_inv'] = $new_inv;
		if ($this->db->insert("bank_deposits", $data)) {
			$this->db->query("UPDATE con_list SET usdt_val='$is_usdt', usdt_marked='1', name='$name_ini', town='$town', nic='$nic' where con_no='$telNo'");

			if($p_type == 'Full Payment' || $p_type == 'Balance Payment'){
				$this->db->query("UPDATE invoice_no SET new_inv = new_inv+1");
			}
			return "success";
		} else {
			return "error";
		}
	}

	/******************** CONTACT SEARCH ********************/
	public function searchKeyword($searchterm, $val, $acc_no)
	{
		$first_char = substr($searchterm, 0, 1);
		$without_first_char = substr($searchterm, 1);

		if ($first_char == '0') {
			$srch = " OR con_list.con_no='$without_first_char' OR whatsapp='$without_first_char' ";
		} else {
			$srch = " OR con_list.con_no='0$searchterm' OR whatsapp='0$searchterm' ";
		}

		$query = $this->db->query("SELECT con_list.id AS cid, con_list.flag, con_list.feedback_reject, con_list.name, con_list.con_no, con_list.reg_no, con_list.date, Employee.Name AS emp_name, con_list.emp_accNo, con_list.workshop, con_list.tic_id, con_list.whatsapp, con_list.spl_cus, con_list.reject_date, con_list.pending_payment, con_list.direct_sale, con_list.self_reg, Usert.agent_code, con_list.self_reg, con_list.reapplied_reject_date, con_list.agent_reject_date, con_list.all_paid FROM con_list JOIN Employee ON con_list.emp_accNo = Employee.Acc_No JOIN Usert ON con_list.emp_accNo = Usert.acc_no WHERE con_list.con_no = '$searchterm' OR con_list.reg_no='$searchterm' OR whatsapp='$searchterm'" . $srch . " ORDER BY date DESC");

		return $query->result_array();
	}
}
