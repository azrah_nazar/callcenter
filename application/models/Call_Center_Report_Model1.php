<?php 
class Call_Center_Report_Model extends CI_Model {
	
   function __construct() { 
      parent::__construct(); 
   } 

   public function details($userdata) {
      $this->db->select('*');
      $this->db->from(' con_list ');
      $this->db->where('userid.ID', $userdata);
      $details = $this->db->get()->row();

      return $details;
   }

}
?>