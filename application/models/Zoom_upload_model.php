<?php
class Zoom_upload_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function load_data($workshop)
    {
        $this->db->select('zoom_attendance.*,  workshops.w_name');
        $this->db->from('zoom_attendance');
        $this->db->join('con_list', 'zoom_attendance.reg_no = con_list.reg_no');
        $this->db->join('workshops', ' workshops.id = con_list.workshop');

        if ($workshop != '') {
            $this->db->where('workshops.id', $workshop);
        }

        $this->db->order_by('con_list.workshop');
        $this->db->order_by('zoom_attendance.id', 'DESC');
        $query = $this->db->get();

        return $query->result();
    }

    public function search_reg_no()
    {
        $keyword = strip_tags(trim($_REQUEST['term']));

        $sugg_json = array();
        $json_row = array();
        $key = preg_replace('/\s+/', ' ', $keyword);

        $sql = "SELECT reg_no FROM con_list WHERE reg_no LIKE '%$key%' GROUP BY reg_no ORDER BY reg_no";
        $query = $this->db->query($sql);
        $telArr = $query->result_array();

        foreach ($telArr as $recResult) {
            $json_row["value"] = $recResult['reg_no'];
            $json_row["label"] = $recResult['reg_no'];
            array_push($sugg_json, $json_row);
        }

        $jsonOutput = json_encode($sugg_json, JSON_UNESCAPED_SLASHES);
        print $jsonOutput;
    }


    /******************** INSERT ********************/
    public function add($data)
    {

        $result = $this->db->get_where("zoom_attendance", array('reg_no' => $data['reg_no']));
        $rowcount = $result->num_rows();
        if ($rowcount == 0) {
            if ($this->db->insert("zoom_attendance", $data)) {
                $st = 'success';
            } else {
                $st = 'error';
            }
        } else {
            $st = "exist";
        }
        return $st;
    }


    public function insert_csv($fp, $user_id)
    {
        $st = 'success';
        while (($value = fgetcsv($fp, 1000, ",")) !== FALSE) {
            $reg_no   = $value[0];
            $email  = $value[1];
            $join_time  = $value[2];
            $leave_time = $value[3];
            $duration = $value[4];
            $is_guest = $value[5];

            if (!empty($reg_no)) {
                $reg_no = htmlspecialchars(strip_tags(trim($reg_no)), ENT_QUOTES, 'UTF-8');
                $email = htmlspecialchars(strip_tags(trim($email)), ENT_QUOTES, 'UTF-8');
                $join_time = htmlspecialchars(strip_tags(trim($join_time)), ENT_QUOTES, 'UTF-8');
                $leave_time = htmlspecialchars(strip_tags(trim($leave_time)), ENT_QUOTES, 'UTF-8');
                $duration = htmlspecialchars(strip_tags(trim($duration)), ENT_QUOTES, 'UTF-8');
                $is_guest = htmlspecialchars(strip_tags(trim($is_guest)), ENT_QUOTES, 'UTF-8');

                $data = array(
                    'reg_no' => $reg_no,
                    'user_email' => $email,
                    'join_time' => $join_time,
                    'leave_time' => $leave_time,
                    'duration' => $duration,
                    'is_guest' => $is_guest,
                    'user_id' => $user_id,
                    'ins_date' => date("Y-m-d H:i:s"),
                    'sms_flag' => 0

                );

                $result = $this->db->get_where("zoom_attendance", array('reg_no' => $reg_no));
                $rowcount = $result->num_rows();

                if ($rowcount == 0) {
                    if ($this->db->insert("zoom_attendance", $data)) {

                        $this->db->select('con_no');
                        $this->db->from('con_list');
                        $this->db->where('reg_no', $reg_no);
                        $query = $this->db->get();
                        $row = $query->row();
                        $mobile_no = $row->con_no;

                        $message =  "Dear Student, Thank you for attending the workshop. If you are interested to continue, please make your deposit to the given account";
                        $data['message'] = trim($message);
                        $count = ceil(strlen($message) / 160);

                        $url = 'https://smsserver.textorigins.com/Send_sms?';

                        $success_count = 0;

                        $data_sms = array(
                            'src' => 'CYCLOMAX257', //Mask
                            'email' => 'newvictoryacademy@gmail.com ', //User Email
                            'pwd' => 'Vic66556', //User Password
                            'msg' => $message, //Message
                            'dst' => $mobile_no
                        ); //Phone Number

                        $msg = http_build_query($data_sms);

                        $url .= $msg;

                        $ch = curl_init($url);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_HEADER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        $result_set = curl_exec($ch);

                        if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
                            $success_count++;

                            $this->db->query("UPDATE zoom_attendance SET sms_flag= '1' WHERE reg_no='$reg_no'");
                        }

                        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);
                    } else {
                        $st = 'error';
                    }
                } else {
                    $this->db->select('sms_flag');
                    $this->db->from('zoom_attendance');
                    $this->db->where('reg_no', $reg_no);
                    $qry = $this->db->get();
                    $rw = $qry->row();
                    $sms_flag = $rw->sms_flag;

                    if ($sms_flag == 0) {
                        $this->db->select('con_no');
                        $this->db->from('con_list');
                        $this->db->where('reg_no', $reg_no);
                        $query = $this->db->get();
                        $row = $query->row();
                        $mobile_no = $row->con_no;

                        $message =  "Dear Student, Thank you for attending the workshop. If you are interested to continue, please make your deposit to the given account";
                        $data['message'] = trim($message);
                        $count = ceil(strlen($message) / 160);

                        $url = 'https://smsserver.textorigins.com/Send_sms?';

                        $success_count = 0;

                        $data_sms = array(
                            'src' => 'CYCLOMAX257', //Mask
                            'email' => 'newvictoryacademy@gmail.com ', //User Email
                            'pwd' => 'Vic66556', //User Password
                            'msg' => $message, //Message
                            'dst' => $mobile_no
                        ); //Phone Number

                        $msg = http_build_query($data_sms);

                        $url .= $msg;

                        $ch = curl_init($url);

                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_HEADER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        $result_set = curl_exec($ch);

                        if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
                            $success_count++;

                            $this->db->query("UPDATE zoom_attendance SET sms_flag= '1' WHERE reg_no='$reg_no'");
                        }

                        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);
                    }
                    $st = "success";
                }
            }
        }
        fclose($fp);
        return $st;
    }

    public function load_attendance($agent, $workshop)
    {
        $this->db->select('con_list.name, con_list.reg_no, con_list.whatsapp, con_list.con_no, workshops.w_name, con_list.spl_cus, con_list.workshop, SUM(bank_deposits.cr) AS deposits, bank_deposits.is_usdt, bank_deposits.flag AS pay_flag, con_list.reminder_date, con_list.tic_id, con_list.id, con_list.emp_accNo, con_list.spl_cus, con_list.date1, con_list.job, con_list.site_flag, con_list.user_id, con_list.town, bank_deposits.inv_no,  con_list.usdt_val, con_list.usdt_marked, con_list.nic');
        $this->db->from('con_list');
        $this->db->join('workshops', 'con_list.workshop = workshops.id', 'LEFT OUTER');
        $this->db->join('bank_deposits', 'con_list.reg_no = bank_deposits.reg_no', 'LEFT OUTER');
        if ($agent != '') {
            $this->db->where('con_list.emp_accNo', $agent);
        }
        $this->db->where('con_list.reapplied_reject <>', 1);
        // $this->db->where('bank_deposits.flag <>', '1');
        $this->db->group_start();
        $this->db->where('con_list.pending_payment', '1');
        $this->db->or_where('con_list.direct_sale', '1');
        $this->db->group_end();
        $this->db->where('con_list.all_paid <>', '1');

        // if ($workshop != '') {
        //     $this->db->where('con_list.workshop', $workshop);
        // }
        $this->db->group_by('con_list.name, con_list.reg_no, con_list.whatsapp, con_list.reminder_date, con_list.tic_id, con_list.id, con_list.emp_accNo, con_list.spl_cus, con_list.date1, con_list.job, con_list.site_flag, con_list.user_id');
        $this->db->order_by('con_list.reg_no', 'DESC');

        $query = $this->db->get();

        return $query->result();
    }

    public function pay_flag()
    {
        // $this->db->select('con_list.reg_no, con_list.con_no, bank_deposits.flag, COUNT(bank_deposits.flag) AS p_flag_cnt, bank_deposits.inv_no, SUM(bank_deposits.cr) AS depps');
        // $this->db->from('con_list');
        // $this->db->join('bank_deposits', 'con_list.reg_no = bank_deposits.reg_no');
        // $this->db->where('con_list.reg_no !=', '');
        // $this->db->where('bank_deposits.flag!=', '2');
        // $this->db->group_by('con_list.reg_no');

        // $query = $this->db->get();

        // return $query->result();
    }

    public function bank_control()
    {
        $this->db->select('con_list.name, con_list.reg_no, con_list.con_no, bank_deposits.deposit_date, bank_deposits.cr, bank_deposits.descrip, bank_deposits.id AS bid, bank_deposits.flag, bank_deposits.bank, bank_deposits.p_type, con_list.nic');
        $this->db->from('con_list');
        $this->db->join('bank_deposits', 'con_list.reg_no = bank_deposits.reg_no');
        $this->db->where('con_list.reg_no !=', '');
        $this->db->where('bank_deposits.flag', '0');
        $this->db->order_by('bank_deposits.ac_rej_date', 'DESC');
        $this->db->order_by('bank_deposits.id', 'DESC');


        // if ($agent != '') {
        //     $this->db->where('con_list.emp_accNo', $agent);
        // }
        // if ($workshop != '') {
        //     $this->db->where('con_list.workshop', $workshop);
        // }

        $query = $this->db->get();

        return $query->result();
    }

    public function response_update($res_flag, $bid, $reg)
    {
        $this->db->select('inv_no')->from('invoice_no');
        $query = $this->db->get();
        $inv_q = $query->row_array();

        $result = $this->db->query("SELECT MAX(inv_no) AS inv_no FROM bank_deposits WHERE reg_no = '$reg'");
        $crr = $result->row_array();
        $rowcount = $result->num_rows();
        if ($rowcount == 1) {

            $agent_code = substr($reg, 2, 1);
            $inv = $inv_q['inv_no'];

            if($crr['inv_no'] == ''){
                $inv_no = 'CR' . $agent_code . $inv;
                $new_inv_no = $inv_q['inv_no'] + 1;
                if ($res_flag <> '2') {
                    $this->db->update('invoice_no', array('inv_no' => $new_inv_no));
                }
            }else{
                $inv_no = $crr['inv_no'];
            }

        } else {
            $agent_code = substr($reg, 2, 1);
            $inv = $inv_q['inv_no'];

            if($crr['inv_no'] == ''){
                $inv_no = 'CR' . $agent_code . $inv;
                $new_inv_no = $inv_q['inv_no'] + 1;
                if ($res_flag <> '2') {
                    $this->db->update('invoice_no', array('inv_no' => $new_inv_no));
                }
            }else{
                $inv_no = $crr['inv_no'];
            }
        }
        $today = date('Y-m-d H:i:s');

        $query = $this->db->query("UPDATE bank_deposits SET flag='$res_flag', ac_rej_date='$today' WHERE id='$bid'");
        if ($query) {
            if ($res_flag <> '2') {
                $query2 = $this->db->query("UPDATE bank_deposits SET inv_no='$inv_no' WHERE id='$bid'");

                $this->db->select('SUM(cr) AS tot_dep, con_list.class_sms, con_list.con_no, con_list.usdt_val');
                $this->db->from('bank_deposits');
                $this->db->join('con_list', 'bank_deposits.reg_no = con_list.reg_no');
                $this->db->where('con_list.reg_no', $reg);
                $this->db->where('bank_deposits.flag <>', '2');
                $query = $this->db->get();
                $row = $query->row();
                $tot_dep = $row->tot_dep;
                $class_sms = $row->class_sms;
                $mobile_no = $row->con_no;
                $is_usdt = $row->usdt_val;

                if ($is_usdt == 1) {
                    if ($tot_dep == '17500') {
                        $query = $this->db->query("UPDATE con_list SET all_paid='1', flag='6' WHERE con_no='$mobile_no'");
                    }
                } else {
                    if ($tot_dep == '15000') {
                        $query = $this->db->query("UPDATE con_list SET all_paid='1', flag='6' WHERE con_no='$mobile_no'");
                    }
                }

                // $msg = "ඔබගේ පාඨමාලා ගාස්තුවට අදාල මුදල් අප ගිණුමට සාර්ථකව බැරවී ඇති බව සතුටින් තහවුරු කරමු. පහත ලින්ක් එක භාවිත කර ඔබගේ රිසිට්පත ලබාගන්න.\nhttps://callcenter.victoryacademylk.com/payment-details/" . trim($inv_no) . "";

                // $urll = 'https://smsserver.textorigins.com/Send_sms?';

                // $success_count2 = 0;

                // $data_sms_pay = array(
                //     'src' => 'CYCLOMAX257', //Mask
                //     'email' => 'newvictoryacademy@gmail.com ', //User Email
                //     'pwd' => 'Vic66556', //User Password
                //     'msg' => $msg, //Message
                //     'dst' => $mobile_no
                // ); //Phone Number

                // $msg = http_build_query($data_sms_pay);

                // $urll .= $msg;

                // $ch2 = curl_init($urll);

                // curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, false);
                // curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
                // curl_setopt($ch2, CURLOPT_HEADER, false);
                // curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);

                // $result_set2 = curl_exec($ch2);

                // if (preg_replace('/\s+/', '', $result_set2) == '{"status":"1601","message":"SMSSendSuccessful"}') {
                //     $success_count2++;
                // }

                // $http_status = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                // curl_close($ch2);

                // if ($tot_dep >= 10000 && $class_sms == 0) {
                //     $this->db->select('con_no');
                //     $this->db->from('con_list');
                //     $this->db->where('con_list.reg_no', $reg);
                //     $query = $this->db->get();
                //     $row = $query->row();

                //     $message = "Dear Student, Please use the below link to register to the class. https://callcenter.hataccounts.com/Con_post_registry/class_register/" . trim($reg);
                //     $data['message'] = trim($message);

                //     $url = 'https://smsserver.textorigins.com/Send_sms?';

                //     $success_count = 0;

                //     $data_sms = array(
                //         'src' => 'CYCLOMAX257', //Mask
                //         'email' => 'newvictoryacademy@gmail.com ', //User Email
                //         'pwd' => 'Vic66556', //User Password
                //         'msg' => $message, //Message
                //         'dst' => $mobile_no
                //     ); //Phone Number

                //     $msg = http_build_query($data_sms);

                //     $url .= $msg;

                //     $ch = curl_init($url);

                //     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                //     curl_setopt($ch, CURLOPT_HEADER, false);
                //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //     $result_set = curl_exec($ch);

                //     if (preg_replace('/\s+/', '', $result_set) == '{"status":"1601","message":"SMSSendSuccessful"}') {
                //         $success_count++;

                //         $this->db->query("UPDATE con_list SET class_sms= '1' WHERE reg_no='$reg'");
                //     }

                //     $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                //     curl_close($ch);
                // }
            } else {
                $query = $this->db->query("UPDATE con_list SET all_paid='0' WHERE reg_no='$reg'");
            }

            return "success";
        } else {
            return "error";
        }
    }

    /******************** UPDATE ********************/
    public function update_zoom($reg)
    {
        $query = $this->db->query("UPDATE zoom_attendance SET sms_flag= '1' WHERE reg_no='$reg'");
        if ($query) {
            return "success";
        } else {
            return "error";
        }
    }

    /******************** GET USDT ********************/
    public function get_usdt($reg)
    {
        $this->db->select('usdt_val, usdt_marked');
        $this->db->from('con_list');
        $this->db->where('reg_no', $reg);
        $query = $this->db->get();

        return $query->result();
    }

    /******************** GET ACCEPTED DEPOSITS ********************/
    public function get_accepted_deposits($reg)
    {
        $this->db->select('SUM(cr) AS totdep');
        $this->db->from('bank_deposits');
        $this->db->where('reg_no', $reg);
        $this->db->where('flag !=', '2');
        $query = $this->db->get();

        return $query->result();
    }

    /******************** GET COURSE FEE ********************/
    public function course_fee()
    {
        $this->db->select('course_fee');
        $this->db->from('course');
        $this->db->where('id', '4');
        $query = $this->db->get();

        return $query->row_array();
    }

    /******************** FULL PAYMENT ********************/
    public function full_payment($date1 = NULL, $date2 = NULL, $val =  NULL, $emp = NULL)
    {
        $this->db->select('course_fee')->from('course')->where('id', '4');
        $query = $this->db->get();
        $inv_q = $query->row_array();

        $course_fee = $inv_q['course_fee'];

        $this->db->select('(cr) AS tot_dep, bank_deposits.reg_no, bank_deposits.inv_no, con_list.name, con_list.con_no, bank_deposits.id AS inv_id, con_list.nic, bank_deposits.bank, bank_deposits.p_type, con_list.job');
        $this->db->from('con_list');
        $this->db->join('bank_deposits', 'con_list.reg_no = bank_deposits.reg_no');

        if ($val == 13) {
            $this->db->where("con_list.emp_accNo", $emp);
        }

        $this->db->where('bank_deposits.flag', '1');
        $this->db->group_start();
        $this->db->where('bank_deposits.p_type', 'Full Payment');
        $this->db->or_where('bank_deposits.p_type', 'Advanced Payment');
        $this->db->or_where('bank_deposits.p_type', 'Balance Payment');
        $this->db->group_end();

        $this->db->where("DATE(ac_rej_date) BETWEEN '" . $date1 . "' AND '" . $date2 . "'");
        $this->db->order_by("bank_deposits.ac_rej_date", "DESC");
        $this->db->order_by("bank_deposits.id", "DESC");

        $query = $this->db->get();

        return $res = $query->result();
    }
}
