<?php
class User_account_model extends CI_Model
{

   function __construct()
   {
      parent::__construct();
   }

   /******************** LOAD STAFF ********************/
   public function load_staff()
   {
      $query = $this->db->query("SELECT emp FROM Usert");
      $query_result = $query->result();
      $emp = array();
      foreach ($query_result as $row) {
         $emp[] = $row->emp;
      }

      $this->db->select('ID, Name');
      $this->db->from('Employee');
      $this->db->where_not_in('ID', $emp);
      $this->db->order_by('Name', "asc");
      $user = $this->db->get();

      return $user->result();
   }

   /******************** LOAD DATA ********************/
   public function load_data()
   {
      $this->db->select('Employee.Name, Usert.acc_no, Usert.ID, Group_Prof.profile, Usert.uid, Usert.Val, Usert.Pws, Usert.call_ID, Usert.agent_code');
      $this->db->from('Usert');
      $this->db->join('Group_Prof', 'Usert.Val = Group_Prof.id');
      $this->db->join('Employee', 'Usert.acc_no = Employee.Acc_No');
      $this->db->where("Usert.uid !=", "1");
      $user = $this->db->get();

      return $user->result();
   }

   /******************** USERNAME EXISTS ********************/
   public function check_user($username)
   {
      $query = $this->db->query("SELECT * FROM Usert WHERE ID = '$username'");

      $rowcount = $query->num_rows();
      if ($rowcount == 0) {
         return "success";
      } else {
         return "username exists";
      }
   }

   /******************** INSERT ********************/
   public function insert($data)
   {
      $result = $this->db->get_where("Employee", array('ID' => $data['emp']));
      $row = $result->row();
      $acc_no = $row->Acc_No;
      $desig = $row->desig;
      $div = $row->Division;

      $result2 = $this->db->get_where("Usert", array('ID' => $data['ID']));
      $rowcount2 = $result2->num_rows();

      $sql = "SELECT acc_no FROM Usert WHERE acc_no='$acc_no'";
      $result3 = $this->db->query("$sql");
      $rowcount3 = $result3->num_rows();

      if ($rowcount2 == 0 && $rowcount3 == 0) {

         $data['stt'] = 0;
         $data['acc_no'] = $acc_no;
         $data['DV'] = $div;

         if ($this->db->insert("Usert", $data)) {
            return "success";
         } else {
            return "error";
         }
      } else {
         return "data exists";
      }
   }

   /******************** GET DATASET ********************/
   public function get_dataset($id)
   {
      $this->db->select('Employee.Name, Usert.Val, Usert.ID,Usert.brandnew, Usert.acc_no, Usert.uid, Usert.cash, Usert.Pws, Usert.parent_portal, Usert.call_ID, Usert.agent_code');
      $this->db->from('Usert');
      $this->db->join('Employee', 'Usert.acc_no = Employee.Acc_No');
      $this->db->where('Usert.acc_no', $id);
      $query = $this->db->get();

      return $query->result();
   }

   /******************** UPDATE ********************/
   public function update($data, $id)
   {
      $q = $this->db->query("SELECT * FROM Usert WHERE acc_no = '$id'");
      $r = $q->row();
      $cid = $r->call_ID;

      $callID = $data['call_ID'];
      $agent_code = $data['agent_code'];
      $query = $this->db->query("SELECT * FROM Usert WHERE call_ID = '$callID' AND call_ID != 0 AND call_ID != '$cid'");
      $query1 = $this->db->query("SELECT * FROM Usert WHERE agent_code = '$agent_code' AND agent_code!=''");

      $rowcount = $query->num_rows();
      $rowcount1 = $query1->num_rows();
      if ($rowcount1 == 0) {
         if ($rowcount == 0) {
            $this->db->set($data);
            $this->db->where("acc_no", $id);
            if ($this->db->update("Usert", $data)) {
               return "success";
            } else {
               return "error";
            }
         } else {
            return "exists";
         }
      }else{
         return "agent code exists";
      }
   }

   /******************** GET CASHIER ********************/
   public function get_cashier()
   {
      $this->db->select(' Employee.Name, Employee.Acc_No');
      $this->db->from('Usert');
      $this->db->join('Employee', 'Usert.emp = Employee.ID');
      $this->db->where('Usert.cash', '1');
      $query = $this->db->get();

      return $query->result();
   }
}
