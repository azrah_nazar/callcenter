<?php 
   class Designation_model extends CI_Model {
   
      function __construct() { 
         parent::__construct(); 
      } 

      /******************** LOAD DATA ********************/
      public function load_data(){
         $this->db->select('designation.ID, designation.desig, designation_1.desig AS upper_levl');
         $this->db->from('designation');
         $this->db->join('designation AS designation_1', 'designation.up_level = designation_1.ID');
         $this->db->order_by('upper_levl');
         $user = $this->db->get();

         return $user->result();
      }

      /******************** TOP LEVEL MANAGEMENT ********************/
      public function top_level(){
         $this->db->select('*');
         $this->db->from('designation');
         $user = $this->db->get();

         return $user->result();
      }

      /******************** INSERT ********************/
      public function insert($data) {
         $result = $this->db->get_where("designation",array('desig'=>$data['desig'], 'up_level'=>$data['up_level']));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            if ($this->db->insert("designation", $data)) {
               return "success";
            }else{
               return "error";
            }
         }else{
            return "data exists";
         }
      }

      /******************** GET DATASET ********************/
      public function get_dataset($id){
         $this->db->select('*');
         $this->db->from('designation');
         $this->db->where('id', $id);
         $query = $this->db->get();

         return $query->result();
      }

      /******************** UPDATE ********************/
      public function update($data, $id){
         $result = $this->db->get_where("designation",array('desig'=>$data['desig'], 'ID!='=>$id));
         $rowcount = $result->num_rows();
         if ($rowcount == 0) {

            $this->db->set($data);
            $this->db->where("id", $id);
            if ($this->db->update("designation", $data)) {
               return "success"; 
            }else{
               return "error";
            }
         }else{
            return "data exists";
         }
      }

      /******************** DELETE ********************/
      public function delete($data){
         if ($this->db->delete("designation", "ID = ".$data['id'])) {
            return true; 
         }else{
            return "error";
         }
      }

      
   } 
?> 