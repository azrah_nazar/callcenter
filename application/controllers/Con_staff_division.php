<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_staff_division extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$load_data = $this->Staff_division_model->load_data();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('staff_division',$data);
		$this->load->view('layout/footer', $data);
	}

	/******************** INSERT ********************/
	public function add(){

		$data = array(
			'division' => $this->input->post('division')
		);
		
		$result = $this->Staff_division_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Staff_division_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update(){

		$data = array(
			'division' => $this->input->post('division_up')
		);
		
		$id = $this->input->post('id_up');

		
		$result = $this->Staff_division_model->update($data,$id);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** DELETE ********************/
	public function delete(){
		$id = strip_tags($this->input->post('id'));

		$data = array(
			'id' => $this->input->post('id'),
		);

		$result = $this->Staff_division_model->delete($data);
		if ($result) {
			$response['status'] = 'success';
		} else if ($result=='error') {
			$response['status'] = 'error';
		} else if($result=='exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/***************************************************/

}
?>