<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_staff_registration extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($menu = NULL, $id = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$division = $this->Staff_division_model->load_data();
		$data['divisions'] = $division;

		$designation = $this->Designation_model->load_data();
		$data['designations'] = $designation;

		$bank = $this->Staff_registration_model->load_bank();
		$data['banks'] = $bank;


		$edit_det = $this->Staff_registration_model->load_data($id);
		if($edit_det == ''){
			$data['edit_det'] = '';
		}else{
			$data['edit_det'] = $edit_det;
			$data['emp_id'] = $id;
		}

		$this->load->view('layout/header', $data);
		$this->load->view('staff/staff_registration',$data);
		$this->load->view('layout/footer', $data);
	}


	/******************** INSERT ********************/
	public function add(){

		$upload_image = $this->upload_image();

		if($upload_image == ''){

			$data = array(
				'Division' => $this->input->post('division'), 
				'desig' => $this->input->post('desig'),
				'Name' => $this->input->post('name'),
				'sinhala_name' => $this->input->post('sinhala_name'),
				'Nic' => $this->input->post('nic'),
				'Tp' => $this->input->post('tp'),
				'tpOffice' => $this->input->post('tp2'),
				'email' => $this->input->post('email'),
				'appoinment' => $this->input->post('app_date'),
				'gender' => $this->input->post('gender'),
				
			);
		}else{
			$data = array(
				'Division' => $this->input->post('division'), 
				'desig' => $this->input->post('desig'),
				'Name' => $this->input->post('name'),
				'sinhala_name' => $this->input->post('sinhala_name'),
				'Nic' => $this->input->post('nic'),
				'Tp' => $this->input->post('tp'),
				'tpOffice' => $this->input->post('tp2'),
				'email' => $this->input->post('email'),
				'appoinment' => $this->input->post('app_date'),
				'prof_pic' => $upload_image,
				'gender' => $this->input->post('gender'),
			);
		}

		$emp_id = $this->input->post('emp_id');

		$result = $this->Staff_registration_model->add($data, $emp_id);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'updated') {
			echo 'updated';
		}else{
			echo 'error';
		}
	}


	/******************** IMAGE UPLOAD ********************/
	public function upload_image()
	{
		$config['upload_path'] = 'uploads/staff_images';
		$config['file_name'] =  uniqid();
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
        // $config['max_size'] = '10000';


		$this->load->library('upload', $config);
		$this->upload->overwrite = true;
		if ( ! $this->upload->do_upload('image'))
		{
			$error = $this->upload->display_errors();
            //return $error;
			return "";
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$type = explode('.', $_FILES['image']['name']);
			$type = $type[count($type) - 1];

			$path = $config['upload_path'].'/'.$config['file_name'].'.'.$type;
			return ($data == true) ? $path : false;            
		}
	}




	public function get_bank_code(){

		$bank_name = $this->input->post('bank_id');
		$data = $this->Staff_registration_model->load_bank_code($bank_name);
		echo json_encode($data);

	}

	public function get_branch_code(){

		$branch_id = $this->input->post('branch_id');
		$bank_id = $this->input->post('bank_id');
		$data = $this->Staff_registration_model->load_branch_code($branch_id,$bank_id);
		echo json_encode($data);

	}

	public function get_branch() {

		$bank_id = $this->input->post('bank_id');

		$load_branch = $this->Staff_registration_model->get_branch($bank_id );
		$data['records'] = $load_branch;

		echo json_encode($data);

	}

	/***************************************************/
}
?>