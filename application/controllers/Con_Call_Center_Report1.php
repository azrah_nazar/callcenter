<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Con_Call_Center_Report extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('Call_Center_Report_Model');

	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');
		$userdata = strip_tags($this->session->userdata('id'));

		// if($this->session->has_userdata('username'))
		// {
		// 	$acc_no = strip_tags($this->session->userdata('acc_no'));
		// 	$user = strip_tags($this->session->userdata('username'));
		// 	$user_id = strip_tags($this->session->userdata('id'));
		// 	$Val = strip_tags($this->session->userdata('Val'));
		// 	$emp_id = strip_tags($this->session->userdata('emp'));

		// 	$user_details = $this->My_profile_model->user_details($user);
		// 	$data['user_details'] = $user_details;

			// $category = $this->Common_model->categories($user_id, $Val);
			// $data['category'] = $category;

			// $menu_list = $this->Common_model->menu_list($user_id, $Val);
			// $data['menu_list'] = $menu_list;

		// }


		$details = $this->Con_Call_Center_Report->details($userdata);
		$data['staff'] = $details;

		$this->load->view('layout/header', $data);
		$this->load->view('Call_Center_Report',$data);
		$this->load->view('layout/footer', $data);
		
	}

}
?>