<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_group_privileges extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($menu = NULL, $project=NULL, $profile=NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$load_project = $this->Menu_category_model->load_project();
		$data['project'] = $load_project;

		$load_profile = $this->Group_profile_model->load_data();
		$data['profile'] = $load_profile;

		$form_result = $this->Group_privileges_model->get_cat();
		$data['form_result'] = $form_result;

		if(!empty($project)) {

			$data['project_id'] = $project;
			$data['profile_id'] = $profile;

			$prjct_cat = $this->Group_privileges_model->get_pro_cat();
			$data['prjct_cat'] = $prjct_cat;

			$menu_list = $this->Group_privileges_model->menu_list($project);
			$data['menu_lst'] = $menu_list;

			$user_priv = $this->Group_privileges_model->user_priv($profile);
			$data['user_priv'] = $user_priv;

		}

		$this->load->view('layout/header', $data);
		$this->load->view('group_privileges',$data);
		$this->load->view('layout/footer', $data);
	}


	/******************** GET MENU CATEGORY ********************/
	public function get_category(){
		$pid = $this->input->post('pid');
		$cat = $this->Group_privileges_model->get_category($pid);	

		echo json_encode($cat);	
	}

	/******************** GET MENU LIST ********************/
	public function get_menu_list(){
		$pr_id = $this->input->post('pr_id');
		$cat_id = $this->input->post('cat_id');
		$menu_list = $this->Group_privileges_model->get_menu_list($pr_id, $cat_id);	

		echo json_encode($menu_list);	
	}

	/******************** GET PRIVILEGES ********************/
	public function get_priv(){
		$grp_id = $this->input->post('grp_id');
		$menu_id = $this->input->post('menu_id');
		$priv = $this->Group_privileges_model->get_priv($grp_id, $menu_id);

		echo json_encode($priv);
	}


	/******************** ADD PRIVILEGES ********************/
	public function add_priv(){

		$data = array(
			'grp_id' => strip_tags($this->input->post('prof')),
			'usr_id' => 0,
			'menu_id' => strip_tags($this->input->post('menu')),
		);

		$result = $this->Group_privileges_model->add_priv($data);
		if ($result) {
			$response['status'] = 'success';
		}else{
			$response['status'] = 'error';
		}

		echo json_encode($response);
	}


	/******************** REMOVE PRIVILEGES ********************/
	public function remove_priv(){

		$data = array(
			'grp_id' => strip_tags($this->input->post('prof')),
			'menu_id' => strip_tags($this->input->post('menu')),
		);

		$result = $this->Group_privileges_model->remove_priv($data);
		if ($result) {
			$response['status'] = 'success';
		}else{
			$response['status'] = 'error';
		}

		echo json_encode($response);
	}

	/***************************************************/

}
?>