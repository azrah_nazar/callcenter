<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_user_account extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$load_staff = $this->User_account_model->load_staff();
		$data['staff'] = $load_staff;

		$load_profile = $this->Group_profile_model->load_data();
		$data['profile'] = $load_profile;

		$load_data = $this->User_account_model->load_data();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('user_account',$data);
		$this->load->view('layout/footer', $data);
	}


	/******************** USERNAME EXISTS ********************/
	public function check_user(){
		$user_name = $this->input->post('user_name');

		$result = $this->User_account_model->check_user($user_name);
		if ($result == 'success') {
			echo "success";
		}else if ($result == 'username exists'){
			echo "username exists";
		}else{
			echo "error";
		}

	}

	/******************** INSERT ********************/
	public function create_user(){

		$data = array(
			'emp' => strip_tags($this->input->post('emp_id')),
			'Val' => strip_tags($this->input->post('prof_id')),
			'ID' => strip_tags($this->input->post('user_name')),
			'Pws' => strip_tags($this->input->post('password')),
			'stt' => 1,
		);

		$result = $this->User_account_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->User_account_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update(){

		$data = array(
			'Val' => $this->input->post('prof_id_up'),
			'Pws' => $this->input->post('password_up'),
			'call_ID' => $this->input->post('callID_up'),
			'agent_code' => $this->input->post('agent_code'),
		);
		
		$id = $this->input->post('id_up');
		
		$result = $this->User_account_model->update($data,$id);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'exists') {
			echo 'exists';
		}else if ($result == 'agent code exists') {
			echo 'agent code exists';
		}else{
			echo 'error';
		}
	}


	/***************************************************/

}
?>