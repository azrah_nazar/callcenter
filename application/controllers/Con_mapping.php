<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_mapping extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('Mapping_model'); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$load_males = $this->Mapping_model->load_males();
		$data['load_males'] = $load_males;

        $load_females = $this->Mapping_model->load_females();
		$data['load_females'] = $load_females;

        $load_data = $this->Mapping_model->load_data();
		$data['load_data'] = $load_data;

		$get_backdate = $this->Mapping_model->get_backdate();
		$data['backdate_days'] = $get_backdate['no_of_days'];

		$this->load->view('layout/header', $data);
		$this->load->view('Operator_mapping',$data);
		$this->load->view('layout/footer', $data);
	}

	/******************** INSERT ********************/
	public function add(){

		$data = array(
			'male' => strip_tags($this->input->post('male')), 
			'female' => strip_tags($this->input->post('female'))	
		);
		
		$result = $this->Mapping_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'order no exists') {
			echo 'order no exists';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Mapping_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** RESET ********************/
	public function reset(){
		$Acc_No = strip_tags($this->input->post('acc_no'));

		$result = $this->Mapping_model->reset($Acc_No);
		if ($result) {
			$response['status'] = 'success';
		} else if ($result=='error') {
			$response['status'] = 'error';
		}

		echo json_encode($response);
	}

	/******************** BACK DATE ********************/
	public function backdate(){
		$no_of_days = strip_tags($this->input->post('no_of_days'));

		$result = $this->Mapping_model->backdate($no_of_days);
		if ($result) {
			$response['status'] = 'success';
		} else if ($result=='error') {
			$response['status'] = 'error';
		}

		echo json_encode($response);
	}

	/***************************************************/

}
?>