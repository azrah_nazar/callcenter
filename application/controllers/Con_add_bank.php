<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_add_bank extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('Add_bank_model'); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$load_data = $this->Add_bank_model->load_data();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('bank_add',$data);
		$this->load->view('layout/footer', $data);
	}

	


	/******************** INSERT ********************/
	public function add(){

		$data = array(
			'bank_name' => strip_tags($this->input->post('bank_name')), 
			'code' => strip_tags($this->input->post('code'))	
		);
		
		$result = $this->Add_bank_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'order no exists') {
			echo 'order no exists';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Add_bank_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update(){

		$data = array(
			'bank_name_up' => strip_tags($this->input->post('bank_name_up')), 
			'code_up' => strip_tags($this->input->post('code_up'))
		);
		
		$id = strip_tags($this->input->post('id_up'));

		
		$result = strip_tags($this->Add_bank_model->update($data,$id));
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data not exists') {
			echo 'data not exists';
		}else{
			echo 'error';
		}
	}

	/******************** DELETE ********************/
	public function delete(){
		$id = strip_tags($this->input->post('id'));

		$data = array(
			'id' => strip_tags($this->input->post('id')),
		);

		$result = $this->Add_bank_model->delete($data);
		if ($result) {
			$response['status'] = 'success';
		} else if ($result=='error') {
			$response['status'] = 'error';
		} else if($result=='exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/***************************************************/

}
?>