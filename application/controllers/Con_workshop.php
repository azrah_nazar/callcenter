<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Con_workshop extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Workshop_model');
		$this->load->model('Contact_CSV_model');
		$this->load->model('Mapping_model');
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$load_data = $this->Workshop_model->load_data();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('workshop', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** INSERT ********************/
	public function add()
	{
		$tm = date("Y-m-d", strtotime($this->input->post('w_date'))) . ' ' . date("G:i", strtotime($this->input->post('w_time')));
		$data = array(
			'w_name' => $this->input->post('workshop'),
			'w_date' => $this->input->post('w_date'),
			'w_time' => $tm
		);

		$result = $this->Workshop_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data exists') {
			echo 'data exists';
		} else {
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset()
	{
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Workshop_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update()
	{
		$tm = date("Y-m-d", strtotime($this->input->post('w_date_up'))) . ' ' . date("G:i", strtotime($this->input->post('w_time_up')));
		$data = array(
			'w_name' => $this->input->post('workshop_up'),
			'w_date' => $this->input->post('w_date_up'),
			'w_time' => $tm
		);

		$id = $this->input->post('id_up');


		$result = $this->Workshop_model->update($data, $id);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data not exists') {
			echo 'data not exists';
		} else {
			echo 'error';
		}
	}

	/******************** DELETE ********************/
	public function delete()
	{
		$id = strip_tags($this->input->post('id'));

		$data = array(
			'id' => $this->input->post('id'),
		);

		$result = $this->Workshop_model->delete($data);
		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/******************** WORKSHOP SMS LINK ********************/
	public function workshop_link($menu)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$workshops = $this->Workshop_model->next_ws();
		$data['workshops'] = $workshops;

		$load_data = $this->Workshop_model->load_zoom_link();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('workshop_link', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** WORKSHOP SMS ********************/
	public function workshop_sms($menu, $ac_date = null)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$last_workshop = $this->Workshop_model->last_workshop();

		if (empty($ac_date)) {
			$date = date('Y-m-d');
			$date1 = date('Y-m-d', strtotime('-7 day', strtotime($date)));
			$date2 = $last_workshop['w_date'];
			$data['wdate'] = $date2;
		} else {
			$daterange = $ac_date;
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$load_data = $this->Workshop_model->load_zoom_link2($date1, $date2);
		$data['load_data'] = $load_data;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {
					$load_data = $this->Workshop_model->load_zoom_link2($date1, $date2);
					$data['load_data'] = $load_data;
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('workshop_sms', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** ADD LINK ********************/
	public function add_workshop_link()
	{

		$data = array(
			'zoom_link' => $this->input->post('zoom_link'),
		);

		$id = $this->input->post('workshop');


		$result = $this->Workshop_model->add_workshop_link($data, $id);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data exists') {
			echo 'data exists';
		} else {
			echo 'error';
		}
	}

	/******************** UPDATE ********************/
	public function update_link()
	{

		$data = array(
			'zoom_link' => $this->input->post('zoom_link_up')
		);

		$id = $this->input->post('id_up');


		$result = $this->Workshop_model->update_link($data, $id);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data not exists') {
			echo 'data not exists';
		} else {
			echo 'error';
		}
	}

	/******************** DELETE LINK ********************/
	public function delete_link()
	{
		$id = strip_tags($this->input->post('id'));

		$data = array(
			'zoom_link' => ''
		);

		$result = $this->Workshop_model->update_link($data, $id);
		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/******************** VIEW DETAILS WORKSHOP STUDENT LIST ********************/
	public function view_details($menu, $wid)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$data['val'] = strip_tags($this->session->userdata('Val'));

		$load_data = $this->Workshop_model->get_ws_students($wid, $acc_no);
		$data['load_data'] = $load_data;

		$wshop = $this->Workshop_model->wshop($wid);
		$data['wshop'] = $wshop;

		$workshops = $this->Workshop_model->next_ws();
		$data['workshops'] = $workshops;

		$this->load->view('layout/header', $data);
		$this->load->view('view_details', $data);
		$this->load->view('layout/footer', $data);
	}

	/********************  LINK ********************/
	public function zoom_link_sms()
	{
		$w_id = strip_tags($this->input->post('id'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$result = $this->Workshop_model->zoom_link_sms($w_id, $acc_no);

		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		} else {
			$response['status'] = $result;
		}

		echo json_encode($response);
	}

	/******************** INDIVIDUAL SMS LINK ********************/
	public function link_sms_individual()
	{
		$reg_no = strip_tags($this->input->post('reg_no'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$result = $this->Workshop_model->link_sms_individual($reg_no, $acc_no);

		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		} else {
			$response['status'] = $result;
		}

		echo json_encode($response);
	}

	/******************** GET STUDENTS ********************/
	public function get_ws_students()
	{
		$id = strip_tags($this->input->post('id'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));

		$get_dataset = $this->Workshop_model->get_ws_students($id, $acc_no);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/********************  REMINDER ********************/
	public function reminder_sms()
	{
		$w_id = strip_tags($this->input->post('id'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$result = $this->Workshop_model->reminder_sms($w_id, $acc_no);

		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		} else {
			$response['status'] = $result;
		}

		echo json_encode($response);
	}

	/******************** INDIVIDUAL REMINDER SMS ********************/
	public function individual_reminder()
	{
		$reg_no = strip_tags($this->input->post('reg_no'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$result = $this->Workshop_model->individual_reminder($reg_no, $acc_no);

		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		} else {
			$response['status'] = $result;
		}

		echo json_encode($response);
	}

	/********************  FEEDBACK ********************/
	public function feedback_sms()
	{
		$w_id = strip_tags($this->input->post('id'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$result = $this->Workshop_model->feedback_sms($w_id, $acc_no);

		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		} else {
			$response['status'] = $result;
		}

		echo json_encode($response);
	}

	/******************** INDIVIDUAL FEEDBACK SMS ********************/
	public function individual_feedback_sms()
	{
		$reg_no = strip_tags($this->input->post('reg_no'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$result = $this->Workshop_model->individual_feedback_sms($reg_no, $acc_no);

		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		} else {
			$response['status'] = $result;
		}

		echo json_encode($response);
	}

	/********************  BANK SMS ********************/
	public function bank_sms()
	{
		$w_id = strip_tags($this->input->post('id'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$result = $this->Workshop_model->bank_sms($w_id, $acc_no);

		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		} else {
			$response['status'] = $result;
		}

		echo json_encode($response);
	}

	/******************** INDIVIDUAL BANK SMS ********************/
	public function individual_bank_sms()
	{
		$reg_no = strip_tags($this->input->post('reg_no'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$result = $this->Workshop_model->individual_bank_sms($reg_no, $acc_no);

		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		} else {
			$response['status'] = $result;
		}

		echo json_encode($response);
	}

	/******************** WORKSHOP REJECT REPORT ********************/
	public function workshop_reject_report($menu = NULL, $daterange = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$load_agent = $this->Call_Report_Model->load_agent();
		$data['load_agent'] = $load_agent;

		$val = strip_tags($this->session->userdata('Val'));
		$data['val'] = $val;

		$agent = $this->input->post('agent');
		if ($agent == '') {
			if ($val == 12 || $val == 15) {
				$agent = '';
			} else {
				$agent = strip_tags($this->session->userdata('acc_no'));
			}
		}

		if ($daterange == '') {
			$date1 = $date2 = date('Y-m-d');
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$resultlist = $this->Workshop_model->workshop_reject_report($agent, $date1, $date2, $val);
		$data['resultlist'] = $resultlist;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {
					$this->form_validation->set_rules('agent', 'Agent', 'trim|xss_clean');
					$this->form_validation->set_rules('date_range', 'Date', 'trim|xss_clean');

					if ($this->form_validation->run() == FALSE) {
					} else {
						$resultlist = $this->Workshop_model->workshop_reject_report($agent, $date1, $date2, $val);
						$data['resultlist'] = $resultlist;
					}
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('workshop_reject_report', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** REJOIN PENDING ********************/
	public function rejoin_pending()
	{
		$reg_no = htmlspecialchars(strip_tags($this->input->post('reg_num')), ENT_QUOTES, 'UTF-8');

		$result = $this->Workshop_model->rejoin_pending($reg_no);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data exists') {
			echo 'data exists';
		} else {
			echo 'error';
		}
	}

	/******************** NEW WORKSHOP ********************/
	public function new_workshop()
	{
		$telNo = strip_tags($this->input->post('telNo'));

		$result = $this->Workshop_model->new_workshop($telNo);
		if ($result) {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/******************** NOTIFICATION ********************/
	public function regular_noti()
	{
		$Val = strip_tags($this->session->userdata('Val'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));

		$unread_msg = $this->Call_Report_Model->regular_noti($acc_no, $Val);
		$data['result'] = $unread_msg;

		echo json_encode($data);
	}

	/******************** MALE WORKSHOP REJECT REPORT ********************/
	public function male_ws_reject($menu = NULL, $daterange = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$acc_no = $this->session->userdata('acc_no');

		$val = strip_tags($this->session->userdata('Val'));
		$data['val'] = $val;

		$max_trans_date = $this->Workshop_model->max_trans_date($acc_no);
		$data['max_trans_date'] = $max_trans_date['max_date'];

		$mapped_operator = $this->Contact_CSV_model->mapped_operator($acc_no);
		$data['mapped_operator'] = $mapped_operator['mapped_to'];
		$data['mapped_operator_name'] = $mapped_operator['Name'];

		$get_backdate = $this->Mapping_model->get_backdate();
		$data['backdate_days'] = $get_backdate['no_of_days'];

		if ($daterange == '') {
			$monthsBack = date('Y-m-d', strtotime($max_trans_date['max_date'] . '-' . $get_backdate['no_of_days'] . ' days'));
			$date1 = $monthsBack;
			$date2 = $monthsBack;
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$resultlist = $this->Workshop_model->male_workshop_reject($date1, $date2, $acc_no, $mapped_operator['mapped_to']);
		$data['resultlist'] = $resultlist;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {
					$this->form_validation->set_rules('date_range', 'Date', 'trim|xss_clean');

					if ($this->form_validation->run() == FALSE) {
					} else {
						$resultlist = $this->Workshop_model->male_workshop_reject($date1, $date2, $acc_no, $mapped_operator['mapped_to']);
						$data['resultlist'] = $resultlist;
					}
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('male_workshop_reject', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** MALE REAPPLIED REJECT REPORT ********************/
	public function male_reapplied_reject($menu = NULL, $daterange = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');
		$val = strip_tags($this->session->userdata('Val'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));

		if ($val == 12 || $val == 15) {
			$agent = '';
		} else {
			$agent = strip_tags($this->session->userdata('acc_no'));
		}

		if ($daterange == '') {
			$date1 = $date2 = date('Y-m-d');
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$max_trans_date = $this->Workshop_model->max_trans_date($acc_no);
		$data['max_trans_date'] = $max_trans_date['max_date'];

		$mapped_operator = $this->Contact_CSV_model->mapped_operator($acc_no);
		$data['mapped_operator'] = $mapped_operator['mapped_to'];
		$data['mapped_operator_name'] = $mapped_operator['Name'];

		$get_backdate = $this->Mapping_model->get_backdate();
		$data['backdate_days'] = $get_backdate['no_of_days'];

		$load_data = $this->Workshop_model->male_reapplied_reject($date1, $date2, $agent, $mapped_operator['mapped_to']);
		$data['load_data'] = $load_data;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {

					$load_data = $this->Workshop_model->male_reapplied_reject($date1, $date2, $agent, $mapped_operator['mapped_to']);
					$data['load_data'] = $load_data;
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('male_reapplied_reject', $data);
		$this->load->view('layout/footer', $data);
	}

	public function pending_contacts()
	{
		$telNo = strip_tags($this->input->post('telNo'));
		$acc_no =  htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');
		$today = date('Y-m-d H:i:s');

		$data = array(
			'emp_accNo' => $acc_no,
			'date' => $today,
			'reg_no' => '',
			'flag' => '0',
			'comment' => '',
			'date1' => '',
			'reminder_date' => '',
			'workshop' => '',
			'confirmation' => '0',
			'bank_sms' => '0',
			'ws_reminder' => '0',
			'feedback_no' => '0',
			'feedback_reject' => '0',
			'reject_date' => '0',
			'pending_payment' => '0',
			'pending_reallocation' => '0',
			'direct_sale' => '0',
			'spl_cus' => '0',
			'reapplied_reject' => '0',
			'reapplied_reject_date' => '0',
			'tic_id' => '',
			'self_reg' => '0'
		);

		$result = $this->Workshop_model->pending_contacts($data, $telNo);
		if ($result) {
			$response = 'success';
		} else if ($result == 'error') {
			$response = 'error';
		}

		echo ($response);
	}

	/******************** MALE WORKSHOP STUDENT LIST ********************/
	public function male_ws_sms($menu, $daterange = NULL)
	{
		$wid = '';
		require_once(APPPATH . 'libraries/User_privileges.php');

		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$data['val'] = strip_tags($this->session->userdata('Val'));

		$max_trans_date = $this->Workshop_model->max_trans_date($acc_no);
		$data['max_trans_date'] = $max_trans_date['max_date'];

		$mapped_operator = $this->Contact_CSV_model->mapped_operator($acc_no);
		$data['mapped_operator'] = $mapped_operator['mapped_to'];
		$data['mapped_operator_name'] = $mapped_operator['Name'];

		$get_backdate = $this->Mapping_model->get_backdate();
		$data['backdate_days'] = $get_backdate['no_of_days'];

		if ($daterange == '') {
			$monthsBack = date('Y-m-d', strtotime($max_trans_date['max_date'] . '-' . $get_backdate['no_of_days'] . ' days'));
			$date1 = $monthsBack;
			$date2 = $monthsBack;
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$load_data = $this->Workshop_model->get_male_ws_students($mapped_operator['mapped_to'], $acc_no, $date1, $date2);
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('male_workshop_sms', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** SPECIAL CSV ********************/
	public function spl_csv($menu = NULL, $ws = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');
		$acc_no = htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');

		$resultlist = $this->Workshop_model->spl_csv($acc_no, $ws);
		$data['resultlist'] = $resultlist;

		$con_cnt = $this->Workshop_model->spl_csv_count($acc_no, $ws);
		$data['con_cnt'] = $con_cnt;

		$this->load->view('layout/header', $data);
		$this->load->view('spl_csv', $data);
		$this->load->view('layout/footer', $data);
	}



	/***************************************************/
}
