<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_user_control extends CI_Controller {

	function __construct() { 
		parent::__construct();
	}


	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$load_data = $this->User_control_model->load_data();
		$data['load_data'] = $load_data;
			
		$this->load->view('layout/header', $data);
		$this->load->view('user_control',$data);
		$this->load->view('layout/footer', $data);

	}


	/******************** UPDATE ********************/
	public function update_stt(){

		$data = array(
			'stt' => $this->input->post('flag')
		);
		
		$id = $this->input->post('acc');

		
		$result = $this->User_control_model->update($data,$id);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** USER LOG ********************/
	public function user_log($menu = NULL, $acc = NULL){
		require_once(APPPATH.'libraries/User_privileges.php');

		$user_log = $this->User_control_model->user_log($acc);
		$data['load_data'] = $user_log;
			
		$this->load->view('layout/header', $data);
		$this->load->view('user_log',$data);
		$this->load->view('layout/footer', $data);

	}

	/***************************************************/

}
?>