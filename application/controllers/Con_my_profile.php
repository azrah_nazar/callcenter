<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_my_profile extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($menu = NULL)
	{
		$data = array();

		if($this->session->has_userdata('username'))
		{
			$acc_no = strip_tags($this->session->userdata('acc_no'));
			$user = strip_tags($this->session->userdata('username'));
			$user_id = strip_tags($this->session->userdata('id'));
			$Val = strip_tags($this->session->userdata('Val'));
			$emp_id = strip_tags($this->session->userdata('emp'));

			$user_details = $this->My_profile_model->user_details($user);
			$data['user_details'] = $user_details;

			$category = $this->Common_model->categories($user_id, $Val);
			$data['category'] = $category;

			$menu_list = $this->Common_model->menu_list($user_id, $Val);
			$data['menu_list'] = $menu_list;

		}else{
			redirect('Con_login', 'refresh');
		}


		$staff = $this->Staff_list_model->staff_details($acc_no);

		if(count($staff) == 0) {
			redirect('staff/staff_list/'.$acc_no);

		} else {
			$data['staff'] = $staff;
			
			$this->load->view('layout/header', $data);
			$this->load->view('my_profile',$data);
			$this->load->view('layout/footer', $data);
		}
	}

	/******************** UPDATE ********************/
	public function update(){
		$uname = strip_tags($this->session->userdata('username'));

		$data = array(
			'user_n' => $this->input->post('acc_up'),
			'date' => date('Y-m-d'),
			'time' => date("h:i:sa"),
			'amount' => $this->input->post('open_bal'),
		);
		
		$result = $this->My_profile_model->update_balance($data, $uname);
		if ($result == 'success') {
			echo 'success';
		}else{
			echo 'error';
		}
	}

	/******************** CHECK CASHIER LOG IN ********************/
	public function check_login(){
		$acc_no = strip_tags($this->input->post('acc_no'));

		$check_login = $this->My_profile_model->check_login($acc_no);
		$data['result'] = $check_login;

		echo json_encode($data);
	}

	/******************** CHANGE PASSWORD ********************/
	public function changepass(){

		$data = array();

		if($this->session->has_userdata('username'))
		{
			$acc_no = strip_tags($this->session->userdata('acc_no'));
			$user = strip_tags($this->session->userdata('username'));
			$user_id = strip_tags($this->session->userdata('id'));
			$Val = strip_tags($this->session->userdata('Val'));
			$emp_id = strip_tags($this->session->userdata('emp'));

			$user_details = $this->My_profile_model->user_details($user);
			$data['user_details'] = $user_details;

			$category = $this->Common_model->categories($user_id, $Val);
			$data['category'] = $category;

			$menu_list = $this->Common_model->menu_list($user_id, $Val);
			$data['menu_list'] = $menu_list;

		}else{
			redirect('Con_login', 'refresh');
		}

		$this->load->view('layout/header', $data);
		$this->load->view('change_password',$data);
		$this->load->view('layout/footer', $data);
	}

	/******************** UPDATE PASSWORD ********************/
	public function updatePassword(){
		$acc_no = strip_tags($this->session->userdata('acc_no'));

		$data = array(
			'Pws' => $this->input->post('new_pass'),
		);
		
		$result = $this->My_profile_model->updatePassword($data, $acc_no);
		if ($result == 'success') {
			echo 'success';
		}else{
			echo 'error';
		}

	}

	/***************************************************/

}
?>