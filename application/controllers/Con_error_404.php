<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_error_404 extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index()
	{
		$this->load->view('errors/error_404');
	}


	/***************************************************/

}
?>