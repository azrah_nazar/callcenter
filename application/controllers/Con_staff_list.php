<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_staff_list extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$designation = $this->Designation_model->load_data();
		$data['designations'] = $designation;

		$load_data = $this->Staff_list_model->load_data();
		$data['load_data'] = $load_data;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');
			
			if (isset($search)) {
				if ($search == 'search_filter') {
					$this->form_validation->set_rules('role', 'Role', 'trim|required|xss_clean');
					
					if ($this->form_validation->run() == FALSE) {
						
					} else {
						$role = $this->input->post('role');
						$resultlist = $this->Staff_list_model->searchByRole($role);
						$data['resultlist'] = $resultlist;
					}
				
				} else if ($search == 'search_full') {
					$this->form_validation->set_rules('search_text', 'Value', 'trim|xss_clean');
					
					if ($this->form_validation->run() == FALSE) {
						
					} else {
						$search_text = $this->input->post('search_text');
						$resultlist = $this->Staff_list_model->searchKeyword($search_text);
						$data['resultlist'] = $resultlist;
					}
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('staff/staff_list',$data);
		$this->load->view('layout/footer', $data);
	}

	/********************** SEARCH ***************************/
	public function searchByRole(){

		$this->form_validation->set_rules('role', 'Role', 'trim|required|xss_clean');
		
		if ($this->form_validation->run() == FALSE) {
			
		} else {
			$role = $this->input->post('role');
			$load_data = $this->Staff_list_model->searchByRole($role);
			$data['load_data'] = $load_data;

			$this->load->view('layout/header', $data);
			$this->load->view('staff/staff_list',$data);
			$this->load->view('layout/footer', $data);
		}
	}


	/***************************************************/

}
?>