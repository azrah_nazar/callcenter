<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_menu_category extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$load_project = $this->Menu_category_model->load_project();
		$data['project'] = $load_project;

		$load_data = $this->Menu_category_model->load_data();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('menu_category',$data);
		$this->load->view('layout/footer', $data);		
	}

	/******************** INSERT ********************/
	public function add(){
		$data = array(
			'p_id' => $this->input->post('pid'),
			'cat' => $this->input->post('cat_name'),
			'icon' => $this->input->post('icon'),
		);
		
		$result = $this->Menu_category_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Menu_category_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update(){

		$data = array(
			'p_id' => $this->input->post('pid_up'),
			'cat' => $this->input->post('cat_name_up'),
			'icon' => $this->input->post('icon_up')
		);
		
		$id = $this->input->post('id_up');

		
		$result = $this->Menu_category_model->update($data,$id);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}


	/***************************************************/

}
?>