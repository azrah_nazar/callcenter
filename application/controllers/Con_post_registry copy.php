<?php
defined('BASEPATH') or exit('No direct script access allowed');

class  Con_post_registry extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Pre_registry_model');
		$this->load->model('Timetable_model');
		$this->load->model('Call_Report_Model');
		$this->load->model('Workshop_model');
		$this->load->model('Zoom_upload_model');
		$this->load->library('Pdf');
	}

	/******************** REGISTER FOR CLASS ********************/
	public function class_register($reg_no = NULL)
	{
		$reg_no = $this->uri->segment(3);

		$st_details = $this->Pre_registry_model->get_st_details($reg_no);
		$data['st_details'] = $st_details;

		$classes = $this->Timetable_model->next_classes();
		$data['classes'] = $classes;

		$this->load->view('class_register', $data);
		$this->load->view('layout/footer', $data);
	}


	/******************** RESERVE ********************/
	public function reserve_class()
	{
		$reg_num = strip_tags($this->input->post('reg_num'));

		$data = array(
			'class' => $this->input->post('clz_id'),
		);

		$result = $this->Pre_registry_model->update_details($data, $reg_num);
		if ($result == "success") {

			$msg = "Dear Student, Please make your deposit to the given account";
			$res = $this->Pre_registry_model->common_sms($reg_num, $msg);
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/******************** CLASS REGISTERED LIST ********************/
	public function registered_list($menu = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$emp = strip_tags($this->session->userdata('acc_no'));
		$val = strip_tags($this->session->userdata('Val'));

		$data['val'] = $val;

		if ($val == 12 || $val == 15) {
			$agent = $this->input->post('agent');
		} else {
			$agent = $emp;
		}

		$load_agent = $this->Call_Report_Model->load_agent();
		$data['load_agent'] = $load_agent;

		$resultlist = $this->Pre_registry_model->registered_list($agent);
		$data['resultlist'] = $resultlist;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {
					$this->form_validation->set_rules('agent', 'Agent', 'trim|xss_clean');

					if ($this->form_validation->run() == FALSE) {
					} else {

						$resultlist = $this->Pre_registry_model->registered_list($agent);
						$data['resultlist'] = $resultlist;
					}
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('registered_list', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** BANK DEPOSITS ********************/
	public function bank_deposit()
	{
		$reg_no = $this->input->post('wr_num');
		$is_usdt = $this->input->post('usdt_val');
		$payment_type = $this->input->post('payment_type');
		$name_ini = $this->input->post('name_ini');
		$town = $this->input->post('town');
		$nic = $this->input->post('nic');
		$telNo = $this->input->post('con_noo');
		$bank = $this->input->post('bank');

		if($bank == 'KOKO and Bank'){
			$bank_amt = $this->input->post('bank_amt');
			$koko_amt = $this->input->post('koko_amt');
		}else{
			$bank_amt = 0;
			$koko_amt = 0;
		}

		// if($payment_type == "default"){
		// 	$cr = $this->input->post('default_amt');
		// }else{
		$cr = $this->input->post('manual_amt');
		// }

		$data = array(
			'reg_no' => $this->input->post('wr_num'),
			'deposit_date' => date('Y-m-d'),
			'descrip' => $this->input->post('desc'),
			'cr' => $cr,
			'dr' => '0',
			'is_usdt' => $is_usdt,
			'bank' => $this->input->post('bank'),
			'p_type' => $this->input->post('p_type'),
			'koko_amt' => $this->input->post('koko_amt'),
			'bank_amt' => $this->input->post('bank_amt'),

		);

		$result = $this->Pre_registry_model->bank_deposit($data, $is_usdt, $reg_no, $name_ini, $town, $nic, $telNo);
		if ($result == 'success') {
			echo 'success';
		} else {
			echo 'error';
		}
	}

	/******************** BANK CONTROL ********************/
	public function bank_control($menu = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$emp = strip_tags($this->session->userdata('acc_no'));
		$val = strip_tags($this->session->userdata('Val'));

		$data['val'] = $val;

		// if ($val == 12 || $val == 15) {
		//     $agent = $this->input->post('agent');
		// } else {
		//     $agent = $emp;
		// }
		// $workshop = $this->input->post('workshop');

		$load_agent = $this->Call_Report_Model->load_agent();
		$data['load_agent'] = $load_agent;

		$load_workshop = $this->Workshop_model->load_data();
		$data['load_workshop'] = $load_workshop;

		$course_fee = $this->Zoom_upload_model->course_fee();
		$data['course_fee'] = $course_fee['course_fee'];

		$load_data = $this->Zoom_upload_model->bank_control();
		$data['load_data'] = $load_data;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {
					$this->form_validation->set_rules('agent', 'Agent', 'trim|xss_clean');
					$this->form_validation->set_rules('workshop', 'Workshop', 'trim|xss_clean');

					if ($this->form_validation->run() == FALSE) {
					} else {
						$load_data = $this->Zoom_upload_model->bank_control();
						$data['load_data'] = $load_data;
					}
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('bank_control', $data);
		$this->load->view('layout/footer', $data);
	}

	public function response_accept()
	{
		$res_flag = $this->input->post('res_flag');
		$bid = $this->input->post('bid');
		$reg = $this->input->post('reg');

		$result = $this->Zoom_upload_model->response_update($res_flag, $bid, $reg);
		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		}

		echo json_encode($response);
	}

	public function payment_details()
	{
		$inv_no = $this->uri->segment(3);
		$inv_id = $this->uri->segment(4);

		$pay_details = $this->Pre_registry_model->get_pay_details($inv_no, $inv_id);
		$data['pay_details'] = $pay_details;

		$name = $pay_details["name"];
		$town = $pay_details["town"];
		$inv_no = $pay_details["inv_no"];
		$cr = $pay_details["cr"];
		$bank = $pay_details["bank"];
		
		$pay_details2 = $this->Pre_registry_model->get_pay_details2($inv_no, $inv_id);
		$data['pay_details2'] = $pay_details2;
		
		$course_fee = $this->Zoom_upload_model->course_fee();
		$data['course_fee'] = $course_fee['course_fee'];
		
		$amt = intval($pay_details['cr']);
		$c_fee = intval($course_fee);
		$usdt_val = $pay_details['usdt_val'];
		$reg_cnt = $pay_details2['reg_cnt'];
		$course_fee = $course_fee['course_fee'];

		if ($usdt_val == 1) {
			$course_fee = $course_fee;
		} else {
			$course_fee = intval($course_fee) - intval(2500);
		}

		$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'a4', true, 'UTF-8', false);

		// set document information
		$tcpdf->SetCreator(PDF_CREATOR);
		$tcpdf->SetAuthor('Victory Academy');
		$tcpdf->SetTitle('Invoice');
		$tcpdf->SetSubject('Victory Academy');
		$tcpdf->SetKeywords('Victory Academy Invoice');

		//set default monospaced textual style
		$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set auto for page breaks
		$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image for scale factor
		$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);



		// it is optional :: set some language-dependent strings
		if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
			// optional
			require_once(dirname(__FILE__) . '/lang/eng.php');
			// optional
			$tcpdf->setLanguageArray($l);
		}

		// set default font for subsetting mode
		$tcpdf->setFontSubsetting(true);

		$tcpdf->SetFont('helvetica', '', 14, '', true);

		$tcpdf->SetPrintHeader(false);
		$tcpdf->SetPrintFooter(false);

		$tcpdf->AddPage();

		// set text shadow for effect
		$tcpdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 197, 198), 'opacity' => 1, 'blend_mode' => 'Normal'));

		$link = base_url();
		if ($bank == 'KOKO') {

			$img_file = $link . '/images/koko.jpeg';
			$tcpdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

			$tcpdf->SetXY(20, 103);
			$tcpdf->Cell(250, 10, trim($name), 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(20, 110);
			$tcpdf->Cell(250, 10, trim($town), 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(145, 95);
			$tcpdf->Cell(25, 10, $inv_no, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

		}else if ($amt == $course_fee) {

			$img_file = $link . '/images/enrollment_payment.jpeg';
			$tcpdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

			$tcpdf->SetXY(20, 100);
			$tcpdf->Cell(250, 10, trim($name), 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(20, 105);
			$tcpdf->Cell(250, 10, trim($town), 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(150, 95);
			$tcpdf->Cell(25, 10, $inv_no, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(20, 140);
			$tcpdf->Cell(25, 10, 'Rs.' . $cr, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(67, 140);
			$tcpdf->Cell(25, 10, 'Class Enrollment Fee', 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(151, 140);
			$tcpdf->Cell(25, 10, 'Rs.' .$course_fee, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

		} else if ($reg_cnt >= 1) {

			$img_file = $link . '/images/enrollment_payment.jpeg';
			$tcpdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

			$tcpdf->SetXY(20, 100);
			$tcpdf->Cell(250, 10, trim($name), 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(20, 105);
			$tcpdf->Cell(250, 10, trim($town), 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(150, 95);
			$tcpdf->Cell(25, 10, $inv_no, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(20, 140);
			$tcpdf->Cell(25, 10, 'Rs.' . $cr, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(67, 140);
			$tcpdf->Cell(25, 10, 'Class Enrollment Fee', 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(151, 140);
			$tcpdf->Cell(25, 10, 'Rs.' . $cr, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');
		
		}else {
			$balance = intval($course_fee) - intval($pay_details['tot']);

			$img_file = $link . '/images/advance_payment.jpg';
			$tcpdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

			$tcpdf->SetXY(20, 105);
			$tcpdf->Cell(250, 10, trim($name), 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(20, 110);
			$tcpdf->Cell(250, 10, trim($town), 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(150, 98);
			$tcpdf->Cell(25, 10, $inv_no, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(18, 145);
			$tcpdf->Cell(25, 10, 'Rs.' . $cr, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(53, 145);
			$tcpdf->Cell(25, 10, 'Class Enrollment Fee', 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(108, 145);
			$tcpdf->Cell(25, 10, 'Rs.' .$course_fee, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(144, 145);
			$tcpdf->Cell(25, 10, 'Rs.' .$balance, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');
		}

		ob_end_clean();
		$tcpdf->Output($inv_no . '.pdf', 'I');
	}

	/******************** FULL PAYMENT STUDENT LIST ********************/
	public function full_payment($menu = NULL, $daterange = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		if ($daterange == '') {
            $date1 = $date2 = date('Y-m-d');
        } else {
            $date1 = substr($daterange, 0, 10);
            $date2 = substr($daterange, 17, 24);
        }
		
		$emp = strip_tags($this->session->userdata('acc_no'));
		$val = strip_tags($this->session->userdata('Val'));

		$course_fee = $this->Zoom_upload_model->course_fee();
		$data['course_fee'] = $course_fee['course_fee'];

		$load_data = $this->Zoom_upload_model->full_payment($date1, $date2, $val, $emp);
		$data['load_data'] = $load_data;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {

					$load_data = $this->Zoom_upload_model->full_payment($date1, $date2, $val, $emp);
					$data['load_data'] = $load_data;
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('paid_students', $data);
		$this->load->view('layout/footer', $data);
	}
}
