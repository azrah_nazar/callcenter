<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_unauthorized extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index()
	{
		
		if($this->session->has_userdata('username')){

			$acc_no = strip_tags($this->session->userdata('acc_no'));
			$user = strip_tags($this->session->userdata('username'));
			$user_id = strip_tags($this->session->userdata('id'));
			$Val = strip_tags($this->session->userdata('Val'));

			$user_details = $this->My_profile_model->user_details($user);
			$data['user_details'] = $user_details;

			$category = $this->Common_model->categories($user_id, $Val);
			$data['category'] = $category;

			$menu_list = $this->Common_model->menu_list($user_id, $Val);
			$data['menu_list'] = $menu_list;

			// $load_data = $this->Grade_model->load_data();
			// $data['load_data'] = $load_data;

			$this->load->view('layout/header', $data);
			$this->load->view('unauthorized');
			$this->load->view('layout/footer', $data);
		}else{
			redirect('Con_login', 'refresh');
		}

	}


	/***************************************************/

}
?>