<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_add_course extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('Add_course_model'); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$load_data = $this->Add_course_model->load_data();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('Add_Course',$data);
		$this->load->view('layout/footer', $data);
	}

	/******************** GET CATEGORY ********************/
	public function get_category() {

		$id = strip_tags($this->input->post('id'));

		$load_cat = $this->Add_course_model->get_category($id );
		$data['records'] = $load_cat;

		echo json_encode($data);

	}


	/******************** INSERT ********************/
	public function add(){

		$data = array(
			'course_name' => strip_tags($this->input->post('course_name')),
			'course_fee' => strip_tags($this->input->post('course_fee')),
			
		);
		
		$result = $this->Add_course_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'order no exists') {
			echo 'order no exists';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Add_course_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update(){

		$data = array(
			'course_name' => strip_tags($this->input->post('course_name_up')),
			'course_fee' => strip_tags($this->input->post('course_fee_up')),
		);
		
		$id = strip_tags($this->input->post('id_up'));

		
		$result = $this->Add_course_model->update($data,$id);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data not exists') {
			echo 'data not exists';
		}else{
			echo 'error';
		}
	}

	/******************** DELETE ********************/
	public function delete(){
		$id = strip_tags($this->input->post('id'));

		$data = array(
			'id' => strip_tags($this->input->post('id')),
		);

		$result = $this->Add_course_model->delete($data);
		if ($result) {
			$response['status'] = 'success';
		} else if ($result=='error') {
			$response['status'] = 'error';
		} else if($result=='exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/***************************************************/

}
?>