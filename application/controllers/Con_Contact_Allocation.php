<?php
defined('BASEPATH') or exit('No direct script access allowed');

class  Con_Contact_Allocation extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Call_center_model');
		$this->load->model('Contact_CSV_model');
		$this->load->model('Call_Report_Model');
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');
		
		$data['desig'] = htmlspecialchars(strip_tags($this->session->userdata('Val')), ENT_QUOTES, 'UTF-8');

		$load_operator = $this->Contact_CSV_model->load_operator();
		$data['load_operator'] = $load_operator;
		
		$load_data = $this->Call_center_model->get_process_list();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('Call_Allocation', $data);
		$this->load->view('layout/footer', $data);
	}

	public function process()
	{
		$response = array();
		$desig = strip_tags($this->session->userdata('Val'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$result = $this->Call_center_model->call_allocation($desig, $acc_no);
		$response['status'] = $result;

		echo json_encode($response);
	}

	/******************** CALL ALLOCATION REPORT ********************/
	public function call_allocation_report($menu = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$agent = $this->input->post('agent');

		$load_agent = $this->Call_Report_Model->load_agent();
		$data['load_agent'] = $load_agent;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			$daterange = $this->input->post('date_range');
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 13, 24);

			if (isset($search)) {
				if ($search == 'search_filter') {

					$load_data = $this->Contact_CSV_model->load_data2($date1, $date2, $agent);
					$data['load_data'] = $load_data;
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('call_allocation_report', $data);
		$this->load->view('layout/footer', $data);
	}
}
