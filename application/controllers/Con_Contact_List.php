<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Con_Contact_List extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('Contact_CSV_model');
	}

	public function index($menu = NULL, $id = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$emp = strip_tags($this->session->userdata('acc_no'));
		$load_data = $this->Contact_CSV_model->load_contact_list($emp);
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('contact_list',$data);
		$this->load->view('layout/footer', $data);
	}
}
?> 