<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_menu_list extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$load_project = $this->Menu_category_model->load_project();
		$data['project'] = $load_project;

		$load_data = $this->Menu_list_model->load_data();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('menu_list',$data);
		$this->load->view('layout/footer', $data);
	}

	/******************** GET CATEGORY ********************/
	public function get_category() {

		$id = $this->input->post('id');

		$load_cat = $this->Menu_list_model->get_category($id );
		$data['records'] = $load_cat;

		echo json_encode($data);

	}


	/******************** INSERT ********************/
	public function add(){

		$data = array(
			'p_id' => $this->input->post('pid'), 
			'Cat_id' => $this->input->post('cat_id'),
			'Category' => $this->input->post('category_name'),
			'Caption' => $this->input->post('name'),
			'Name' => $this->input->post('url'),
			'R_Category' => 0,
			'subcat_id' => $this->input->post('oid') 
		);
		
		$result = $this->Menu_list_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'order no exists') {
			echo 'order no exists';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Menu_list_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update(){

		$data = array(
			'p_id' => $this->input->post('pid_up'), 
			'Cat_id' => $this->input->post('cat_id_up'),
			'Caption' => $this->input->post('name_up'),
			'Name' => $this->input->post('url_up'),
			'subcat_id' => $this->input->post('oid_up')
		);
		
		$id = $this->input->post('id_up');

		
		$result = $this->Menu_list_model->update($data,$id);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data not exists') {
			echo 'data not exists';
		}else{
			echo 'error';
		}
	}

	/******************** DELETE ********************/
	public function delete(){
		$id = strip_tags($this->input->post('id'));

		$data = array(
			'id' => $this->input->post('id'),
		);

		$result = $this->Menu_list_model->delete($data);
		if ($result) {
			$response['status'] = 'success';
		} else if ($result=='error') {
			$response['status'] = 'error';
		} else if($result=='exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/***************************************************/

}
?>