<?php
defined('BASEPATH') or exit('No direct script access allowed');

class  Con_Contact_CSV extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('CSVReader');
		$this->load->model('Contact_CSV_model');
		$this->load->model('Call_center_model');
		$this->load->model('Mapping_model');
	}

	public function index($menu = NULL, $daterange =  NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');
		$data['desig'] = htmlspecialchars(strip_tags($this->session->userdata('Val')), ENT_QUOTES, 'UTF-8');
		$data['acc_no'] = htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');
		$data['gender'] = htmlspecialchars(strip_tags($this->session->userdata('gender')), ENT_QUOTES, 'UTF-8');

		if ($daterange == '') {
			$date1 = date('Y-m-d');
			$date2 = date('Y-m-d');
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$load_operator = $this->Contact_CSV_model->load_operator();
		$data['load_operator'] = $load_operator;

		$load_females = $this->Mapping_model->load_females();
		$data['map_operator'] = $load_females;

		$mapped_operator = $this->Contact_CSV_model->mapped_operator($data['acc_no']);
		if (!empty($mapped_operator)) {
			$data['mapped_operator'] = $mapped_operator['mapped_to'];
			$data['mapped_operator_name'] = $mapped_operator['Name'];
		} else {
			$data['mapped_operator'] = '';
			$data['mapped_operator_name'] = '';
		}

		$get_backdate = $this->Mapping_model->get_backdate();
		$data['backdate_days'] = $get_backdate['no_of_days'];

		$load_data = $this->Contact_CSV_model->load_data();
		$data['load_data'] = $load_data;

		$query =  $this->db->query("SELECT COUNT(emp_accNo) AS call_cnt, Employee.Name FROM con_list JOIN Employee ON con_list.emp_accNo = Employee.Acc_No JOIN Usert ON con_list.emp_accNo = Usert.acc_no WHERE date(con_list.date) BETWEEN '$date1' AND '$date2' AND self_reg!='1' AND Usert.stt <> '2' GROUP BY emp_accNo ORDER BY call_cnt");

		$record = $query->result();
		$data2[] = '';
		foreach ($record as $row) {
			$data2['label'][] = $row->Name;
			$data2['data'][] = (int) $row->call_cnt;
		}
		$data['chart_data'] = json_encode($data2);

		$this->load->view('layout/header', $data);
		$this->load->view('contact_csv', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** INSERT ********************/
	public function add()
	{
		$user_id = htmlspecialchars(strip_tags($this->session->userdata('id')), ENT_QUOTES, 'UTF-8');
		$desig = htmlspecialchars(strip_tags($this->session->userdata('Val')), ENT_QUOTES, 'UTF-8');
		$acc_no = htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');
		$con_no = htmlspecialchars(strip_tags($this->input->post('con_no')), ENT_QUOTES, 'UTF-8');
		$upload_type = htmlspecialchars(strip_tags($this->input->post('upload_type')), ENT_QUOTES, 'UTF-8');
		$tp = str_replace(' ', '', $con_no);

		if ($desig == 13) {
			$accNo = $acc_no;
			$self_reg = 1;
			$upload_typ = '';
		} else {
			$accNo = '';
			$self_reg = 0;
			$upload_typ = $upload_type;
		}

		$this->form_validation->set_rules('con_name', 'Name', 'trim|xss_clean');
		$this->form_validation->set_rules('con_adrz', 'Address', 'trim|xss_clean');
		$this->form_validation->set_rules('con_whatsapp', 'WhatsApp Number', 'trim|xss_clean');
		$this->form_validation->set_rules('con_job', 'Job', 'trim|xss_clean');
		$this->form_validation->set_rules('con_dob', 'DOB', 'trim|xss_clean');
		$this->form_validation->set_rules('upload_type', 'Upload Type', 'trim|xss_clean');

		if ($this->form_validation->run() == TRUE) {

			$data = array(
				'con_no' => $tp,
				'name' => htmlspecialchars(strip_tags($this->input->post('con_name')), ENT_QUOTES, 'UTF-8'),
				'address' => htmlspecialchars(strip_tags($this->input->post('con_adrz')), ENT_QUOTES, 'UTF-8'),
				'whatsapp' => htmlspecialchars(strip_tags($this->input->post('con_whatsapp')), ENT_QUOTES, 'UTF-8'),
				'job' => htmlspecialchars(strip_tags($this->input->post('con_job')), ENT_QUOTES, 'UTF-8'),
				'dob' => htmlspecialchars(strip_tags($this->input->post('con_dob')), ENT_QUOTES, 'UTF-8'),
				'user_id' => $user_id,
				'emp_accNo' => $accNo,
				'self_reg' => $self_reg,
				'upload_type' => $upload_typ,
				'date' => date("Y-m-d H:i:s")
			);

			echo $result = $this->Contact_CSV_model->add($data);
			// $this->Call_center_model->call_allocation($desig, $acc_no);
		}
	}

	function readExcel()
	{
		$user_id = htmlspecialchars(strip_tags($this->session->userdata('id')), ENT_QUOTES, 'UTF-8');
		$desig = htmlspecialchars(strip_tags($this->session->userdata('Val')), ENT_QUOTES, 'UTF-8');
		$acc_no = htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');
		$upload_type = htmlspecialchars(strip_tags($this->input->post('upload_type')), ENT_QUOTES, 'UTF-8');

		if ($desig == 13) {
			$upload_typ = '';
		} else {
			$upload_typ = $upload_type;
		}

		$response = array();
		$file_tmp = $_FILES["con_csv"]["tmp_name"];
		$file_extension = pathinfo($_FILES["con_csv"]["name"], PATHINFO_EXTENSION);
		$filename = pathinfo($_FILES['con_csv']['name'], PATHINFO_FILENAME);

		if ($file_extension == "csv") {
			if (($fp = fopen($file_tmp, "r")) !== FALSE) {
				// Skip the first line
				fgetcsv($fp);

				//$data['csvData'] =  $fp;
				$result = $this->Contact_CSV_model->insert_csv($fp, $user_id, $user_id, $filename, $upload_typ);
				//$this->Call_center_model->call_allocation($desig, $acc_no);

				if ($result == 'success') {
					$response['status'] = 'success';
				} else if ($result == 'error') {
					$response['status'] = 'error';
				} else {
					$response['status'] = $result;
				}
			} else {
				$response['status'] = 'error2';
			}
		} else {
			$response['status'] = $file_extension;
		}
		echo json_encode($response);
	}

	/******************** MAP OPERATOR ********************/
	public function map_operator()
	{
		$operator = htmlspecialchars(strip_tags($this->input->post('operator')), ENT_QUOTES, 'UTF-8');
		$acc_no =  htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');

		$data = array(
			'mapped_to' => $operator,
		);

		$result = $this->Contact_CSV_model->mapping_operator($data, $acc_no);

		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		}

		echo json_encode($response);
	}

	/******************** con_no EXISTS ********************/

	function search_con_no()
	{
		$this->Contact_CSV_model->search_con_no();
	}

	public function con_no_check()
	{
		$con_no = htmlspecialchars(strip_tags($this->input->post('con_no')), ENT_QUOTES, 'UTF-8');

		$result = $this->Contact_CSV_model->con_no_check($con_no);
		if ($result == 'success') {
			echo "success";
		} else if ($result == 'con_no exists') {
			echo "con_no exists";
		} else {
			echo "error";
		}
	}


	/******************** GET DATASET ********************/
	public function get_dataset()
	{
		$con_no = htmlspecialchars(strip_tags($this->input->post('con_no')), ENT_QUOTES, 'UTF-8');

		$get_dataset = $this->Contact_CSV_model->get_dataset($con_no);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** call summary ********************/
	public function call_summary()
	{
		$date1 = htmlspecialchars(strip_tags($this->input->post('date1')), ENT_QUOTES, 'UTF-8');
		$date2 = htmlspecialchars(strip_tags($this->input->post('date2')), ENT_QUOTES, 'UTF-8');

		$get_dataset = $this->Contact_CSV_model->call_summary($date1, $date2);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** PAUSE / ACTIVATE OPERATOR ********************/
	public function pause_operator()
	{
		$id = htmlspecialchars(strip_tags($this->input->post('id')), ENT_QUOTES, 'UTF-8');
		$is_present = htmlspecialchars(strip_tags($this->input->post('is_present')), ENT_QUOTES, 'UTF-8');

		$data = array(
			'is_present' => $is_present,
		);

		$result = $this->Contact_CSV_model->pause_operator($data, $id);

		if ($result) {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/******************** GOOGLE CONTACT ********************/
	public function google_contacts($menu = NULL, $dater = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$acc_no = htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');

		$resultlist = $this->Contact_CSV_model->google_contacts($dater, $acc_no);
		$data['resultlist'] = $resultlist;

		$con_cnt = $this->Contact_CSV_model->google_contacts_count($dater, $acc_no);
		$data['con_cnt'] = $con_cnt;

		$this->load->view('layout/header', $data);
		$this->load->view('google_contacts', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** ALLOCATE ********************/
	public function allocate()
	{

		$result = $this->Contact_CSV_model->allocate();

		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else {
			$response['status'] = $result;
		}

		echo json_encode($response);
	}

	/******************** INSERTED CONTACTS ********************/
	public function inserted_contacts($menu = NULL, $ac_date = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		if (empty($ac_date)) {
			$date = date("Y-m-d");
			$date1 = $date2 = $date;
		} else {
			$daterange = $ac_date;
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$load_data = $this->Contact_CSV_model->inserted_contacts($date1, $date2);
		$data['load_data'] = $load_data;

		$get_csv = $this->Contact_CSV_model->get_csv($date1, $date2);
		$data['get_csv'] = $get_csv;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			$daterange = $this->input->post('date_range');
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 13, 24);

			if (isset($search)) {
				if ($search == 'search_filter') {

					$load_data = $this->Contact_CSV_model->inserted_contacts($date1, $date2);
					$data['load_data'] = $load_data;
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('inserted_contacts', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** REAPPLIED CONTACTS ********************/
	public function reapplied_contacts($menu = NULL, $daterange = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');
		$val = strip_tags($this->session->userdata('Val'));

		$data['val'] = $val;

		if ($val == 12 || $val == 15) {
			$agent = '';
		} else {
			$agent = strip_tags($this->session->userdata('acc_no'));
		}

		if ($daterange == '') {
			$date1 = $date2 = date('Y-m-d');
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$load_data = $this->Contact_CSV_model->reapplied_contacts($date1, $date2, $agent);
		$data['load_data'] = $load_data;


		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {

					$load_data = $this->Contact_CSV_model->reapplied_contacts($date1, $date2, $agent);
					$data['load_data'] = $load_data;
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('reapplied_contacts', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** REAPPLIED REJECT ********************/
	public function reapplied_reject()
	{
		$cid = htmlspecialchars(strip_tags($this->input->post('cid')), ENT_QUOTES, 'UTF-8');
		$con_no = htmlspecialchars(strip_tags($this->input->post('con_no')), ENT_QUOTES, 'UTF-8');

		$result = $this->Contact_CSV_model->reapplied_reject($cid, $con_no);
		if ($result == 'success') {
			echo 'success';
		} else {
			echo 'error';
		}
	}

	/******************** REAPPLIED CONTACTS ********************/
	public function reapplied_reject_list($menu = NULL, $daterange = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');
		$val = strip_tags($this->session->userdata('Val'));


		if ($val == 12 || $val == 15) {
			$agent = '';
		} else {
			$agent = strip_tags($this->session->userdata('acc_no'));
		}

		if ($daterange == '') {
			$date1 = $date2 = date('Y-m-d');
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		};

		$load_data = $this->Contact_CSV_model->reapplied_reject_list($date1, $date2, $agent);
		$data['load_data'] = $load_data;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');


			if (isset($search)) {
				if ($search == 'search_filter') {
					$load_data = $this->Contact_CSV_model->reapplied_reject_list($date1, $date2, $agent);
					$data['load_data'] = $load_data;
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('reapplied_reject_list', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** DELETE ********************/
	public function delete_csv()
	{

		$data = array(
			'csv_name' => $this->input->post('csv_name'),
		);

		$result = $this->Contact_CSV_model->delete_csv($data);
		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/******************** AGENT TANSFER - WORKSHOP REJECT ********************/
	public function transfer_operator()
	{
		$mapped_operator = strip_tags($this->input->post('mapped_operator'));
		$backdate_days = strip_tags($this->input->post('backdate_days'));
		$acc_no =  htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');

		$result = $this->Contact_CSV_model->transfer_operator($acc_no, $mapped_operator, $backdate_days);
		if ($result) {
			$response = 'success';
		} else if ($result == 'error') {
			$response = 'error';
		}

		echo ($response);
	}

	/******************** AGENT TANSFER - WORKSHOP SMS ********************/
	public function transfer_operator_ws()
	{
		$mapped_operator = strip_tags($this->input->post('mapped_operator'));
		$backdate_days = strip_tags($this->input->post('backdate_days'));
		$acc_no =  htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');

		$result = $this->Contact_CSV_model->transfer_operator_ws($mapped_operator, $backdate_days, $acc_no);
		if ($result == 'success') {
			$response = 'success';
		} else {
			$response = 'error';
		}

		echo ($response);
	}

	/******************** REAPPLIED REJECT ********************/
	public function reapplied_reject_retrieve()
	{
		$mapped_operator = strip_tags($this->input->post('mapped_operator'));
		$backdate_days = strip_tags($this->input->post('backdate_days'));
		$acc_no =  htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');

		$result = $this->Contact_CSV_model->reapplied_reject_retrieve($acc_no, $mapped_operator, $backdate_days);
		if ($result) {
			$response = 'success';
		} else if ($result == 'error') {
			$response = 'error';
		}

		echo ($response);
	}
}
