<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_group_profile extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');
			
		$load_data = $this->Group_profile_model->load_data();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('group_profile',$data);
		$this->load->view('layout/footer', $data);
			

	}

	/******************** INSERT ********************/
	public function add(){

		if($this->session->has_userdata('username')){
			$this->data = array();

			$user = strip_tags($this->session->userdata('username'));

			$user_details = $this->My_profile_model->user_details($user);
			$data['user_details'] = $user_details;


			$data = array(
				'profile' => $this->input->post('group_name')
			);
			
			$result = $this->Group_profile_model->insert($data);
			if ($result == 'success') {
				echo 'success';
			}else if ($result == 'data exists') {
				echo 'data exists';
			}else{
				echo 'error';
			}
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Group_profile_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update(){

		$data = array(
			'profile' => $this->input->post('group_name_up')
		);
		
		$id = $this->input->post('id_up');

		
		$result = $this->Group_profile_model->update($data,$id);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** DELETE ********************/
	public function delete(){
		$id = strip_tags($this->input->post('id'));

		$data = array(
			'id' => $this->input->post('id'),
		);

		$result = $this->Group_profile_model->delete($data);
		if ($result) {
			$response['status'] = 'success';
		} else if ($result=='error') {
			$response['status'] = 'error';
		} else if($result=='exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/***************************************************/

}
?>