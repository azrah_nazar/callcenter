<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_timetable extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('Add_course_model'); 
		$this->load->model('Staff_list_model'); 
		$this->load->model('Timetable_model'); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$load_lec = $this->Staff_list_model->load_data();
		$data['load_lec'] = $load_lec;

		$load_course = $this->Add_course_model->load_data();
		$data['load_course'] = $load_course;

		$load_data = $this->Timetable_model->load_data();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('timetable',$data);
		$this->load->view('layout/footer', $data);
	}

	/******************** INSERT ********************/
	public function add(){

		$data = array(
			'lecturer' => $this->input->post('lecturer'),
			'course' => $this->input->post('course_name'),
			'c_date' => $this->input->post('c_date'),
			'c_time' => $this->input->post('c_time'),
			'no_slot' => $this->input->post('slot'),
			
		);
		
		$result = $this->Timetable_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'order no exists') {
			echo 'order no exists';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Timetable_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update(){

		$data = array(
			'lecturer' => $this->input->post('lecturer_up'),
			'course' => $this->input->post('course_name_up'),
			'c_date' => $this->input->post('c_date_up'),
			'c_time' => $this->input->post('c_time_up'),
			'no_slot' => $this->input->post('slot_up'),
		);
		
		$id = $this->input->post('id_up');

		
		$result = $this->Timetable_model->update($data,$id);
		if ($result == 'success') {
			echo 'success';
		}else{
			echo 'error';
		}
	}

	/******************** DELETE ********************/
	public function delete(){
		$id = strip_tags($this->input->post('id'));

		$data = array(
			'id' => $this->input->post('id'),
		);

		$result = $this->Timetable_model->delete($data);
		if ($result) {
			$response['status'] = 'success';
		} else if ($result=='error') {
			$response['status'] = 'error';
		} else if($result=='exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/***************************************************/

}
?>