<?php
defined('BASEPATH') or exit('No direct script access allowed');

class  Con_Call_Center_Summary extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Call_Center_Summary_Model');
	}

	public function index($menu = NULL, $daterange = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		if ($daterange == '') {
			$date1 = $date2 = date('Y-m-d');
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}
		
		$list = $this->Call_Center_Summary_Model->details($date1, $date2);
		$data['list'] = $list;
		
		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');


			if (isset($search)) {
				if ($search == 'search_filter') {
					$list = $this->Call_Center_Summary_Model->details($date1, $date2);
					$data['list'] = $list;
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('Call_Center_Summary', $data);
		$this->load->view('layout/footer', $data);
	}
}
