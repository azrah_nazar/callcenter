<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Con_call_center extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('Call_center_model');

	}

	public function index($menu = NULL, $id = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');
		
		$emp = strip_tags($this->session->userdata('acc_no'));

		$load_data = $this->Call_center_model->load_contact_list($emp);
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('Call_center',$data);
		$this->load->view('layout/footer', $data);
	}

	public function get_data()
	{
		$output = $this->Call_center_model->get_data();
		echo json_encode($output);
	}

	public function update_number()
	{
		$number = $this->input->post('number');
		$output = $this->Call_center_model->update_number($number);
	}

	public function response_accept(){
		$number = $this->input->post('number');
		$res_flag = $this->input->post('res_flag');

		$result = $this->Call_center_model->response_update($number, $res_flag);
		if ($result=='success') {
			$res_sms = $this->Call_center_model->welcome_sms($number);
			$response['status'] = 'success';
		}else if ($result=='success2') {
			$response['status'] = 'success';
		}else if ($result=='error') {
			$response['status'] = 'error';
		}

		echo json_encode($response);
	}

	public function response_update(){
		$number = $this->input->post('number');
		$res_flag = $this->input->post('res_flag');

		$result = $this->Call_center_model->response_update($number, $res_flag);
		if ($result=='success') {
			$response['status'] = 'success';
		}else if ($result=='error') {
			$response['status'] = 'error';
		}

		echo json_encode($response);
	}

	/******************** UPDATE REMINDER ********************/
	public function update_reminder(){
		$tm = date("Y-m-d", strtotime($this->input->post('reminder_date'))) . ' ' . date("G:i", strtotime($this->input->post('r_time')));

		$data = array(
			'flag' => '0',
			'reminder_date' => $tm,
			'reminder_note' => $this->input->post('note')
		);

		$data2 = array(
			'con_id' => $this->input->post('id_up'),
			'sender' => strip_tags($this->session->userdata('acc_no')),
			'message' => "Reminder: ".$this->input->post('note'),
			'dt_time' => $tm,
			'flag' => 1,
			'reply_id' => ''
		);
		$result2 = $this->Call_Report_Model->send_reply($data2);
		
		$tel_no = $this->input->post('tel_no');

		
		$result = $this->Call_center_model->update_reminder($data,$tel_no);
		if ($result == 'success') {
			echo 'success';
		}else{
			echo 'error';
		}
	}

	/******************** SHUFFLE ********************/
	public function shuffle_op(){

		$tp = $this->input->post('tp');

		$result = $this->Call_center_model->shuffle_op($tp);
		if ($result == 'success') {
			echo 'success';
		}else{
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Call_center_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update(){
		$con_no = $this->input->post('con_no');
		$tp = str_replace(' ', '', $con_no);
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$dt_tm = date('Y-m-d H:i:s');

		$data = array(
			'con_no' => $tp, 
			'name' => $this->input->post('con_name'),
			'address' => $this->input->post('con_adrz'),
			'whatsapp' => $this->input->post('con_whatsapp'),
			'job' => $this->input->post('con_job'),
			'dob' => $this->input->post('con_dob'),
			'updated_user' => $acc_no,
			'updated_time' => $dt_tm,
		);
		
		$id = $this->input->post('con_id');

		
		$result = $this->Call_center_model->update($data,$id);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data not exists') {
			echo 'data not exists';
		}else{
			echo 'error';
		}
	}

	/******************** DIRECT SALE ********************/
	public function direct_sale(){
		
		$tp = $this->input->post('tp');
		
		$result = $this->Call_center_model->direct_sale($tp);
		if ($result == 'success') {
			echo 'success';
		}else{
			echo 'error';
		}
	}

	/******************** DIRECT SALE ********************/
	public function direct_sale_view($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');
		
		$emp = strip_tags($this->session->userdata('acc_no'));

		$load_data = $this->Call_center_model->direct_sale_list($emp);
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('direct_sale',$data);
		$this->load->view('layout/footer', $data);
	}

	/******************** SPECIAL CUSTOMER ********************/
	public function spl_customer(){
		
		$tp = $this->input->post('tp');
		$flag = $this->input->post('flag');

		if($flag == 1){
			$cus = '1';
		}else{
			$cus = '0';
		}
		
		$result = $this->Call_center_model->spl_customer($tp, $cus);
		if ($result == 'success') {
			echo 'success';
		}else{
			echo 'error';
		}
	}

}
?>