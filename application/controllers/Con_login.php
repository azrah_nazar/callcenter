<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Con_login extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('login');
	}


	/* 
		Check if the login form is submitted, and validates the user credential
		If not submitted it redirects to the login page
	*/
	public function login()
	{

		// if ($this->auth->logged_in()) {
		//     $this->auth->is_logged_in(true);
		// }

		$this->form_validation->set_rules('username', 'User Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == TRUE) {
			// true case
			$user_exists = $this->User_account_model->check_user($this->input->post('username'));

			if ($user_exists == 'username exists') {
				$login = $this->Login_model->login($this->input->post('username'), $this->input->post('password'));

				if ($login == 'access denied') {
					$this->data['errors'] = 'Access Denied!';
					$this->load->view('login', $this->data);
				}else if ($login) {
					$acc_no = $login['acc_no'];
					$desig = $login['Val'];
					$dt = date("Y-m-d H:i:s");
					$time = date("H:i:s");

					$data = array(
						'Usr' => $acc_no,
						'Panel' => 'System Login',
						'Date' => $dt,
						'Tym' => $time,
						'p_id' => 8
					);

					$user_log = $this->Login_model->user_log($data);

					$logged_in_sess = array(
						'id' => $login['uid'],
						'username'  => $login['ID'],
						'Val'  => $login['Val'],
						'acc_no'  => $login['acc_no'],
						'u_shift_id'  => $login['Shift'],
						'cash'  => $login['cash'],
						'gender'  => $login['gender'],
						'logged_in' => TRUE
					);

					$this->session->set_userdata($logged_in_sess);

					if ($desig == 12) {
						redirect('Con_Contact_Allocation/index/191', 'refresh');
					}else if ($desig == 17 || $desig == 18) {
						redirect('Con_post_registry/full_payment/216', 'refresh');
					} else {
						redirect('Con_call_center/index/179', 'refresh');
					}
				} else {
					$this->data['errors'] = 'Incorrect Username / Password combination!';
					$this->load->view('login', $this->data);
				}
			} else {
				$this->data['errors'] = 'User Name does not exists';

				$this->load->view('login', $this->data);
			}
		} else {
			// false case
			$this->load->view('login');
		}
	}

	public function logged_in()
	{
		$session_data = $this->session->userdata();
		if ($session_data['logged_in'] == TRUE) {
			redirect('Con_my_profile/index', 'refresh');
		}
	}

	public function not_logged_in()
	{
		$session_data = $this->session->userdata();
		if ($session_data['logged_in'] == FALSE) {
			redirect('Con_login', 'refresh');
		}
	}

	/*
		clears the session and redirects to login page
	*/
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('Con_login', 'refresh');
	}

	/******************** USER LOG ********************/
	function user_log($activity, $dt, $time, $acc_no)
	{

		$data = array(
			'Usr' => $acc_no,
			'Panel' => $activity,
			'Date' => $dt,
			'Tym' => $time,
			'p_id' => 8,
		);

		$result = $this->Common_model->insert_log($data);
	}
}
?>
