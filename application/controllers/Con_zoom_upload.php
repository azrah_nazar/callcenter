<?php
defined('BASEPATH') or exit('No direct script access allowed');

class  Con_zoom_upload extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('CSVReader');
        $this->load->model('Zoom_upload_model');
        $this->load->model('Call_Report_Model');
        $this->load->model('Workshop_model');
        $this->load->model('Pre_registry_model');
    }

    public function index($menu = NULL, $id = NULL)
    {
        require_once(APPPATH . 'libraries/User_privileges.php');

        $load_data = $this->Workshop_model->load_data();
        $data['load_ws'] = $load_data;

        $workshop = $this->input->post('workshop');

        $load_data = $this->Zoom_upload_model->load_data($workshop);
        $data['load_data'] = $load_data;

        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $search = $this->input->post('search');

            if (isset($search)) {
                if ($search == 'search_filter') {
                    $load_data = $this->Zoom_upload_model->load_data($workshop);
                    $data['load_data'] = $load_data;
                }
            }
        }

        $this->load->view('layout/header', $data);
        $this->load->view('zoom_upload', $data);
        $this->load->view('layout/footer', $data);
    }

    /******************** reg NO EXISTS ********************/
    function search_reg_no()
    {
        $this->Zoom_upload_model->search_reg_no();
    }

    /******************** CSV ********************/
    function readExcel()
    {
        $user_id = strip_tags($this->session->userdata('id'));
        $response = array();
        $file_tmp = $_FILES["con_csv"]["tmp_name"];
        $file_extension = pathinfo($_FILES["con_csv"]["name"], PATHINFO_EXTENSION);

        if ($file_extension == "csv") {
            if (($fp = fopen($file_tmp, "r")) !== FALSE) {
                // Skip the first line
                fgetcsv($fp);

                $result = $this->Zoom_upload_model->insert_csv($fp, $user_id);

                if ($result == 'success') {
                    $response['status'] = 'success';
                } else {
                    $response['status'] = 'error';
                }
            } else {
                $response['status'] = 'error2';
            }
        } else {
            $response['status'] = 'invalid';
        }

        echo json_encode($response);
    }

    /******************** INSERT ********************/
    public function add()
    {
        $user_id = strip_tags($this->session->userdata('id'));

        $reg_no = $this->input->post('reg_no');
        $email = $this->input->post('email');
        $join_time = $this->input->post('join_time');
        $leave_time = $this->input->post('leave_time');
        $duration = $this->input->post('duration');
        $is_guest = $this->input->post('is_guest');

        $data = array(
            'reg_no' => $reg_no,
            'user_email' => $email,
            'join_time' => $join_time,
            'leave_time' => $leave_time,
            'duration' => $duration,
            'is_guest' => $is_guest,
            'user_id' => $user_id,
            'ins_date' => date("Y-m-d H:i:s")
        );

        $this->db->select('Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice');
        $this->db->from('con_list');
        $this->db->join('Employee', 'Employee.Acc_No = con_list.emp_accNo');
        $this->db->where("con_list.reg_no", $reg_no);
        $qry = $this->db->get();
        $rw = $qry->row();
        $emp_name = $rw->name;
        $emp_tp = $rw->Tp;
        $emp_tp2 = ' / ' . $rw->tpOffice;
        $sinhala_name = $rw->sinhala_name;

        $msg = "අපගේ හඳුන්වා දීමේ වැඩසටහන සඳහා ඔබගේ වටිනා කාලය වෙන්කිරීම පිළිබදව අපගේ ස්තුතිය! පාඨමාලාව සඳහා ඔබ සහභාගි වීමට අදහස් කරන්නේ නම් පහත ලින්ක් එක භාවිත කර ඔබගේ ලියාපදිංචිය සදහා අවශ්‍ය තොරතුරු ලබා ගන්න https://callcenter.victoryacademylk.com/bank-details/" . $reg_no . "\n" . $sinhala_name . " " . $emp_tp . '' . $emp_tp2;
        $this->Pre_registry_model->common_sms($reg_no, $msg);
        $this->Zoom_upload_model->update_zoom($reg_no);

        echo $result = $this->Zoom_upload_model->add($data);
    }

    /******************** WORKSHOP FOLLOW UP ********************/
    public function workshop_follow_up($menu = NULL)
    {
        require_once(APPPATH . 'libraries/User_privileges.php');

        $emp = strip_tags($this->session->userdata('acc_no'));
        $val = strip_tags($this->session->userdata('Val'));

        $data['val'] = $val;

        if ($val == 12 || $val == 15) {
            $agent = $this->input->post('agent');
        } else {
            $agent = $emp;
        }
        $workshop = $this->input->post('workshop');

        $load_agent = $this->Call_Report_Model->load_agent_except_me($emp);
        $data['load_agent'] = $load_agent;

        $course_fee = $this->Zoom_upload_model->course_fee();
        $data['course_fee'] = $course_fee['course_fee'];

        $load_workshop = $this->Workshop_model->load_data();
        $data['load_workshop'] = $load_workshop;

        $load_data = $this->Zoom_upload_model->load_attendance($agent, $workshop);
        $data['load_data'] = $load_data;

        $pay_flag = $this->Zoom_upload_model->pay_flag();
        $data['pay_flg'] = $pay_flag;

        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $search = $this->input->post('search');

            if (isset($search)) {
                if ($search == 'search_filter') {
                    $this->form_validation->set_rules('agent', 'Agent', 'trim|xss_clean');
                    $this->form_validation->set_rules('workshop', 'Workshop', 'trim|xss_clean');

                    if ($this->form_validation->run() == FALSE) {
                    } else {

                        $load_data = $this->Zoom_upload_model->load_attendance($agent, $workshop);
                        $data['load_data'] = $load_data;

                        $pay_flag = $this->Zoom_upload_model->pay_flag();
                        $data['pay_flag'] = $pay_flag;
                    }
                }
            }
        }

        $this->load->view('layout/header', $data);
        $this->load->view('workshop_follow_up', $data);
        $this->load->view('layout/footer', $data);
    }

    /******************** AGENT TANSFER ********************/
	public function agent_tranfer()
	{
		$con_id = strip_tags($this->input->post('con_id2'));
		$today = date('Y-m-d H:i:s');

		$data = array(
			'emp_accNo' => strip_tags($this->input->post('agent')),
			'date' => $today,
		);

		$result = $this->Call_Report_Model->agent_tranfer($data, $con_id);
		if ($result) {
			$response = 'success';
		} else if ($result == 'error') {
			$response = 'error';
		} else if ($result == 'exists') {
			$response = 'exists';
		}

		echo ($response);
	}

    /******************** GET USDT ********************/
    public function get_usdt()
    {
        $reg = strip_tags($this->input->post('reg'));

        $get_dataset = $this->Zoom_upload_model->get_usdt($reg);
        $data['result'] = $get_dataset;

        echo json_encode($data);
    }

    /******************** GET ACCEPTED DEPOSITS ********************/
    public function get_accepted_deposits()
    {
        $reg = strip_tags($this->input->post('reg'));

        $get_dataset = $this->Zoom_upload_model->get_accepted_deposits($reg);
        $data['result'] = $get_dataset;

        echo json_encode($data);
    }
}
