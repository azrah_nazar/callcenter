<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_designation extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($menu = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		$top_level = $this->Designation_model->top_level();
		$data['top_level'] = $top_level;

		$load_data = $this->Designation_model->load_data();
		$data['load_data'] = $load_data;

		$this->load->view('layout/header', $data);
		$this->load->view('designation',$data);
		$this->load->view('layout/footer', $data);
	}

	/******************** INSERT ********************/
	public function add(){

		$data = array(
			'up_level' => $this->input->post('up_levl'),
			'desig' => $this->input->post('desig')
		);
		
		$result = $this->Designation_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** GET DATASET ********************/
	public function get_dataset(){
		$id = strip_tags($this->input->post('id'));

		$get_dataset = $this->Designation_model->get_dataset($id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/******************** UPDATE ********************/
	public function update(){

		$data = array(
			'up_level' => $this->input->post('levl_up'),
			'desig' => $this->input->post('desig_up')
		);
		
		$id = $this->input->post('id_up');

		
		$result = $this->Designation_model->update($data,$id);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	/******************** DELETE ********************/
	public function delete(){
		$id = strip_tags($this->input->post('id'));

		$data = array(
			'id' => $this->input->post('id'),
		);

		$result = $this->Designation_model->delete($data);
		if ($result) {
			$response['status'] = 'success';
		} else if ($result=='error') {
			$response['status'] = 'error';
		} else if($result=='exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

}
?>