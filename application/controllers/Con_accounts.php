<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Con_accounts extends CI_Controller {

	function __construct() { 
		parent::__construct(); 
	}

	public function index($account = NULL, $menu = NULL, $ac_date = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		if (empty($ac_date)) {
		  $date = date("Y-m-d");
		  $date1 = $date2 = $date; 
		} else {
		  $daterange = $ac_date;
		  $date1 = substr($daterange, 0, 10);
		  $date2 = substr($daterange, 17, 23);
		}

			$list = $this->Accounts_model->account($account, $date1, $date2);
			$data['list'] = $list;

			$op_bal = $this->Accounts_model->opening_balance($account, $date1);
			$data['op_bal'] = $op_bal;
		
			$this->load->view('layout/header', $data);
			$this->load->view('accounts',$data);
			$this->load->view('layout/footer', $data);
	}

	

	/***************************************************/

}
?>