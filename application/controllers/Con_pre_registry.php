<?php
defined('BASEPATH') or exit('No direct script access allowed');

class  Con_pre_registry extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Pre_registry_model');
		$this->load->model('Workshop_model');
		$this->load->model('Call_Report_Model');
		$this->load->model('Call_center_model');
	}

	public function index($menu = NULL, $id = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$emp = strip_tags($this->session->userdata('acc_no'));

		$load_data = $this->Pre_registry_model->load_follow_up($emp);
		$data['load_data'] = $load_data;

		$pending_count = $this->Pre_registry_model->pending_count($emp);
		$data['pending_count'] = $pending_count;

		$this->load->view('layout/header', $data);
		$this->load->view('follow_ups', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** UPDATE REMINDER ********************/
	public function update_reminder(){
		$tm = date("Y-m-d", strtotime($this->input->post('reminder_date'))) . ' ' . date("G:i", strtotime($this->input->post('r_time')));

		$data = array(
			'flag' => '1',
			'reminder_date' => $tm,
			'reminder_note' => $this->input->post('note')
		);

		$data2 = array(
			'con_id' => $this->input->post('id_up'),
			'sender' => strip_tags($this->session->userdata('acc_no')),
			'message' => "Reminder: ".$this->input->post('note'),
			'dt_time' => $tm,
			'flag' => 1,
			'reply_id' => ''
		);
		$result2 = $this->Call_Report_Model->send_reply($data2);
		
		$tel_no = $this->input->post('tel_no');

		
		$result = $this->Call_center_model->update_reminder($data,$tel_no);
		if ($result == 'success') {
			echo 'success';
		}else{
			echo 'error';
		}
	}

	public function cron_job()
	{
		$load_data = $this->Pre_registry_model->cron_job();
		$data['load_data'] = $load_data;
	}

	public function workshop_pre_registry()
	{
		$reg_no = htmlspecialchars(strip_tags($this->uri->segment(2)), ENT_QUOTES, 'UTF-8');

		$st_details = $this->Pre_registry_model->get_st_details($reg_no);
		$data['st_details'] = $st_details;

		$workshops = $this->Workshop_model->next_ws2();
		$data['workshops'] = $workshops;

		$this->load->view('workshop_pre_registry', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** BANK DETAILS ********************/
	public function bank_details()
	{
		$reg_no = htmlspecialchars(strip_tags($this->uri->segment(2)), ENT_QUOTES, 'UTF-8');

		$st_details = $this->Pre_registry_model->get_st_details($reg_no);
		$data['st_details'] = $st_details;

		$this->load->view('bank_details', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** FEEDBACK DETAILS ********************/
	public function feedback()
	{
		$reg_no = htmlspecialchars(strip_tags($this->uri->segment(2)), ENT_QUOTES, 'UTF-8');

		$st_details = $this->Pre_registry_model->get_st_ws_details($reg_no);
		$data['st_details'] = $st_details;

		$this->load->view('feedback', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** FEEDBACK YES ********************/
	public function feedback_yes()
	{
		$telNo = htmlspecialchars(strip_tags($this->input->post('telNo')), ENT_QUOTES, 'UTF-8');

		$result = $this->Workshop_model->confirmation($telNo);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data exists') {
			echo 'data exists';
		} else {
			echo 'error';
		}
	}

	/******************** FEEDBACK No ********************/
	public function feedback_no()
	{
		$telNo = htmlspecialchars(strip_tags($this->input->post('telNo')), ENT_QUOTES, 'UTF-8');

		$result = $this->Workshop_model->confirmation_no($telNo);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data exists') {
			echo 'data exists';
		} else {
			echo 'error';
		}
	}

	/******************** FEEDBACK REJECT ********************/
	public function pending_payment()
	{
		$telNo = htmlspecialchars(strip_tags($this->input->post('telNo')), ENT_QUOTES, 'UTF-8');
		$flag = htmlspecialchars(strip_tags($this->input->post('flag')), ENT_QUOTES, 'UTF-8');

		$result = $this->Workshop_model->pending_payment($telNo, $flag);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data exists') {
			echo 'data exists';
		} else {
			echo 'error';
		}
	}

	/******************** PENDING REALLOCATION ********************/
	public function pending_reallocation()
	{
		$telNo = htmlspecialchars(strip_tags($this->input->post('telNo')), ENT_QUOTES, 'UTF-8');
		$flag = htmlspecialchars(strip_tags($this->input->post('flag')), ENT_QUOTES, 'UTF-8');

		$result = $this->Workshop_model->pending_reallocation($telNo, $flag);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data exists') {
			echo 'data exists';
		} else {
			echo 'error';
		}
	}

	/******************** FEEDBACK REJECT ********************/
	public function feedback_reject()
	{
		$telNo = htmlspecialchars(strip_tags($this->input->post('telNo')), ENT_QUOTES, 'UTF-8');

		$result = $this->Workshop_model->confirmation_reject($telNo);
		if ($result == 'success') {
			echo 'success';
		} else {
			echo 'error';
		}
	}

	/******************** CLASS FOLLOWWUP ********************/
	public function clz_followup()
	{
		$telNo = htmlspecialchars(strip_tags($this->input->post('telNo')), ENT_QUOTES, 'UTF-8');

		$result = $this->Workshop_model->clz_followup($telNo);
		if ($result == 'success') {
			echo 'success';
		} else {
			echo 'error';
		}
	}

	/******************** UPDATE ********************/
	public function update_details()
	{
		$con_no = htmlspecialchars(strip_tags($this->input->post('con_no')), ENT_QUOTES, 'UTF-8');
		$data = array(
			'name' => htmlspecialchars(strip_tags($this->input->post('con_name')), ENT_QUOTES, 'UTF-8'),
			'address' => htmlspecialchars(strip_tags($this->input->post('con_adrz')), ENT_QUOTES, 'UTF-8'),
			'whatsapp' => htmlspecialchars(strip_tags($this->input->post('con_whatsapp')), ENT_QUOTES, 'UTF-8'),
			'email' => htmlspecialchars(strip_tags($this->input->post('con_email')), ENT_QUOTES, 'UTF-8'),
			'nic' => htmlspecialchars(strip_tags($this->input->post('con_nic')), ENT_QUOTES, 'UTF-8'),
		);

		$reg_no = htmlspecialchars(strip_tags($this->input->post('reg_num')), ENT_QUOTES, 'UTF-8');


		$result = $this->Pre_registry_model->update_details($data, $reg_no, $con_no);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data exists') {
			echo 'data exists';
		} else {
			echo 'error';
		}
	}

	/******************** RESERVE ********************/
	public function reserve_ws()
	{
		$reg_num = htmlspecialchars(strip_tags($this->input->post('reg_num')), ENT_QUOTES, 'UTF-8');
		$con_no = htmlspecialchars(strip_tags($this->input->post('con_no')), ENT_QUOTES, 'UTF-8');
		$acc_no = strip_tags($this->session->userdata('acc_no'));

		$data = array(
			'workshop' => htmlspecialchars(strip_tags($this->input->post('ws_id')), ENT_QUOTES, 'UTF-8'),
			'flag' => 4,
		);

		$result = $this->Pre_registry_model->update_details($data, $reg_num, $con_no);

		$result2 = $this->Workshop_model->link_sms_individual($reg_num, $acc_no= null);

		if ($result) {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		}

		echo json_encode($response);
	}

	/******************** WORKSHOP REGISTERED LIST ********************/
	public function registered_list($menu = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$emp = htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');
		$val = htmlspecialchars(strip_tags($this->session->userdata('Val')), ENT_QUOTES, 'UTF-8');

		$data['val'] = $val;

		if ($val == 12 || $val == 15) {
			$agent = htmlspecialchars(strip_tags($this->input->post('agent')), ENT_QUOTES, 'UTF-8');
		} else {
			$agent = $emp;
		}

		$load_agent = $this->Call_Report_Model->load_agent();
		$data['load_agent'] = $load_agent;

		$resultlist = $this->Pre_registry_model->registered_list($agent);
		$data['resultlist'] = $resultlist;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {
					$this->form_validation->set_rules('agent', 'Agent', 'trim|xss_clean');

					if ($this->form_validation->run() == FALSE) {
					} else {

						$resultlist = $this->Pre_registry_model->registered_list($agent);
						$data['resultlist'] = $resultlist;
					}
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('registered_list', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** CLASS SMS ********************/
	public function class_sms()
	{
		$reg = htmlspecialchars(strip_tags($this->input->post('reg')), ENT_QUOTES, 'UTF-8');
		
		$this->db->select('Employee.name, Employee.Tp, Employee.sinhala_name, Employee.tpOffice');
		$this->db->from('con_list');
		$this->db->join('Employee', 'Employee.Acc_No = Usert.acc_no');
		$this->db->where("con_list.reg_no", $reg);
		$qry = $this->db->get();
		$rw = $qry->row();
		$emp_name = $rw->name;
		$emp_tp = $rw->Tp;
		$emp_tp2 = ' / '.$rw->tpOffice;
		$sinhala_name = $rw->sinhala_name;
		
		$msg = "ඔබගේ පාඨමාලා ගාස්තුවට අදාල මුදල් අප ගිණුමට සාර්ථකව බැරවී  ඇති බව සතුටින් තහවුරු කරමු. පහත ලින්ක් එක භාවිත කර ඔබගේ රිසිට්පත ලබාගන්න.\nhttps://callcenter.victoryacademylk.com/class-register/" . trim($reg) . "\n" . $sinhala_name . " " . $emp_tp.' '.$emp_tp2;
		$result = $this->Pre_registry_model->common_sms($reg, $msg);
		
		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		} else {
			$response['status'] = $result;
		}
		
		echo json_encode($response);
	}
	
	/******************** SEARCH STUDENT ********************/
	public function contact_search($menu = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$val = htmlspecialchars(strip_tags($this->session->userdata('Val')), ENT_QUOTES, 'UTF-8');
		$acc_no = htmlspecialchars(strip_tags($this->session->userdata('acc_no')), ENT_QUOTES, 'UTF-8');

		$data['val'] = $val;
		$data['acc_no'] = $acc_no;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_full') {
					$this->form_validation->set_rules('search_text', 'keyword', 'trim|xss_clean|required');
					
					if ($this->form_validation->run() == FALSE) {
					
					} else {
						$search_text = $this->input->post('search_text');
						$resultlist = $this->Pre_registry_model->searchKeyword($search_text, $val, $acc_no);
						$data['resultlist'] = $resultlist;
					}
				}
			}
		}

		json_encode($data);

		$this->load->view('layout/header', $data);
		$this->load->view('contact_search', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** REALLOCATE WORKSHOP ********************/
	public function reallocate_ws(){

		$w_id = strip_tags($this->input->post('workshop'));
		$acc_no = strip_tags($this->session->userdata('acc_no'));
		$prev_wid = strip_tags($this->input->post('wid_up'));
		
		$result = strip_tags($this->Workshop_model->reallocate_ws($w_id, $acc_no, $prev_wid));
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'data not exists') {
			echo 'data not exists';
		}else{
			echo 'error';
		}
	}
}
