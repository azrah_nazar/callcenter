<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Con_refund extends CI_Controller {

	function __construct() { 
		parent::__construct();
		$this->load->model('Refund_model');

	}

	public function index($menu = NULL, $id = NULL)
	{
		require_once(APPPATH.'libraries/User_privileges.php');

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_full') {
					$this->form_validation->set_rules('search_text', 'keyword', 'trim|xss_clean|required');
					
					if ($this->form_validation->run() == FALSE) {
					
					} else {
						$search_text = $this->input->post('search_text');
						$load_data = $this->Refund_model->load_data($search_text);
						$data['load_data'] = $load_data;
					}
				}
			}
		}

		json_encode($data);

		$this->load->view('layout/header', $data);
		$this->load->view('refunds',$data);
		$this->load->view('layout/footer', $data);
	}

    /******************** GET DATASET ********************/
	public function get_dataset(){
		$reg_no = strip_tags($this->input->post('reg_no'));

		$get_dataset = $this->Refund_model->get_dataset($reg_no);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

    /******************** INSERT ********************/
	public function insert(){

		$data = array(
			'reg_no' => strip_tags($this->input->post('reg_up')), 
			'ref_amt' => strip_tags($this->input->post('refund_amt')), 
			'ref_date' => date('Y-m-d H:i:s')	
		);
		
		$result = $this->Refund_model->insert($data);
		if ($result == 'success') {
			echo 'success';
		}else if ($result == 'order no exists') {
			echo 'order no exists';
		}else if ($result == 'data exists') {
			echo 'data exists';
		}else{
			echo 'error';
		}
	}

	
}
