<?php
defined('BASEPATH') or exit('No direct script access allowed');

class  Con_Call_Report extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Call_Report_Model');
	}

	public function index($menu = NULL, $daterange = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');
		$userdata = strip_tags($this->session->userdata('acc_no'));
		$load_agent = $this->Call_Report_Model->load_agent();
		$data['load_agent'] = $load_agent;


		$val = strip_tags($this->session->userdata('Val'));
		$data['val'] = $val;

		$agent = $this->input->post('agent');
		if ($agent == '') {
			if ($val == 12 || $val == 15) {
				$agent = '';
			} else {
				$agent = strip_tags($this->session->userdata('acc_no'));
			}
		}

		if ($daterange == '') {
			$date1 = $date2 = date('Y-m-d');
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$list = $this->Call_Report_Model->load_details($userdata, $date1, $date2, $val, $agent);
		$data['list'] = $list;


		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {
					$list = $this->Call_Report_Model->load_details($userdata, $date1, $date2, $val, $agent);
					$data['list'] = $list;
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('Call_Report', $data);
		$this->load->view('layout/footer', $data);
	}

	public function get_data()
	{
		$userdata = strip_tags($this->session->userdata('acc_no'));
		$val = strip_tags($this->session->userdata('Val'));
		$response = $this->Call_Report_Model->details($userdata, $val);

		echo json_encode($response);
	}

	public function reject_report($menu = NULL, $daterange = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$load_agent = $this->Call_Report_Model->load_agent();
		$data['load_agent'] = $load_agent;

		$val = strip_tags($this->session->userdata('Val'));
		$data['val'] = $val;

		$agent = $this->input->post('agent');
		if ($agent == '') {
			if ($val == 12 || $val == 15) {
				$agent = '';
			} else {
				$agent = strip_tags($this->session->userdata('acc_no'));
			}
		}

		if ($daterange == '') {
			$date1 = $date2 = date('Y-m-d');
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$resultlist = $this->Call_Report_Model->agent_reject_report($agent, $date1, $date2, $val);
		$data['resultlist'] = $resultlist;


		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {
					$this->form_validation->set_rules('agent', 'Agent', 'trim|xss_clean');
					$this->form_validation->set_rules('date_range', 'Date', 'trim|xss_clean');

					if ($this->form_validation->run() == FALSE) {
					} else {
						$resultlist = $this->Call_Report_Model->agent_reject_report($agent, $date1, $date2, $val);
						$data['resultlist'] = $resultlist;
					}
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('reject_report', $data);
		$this->load->view('layout/footer', $data);
	}

	/**************************GET HISTORY*************************/
	public function get_history()
	{
		$con_id = strip_tags($this->input->post('con_id'));

		$get_dataset = $this->Call_Report_Model->get_history($con_id);
		$data['result'] = $get_dataset;

		echo json_encode($data);
	}

	/************************* ADD TICKET *************************/
	public function add_ticket()
	{
		$data = array(
			'sender' => strip_tags($this->session->userdata('acc_no')),
			'con_id' => $this->input->post('con_id'),
			'message' => $this->input->post('message'),
			'dt_time' => date('Y-m-d H:i:s'),
			'flag' => 0,
			'reply_id' => ''
		);

		$result = $this->Call_Report_Model->add_ticket($data);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data exists') {
			echo 'data exists';
		} else {
			echo 'error';
		}
	}

	/******************** SEND REPLY ********************/
	public function send_reply()
	{

		$data = array(
			'con_id' => $this->input->post('id_up'),
			'sender' => strip_tags($this->session->userdata('acc_no')),
			'message' => $this->input->post('reply_up'),
			'dt_time' => date('Y-m-d H:i:s'),
			'flag' => 0,
			'reply_id' => $this->input->post('tic_id')
		);

		$id = strip_tags($this->input->post('tic_id'));
		$data2 = array(
			'flag' => 2
		);

		$this->Call_Report_Model->update_flag($data2, $id);

		$result = $this->Call_Report_Model->send_reply($data);
		if ($result == 'success') {
			echo 'success';
		} else if ($result == 'data exists') {
			echo 'data exists';
		} else {
			echo 'error';
		}
	}

	/******************** CLOSE CHAT ********************/
	public function close_chat()
	{
		$accNo = strip_tags($this->session->userdata('acc_no'));
		$tic_id = strip_tags($this->input->post('tic_id'));
		$id_up = strip_tags($this->input->post('id_up'));

		$data = array(
			'tic_id' => '',
		);

		$result = $this->Call_Report_Model->close_chat($data, $tic_id, $accNo, $id_up);
		if ($result == 'success') {
			$response['status'] = 'success';
		} else if ($result == 'error') {
			$response['status'] = 'error';
		} else if ($result == 'exists') {
			$response['status'] = 'exists';
		} else {
			$response['status'] = $result;
		}

		echo json_encode($response);
	}

	/******************** REMINDER REPORT ********************/
	public function reminder_report($menu = NULL, $daterange = NULL)
	{
		require_once(APPPATH . 'libraries/User_privileges.php');

		$load_agent = $this->Call_Report_Model->load_agent();
		$data['load_agent'] = $load_agent;

		$val = strip_tags($this->session->userdata('Val'));
		$data['val'] = $val;

		$agent = $this->input->post('agent');
		if ($agent == '') {
			if ($val == 12 || $val == 15) {
				$agent = '';
			} else {
				$agent = strip_tags($this->session->userdata('acc_no'));
			}
		}

		if ($daterange == '') {
			$date1 = $date2 = date('Y-m-d');
		} else {
			$date1 = substr($daterange, 0, 10);
			$date2 = substr($daterange, 17, 24);
		}

		$resultlist = $this->Call_Report_Model->reminder_report($agent, $date1, $date2, $val);
		$data['resultlist'] = $resultlist;

		if ($this->input->server('REQUEST_METHOD') == "POST") {
			$search = $this->input->post('search');

			if (isset($search)) {
				if ($search == 'search_filter') {
					$this->form_validation->set_rules('agent', 'Agent', 'trim|xss_clean');
					$this->form_validation->set_rules('date_range', 'Date', 'trim|xss_clean');

					if ($this->form_validation->run() == FALSE) {
					} else {
						$resultlist = $this->Call_Report_Model->reminder_report($agent, $date1, $date2, $val);
						$data['resultlist'] = $resultlist;
					}
				}
			}
		}

		$this->load->view('layout/header', $data);
		$this->load->view('reminder_report', $data);
		$this->load->view('layout/footer', $data);
	}

	/******************** UPDATE ********************/
	public function reset_reminder()
	{

		$data = array(
			'reminder_date' => '',
			'reminder_note' => ''
		);

		$con_no = strip_tags($this->input->post('con_no'));

		$result = strip_tags($this->Call_Report_Model->reset_reminder($data, $con_no));
		if ($result == 'success') {
			echo 'success';
		} else {
			echo 'error';
		}
	}

	/******************** AGENT TANSFER ********************/
	public function agent_tranfer()
	{
		$con_id = strip_tags($this->input->post('con_id2'));
		$today = date('Y-m-d H:i:s');

		$data = array(
			'emp_accNo' => strip_tags($this->input->post('agent')),
			'date' => $today,
			'reg_no' => '',
			'flag' => '0',
			'comment' => '',
			'date1' => '',
			'reminder_date' => '',
			'workshop' => '',
			'confirmation' => '0',
			'bank_sms' => '0',
			'ws_reminder' => '0',
			'feedback_no' => '0',
			'feedback_reject' => '0',
			'reject_date' => '0',
			'pending_payment' => '0',
			'pending_reallocation' => '0',
			'direct_sale' => '0',
			'spl_cus' => '0',
			'tic_id' => '',
			'self_reg' => '0', 
			'agent_reject_date' => '', 
		);

		$result = $this->Call_Report_Model->agent_tranfer($data, $con_id);
		if ($result) {
			$response = 'success';
		} else if ($result == 'error') {
			$response = 'error';
		} else if ($result == 'exists') {
			$response = 'exists';
		}

		echo ($response);
	}
}
