<?php
defined('BASEPATH') or exit('No direct script access allowed');

class  Con_sales_report extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Sales_report_model');
        $this->load->model('Refund_model');
        $this->load->library('Pdf');
    }

    /******************** ACCOUNT CREATION ********************/
    public function account_creation($menu = NULL, $daterange = NULL)
    {
        require_once(APPPATH . 'libraries/User_privileges.php');
        $val = strip_tags($this->session->userdata('Val'));

        if ($val == 12 || $val == 15) {
            $agent = '';
        } else {
            $agent = strip_tags($this->session->userdata('acc_no'));
        }

        if ($daterange == '') {
            $date1 = $date2 = date('Y-m-d');
        } else {
            $date1 = substr($daterange, 0, 10);
            $date2 = substr($daterange, 17, 24);
        }

        $load_data = $this->Sales_report_model->account_creation($date1, $date2, $agent);
        $data['load_data'] = $load_data;

        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $search = $this->input->post('search');

            $daterange = $this->input->post('date_range');
            $date1 = substr($daterange, 0, 10);
            $date2 = substr($daterange, 13, 24);

            if (isset($search)) {
                if ($search == 'search_filter') {

                    $load_data = $this->Sales_report_model->account_creation($date1, $date2, $agent);
                    $data['load_data'] = $load_data;
                }
            }
        }

        $this->load->view('layout/header', $data);
        $this->load->view('account_creation', $data);
        $this->load->view('layout/footer', $data);
    }

    /******************** REFUND LIST ********************/
    public function refund_list($menu = NULL, $daterange =  NULL)
    {
        require_once(APPPATH . 'libraries/User_privileges.php');
        $val = strip_tags($this->session->userdata('Val'));


        if ($val == 12 || $val == 15) {
            $agent = '';
        } else {
            $agent = strip_tags($this->session->userdata('acc_no'));
        }

        if ($daterange == '') {
            $date1 = $date2 = date('Y-m-d');
        } else {
            $date1 = substr($daterange, 0, 10);
            $date2 = substr($daterange, 17, 24);
        }

        $load_data = $this->Sales_report_model->refund_list($date1, $date2, $agent);
        $data['load_data'] = $load_data;

        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $search = $this->input->post('search');

            $daterange = $this->input->post('date_range');
            $date1 = substr($daterange, 0, 10);
            $date2 = substr($daterange, 13, 24);

            if (isset($search)) {
                if ($search == 'search_filter') {

                    $load_data = $this->Sales_report_model->refund_list($date1, $date2, $agent);
                    $data['load_data'] = $load_data;
                }
            }
        }

        $this->load->view('layout/header', $data);
        $this->load->view('refund_list', $data);
        $this->load->view('layout/footer', $data);
    }

    public function refund_details()
	{
		$inv_no = $this->uri->segment(3);
		$ref_id = $this->uri->segment(4);

		$pay_details = $this->Sales_report_model->get_refund_details($inv_no, $ref_id);
		$data['pay_details'] = $pay_details;

		$name = $pay_details["name"];
		$town = $pay_details["town"];
		$inv_no = $pay_details["inv_no"];
		$cr = $pay_details["ref_amt"];
	

		$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'a4', true, 'UTF-8', false);

		// set document information
		$tcpdf->SetCreator(PDF_CREATOR);
		$tcpdf->SetAuthor('Victory Academy');
		$tcpdf->SetTitle('Invoice');
		$tcpdf->SetSubject('Victory Academy');
		$tcpdf->SetKeywords('Victory Academy Invoice');

		//set default monospaced textual style
		$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set auto for page breaks
		$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image for scale factor
		$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);



		// it is optional :: set some language-dependent strings
		if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
			// optional
			require_once(dirname(__FILE__) . '/lang/eng.php');
			// optional
			$tcpdf->setLanguageArray($l);
		}

		// set default font for subsetting mode
		$tcpdf->setFontSubsetting(true);

		$tcpdf->SetFont('helvetica', '', 14, '', true);

		$tcpdf->SetPrintHeader(false);
		$tcpdf->SetPrintFooter(false);

		$tcpdf->AddPage();

		// set text shadow for effect
		$tcpdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 197, 198), 'opacity' => 1, 'blend_mode' => 'Normal'));

		$link = base_url();


			$img_file = $link . '/images/refund_payment.jpg';
			$tcpdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

			$tcpdf->SetXY(20, 100);
			$tcpdf->Cell(250, 10, trim($name), 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(20, 105);
			$tcpdf->Cell(250, 10, trim($town), 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(145, 99);
			$tcpdf->Cell(25, 10, $inv_no, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(16, 145);
			$tcpdf->Cell(25, 10, 'Rs.' . $cr, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(64, 145);
			$tcpdf->Cell(25, 10, 'Cash Refund Invoice', 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');

			$tcpdf->SetXY(146, 145);
			$tcpdf->Cell(25, 10, 'Rs.' .$cr, 0, $ln = 0, 'L', 0, '', 0, false, 'B', 'B');
		

		ob_end_clean();
		$tcpdf->Output($inv_no . '.pdf', 'I');
	}

    /******************** DAILY SALES ********************/
    public function daily_sales($menu = NULL, $date = NULL)
    {
        require_once(APPPATH . 'libraries/User_privileges.php');

        $daily_sales = $this->Sales_report_model->daily_sales($date);
        $data['daily_sales'] = $daily_sales;

        $total_sales = $this->Sales_report_model->total_sales($date);
        $data['total_sales'] = $total_sales;

        $this->load->view('layout/header', $data);
        $this->load->view('daily_sales', $data);
        $this->load->view('layout/footer', $data);
    }

    /******************** PERSONAL BANK REPORT ********************/
    public function personal_bank_report($menu = NULL, $daterange = NULL)
    {
        require_once(APPPATH . 'libraries/User_privileges.php');

        if ($daterange == '') {
            $date1 = $date2 = date('Y-m-d');
        } else {
            $date1 = substr($daterange, 0, 10);
            $date2 = substr($daterange, 17, 24);
        }

        $load_data = $this->Sales_report_model->personal_bank_report($date1, $date2);
        $data['load_data'] = $load_data;

        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $search = $this->input->post('search');

            $daterange = $this->input->post('date_range');
            $date1 = substr($daterange, 0, 10);
            $date2 = substr($daterange, 13, 24);

            if (isset($search)) {
                if ($search == 'search_filter') {

                    $load_data = $this->Sales_report_model->personal_bank_report($date1, $date2);
                    $data['load_data'] = $load_data;
                }
            }
        }

        $this->load->view('layout/header', $data);
        $this->load->view('personal_bank_report', $data);
        $this->load->view('layout/footer', $data);
    }

    /******************** DUE PAYMENT SUMMARY ********************/
    public function due_payment_summary($menu = NULL)
    {
        require_once(APPPATH . 'libraries/User_privileges.php');

        $load_data = $this->Sales_report_model->due_payment_summary();
        $data['load_data'] = $load_data;

        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $search = $this->input->post('search');

            if (isset($search)) {
                if ($search == 'search_filter') {

                    $load_data = $this->Sales_report_model->due_payment_summary();
                    $data['load_data'] = $load_data;
                }
            }
        }

        $this->load->view('layout/header', $data);
        $this->load->view('due_payment_summary', $data);
        $this->load->view('layout/footer', $data);
    }

    /******************** DETAILED DUE PAYMENT ********************/
    public function detailed_due_payments($menu = NULL)
    {
        require_once(APPPATH . 'libraries/User_privileges.php');

        $load_data = $this->Sales_report_model->detailed_due_payments();
        $data['load_data'] = $load_data;

        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $search = $this->input->post('search');

            if (isset($search)) {
                if ($search == 'search_filter') {

                    $load_data = $this->Sales_report_model->detailed_due_payments();
                    $data['load_data'] = $load_data;
                }
            }
        }

        $this->load->view('layout/header', $data);
        $this->load->view('detailed_due_payments', $data);
        $this->load->view('layout/footer', $data);
    }

    /******************** REJECT LIST ********************/
    public function reject_list($menu = NULL, $daterange = NULL)
    {
        require_once(APPPATH . 'libraries/User_privileges.php');

        if ($daterange == '') {
            $date1 = $date2 = date('Y-m-d');
        } else {
            $date1 = substr($daterange, 0, 10);
            $date2 = substr($daterange, 17, 24);
        }

        $load_data = $this->Sales_report_model->reject_list($date1, $date2);
        $data['load_data'] = $load_data;

        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $search = $this->input->post('search');

            $daterange = $this->input->post('date_range');
            $date1 = substr($daterange, 0, 10);
            $date2 = substr($daterange, 13, 24);

            if (isset($search)) {
                if ($search == 'search_filter') {

                    $load_data = $this->Sales_report_model->reject_list($date1, $date2);
                    $data['load_data'] = $load_data;
                }
            }
        }

        $this->load->view('layout/header', $data);
        $this->load->view('reject_list', $data);
        $this->load->view('layout/footer', $data);
    }


    /******************** DB UPDATE ********************/
    public function db_update()
    {
        $db_update = $this->Sales_report_model->db_update();
        $data['db_update'] = $db_update;
    }
}
